﻿using MagentoInterface.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagentoVRSInterface.VRSCartService
{
    public class CartDataDTO : BaseDTO 
    {
        public Int32? cartId { get; set; }
        public string cartName { get; set; }
        public List<CartItemDataDTO> cartItemList { get; set; }

        public CartDataDTO()
        {
            this.cartItemList = new List<CartItemDataDTO>();
        } 
    }
}
