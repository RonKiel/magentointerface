﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagentoVRSInterface.VRSCartService
{
    public class ResponseQueueCartDTO
    {
        public string statusCode { get; set; }
        public string statusMsg { get; set; }
        public Int32 queueId { get; set; }
    }
}
