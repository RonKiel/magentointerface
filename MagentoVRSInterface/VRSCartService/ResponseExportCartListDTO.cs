﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagentoVRSInterface.VRSCartService
{
    public class ResponseExportCartListDTO
    {
        public string statusCode { get; set; }
        public string statusMsg { get; set; }
        public Int32 totalEntries { get; set; }
        public List<CartDataDTO> cartList { get; set; }

        public ResponseExportCartListDTO()
        {
            this.statusCode = "0";
            this.statusMsg = "OK";
            this.cartList = new List<CartDataDTO>();
        }

    }
}
