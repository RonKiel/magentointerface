﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagentoVRSInterface.VRSCartService
{
    public class ResponseCustomerCartsDTO
    {
        public string statusCode { get; set; }
        public string statusMsg { get; set; }
        public string customerAcctNum { get; set; }
        public List<CartDataDTO> cartList { get; set; }

        public ResponseCustomerCartsDTO()
        {
            this.statusCode = "0";
            this.statusMsg = "OK";
            this.cartList = new List<CartDataDTO>();
        }
    }
}
