﻿using System;
namespace MagentoVRSInterface.VRSCartService
{
    public interface ICartServiceController
    {
        CartDataDTO getCartDataForCustomer(string CustomerAcctNo, string cartName);
        ResponseCustomerCartsDTO GetCustomerCarts(string CustomerAcctNo);
        ResponseQueueCartDTO sendSingleCart(RequestQueueCartDTO oRequest);
        ResponseExportCartListDTO sendExportCartListToWeblink(BasicWeblinkRequestDTO oRequest);
        string sendWebRequestForMagentoApi(string url, string msgBody, string userAgent);
        string sendWebRequestForWeblinkApi(string url, string msgBody);
        void setUrlForWeblinkInterface(string src);
        void setUrlForMagentoCartExportInterface(string src);
    }
}
