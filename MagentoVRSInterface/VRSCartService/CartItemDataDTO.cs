﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagentoVRSInterface.VRSCartService
{
    public class CartItemDataDTO
    {
        public Int32? cartId { get; set; }
        public string SKU { get; set; }
        public string AltPartNum { get; set; }
        public Int32? Qty { get; set; }
    }
}
