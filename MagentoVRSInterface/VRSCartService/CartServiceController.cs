﻿using MagentoInterface.Controller;
using MagentoVRSInterface.Controller;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace MagentoVRSInterface.VRSCartService
{
    public class CartServiceController : BaseController, MagentoVRSInterface.VRSCartService.ICartServiceController
    {
        public ILogController log = LogController.Instance;

        /*
         Production 
        public string urlForWeblinkInterface = "https://store.notionsmarketing.com/WeblinkApi";
        public string receiveCartsFromUrl = "https://store.notionsmarketing.com/MagentoApi";


         Development
        public string urlForWeblinkInterface = "http://ndevstore.virtualretail.com/WeblinkApi";
        public string receiveCartsFromUrl = "http://ndevstore.virtualretail.com/MagentoApi";

         */

        private string urlForWeblinkInterface = "http://ndevstore.virtualretail.com/WeblinkApi";
        private string urlForMagentoCartExportInterface = "https://store.notionsmarketing.com/MagentoApi";

        public void setUrlForWeblinkInterface(string src)
        {
            urlForWeblinkInterface = src;
        }
        public void setUrlForMagentoCartExportInterface(string src)
        {
            urlForMagentoCartExportInterface = src;
        }
        
        public string getUrlForWeblinkInterface()
        {
            return urlForWeblinkInterface;
        }

        Dictionary<string, ResponseCustomerCartsDTO> custCartMap = new Dictionary<string, ResponseCustomerCartsDTO>();

        public ResponseQueueCartDTO sendSingleCart(RequestQueueCartDTO oRequest) 
        {
            ResponseQueueCartDTO rtnValue = new ResponseQueueCartDTO();
            string strRequest = "";
            strRequest = JsonConvert.SerializeObject(oRequest, Newtonsoft.Json.Formatting.Indented);

            log.logMsg(LogController.LogClass.Debug, "Request prepared:" + strRequest );

            string strResponse = sendWebRequestForWeblinkApi(urlForWeblinkInterface, strRequest);
            if (!string.IsNullOrWhiteSpace(strResponse))
            {
                log.logMsg(LogController.LogClass.Debug, "Parsing response");
                try
                {
                    rtnValue = JsonConvert.DeserializeObject<ResponseQueueCartDTO>(strResponse);
                }
                catch (Exception ex)
                {
                    log.logMsg(LogController.LogClass.Error, ex);
                }
            }
            else
            {
                log.logMsg(LogController.LogClass.Error, "No response");
            }

            return rtnValue;
        }

        public ResponseExportCartListDTO sendExportCartListToWeblink(BasicWeblinkRequestDTO oRequest)
        {
            ResponseExportCartListDTO rtnValue = new ResponseExportCartListDTO();
            string strRequest = "";
            strRequest = JsonConvert.SerializeObject(oRequest, Newtonsoft.Json.Formatting.Indented);

            log.logMsg(LogController.LogClass.Debug, "Request prepared:" + strRequest);

            string strResponse = sendWebRequestForWeblinkApi(urlForWeblinkInterface, strRequest);
            if (!string.IsNullOrWhiteSpace(strResponse))
            {
                log.logMsg(LogController.LogClass.Debug, "Parsing response");
                try
                {
                    rtnValue = JsonConvert.DeserializeObject<ResponseExportCartListDTO>(strResponse);
                }
                catch (Exception ex)
                {
                    log.logMsg(LogController.LogClass.Error, ex);
                }
            }
            else
            {
                log.logMsg(LogController.LogClass.Error, "No response");
            }

            return rtnValue;
        }

        public CartDataDTO getCartDataForCustomer(string CustomerAcctNo, string cartName)
        {
            ResponseCustomerCartsDTO customerCarts = null;

            if (custCartMap.Keys.Contains(CustomerAcctNo.Trim()))
            {
                customerCarts = custCartMap[CustomerAcctNo.Trim()];
            }
            else
            {
                customerCarts = GetCustomerCarts(CustomerAcctNo.Trim());
            }

            if (customerCarts != null)
            {
                foreach (CartDataDTO cartHdr in customerCarts.cartList)
                {
                    if (cartHdr.cartName.Trim() == cartName)
                    {
                        return cartHdr;
                    }
                }
            }
            return null;
        }

        public ResponseCustomerCartsDTO GetCustomerCarts(string CustomerAcctNo)
        {
            RequestCartDataDTO oRequest = new RequestCartDataDTO();
            ResponseCustomerCartsDTO oResponse = new ResponseCustomerCartsDTO();
            string strRequest = "";
            string strResponse = "";

            oRequest.appKey = "Magento1492";
            oRequest.customerAcctNum = CustomerAcctNo;

            strRequest = JsonConvert.SerializeObject(oRequest, Newtonsoft.Json.Formatting.Indented);

            log.logMsg(LogController.LogClass.Debug, "Request prepared");

            strResponse = sendWebRequestForMagentoApi(urlForMagentoCartExportInterface, strRequest, "Cart Export Client");
            if (!string.IsNullOrWhiteSpace(strResponse))
            {
                log.logMsg(LogController.LogClass.Debug, "Parsing response");
                try
                {
                    oResponse = JsonConvert.DeserializeObject<ResponseCustomerCartsDTO>(strResponse);
                }
                catch (Exception ex)
                {
                    log.logMsg(LogController.LogClass.Error, ex);
                }
            }
            else
            {
                log.logMsg(LogController.LogClass.Error, "No response");
            }

            if (custCartMap.Keys.Contains(CustomerAcctNo.Trim()))
            {
                custCartMap[CustomerAcctNo.Trim()] = oResponse;
            }
            else
            {
                custCartMap.Add(CustomerAcctNo.Trim(), oResponse);
            }
            return oResponse;
        }

        public string sendWebRequestForWeblinkApi(string url, string msgBody)
        {
            string strRequest = msgBody;
            string strResponse = "";

            HttpWebRequest oReq;
            HttpWebResponse oReply;
            StreamReader respStr;
            StreamWriter myWriter = null;
            Boolean boolHasError = false;

            string strURL = url;

            try
            {
                oReq = (HttpWebRequest)WebRequest.Create(strURL);
                oReq.Headers.Clear();
                oReq.Method = "POST";
                oReq.ContentLength = strRequest.Length;
                oReq.UserAgent = "TODO";
                oReq.ContentType = "application/json";
            }
            catch (Exception ex)
            {
                log.logMsg(LogController.LogClass.Error, ex);
                return "";
            }

            try
            {
                myWriter = new StreamWriter(oReq.GetRequestStream());
                myWriter.Write(strRequest);
                log.logMsg(LogController.LogClass.Debug, "->  Request sent successfully");
            }
            catch (Exception ex)
            {
                log.logMsg(LogController.LogClass.Error, ex);
                boolHasError = true;
            }
            finally
            {
                if (myWriter != null)
                {
                    try
                    {
                        myWriter.Close();
                    }
                    catch (Exception)
                    {
                    }
                }
            }
            if (boolHasError)
            {
                return "";
            }

            try
            {
                log.logMsg(LogController.LogClass.Debug, "->  Getting response");
                oReply = (HttpWebResponse)oReq.GetResponse();
                respStr = new StreamReader(oReply.GetResponseStream());
                strResponse = respStr.ReadToEnd();
                respStr.Close();
                oReply.Close();
                log.logMsg(LogController.LogClass.Debug, "->  Response received");
            }
            catch (Exception ex)
            {
                log.logMsg(LogController.LogClass.Error, ex);
            }

            if (string.IsNullOrWhiteSpace(strResponse))
            {
                log.logMsg(LogController.LogClass.Error, "->  Response empty");
                return "";
            }

            try
            {
                ResponseBasicDTO oResponseBasic;
                oResponseBasic = JsonConvert.DeserializeObject<ResponseBasicDTO>(strResponse);
                if (oResponseBasic == null)
                {
                    log.logMsg(LogController.LogClass.Error, "->  unable to parse response");
                }
                log.logMsg(LogController.LogClass.Debug, string.Format("statusCode;({0}) status Message:({1})", oResponseBasic.statusCode, oResponseBasic.statusMsg));

            }
            catch (Exception ex)
            {
                log.logMsg(LogController.LogClass.Error, ex);
            }

            return strResponse;
        }

        public string sendWebRequestForMagentoApi(string url, string msgBody, string userAgent )
        {
            string strRequest = msgBody;
            string strResponse = "";

            HttpWebRequest oReq;
            HttpWebResponse oReply;
            StreamReader respStr;
            StreamWriter myWriter = null;
            Boolean boolHasError = false;
            string strURL = url;

            try
            {
                oReq = (HttpWebRequest)WebRequest.Create(strURL);
                oReq.Headers.Clear();
                oReq.UserAgent = userAgent;
                oReq.Method = "POST";
                oReq.ContentLength = strRequest.Length;
                oReq.ContentType = "application/json";
            }
            catch (Exception ex)
            {
                log.logMsg(LogController.LogClass.Error, ex);
                return "";
            }

            try
            {
                myWriter = new StreamWriter(oReq.GetRequestStream());
                myWriter.Write(strRequest);
                log.logMsg(LogController.LogClass.Debug, "->  Request sent successfully");
            }
            catch (Exception ex)
            {
                log.logMsg(LogController.LogClass.Error, ex);
                boolHasError = true;
            }
            finally
            {
                if (myWriter != null)
                {
                    try
                    {
                        myWriter.Close();
                    }
                    catch (Exception)
                    {
                    }
                }
            }
            if (boolHasError)
            {
                return "";
            }

            try
            {
                log.logMsg(LogController.LogClass.Debug, "->  Getting response");
                oReply = (HttpWebResponse)oReq.GetResponse();
                respStr = new StreamReader(oReply.GetResponseStream());
                strResponse = respStr.ReadToEnd();
                respStr.Close();
                oReply.Close();
                log.logMsg(LogController.LogClass.Debug, "->  Response received");
            }
            catch (Exception ex)
            {
                log.logMsg(LogController.LogClass.Error, ex);
            }

            if (string.IsNullOrWhiteSpace(strResponse))
            {
                log.logMsg(LogController.LogClass.Error, "->  Response empty");
                return "";
            }

            try
            {
                ResponseBasicDTO oResponseBasic;
                oResponseBasic = JsonConvert.DeserializeObject<ResponseBasicDTO>(strResponse);
                if (oResponseBasic == null)
                {
                    log.logMsg(LogController.LogClass.Error, "->  unable to parse response");
                }
                log.logMsg(LogController.LogClass.Debug, string.Format("statusCode;({0}) status Message:({1})", oResponseBasic.statusCode, oResponseBasic.statusMsg));

            }
            catch (Exception ex)
            {
                log.logMsg(LogController.LogClass.Error, ex);
            }

            return strResponse;
        }



    }
}
