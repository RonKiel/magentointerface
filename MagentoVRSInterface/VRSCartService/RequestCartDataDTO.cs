﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagentoVRSInterface.VRSCartService
{
    public class RequestCartDataDTO
    {
        public string appKey { get; set; }
        public string customerAcctNum { get; set; }
    }
}
