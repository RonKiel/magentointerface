﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagentoVRSInterface.VRSCartService
{
    public class ResponseBasicDTO
    {
        public string statusCode { get; set; }
        public string statusMsg { get; set; }

        public ResponseBasicDTO()
        {
            this.statusCode = "";
            this.statusMsg = "";
        }

        public ResponseBasicDTO(string StatusCode, string StatusMessage)
        {
            this.statusCode = StatusCode;
            this.statusMsg = StatusMessage;
        }
    }
}
