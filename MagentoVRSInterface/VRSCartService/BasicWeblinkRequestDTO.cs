﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagentoVRSInterface.VRSCartService
{
    public class BasicWeblinkRequestDTO
    {
        public string appName { get; set; }
        public string appVersion { get; set; }
        public string partnerCode { get; set; }
        public string loginId { get; set; }
        public string password { get; set; }
        public string accountNumber { get; set; }
        public string actionCode { get; set; } 
    }
}
