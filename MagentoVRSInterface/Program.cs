﻿using MagentoInterface.Controller;
using MagentoVRSInterface.Controller;
using MagentoVRSInterface.DAO;
using MagentoVRSInterface.DTO;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MagentoVRSInterface
{
    class Program
    {
        static UtilController util = new UtilController();
        static ContextController contextController = ContextController.Instance;
        static BlueScreenUtil bsu = new BlueScreenUtil();
        static LogController log = LogController.Instance;

        public static void Main( string[] args )
        {
            List<string> argList = new List<string>(args);

            IDbConnection stagedConn = bsu.getConnection(ConfigurationManager.ConnectionStrings["StagedData"].ProviderName,
                                                   ConfigurationManager.ConnectionStrings["StagedData"].ConnectionString);

            IDbConnection blueConn = bsu.getConnection(ConfigurationManager.ConnectionStrings["BlueScreen"].ProviderName,
                                                   ConfigurationManager.ConnectionStrings["BlueScreen"].ConnectionString);

            if ( blueConn is SqlConnection )
            {
                IDbCommand cmd = blueConn.CreateCommand();
                cmd.CommandText = "SET TRANSACTION ISOLATION LEVEL SNAPSHOT";
                cmd.ExecuteNonQuery();
            }

            log.setupDBConnection(ConfigurationManager.ConnectionStrings["StagedData"].ConnectionString,
                                  ConfigurationManager.ConnectionStrings["StagedData"].ProviderName);

            List<string> largs = util.lowerCaseList(argList);
            if (largs.Contains("-only"))
            {
                Int32 foundAt = largs.IndexOf("-only");
                contextController.outputOnly = argList[foundAt + 1];
            }

            if (( argList.Count == 0) ||
                ((contextController.outputOnly != null) &&
                argList.Count <= 2))
            {
                fullDataExport(stagedConn, blueConn, null );
            }
            else
            {

                //Send image data to media store.
                if (largs.Contains("-i"))
                {
                    MediaStoreItemsDataExport mediaStoreItemsDataExport = new MediaStoreItemsDataExport();
                    Int32 willBeloadId = util.peekNextLoadId(stagedConn);
                    mediaStoreItemsDataExport.sendFilesToMediaStore(willBeloadId);
                }
                if ( largs.Contains("-c") )
                {
                    customerHourlyExport(stagedConn, blueConn);
                }
                if (largs.Contains("-prior"))
                {
                    Int32 foundAt = largs.IndexOf("-prior");
                    Int32 priorRun = Int32.Parse(largs[foundAt + 1]);
                    fullDataExport(stagedConn, blueConn, priorRun);
                }
            }
        }
        
        private static void customerCartExport(IDbConnection stagedConn, IDbConnection blueConn)
        {
            try
            {
                log.logMsg(LogController.LogClass.Info, "Getting cart data");
                ProcessCustomerCarts processCustomerCarts = new ProcessCustomerCarts();

                string sourceDirectory = ConfigurationManager.AppSettings["SourceFileLocation"].ToString();
                string customerDataFile = sourceDirectory + "customerfile.tab";

                List<KeyValuePair<string, StringBuilder>> filesToWrite = processCustomerCarts.processFile(stagedConn, customerDataFile);

                writeOutputFilesIfAppicable(processCustomerCarts, filesToWrite);
                transmitViaFTPIfAppicable(filesToWrite);
            }
            catch (Exception ex1)
            {
                log.logMsg(LogController.LogClass.Error, ex1);
            }
            finally
            {
                stagedConn.Close();
                blueConn.Close();
            }
        }

        private static void customerHourlyExport(IDbConnection stagedConn, IDbConnection blueConn)
        {
            try
            {
                log.logMsg(LogController.LogClass.Info, "Looking for customer hourly file.");
                Int32 secondsRequiredToBeValid = Int32.Parse(ConfigurationManager.AppSettings["TimeRequiredToBeValidInSeconds"].ToString());
                string fileName = ConfigurationManager.AppSettings["UNCLocationOfNewCustomers"].ToString();

                DateTime fTime = File.GetCreationTime(fileName);
                TimeSpan howOld = DateTime.Now.Subtract(fTime);
                Int32 secondsOld = (Int32)howOld.TotalSeconds;
                if (secondsOld > secondsRequiredToBeValid)
                {
                    ProcessHourlyCustomerFile processHourlyCustomerFile = new ProcessHourlyCustomerFile();
                    List<KeyValuePair<string, StringBuilder>> filesToWrite = processHourlyCustomerFile.processFile(stagedConn, fileName, fTime);

                    writeOutputFilesIfAppicable(processHourlyCustomerFile, filesToWrite);
                    transmitViaFTPIfAppicable(filesToWrite);
                }
            }
            catch (Exception ex1)
            {
                log.logMsg(LogController.LogClass.Error, ex1);
            }
            finally
            {
                stagedConn.Close();
                blueConn.Close();
            }
        }

        private static void fullDataExport(IDbConnection stagedConn, IDbConnection blueConn, Int32? priorOverride)
        {
            try
            {
                log.systemName = "MagentoVRS";
                log.logMsg(LogController.LogClass.Info, "Looking for work.");
                List<string> readyFiles = loadReadFileListWithRetry();

                if (readyFiles.Count > 0)
                {
                    Int32 willBeloadId = util.peekNextLoadId(stagedConn);
                    log.systemName = string.Format("LoadId({0})", willBeloadId);
                    log.logMsg(LogController.LogClass.Info, string.Format("Filling Context for LoadId({0})", willBeloadId));

                    Dictionary<string, string> skuMap = util.loadContextData(contextController, blueConn, log);

                    if (skuMap != null)
                    {
                        Int32 loadId = util.getNextLoadId(stagedConn);
                        Int32 priorGoodLoad = (priorOverride == null) ? util.getPriorLoadId(stagedConn) : (Int32)priorOverride;
                        contextController.loadId = loadId;
                        contextController.priorGoodLoad = priorGoodLoad;
                        log.logMsg(LogController.LogClass.Info, string.Format("Processing LoadId({0}) Prior({1})", loadId, priorGoodLoad));

                        ProcessFileFactory factory = new ProcessFileFactory();

                        processAllFilesVRSFiles(stagedConn, blueConn, readyFiles, loadId, factory);

                        MediaStoreFileTransfer mediaStoreFileTransfer = new MediaStoreFileTransfer();
                        if (mediaStoreFileTransfer.CopyMediaStoreFiles(stagedConn, loadId))
                        {
                            processDataFromFTPFeed(stagedConn, new ProcessImageStoreEntries());
                        }
                        
                        processPromos(stagedConn, blueConn);

                        if ( dowladFileDataFromVendorPortal(stagedConn, loadId) > 0 )
                        {
                            processDataFromFTPFeed(stagedConn, new ProcessVendorPortalData());
                        }

                        processAllFilesWithDelayedWrite(stagedConn, readyFiles, loadId, priorGoodLoad, factory);

                        processBothMajorAndMinorDeletes(loadId);
                        processCategoryAndBasketDeletes(loadId);

                        util.purgeOldVersions(stagedConn, log, loadId);
                        util.purgeOldLogs(stagedConn, log);
                        util.markVRSHistoryAsComplete(stagedConn, loadId);

                        log.logMsg(LogController.LogClass.Info, string.Format("Done LoadId({0}) Prior({1})", loadId, priorGoodLoad));
                    }
                }
            }
            catch (Exception ex1)
            {
                log.logMsg(LogController.LogClass.Error, ex1); 
            }
            finally
            {
                stagedConn.Close();
                blueConn.Close();
            }
        }

        private static void processAllFilesWithDelayedWrite(IDbConnection stagedConn, List<string> readyFiles, Int32 loadId, Int32 priorGoodLoad, ProcessFileFactory factory)
        {
            foreach (string rdyFile in readyFiles)
            {
                FileProcessor fileProcessor = factory.getProcessor(rdyFile);
                if (fileProcessor == null)
                {
                    log.logMsg(LogController.LogClass.Error, "Processor Missing for file: " + rdyFile);
                }
                else
                {
                    if (fileProcessor is IDeferedWriteOfVRSData)
                    {
                        List<KeyValuePair<string, StringBuilder>> filesToWrite = new List<KeyValuePair<string, StringBuilder>>();
                        ((IDeferedWriteOfVRSData)fileProcessor).buildDataFileToWrite(stagedConn, loadId, priorGoodLoad, filesToWrite);
                        moveSourceFileIfApplicable(loadId, rdyFile);
                        writeOutputFilesIfAppicable(fileProcessor, filesToWrite);
                        transmitViaFTPIfAppicable(filesToWrite);
                    }
                }
            }
        }

        private static void processAllFilesVRSFiles(IDbConnection stagedConn, IDbConnection blueConn, List<string> readyFiles, Int32 loadId, ProcessFileFactory factory)
        {
            foreach (string rdyFile in readyFiles)
            {
                FileProcessor fileProcessor = factory.getProcessor(rdyFile);
                if (fileProcessor == null)
                {
                    log.logMsg(LogController.LogClass.Error, "Processor Missing for file: " + rdyFile);
                }
                else
                {
                    log.logMsg(LogController.LogClass.Info, "Processing: " + rdyFile);
                    processReceviedFile(stagedConn, blueConn, loadId, rdyFile, fileProcessor);
                }
            }
        }

        private static void processBothMajorAndMinorDeletes(Int32 loadId)
        {
            if (!string.IsNullOrWhiteSpace(contextController.productDeleteBuffer.ToString()))
            {
                string DestDirectory = ConfigurationManager.AppSettings["FileBuildLocation"].ToString();
                string chgFile = DestDirectory + "productdeletion" + loadId.ToString("D8") + ".tab";
                List<KeyValuePair<string, StringBuilder>> filesToWrite = new List<KeyValuePair<string, StringBuilder>>();
                filesToWrite.Add(new KeyValuePair<string, StringBuilder>(chgFile, contextController.productDeleteBuffer));
                FileProcessor justAnyFileProcessor = new ProcessTItemFile();

                log.logMsg(LogController.LogClass.Info, "Processing: MajorAndMinorDeletes");
                writeOutputFilesIfAppicable(justAnyFileProcessor, filesToWrite);
                transmitViaFTPIfAppicable(filesToWrite);
            }
        }

        private static void processCategoryAndBasketDeletes(Int32 loadId)
        {
            if (!string.IsNullOrWhiteSpace(contextController.catAndBasketDeleteBuffer.ToString()))
            {
                string DestDirectory = ConfigurationManager.AppSettings["FileBuildLocation"].ToString();
                string chgFile = DestDirectory + "CategoryDeleteFile" + loadId.ToString("D8") + ".tab";
                List<KeyValuePair<string, StringBuilder>> filesToWrite = new List<KeyValuePair<string, StringBuilder>>();
                filesToWrite.Add(new KeyValuePair<string, StringBuilder>(chgFile, contextController.catAndBasketDeleteBuffer));
                FileProcessor justAnyFileProcessor = new ProcessTItemFile();

                log.logMsg(LogController.LogClass.Info, "Processing: Category and Basket Deletes");
                writeOutputFilesIfAppicable(justAnyFileProcessor, filesToWrite);
                transmitViaFTPIfAppicable(filesToWrite);
            }
        }

        private static void processReceviedFile(IDbConnection stagedConn, IDbConnection blueConn, Int32 loadId, string rdyFile, FileProcessor fileProcessor)
        {
            List<KeyValuePair<string, StringBuilder>> filesToWrite;

            if (fileProcessor is ProcessCustomerFile)
            {
                filesToWrite = fileProcessor.processFileBlue(stagedConn, blueConn, rdyFile);
            }
            else 
            {
                filesToWrite = fileProcessor.processFile(stagedConn, rdyFile);
            }

            if (!(fileProcessor is IDeferedWriteOfVRSData))
            {
                moveSourceFileIfApplicable(loadId, rdyFile);
                writeOutputFilesIfAppicable(fileProcessor, filesToWrite);
                transmitViaFTPIfAppicable(filesToWrite);
            }
        }

        private static void processPromos(IDbConnection stagedConn, IDbConnection blueConn)
        {
            PromoController promoController = new PromoController();
            List<KeyValuePair<string, StringBuilder>> promoFiles = promoController.processPromos(stagedConn, blueConn);
            writeOutputFilesIfAppicable(promoController, promoFiles);
            transmitViaFTPIfAppicable(promoFiles);
        }

        private static void processDataFromFTPFeed(IDbConnection stagedConn, FileProcessor fileProcessor)
        {
            List<KeyValuePair<string, StringBuilder>> dataFiles = fileProcessor.processFile(stagedConn, null);
            writeOutputFilesIfAppicable(fileProcessor, dataFiles);
            transmitViaFTPIfAppicable(dataFiles);
        }

        private static Int32 dowladFileDataFromVendorPortal(IDbConnection stagedConn, Int32 loadId)
        {
            Int32 tryCount = 0;
            List<string> filesDownloaded = new List<string>();
            while (tryCount < 4 && filesDownloaded.Count < 1)
            {
                VendorPortalFileHistoryDAO vendorPortalFileHistoryDAO = new VendorPortalFileHistoryDAO();
                vendorPortalFileHistoryDAO.DeleteFilesForRun(stagedConn, null, loadId);
                bool checkAgeOfFile = (tryCount == 3) ? false : true;
                filesDownloaded = util.downloadFromVendorPortalViaSFTP(stagedConn, loadId, "dat", checkAgeOfFile);
                if (filesDownloaded.Count < 1)
                {
                    log.logMsg(LogController.LogClass.Warning, string.Format("Retrying download of vendor poral file, LoadId({0}) Try count({1})", loadId, tryCount));
                    Thread.Sleep(10000); //Sleep for 10 seconds.
                }
                tryCount++;
            }
            return filesDownloaded.Count;
        }

        private static void transmitViaFTPIfAppicable(List<KeyValuePair<string, StringBuilder>> filesToWrite)
        {
            UtilController util = new UtilController();
            string transmitViaFTPS = ConfigurationManager.AppSettings["UseFTPS"].ToString();
            string transmitViaSFTP = ConfigurationManager.AppSettings["UseSFTP"].ToString();

            if (transmitViaFTPS.ToUpper().Equals("YES") || transmitViaSFTP.ToUpper().Equals("YES") )
            {
                foreach (KeyValuePair<string, StringBuilder> currFile in filesToWrite)
                {
                    if (transmitViaFTPS.ToUpper().Equals("YES"))
                    {
                        util.sendFileViaFTPS(currFile);
                    }
                    else if (transmitViaSFTP.ToUpper().Equals("YES"))
                    {
                        //util.sendFileViaSFTPWithHostKey(currFile);
                        util.UploadFileViaSFTPWithHostKey(currFile);
                    }
                }
            }
        }

        private static void writeOutputFilesIfAppicable(FileProcessor fileProcessor, List<KeyValuePair<string, StringBuilder>> filesToWrite)
        {
            string saveUploadedFiles = ConfigurationManager.AppSettings["SaveUploadedFiles"].ToString();
            if (contextController.outputOnly != null)
            {
                List<KeyValuePair<string, StringBuilder>> writeOnly = new List<KeyValuePair<string, StringBuilder>>();
                foreach(KeyValuePair<string, StringBuilder>fileToWrite in filesToWrite)
                {
                    if (fileToWrite.Key.Contains(contextController.outputOnly))
                    {
                        writeOnly.Add(fileToWrite);
                    }
                }
                filesToWrite = writeOnly;
            }

            if ((saveUploadedFiles.ToUpper().Equals("YES")) && (filesToWrite.Count > 0))
            {
                fileProcessor.writeFile(filesToWrite);
            }
        }

        private static void moveSourceFileIfApplicable(Int32 loadId, string rdyFile)
        {
            string movedFileOnceProcessed = ConfigurationManager.AppSettings["MoveFileOnceProcessed"].ToString();
            if (movedFileOnceProcessed.ToUpper().Equals("YES"))
            {
                File.Move(rdyFile, rdyFile.Replace(".tab", ".Processed-" + loadId.ToString("D8")));
            }
        }

        private static List<string> loadReadFileListWithRetry()
        {
            List<string> readyFiles = getReadyFileList();
            for (Int32 i = 0; i < 3; i++)
            {
                if (readyFiles.Count == 0)
                {
                    Thread.Sleep(15000);
                    log.logMsg(LogController.LogClass.Info, string.Format("Looking for work ({0}).", i + 2));
                    readyFiles = getReadyFileList();
                }
            }
            return readyFiles;
        }

        private static List<string> getReadyFileList()
        {
            LogController log = LogController.Instance;
            bool vendorFileEncountered = false;
            bool categoryFileEncountered = false;
            //List<string> fileOrder = new List<string>(new string[] { "attributecode.tab", "tvendorfile.tab", "categoryfile.tab", 
            //                                                         "titemsfile.tab", "tproductsfile.tab", "tproductcatfile.tab", 
            //                                                         "customerfile.tab", "recomlibrary.tab", "recomitems.tab",
            //                                                         "itemattrib.tab", "itemstatus.tab" } 
            //                                                        );
            List<string> fileOrder = new List<string>(new string[] { "attributecode.tab", "tvendorfile.tab", "categoryfile.tab",
                                                                     "titemsfile.tab", "tproductcatfile.tab", "tproductsfile.tab",
                                                                     "customerfile.tab", "recomlibrary.tab", "recomitems.tab",
                                                                     "itemattrib.tab", "itemstatus.tab" }
                                                                    );
            List<string> readyFiles = new List<string>();
            string sourceDirectory = ConfigurationManager.AppSettings["SourceFileLocation"].ToString();
            Int32 secondsRequiredToBeValid = Int32.Parse(ConfigurationManager.AppSettings["TimeRequiredToBeValidInSeconds"].ToString());
            List<string> files = Directory.GetFiles(sourceDirectory, "*.tab").ToList();

            Int32 foundFileCount = 0;
            foreach (string fileInOrder in fileOrder)
            {

                string fileName = files.Where(x => x.ToUpper().Contains(fileInOrder.ToUpper())).FirstOrDefault();
                if (!string.IsNullOrWhiteSpace(fileName))
                {
                    foundFileCount++;

                    if (fileInOrder == "tvendorfile.tab")
                    {
                        vendorFileEncountered = true;
                    }
                    if (fileInOrder == "categoryfile.tab")
                    {
                        categoryFileEncountered = true;
                    }

                    DateTime fTime = File.GetCreationTime(fileName);
                    TimeSpan howOld = DateTime.Now.Subtract(fTime);
                    Int32 secondsOld = (Int32)howOld.TotalSeconds;
                    if (secondsOld > secondsRequiredToBeValid)
                    {
                        readyFiles.Add(fileName);
                    }
                    else
                    {
                        log.logMsg(LogController.LogClass.Warning, string.Format("File: ({0}) ignored because not old enough, might be modified.", fileName));
                    }
                }
            }

            //Only if all of the files that exist are at least a given age, then process them.

            if (!vendorFileEncountered)
            {
                log.logMsg(LogController.LogClass.Warning, "Can't run because missing vendor file.");
            }
            if (!categoryFileEncountered)
            {
                log.logMsg(LogController.LogClass.Warning, "Can't run because missing category file.");
            }

            if (foundFileCount == readyFiles.Count && vendorFileEncountered && categoryFileEncountered )
            {
                return readyFiles;
            }
            return new List<string>();
        }
    }
}
