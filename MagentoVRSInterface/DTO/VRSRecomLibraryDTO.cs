﻿using MagentoInterface.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagentoVRSInterface.DTO
{
    public class VRSRecomLibraryDTO : BaseDTO
    {
        public Int32? LoadId { get; set; }
        public string LibraryCode { get; set; }
        public string Title { get; set; }
        public string Visible { get; set; }
        public string Expire { get; set; }
        public string SortKey { get; set; }
        public string Comments { get; set; }
        public string RootCatCode { get; set; }
        public string BasketImageSku { get; set; }
    }

}
