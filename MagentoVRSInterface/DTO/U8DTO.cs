﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagentoVRSInterface.DTO
{
    public class U8DTO
    {
        public string SKU { get; set; }
        public string ADDDT { get; set; }
        public string MAJKY { get; set; }
    }
}
