﻿using MagentoInterface.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagentoVRSInterface.DTO
{
    public class VRSQprcDTO : BaseDTO
    {
        public Int32? LoadId { get; set; }
        public string MAJKY { get; set; }
        public Decimal? MULT { get; set; }
        public Decimal? MINMULT { get; set; }
        public Decimal? MAXMULT { get; set; }
        public Decimal? MINQTY { get; set; }
        public string BEGDT { get; set; }
        public string ENDDT { get; set; }
        public string TLXFL { get; set; }
        public string TEXT1 { get; set; }
        public string PROMO { get; set; }
        public string EVNTNO { get; set; }
        public Decimal? MAXQTY { get; set; }
        public Decimal? TOTQTY { get; set; }
        public Decimal? TOTRTL { get; set; }
        public Decimal? COGS { get; set; }
        public Decimal? CCOST { get; set; }
    }

}
