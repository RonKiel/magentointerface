﻿using MagentoInterface.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagentoVRSInterface.DTO
{
    public class VendorPortalFileHistoryDTO : BaseDTO
    {
        public Int32? LoadId { get; set; }
        public string FTPEntry { get; set; }
        public DateTime? DateTime { get; set; }
        public string FileName { get; set; }
        public string RawFile { get; set; }
        public string FileProcessed { get; set; }
    }
}
