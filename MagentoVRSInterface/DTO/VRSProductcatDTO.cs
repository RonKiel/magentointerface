﻿using MagentoInterface.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagentoVRSInterface.DTO
{
    public class VRSProductcatDTO : BaseDTO 
    {
        public Int32? LoadId { get; set; }
        public string CategoryCode { get; set; }
        public string SortKey { get; set; }
        public string MfgCode { get; set; }
        public string CommonPartNum { get; set; }
        public string ProductKey { get; set; }
        public string IsPrimary { get; set; }
    }

}
