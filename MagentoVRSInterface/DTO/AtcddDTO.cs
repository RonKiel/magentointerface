﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagentoVRSInterface.DTO
{
    public class AtcddDTO : MagentoInterface.DTO.BaseDTO
    {
        public string ATRBCD { get; set; }
        public string VALU { get; set; }
        public string ADDDT { get; set; }
        public string ADDBY { get; set; }
        public string CHGDT { get; set; }
        public string CHGBY { get; set; }
        public string LVAR { get; set; }
    }
}
