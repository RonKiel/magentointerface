﻿using MagentoInterface.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagentoVRSInterface.DTO
{
    public class VRSVendorDTO : BaseDTO
    {
        public Int32? LoadId { get; set; }
        public string MfgCode { get; set; }
        public string CompanyName { get; set; }
    }

}
