﻿using MagentoInterface.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagentoVRSInterface.DTO
{
    public class VRSTItemDTO : BaseDTO
    {
        public Int32? LoadId { get; set; }
        public string SKU { get; set; }
        public string MfgCode { get; set; }
        public string CommonPartNum { get; set; }
        public string ProductKey { get; set; }
        public string LegacySKU { get; set; }
        public string PartNum { get; set; }
        public string MfgPartNum { get; set; }
        public string IsActive { get; set; }
        public string MfgSugRetailPrice { get; set; }
        public string Weight { get; set; }
        public string Height { get; set; }
        public string Width { get; set; }
        public string Depth { get; set; }
        public string MinQty { get; set; }
        public string OrderQty { get; set; }
        public string CurrentRetailPrice { get; set; }
        public string ReceiptText { get; set; }
        public string Description { get; set; }
        public string UPC { get; set; }
        public string NewDate { get; set; }
        public string StartDate { get; set; }
        public string InventoryFlag { get; set; }
        public string fobdesc { get; set; }
        public string discflag { get; set; }
        public string discreason { get; set; }
        public string subsku { get; set; }
        public string specorder { get; set; }
        public string ean { get; set; }
        public string VendorAvail { get; set; }
        public string EstArrival { get; set; }
        public string Launch { get; set; }
        public string whse { get; set; }
        public string disc { get; set; }
        public string itemcoavail { get; set; }
        public string itemcounavail { get; set; }
        public string Name { get; set; }
        public string WhatsNewMulti { get; set; }
    }

}
