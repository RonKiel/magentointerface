﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagentoVRSInterface.DTO
{
    public class VRSAttributeCodeDTO : MagentoInterface.DTO.BaseDTO 
    {
        public Int32? LoadId { get; set; }
        public string AttributeTypeCode { get; set; }
        public string AttributeDescription { get; set; }
        public string Sortkey { get; set; }
    }

}
