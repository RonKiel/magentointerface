﻿using MagentoInterface.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagentoVRSInterface.DTO
{
    public class VRSmlpromodDTO : BaseDTO
    {
        public Int32? LoadId { get; set; }
        public string SKU { get; set; }
        public string PROMO { get; set; }
        public string ITEM { get; set; }
        public Decimal? MULT { get; set; }
        public string ADDDT { get; set; }
        public string CHGDT { get; set; }
        public string ADDBY { get; set; }
        public string CHGBY { get; set; }
        public Decimal? TOTQTY { get; set; }
        public Decimal? TOTRTL { get; set; }
        public Decimal? COGS { get; set; }
        public Decimal? CCOST { get; set; }
        public string LVAR { get; set; }
    }
}
