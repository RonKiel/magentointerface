﻿using MagentoInterface.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagentoVRSInterface.DTO
{
    public class CustomerShowCostDto : BaseDTO
    {
        public string   CustomerAcctNo { get; set; }
        public bool     ShowCost { get; set; }
    }

}
