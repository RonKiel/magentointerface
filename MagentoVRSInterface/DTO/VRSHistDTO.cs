﻿using MagentoInterface.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagentoVRSInterface.DTO
{
    public class VRSHistDTO : BaseDTO
    {
        public Int32? LoadId { get; set; }
        public DateTime? LoadDate { get; set; }
        public DateTime? CompletionTime { get; set; }
    }

}
