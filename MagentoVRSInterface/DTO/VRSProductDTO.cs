﻿using MagentoInterface.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagentoVRSInterface.DTO
{
    public class VRSProductDTO : BaseDTO
    {
        public Int32? LoadId { get; set; }
        public string MfgCode { get; set; }
        public string CommonPartNum { get; set; }
        public string ProductKey { get; set; }
        public string HasVariations { get; set; }
        public string IsActive { get; set; }
        public string Keywords { get; set; }
        public string CatalogPage { get; set; }
        public string ShortDesc { get; set; }
        public string LongDesc { get; set; }
        public string NewDate { get; set; }
        public string frtcollect { get; set; }
        public string unitmeas { get; set; }
        public string OldCommonPartNum { get; set; }
        public string ormd { get; set; }
        public string noexport { get; set; }
        public string countryavail { get; set; }
        public string countryunavail { get; set; }
        public string dsminorder { get; set; }
        public string dsvendorreq { get; set; }
        public string consumerdesc { get; set; }
        public string notsearch { get; set; }
        public string prodclearance { get; set; }
        public string countryorigin { get; set; }
        public string tariffcode { get; set; }
        public string map { get; set; }
        public string imperialDepth { get; set; }
        public string imperialHeight { get; set; }
        public string imperialWidth { get; set; }
        public string metricDepth { get; set; }
        public string metricHeight { get; set; }
        public string metricWidth { get; set; }
    }

}
