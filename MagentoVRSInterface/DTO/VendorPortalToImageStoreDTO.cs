﻿using MagentoInterface.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagentoVRSInterface.DTO
{
    public class VendorPortalToImageStoreDTO :BaseDTO
    {
        public Int32? MediaObjectId { get; set; }
        public Int32? MediaType { get; set; }
        public string URLPreview { get; set; }
        public string URLData { get; set; }
        public string VendorId { get; set; }
        public string MediaFileName { get; set; }
        public string sFileTitle { get; set; }
        public string sLongDescription { get; set; }
    }
}
