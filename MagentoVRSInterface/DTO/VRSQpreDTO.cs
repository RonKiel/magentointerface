﻿using MagentoInterface.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagentoVRSInterface.DTO
{
    public class VRSQpreDTO : BaseDTO
    {
        public Int32? LoadId { get; set; }
        public string PROMO { get; set; }
        public string TEXT1 { get; set; }
        public string NUMDYS { get; set; }
        public string LVAR { get; set; }
    }

}
