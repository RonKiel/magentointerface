﻿using MagentoInterface.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagentoVRSInterface.DTO
{
    public class LogDTO : BaseDTO
    {
        public Int32? ilogId { get; set; }
        public DateTime? dtTimeCreated { get; set; }
        public string sSystemName { get; set; }
        public string sLogClass { get; set; }
        public string sStackTrace { get; set; }
        public string sLogMessage { get; set; }
    }
}
