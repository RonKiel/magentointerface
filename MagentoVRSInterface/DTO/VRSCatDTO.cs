﻿using MagentoInterface.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagentoVRSInterface.DAO
{
    public class VRSCatDTO : BaseDTO
    {
        public Int32? LoadId { get; set; }
        public string CategoryID { get; set; }
        public string ParentID { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Attributes { get; set; }
        public string Position { get; set; }
    }

}
