﻿using MagentoInterface.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagentoVRSInterface.DTO
{
    public class VendorDTO :BaseDTO
    {
        public string VEND { get; set; }
        public string VNAME { get; set; }
    }

}
