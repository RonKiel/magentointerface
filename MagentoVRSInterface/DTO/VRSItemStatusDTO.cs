﻿using MagentoInterface.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagentoVRSInterface.DTO
{
    public class VRSItemStatusDTO : BaseDTO
    {
        public Int32? LoadId { get; set; }
        public string MfgCode { get; set; }
        public string CommonPartNum { get; set; }
        public string SKU { get; set; }
        public string LegacySKU { get; set; }
        public string IsActive { get; set; }
        public string QtyOnHand { get; set; }
        public string newnoonhand { get; set; }
        public string VendorAvail { get; set; }
        public string EstArrival { get; set; }
        public string prepaidflag { get; set; }
        public string costvalue { get; set; }
        public string promoflag { get; set; }
        public string clearance { get; set; }
        public string itemstatus { get; set; }
        public string itemcartstatus { get; set; }
        public string FirstRecDate { get; set; }
        public string Launch { get; set; }
        public string ClearanceDate { get; set; }
        public string Preorder { get; set; }
        public string AddDateNa { get; set; }
        public string itemcoavail { get; set; }
        public string itemcounavail { get; set; }
        public string promoid { get; set; }
        public string promotitle { get; set; }
        public string Availability { get; set; }
    }
}
