﻿using MagentoInterface.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagentoVRSInterface.DTO
{
    public class VRSImageDTO : BaseDTO 
    {
        public Int32? LoadId { get; set; }
        public string ImageType { get; set; }
        public string ProductKey { get; set; }
        public string SKU { get; set; }
        public string LegacySKU { get; set; }
        public Int32? Seq { get; set; }
        public string URL { get; set; }
        public string Preferred { get; set; }
    }

}
