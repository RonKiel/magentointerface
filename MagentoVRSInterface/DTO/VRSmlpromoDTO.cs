﻿using MagentoInterface.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagentoVRSInterface.DTO
{
    public class VRSmlpromoDTO : BaseDTO
    {
        public Int32? LoadId { get; set; }
        public string PROMO { get; set; }
        public string TEXT1 { get; set; }
        public string BEGDT { get; set; }
        public string ENDDT { get; set; }
        public Decimal? MINQTY { get; set; }
        public string ADDDT { get; set; }
        public string CHGDT { get; set; }
        public string ADDBY { get; set; }
        public string CHGBY { get; set; }
        public string LVAR { get; set; }
    }

}
