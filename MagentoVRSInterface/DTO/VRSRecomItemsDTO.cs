﻿using MagentoInterface.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagentoVRSInterface.DTO
{
    public class VRSRecomItemsDTO : BaseDTO 
    {
        public Int32? LoadId { get; set; }
        public string LibraryCode { get; set; }
        public string SortKey { get; set; }
        public string SKU { get; set; }
        public string LegacySKU { get; set; }
    }
}
