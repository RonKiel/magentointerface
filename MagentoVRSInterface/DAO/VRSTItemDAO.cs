﻿using MagentoInterface.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagentoVRSInterface.DAO
{
    class VRSTItemDAO : BaseDAO
    {
        public override string getTableName()
        {
            return "VRSTItems";
        }
        public override string getPrimaryKey()
        {
            return null;
        }
        public override object getEmptyObject()
        {
            return new DTO.VRSTItemDTO();
        }
    }
}
