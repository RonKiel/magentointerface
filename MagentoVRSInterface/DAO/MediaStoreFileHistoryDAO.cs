﻿using MagentoInterface.DAO;
using MagentoVRSInterface.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagentoVRSInterface.DAO
{
    public class MediaStoreFileHistoryDAO : BaseDAO
    {
        public string tableName = "MediaStoreFileHistory";

        public override string getTableName()
        {
            return tableName;
        }
        public override string getPrimaryKey()
        {
            return null;
        }
        public override object getEmptyObject()
        {
            return new MediaStoreFileHistoryDTO();
        }
        public override List<string> getInsertIgnoreList()
        {
            return new List<string>();
        }
        public override string getIdentityColumnName()
        {
            return null;
        }

        public Int32 MarkFilesForRunComplete(IDbConnection currConnection, IDbTransaction currTran, Int32 loadId)
        {
            Int32 rtnValue = 0;
            string updateSql = string.Format("UPDATE MediaStoreFileHistory set fileProcessed = 'Yes' WHERE LoadId ={0}", loadId);
            IDbCommand cmd4 = currConnection.CreateCommand();
            cmd4.Transaction = currTran;
            cmd4.CommandText = updateSql;
            rtnValue = cmd4.ExecuteNonQuery();
            return rtnValue;
        }

        public Int32 DeleteFilesForRun(IDbConnection currConnection, IDbTransaction currTran, Int32 loadId)
        {
            Int32 rtnValue = 0;
            string updateSql = string.Format("DELETE FROM MediaStoreFileHistory WHERE LoadId ={0}", loadId);
            IDbCommand cmd4 = currConnection.CreateCommand();
            cmd4.Transaction = currTran;
            cmd4.CommandText = updateSql;
            rtnValue = cmd4.ExecuteNonQuery();
            return rtnValue;
        }

        public List<MediaStoreFileHistoryDTO> convertToMyList(List<object> rawList)
        {
            List<MediaStoreFileHistoryDTO> rtnList = new List<MediaStoreFileHistoryDTO>();
            foreach (object rawobj in rawList)
            {
                rtnList.Add((MediaStoreFileHistoryDTO)rawobj);
            }
            return rtnList;
        }

        public List<MediaStoreFileHistoryDTO> selectRecordsRtnList(IDbConnection currConnection, string whereClause)
        {
            return selectRecordsRtnList(currConnection, whereClause, null, null);
        }
        public List<MediaStoreFileHistoryDTO> selectRecordsRtnList(IDbConnection currConnection, string whereClause, string orderBy)
        {
            return selectRecordsRtnList(currConnection, whereClause, null, orderBy);
        }
        public List<MediaStoreFileHistoryDTO> selectRecordsRtnList(IDbConnection currConnection, string whereClause, List<IDbDataParameter> parameters)
        {
            return selectRecordsRtnList(currConnection, null, whereClause, parameters, null);
        }
        public List<MediaStoreFileHistoryDTO> selectRecordsRtnList(IDbConnection currConnection, string whereClause, List<IDbDataParameter> parameters, string orderBy)
        {
            return selectRecordsRtnList(currConnection, null, whereClause, parameters, orderBy);
        }
        public List<MediaStoreFileHistoryDTO> selectRecordsRtnList(IDbConnection currConnection, IDbTransaction currTran, string whereClause)
        {
            return selectRecordsRtnList(currConnection, currTran, whereClause, null, null);
        }
        public List<MediaStoreFileHistoryDTO> selectRecordsRtnList(IDbConnection currConnection, IDbTransaction currTran, string whereClause, string orderBy)
        {
            return selectRecordsRtnList(currConnection, currTran, whereClause, null, orderBy);
        }
        public List<MediaStoreFileHistoryDTO> selectRecordsRtnList(IDbConnection currConnection, IDbTransaction currTran, string whereClause, List<IDbDataParameter> parameters, string orderBy)
        {
            DataTable dt = null;
            if (string.IsNullOrWhiteSpace(orderBy))
            {
                dt = base.selectRecordsRtnDataTable(currConnection, currTran, whereClause, parameters);
            }
            else
            {
                dt = base.sortDataTable(base.selectRecordsRtnDataTable(currConnection, currTran, whereClause, parameters), orderBy);
            }
            List<object> rawObjects = base.moveFromDataRowsToRawObjectList(dt);
            return convertToMyList(rawObjects);
        }


    }
}
