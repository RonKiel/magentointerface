﻿using MagentoInterface.DAO;
using MagentoVRSInterface.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagentoVRSInterface.DAO
{
    public class VendorPortalFileHistoryDAO : BaseDAO
    {
        public string tableName = "VendorPortalFileHistory";

        public override string getTableName()
        {
            return tableName;
        }
        public override string getPrimaryKey()
        {
            return null;
        }
        public override object getEmptyObject()
        {
            return new VendorPortalFileHistoryDTO();
        }
        public override List<string> getInsertIgnoreList()
        {
            return new List<string>();
        }
        public override string getIdentityColumnName()
        {
            return null;
        }

        public Int32 MarkFilesForRunComplete(IDbConnection currConnection, IDbTransaction currTran, Int32 loadId)
        {
            Int32 rtnValue = 0;
            string updateSql = string.Format("UPDATE {0} set fileProcessed = 'Yes' WHERE LoadId ={1}", tableName, loadId);
            IDbCommand cmd4 = currConnection.CreateCommand();
            cmd4.Transaction = currTran;
            cmd4.CommandText = updateSql;
            rtnValue = cmd4.ExecuteNonQuery();
            return rtnValue;
        }

        public Int32 DeleteFilesForRun(IDbConnection currConnection, IDbTransaction currTran, Int32 loadId)
        {
            Int32 rtnValue = 0;
            string updateSql = string.Format("DELETE FROM {0} WHERE LoadId ={1}",tableName, loadId);
            IDbCommand cmd4 = currConnection.CreateCommand();
            cmd4.Transaction = currTran;
            cmd4.CommandText = updateSql;
            rtnValue = cmd4.ExecuteNonQuery();
            return rtnValue;
        }


        public List<VendorPortalFileHistoryDTO> convertToMyList(List<object> rawList)
        {
            List<VendorPortalFileHistoryDTO> rtnList = new List<VendorPortalFileHistoryDTO>();
            foreach (object rawobj in rawList)
            {
                rtnList.Add((VendorPortalFileHistoryDTO)rawobj);
            }
            return rtnList;
        }

        public List<VendorPortalFileHistoryDTO> selectRecordsRtnList(IDbConnection currConnection, string whereClause)
        {
            return selectRecordsRtnList(currConnection, whereClause, null, null);
        }
        public List<VendorPortalFileHistoryDTO> selectRecordsRtnList(IDbConnection currConnection, string whereClause, string orderBy)
        {
            return selectRecordsRtnList(currConnection, whereClause, null, orderBy);
        }
        public List<VendorPortalFileHistoryDTO> selectRecordsRtnList(IDbConnection currConnection, string whereClause, List<IDbDataParameter> parameters)
        {
            return selectRecordsRtnList(currConnection, null, whereClause, parameters, null);
        }
        public List<VendorPortalFileHistoryDTO> selectRecordsRtnList(IDbConnection currConnection, string whereClause, List<IDbDataParameter> parameters, string orderBy)
        {
            return selectRecordsRtnList(currConnection, null, whereClause, parameters, orderBy);
        }
        public List<VendorPortalFileHistoryDTO> selectRecordsRtnList(IDbConnection currConnection, IDbTransaction currTran, string whereClause)
        {
            return selectRecordsRtnList(currConnection, currTran, whereClause, null, null);
        }
        public List<VendorPortalFileHistoryDTO> selectRecordsRtnList(IDbConnection currConnection, IDbTransaction currTran, string whereClause, string orderBy)
        {
            return selectRecordsRtnList(currConnection, currTran, whereClause, null, orderBy);
        }
        public List<VendorPortalFileHistoryDTO> selectRecordsRtnList(IDbConnection currConnection, IDbTransaction currTran, string whereClause, List<IDbDataParameter> parameters, string orderBy)
        {
            DataTable dt = null;
            if (string.IsNullOrWhiteSpace(orderBy))
            {
                dt = base.selectRecordsRtnDataTable(currConnection, currTran, whereClause, parameters);
            }
            else
            {
                dt = base.sortDataTable(base.selectRecordsRtnDataTable(currConnection, currTran, whereClause, parameters), orderBy);
            }
            List<object> rawObjects = base.moveFromDataRowsToRawObjectList(dt);
            return convertToMyList(rawObjects);
        }
    }
}
