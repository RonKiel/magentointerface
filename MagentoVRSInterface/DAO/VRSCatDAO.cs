﻿using MagentoInterface.DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagentoVRSInterface.DAO
{
    public class VRSCatDAO : BaseDAO
    {
        public override string getTableName()
        {
            return "VRSCategory";
        }
        public override string getPrimaryKey()
        {
            return null;
        }
        public override object getEmptyObject()
        {
            return new VRSCatDTO();
        }
        public override List<string> getInsertIgnoreList()
        {
            return new List<string>();
        }
        public override string getIdentityColumnName()
        {
            return null;
        }

        public List<VRSCatDTO> convertToMyList(List<object> rawList)
        {
            List<VRSCatDTO> rtnList = new List<VRSCatDTO>();
            foreach (object rawobj in rawList)
            {
                rtnList.Add((VRSCatDTO)rawobj);
            }
            return rtnList;
        }

        public List<VRSCatDTO> selectRecordsRtnList(IDbConnection currConnection, string whereClause)
        {
            return selectRecordsRtnList(currConnection, whereClause, null, null);
        }
        public List<VRSCatDTO> selectRecordsRtnList(IDbConnection currConnection, string whereClause, string orderBy)
        {
            return selectRecordsRtnList(currConnection, whereClause, null, orderBy);
        }
        public List<VRSCatDTO> selectRecordsRtnList(IDbConnection currConnection, string whereClause, List<IDbDataParameter> parameters)
        {
            return selectRecordsRtnList(currConnection, null, whereClause, parameters, null);
        }
        public List<VRSCatDTO> selectRecordsRtnList(IDbConnection currConnection, string whereClause, List<IDbDataParameter> parameters, string orderBy)
        {
            return selectRecordsRtnList(currConnection, null, whereClause, parameters, orderBy);
        }
        public List<VRSCatDTO> selectRecordsRtnList(IDbConnection currConnection, IDbTransaction currTran, string whereClause)
        {
            return selectRecordsRtnList(currConnection, currTran, whereClause, null, null);
        }
        public List<VRSCatDTO> selectRecordsRtnList(IDbConnection currConnection, IDbTransaction currTran, string whereClause, string orderBy)
        {
            return selectRecordsRtnList(currConnection, currTran, whereClause, null, orderBy);
        }
        public List<VRSCatDTO> selectRecordsRtnList(IDbConnection currConnection, IDbTransaction currTran, string whereClause, List<IDbDataParameter> parameters, string orderBy)
        {
            DataTable dt = null;
            if (string.IsNullOrWhiteSpace(orderBy))
            {
                dt = base.selectRecordsRtnDataTable(currConnection, currTran, whereClause, parameters);
            }
            else
            {
                dt = base.sortDataTable(base.selectRecordsRtnDataTable(currConnection, currTran, whereClause, parameters), orderBy);
            }
            List<object> rawObjects = base.moveFromDataRowsToRawObjectList(dt);
            return convertToMyList(rawObjects);
        }

    }

}
