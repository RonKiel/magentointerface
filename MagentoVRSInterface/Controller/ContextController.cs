﻿using MagentoVRSInterface.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagentoVRSInterface.Controller
{
    public class ContextController
    {
        private ContextController() { }
        private static volatile ContextController instance;
        private static object syncRoot = new Object();

        public Dictionary<string, string> skuMap = null;
        public Dictionary<string, string> vendorMap = null;
        public Dictionary<string, string> productMap = null;
        
        public Dictionary<string, List<string>> productToSkusMap = null;

        public Dictionary<string, List<string>> productToSkusMapExtended = null;
        public Dictionary<string, string> skusToProductsExtended = null;

        public Dictionary<string, string> majkeyToSku = null;
        public HashSet<string> validSkuSet = null;
        public HashSet<string> changedProductSet = null;
        public HashSet<string> validProductSet = null;
        public List<AtcddDTO> atccdList = null;

        public Dictionary<string, List<VRSItemAttribDTO>> itemAttributesAll = null;
        public HashSet<string> itemHaveNewItemAttributes = null;
        public HashSet<string> itemHaveRemovedItemAttributes = null;

        public Int32 loadId;
        public Int32 priorGoodLoad;

        public List<string> badProductsBecauseOfMfgr = null;

        public StringBuilder productDeleteBuffer = new StringBuilder();
        public StringBuilder catAndBasketDeleteBuffer = new StringBuilder();

        public Dictionary<string, VRSProductDTO> productDict = null;

        public StringBuilder allVendorPortalData = new StringBuilder();
        public String outputOnly = null;


        public static ContextController Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                        {
                            try
                            {
#if WEBAPP
                                if (System.Web.HttpRuntime.Cache["CONTEXTCONTROLLER"] != null)
                                {
                                    instance = (ContextController)System.Web.HttpRuntime.Cache["CONTEXTCONTROLLER"];
                                }
#endif
                                if (instance == null)
                                {
                                    instance = new ContextController();
                                    GC.KeepAlive(instance);  //Don't let garbage collection happen on this instance.
#if WEBAPP
                                    System.Web.HttpRuntime.Cache.Insert("CONTEXTCONTROLLER", instance, null, System.Web.Caching.Cache.NoAbsoluteExpiration, System.Web.Caching.Cache.NoSlidingExpiration,
                                        System.Web.Caching.CacheItemPriority.NotRemovable, null);
#endif
                                }
                            }
                            catch (Exception)
                            {
                                //Just CYA
                                instance = new ContextController();
                                GC.KeepAlive(instance);  //Don't let garbage collection happen on this instance.
                            }
                        }
                    }
                }
                return instance;
            }
        }
    }
}
