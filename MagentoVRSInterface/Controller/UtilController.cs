﻿using MagentoVRSInterface.DAO;
using MagentoVRSInterface.DTO;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MagentoVRSInterface.Controller
{
    public class UtilController
    {
        enum ModeType { AtEnd, ProcessingSeq, DoneProcessingSeq };
        LogController log = LogController.Instance;

        public string removeSequenceFromFile(string src)
        {
            ModeType currMode = ModeType.AtEnd;
            string numbers = "0123456789";
            char[] charArrayOfSrc = src.ToCharArray();
            string rtnValue = "";
            Int32 lenOfSrc = src.Length;
            Int32 startOfSeq = -1;
            Int32 endOfSeq = -1;

            for (Int32 i = lenOfSrc; i > 0; i--)
            {
                if (currMode == ModeType.AtEnd)
                {
                    if (numbers.Contains(charArrayOfSrc[i-1]))
                    {
                        endOfSeq = (i-1);
                        currMode = ModeType.ProcessingSeq;
                    }
                }
                else if (currMode == ModeType.ProcessingSeq)
                {
                    if (!numbers.Contains(charArrayOfSrc[i]))
                    {
                        startOfSeq = (i);
                        currMode = ModeType.DoneProcessingSeq;
                    }
                }
            }

            if (startOfSeq > 0 && endOfSeq > 0)
            {
                rtnValue = src.Substring(0, startOfSeq+1 ) + src.Substring(endOfSeq + 1);
            }
            else
            {
                rtnValue = src;
            }
            return rtnValue;
        }

        public Dictionary<string, string> loadContextData(ContextController contextController, IDbConnection blueConn, LogController log)
        {
            AtcddDAO atcddDAO = new AtcddDAO();
            Dictionary<string, string> vendorMap = null;
            Dictionary<string, string> productMap = new Dictionary<string, string>(); ;
            Dictionary<string, string> majkeyToSku = new Dictionary<string, string>();
            Dictionary<string, string> skuMap = null;
            List<AtcddDTO> atccdList = null;

            try
            {
                log.logMsg(LogController.LogClass.Info, "Reading ATCDD table.");
                atccdList = atcddDAO.selectRecordsRtnList(blueConn, "");
            }
            catch (Exception ex1)
            {
                log.logMsg(LogController.LogClass.Error, ex1.StackTrace, "Error loading Atccd, will try again.");
                Thread.Sleep(15000);
                atccdList = atcddDAO.selectRecordsRtnList(blueConn, "");
            }

            try
            {
                vendorMap = loadVendorIdToNameMap(blueConn, log);
            }
            catch (Exception ex1)
            {
                log.logMsg(LogController.LogClass.Error, ex1.StackTrace, "Error loading vendor map, will try again.");
                Thread.Sleep(15000);
                vendorMap = loadVendorIdToNameMap(blueConn, log);
            }

            try
            {
                loadProductAndAddDateMap(blueConn, log, productMap, majkeyToSku);
            }
            catch (Exception ex1)
            {
                log.logMsg(LogController.LogClass.Error, ex1.StackTrace, "Error loading product map, will try again.");
                Thread.Sleep(15000);
                productMap = new Dictionary<string, string>();
                majkeyToSku = new Dictionary<string, string>();
                loadProductAndAddDateMap(blueConn, log, productMap, majkeyToSku);
            }

            try
            {
                skuMap = loadSkuAndAddDateMap(blueConn, log);
            }
            catch (Exception ex1)
            {
                log.logMsg(LogController.LogClass.Error, ex1.StackTrace, "Error loading SKU map, will try again.");
                Thread.Sleep(15000);
                skuMap = loadSkuAndAddDateMap(blueConn, log);
            }

            contextController.skuMap = skuMap;
            contextController.vendorMap = vendorMap;
            contextController.productMap = productMap;
            contextController.atccdList = atccdList;
            contextController.majkeyToSku = majkeyToSku;
            return skuMap;
        }

        private Dictionary<string, string> loadSkuAndAddDateMap(IDbConnection blueConn, LogController log)
        {
            InvenDAO invenDAO = new InvenDAO();
            log.logMsg(LogController.LogClass.Info, "Reading INVEN table.");
            List<InvenDTO> invenRecs = invenDAO.selectRecordsRtnList(blueConn, "");
            Dictionary<string, string> skuMap = buildSkuMap(invenRecs);
            if (skuMap == null)
            {
                log.logMsg(LogController.LogClass.Error, "Error building Inven Dictionary.");
            }
            return skuMap;
        }

        private Dictionary<string, string> buildSkuMap(List<InvenDTO> invenRecs)
        {
            ProcessRecomItemsFile processRecomItemsFile = new ProcessRecomItemsFile(); //Just use any File processor.
            Dictionary<string, string> skuMap = null;
            try
            {
                skuMap = processRecomItemsFile.buildReplaceSkuDictionary(invenRecs);
            }
            catch (Exception ex1)
            {
                return null;
            }
            return skuMap;
        }

        private Dictionary<string, string> loadVendorIdToNameMap(IDbConnection blueConn, LogController log)
        {
            log.logMsg(LogController.LogClass.Info, "Reading Vendor table.");

            Dictionary<string, string> rtnDict = new Dictionary<string, string>();
            IDbCommand cmd = blueConn.CreateCommand();

            cmd.CommandText = "select VEND as VENDORID, VName AS NAME FROM vendor";

            IDataReader dr = cmd.ExecuteReader();
            DataTable rtnTable = new DataTable();
            rtnTable.Load(dr);
            dr.Close();

            foreach (DataRow row in rtnTable.Rows)
            {
                string name = row["NAME"].ToString();
                string titleName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(name.ToLower());
                rtnDict.Add(row["VENDORID"].ToString().Trim(), titleName);
            }

            return rtnDict;
        }

        private void loadProductAndAddDateMap(IDbConnection blueConn, LogController log, Dictionary<string, string> productMap, Dictionary<string, string> majkeyToSku)
        {
            U8DAO u8DAO = new U8DAO();
            log.logMsg(LogController.LogClass.Info, "Reading U8 table.");
            List<U8DTO> prodRecs = u8DAO.selectRecordsRtnList(blueConn, "");

            if (!buildProductMap(prodRecs, productMap, majkeyToSku))
            {
                log.logMsg(LogController.LogClass.Error, "Error building Product Dictionary.");
            }
        }

        private bool buildProductMap(List<U8DTO> u8Recs, Dictionary<string, string> productToLongDict, Dictionary<string, string> majkeyToSku)
        {
            bool rtnCode = true;
            List<U8DTO> u8RecsWithProdList = u8Recs.Where(x => !string.IsNullOrWhiteSpace(x.SKU)).ToList();

            foreach (U8DTO dto in u8RecsWithProdList)
            {
                try
                {
                    string SkuWithAddDt = buildProductToLongDict(productToLongDict, dto);
                    majkeyToSku.Add(dto.MAJKY, SkuWithAddDt);
                }
                catch (Exception ex1)
                {
                    string errorMsg = string.Format("Error when determing...{0}  Msg:({1})", dto.ToString(), ex1.Message);
                    LogController log = LogController.Instance;
                    log.logMsg(LogController.LogClass.Error, errorMsg);
                    rtnCode = false;
                }
            }
            return rtnCode;
        }

        private string buildProductToLongDict(Dictionary<string, string> productToLongDict, U8DTO dto)
        {
            if (!productToLongDict.Keys.Contains(dto.SKU.Trim()))
            {
                if (string.IsNullOrWhiteSpace(dto.ADDDT))
                {
                    productToLongDict.Add(dto.SKU.Trim(), dto.SKU.Trim() + "-000000");
                }
                else
                {
                    productToLongDict.Add(dto.SKU.Trim(), dto.SKU.Trim() + "-" + dto.ADDDT.Trim().Replace(@"/", ""));
                }
            }
            else
            {
                string currADDT = dto.ADDDT.Trim();

                string previousSku = productToLongDict[dto.SKU.Trim()];
                string oldDateFromSku = previousSku.Split('-').ToList()[1];
                string oldADDT = oldDateFromSku.Substring(0, 2) + @"/" + oldDateFromSku.Substring(2, 2) + @"/" + oldDateFromSku.Substring(4, 2);

                Int32 currADDTi = -1;
                Int32 oldADDTi = -1;
                Int32.TryParse(comparableDate(currADDT), out currADDTi);
                Int32.TryParse(comparableDate(oldADDT), out oldADDTi);

                if (currADDTi > oldADDTi)
                {
                    productToLongDict[dto.SKU.Trim()] = dto.SKU.Trim() + "-" + currADDT.Trim().Replace(@"/", "");
                }
            }

            return productToLongDict[dto.SKU.Trim()] + "-000000";
        }

        private string comparableDate(string srcDate)
        {
            string rtnValue = "000000";

            try
            {
                string month = srcDate.Substring(0, 2);
                string day = srcDate.Substring(3, 2);
                string year = srcDate.Substring(6, 2);

                if (year == "19" || year == "20")
                {
                    rtnValue = (Int32.Parse(year) > 60) ? "19" + year : "20" + year;
                    rtnValue += month;
                    rtnValue += day;
                }
            }
            catch (Exception)
            {
            }
            return rtnValue;
        }

        public int getNextLoadId(IDbConnection stagedConn)
        {
            IDbCommand cmd = stagedConn.CreateCommand();
            cmd.CommandText = "INSERT INTO VRSHist(LoadDate) values(getdate());SELECT @@IDENTITY"; //HACK sql server specific. 
            Int32 loadId = Int32.Parse(cmd.ExecuteScalar().ToString());
            return loadId;
        }

        public int peekNextLoadId(IDbConnection stagedConn)
        {
            IDbCommand cmd = stagedConn.CreateCommand();
            cmd.CommandText = "select IDENT_CURRENT( 'VRSHIST')"; //HACK sql server specific. 
            Int32 loadId = Int32.Parse(cmd.ExecuteScalar().ToString()) +1;
            return loadId;
        }

        public int getPriorLoadId(IDbConnection stagedConn)
        {
            IDbCommand cmd = stagedConn.CreateCommand();
            cmd.CommandText = "select ISNULL(MAX(LoadId),-1) FROM VRSHist where CompletionTime IS NOT NULL;";
            Int32 priorId = Int32.Parse(cmd.ExecuteScalar().ToString());
            return priorId;
        }

        public void purgeOldVersions(IDbConnection stagedConn, LogController log, Int32 loadId)
        {
            VRSHistDAO vRSHistDAO = new VRSHistDAO();
            //Delete old history.  This will cascasde delete from all of the staged tables.
            log.logMsg(LogController.LogClass.Info, "Cleanup old versions.");
            Int32 numGenToKeep = Int32.Parse(ConfigurationManager.AppSettings["NumberOfGenerationsToKeep"].ToString());

            List<VRSHistDTO> dtos = vRSHistDAO.selectRecordsRtnList(stagedConn, "", "LoadId");
            if (dtos.Count > numGenToKeep)
            {
                List<string> loadidsToDelete = new List<string>();
                List<VRSHistDTO> oldFailures = dtos.Where(x => x.CompletionTime == null && x.LoadId != loadId).ToList();
                List<VRSHistDTO> oldPriors = dtos.Where(x => x.LoadId != loadId).ToList();

                foreach (VRSHistDTO dto in oldFailures)
                {
                    loadidsToDelete.Add(dto.LoadId.ToString());
                }

                while ((dtos.Count - loadidsToDelete.Count) > numGenToKeep)
                {
                    loadidsToDelete.Add(oldPriors.First().LoadId.ToString());
                }
                string loadIdsInWhereClause = string.Format("({0})", string.Join(",", loadidsToDelete));

                IDbCommand cmd3 = stagedConn.CreateCommand();
                cmd3.CommandTimeout = 60000;
                cmd3.CommandText = string.Format("DELETE FROM VRSHist where LoadId IN {0}", loadIdsInWhereClause );
                cmd3.ExecuteNonQuery();
            }
        }

        public void markVRSHistoryAsComplete(IDbConnection stagedConn, Int32 loadId)
        {
            IDbCommand cmd4 = stagedConn.CreateCommand();
            cmd4.CommandText = string.Format("UPDATE VRSHist SET CompletionTime = getdate() WHERE LoadId = {0}", loadId); //HACK sql server specific. 
            cmd4.ExecuteNonQuery();
        }

        public void purgeOldLogs(IDbConnection stagedConn, LogController log)
        {
            log.logMsg(LogController.LogClass.Info, "Deleting old logs.");
            string numberOfDaysToKeep = ConfigurationManager.AppSettings["NumberOfDaysToKeepLogs"].ToString();
            IDbCommand cmd3 = stagedConn.CreateCommand();
            cmd3.CommandTimeout = 60000;
            cmd3.CommandText = string.Format("DELETE FROM Log  WHERE dtTimeCreated < DATEADD(dd,-{0},GETDATE()) ", numberOfDaysToKeep);
            cmd3.ExecuteNonQuery();
        }

        public void sendFileViaFTPS(KeyValuePair<string, StringBuilder> currFile)
        {
            string server = ConfigurationManager.AppSettings["FTPUrl"].ToString();
            string userName = ConfigurationManager.AppSettings["FTPUserName"].ToString();
            string pw = ConfigurationManager.AppSettings["FTPPw"].ToString();

            Int32 lastIndexOfSlash = currFile.Key.LastIndexOf(@"\");
            string fileName = removeSequenceIfApplicable(currFile.Key.Substring(lastIndexOfSlash).Replace(@"\", ""));

            byte[] buffer = Encoding.ASCII.GetBytes(currFile.Value.ToString());

            WebRequest request = WebRequest.Create("ftp://" + server + "/" + fileName);
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(userName, pw);

            Stream reqStream = request.GetRequestStream();
            reqStream.Write(buffer, 0, buffer.Length);
            reqStream.Close();
        }

        #region DOWNLOADVIASFTP

        public List<string> getFileListViaSFTP(string fileExtension, string execName, string server, string userName, string pw, string MediaStoreSourceFolder)
        {
            string winscpOpen = string.Format("\"open sftp://{0}:{1}@{2}/\"", userName, pw, server);
            return getFileListViaSFTP(fileExtension, execName, winscpOpen, MediaStoreSourceFolder);

        }

        public List<string> getFileListViaSFTP(string fileExtension, string execName, string winscpOpen, string srouceDir)
        {
            List<string> rtnList = new List<string>();

            string winscpOptions = "/command \"option batch abort\" \"option confirm off\"";

            string winscpLs = string.Format("\"ls {0}\"", srouceDir);
            string completeSection = "\"exit\"";

            string wincpCmd = string.Format("{0} {1} {2} {3}", winscpOptions, winscpOpen, winscpLs, completeSection);

            Process p = new Process();
            p.StartInfo.UseShellExecute = false;
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.FileName = execName;
            p.StartInfo.Arguments = wincpCmd;
            p.Start();
            string output = p.StandardOutput.ReadToEnd();
            p.WaitForExit();
            log.logMsg(LogController.LogClass.Info, string.Format("SFTP Result({0})", output));


            if (!string.IsNullOrWhiteSpace(fileExtension))
            {
                Int32 eLen = fileExtension.Length;
                rtnList = output.Replace("\r", "").Split('\n').ToList().
                    Where(x => x.Length > eLen).ToList().
                    Where(x => x.Substring(x.Length - eLen).Equals(fileExtension)).
                    ToList();
                ;
            }
            else
            {
                rtnList = output.Replace("\r", "").Split('\n').ToList();
            }

            return rtnList;
        }

        public string getFileNameFromSFTPEntry(string entry)
        {
            //"----------   0                    23008601 Apr 17 15:18:52 2015 CID_NDEV_IM.csv"
            List<string> fileParts = entry.Split(' ').ToList();
            return fileParts.Last();
        }

        public DateTime getCreatedTimeFromSFTPEntry(string entry)
        {
            //"----------   0                    23008601 Apr 17 15:18:52 2015 CID_NDEV_IM.csv"
            List<string> alphaMonth = new List<string>(){ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
            List<string> fileParts = entry.Split(' ').ToList().Where(x=> x.Length > 0).ToList();
            string year = fileParts[fileParts.Count - 2];
            string time = fileParts[fileParts.Count - 3];
            string day = fileParts[fileParts.Count - 4];
            string month = fileParts[fileParts.Count - 5];

            Int32 yearInt = Int32.Parse(year);
            Int32 monthInt = alphaMonth.IndexOf(month) + 1;
            Int32 dayInt = Int32.Parse(day);

            List<string> timeParts = time.Split(':').ToList();
            Int32 hourInt = Int32.Parse(timeParts[0]);
            Int32 minInt = Int32.Parse(timeParts[1]);
            Int32 secondInt = Int32.Parse(timeParts[2]);

            DateTime rtnDateTime = new DateTime(yearInt, monthInt, dayInt, hourInt, minInt, secondInt);

            return rtnDateTime;
        }

        public Int64 getFileSizeFromSFTPEntry(string entry)
        {
            List<string> fileParts = entry.Split(' ').ToList().Where(x => x.Length > 0).ToList();
            string fileSizeStr = fileParts[fileParts.Count - 6];
            return Int64.Parse(fileSizeStr);
        }

        public List<string> downloadFromVendorPortalViaSFTP(IDbConnection stagedConn, Int32 loadId, string fileExtension, bool checkAgeOfFile)
        {
            string execName = string.Format("\"{0}\"", ConfigurationManager.AppSettings["SFTPExec"].ToString());

            string server = ConfigurationManager.AppSettings["VendorPortalFTPUrl"].ToString();
            string userName = ConfigurationManager.AppSettings["VendorPortalFTPUserName"].ToString();
            string pw = ConfigurationManager.AppSettings["VendorPortalFTPPw"].ToString();
            string sourceDir = ConfigurationManager.AppSettings["VendorPortalSourceDir"].ToString();
            string localDir = ConfigurationManager.AppSettings["VendorPortalLocalDir"].ToString();

            return downloadFromVendorPortalViaSFTP(stagedConn, loadId, fileExtension, execName, server, userName, pw, sourceDir, localDir, checkAgeOfFile);
        }

        //public List<string> DownloadFromMediaStoreViaSFTP(IDbConnection stagedConn, Int32 loadId, string fileExtension, bool checkAgeOfFile)
        //{
        //    string mediaStoreSourceFolder   = ConfigurationManager.AppSettings["MediaStoreSourceFolder"].ToString();
        //    string mediaStoreLocalFolder    = ConfigurationManager.AppSettings["MediaStoreLocalFolder"].ToString();
        //    int secondsRequired             = int.Parse(ConfigurationManager.AppSettings["TimeRequiredToBeValidInSeconds"].ToString());

        //    List<string> filesDownloaded    = new List<string>();

        //    MediaStoreFileHistoryDAO mediaStoreFileHistoryDAO   = new MediaStoreFileHistoryDAO();
        //    List<string> fileList                               = GetMediaStoreFileList(mediaStoreSourceFolder);

        //    foreach (string fileName in fileList)
        //    {
        //        DateTime fileTime = getCreatedTimeFromSFTPEntry(fileName);
        //        TimeSpan span = DateTime.Now - fileTime;
        //        Int32 secondsOld = (Int32)span.TotalSeconds;


        //        if (!checkAgeOfFile || secondsOld > secondsRequired)
        //        {
        //            string destFileName = DownloadMediaStoreFiles(mediaStoreSourceFolder, mediaStoreLocalFolder, filesDownloaded, fileName);

        //            MediaStoreFileHistoryDTO fileProcessed = new MediaStoreFileHistoryDTO();
        //            fileProcessed.FTPEntry = fileName;
        //            fileProcessed.FileName = getFileNameFromSFTPEntry(fileName);
        //            fileProcessed.DateTime = fileTime;

        //            if (fileProcessed.FileName.Contains("_IT.csv"))
        //            {
        //                string activeRecords = filterImageFilesForOnlyActive(fileProcessed.FileName, File.ReadAllText(destFileName));
        //                fileProcessed.RawFile = orderBySeqAndPreferredLimitToOnePerSku(activeRecords);
        //            }
        //            else
        //            {
        //                string buffer = filterImageFilesForOnlyActive(fileProcessed.FileName, File.ReadAllText(destFileName));
        //                fileProcessed.RawFile = buffer;
        //            }
        //            fileProcessed.FileProcessed = "No";
        //            fileProcessed.LoadId = loadId;
        //            mediaStoreFileHistoryDAO.insert(stagedConn, fileProcessed);
        //        }
        //        else
        //        {
        //            log.logMsg(LogController.LogClass.Warning, string.Format("Skipping Media file:({0}) because it was NOT old enough, could be in the process of being built.", fileName));
        //        }
        //    }

        //    return filesDownloaded;
        //}

        //public List<string> downloadFromMediaStoreViaSFTP(IDbConnection stagedConn, Int32 loadId, string fileExtension, string execName, string server, string userName, string pw, string srouceDir, string localDownloadDir, bool checkAgeOfFile)
        //{
        //    MediaStoreFileHistoryDAO mediaStoreFileHistoryDAO = new MediaStoreFileHistoryDAO();
        //    List<string> filesDownloaded = new List<string>();
        //    List<string> fileList = getFileListViaSFTP(fileExtension, execName, server, userName, pw, srouceDir);
        //    foreach (string fileEntry in fileList)
        //    {
        //        DateTime fileTime = getCreatedTimeFromSFTPEntry(fileEntry);
        //        TimeSpan span = DateTime.Now - fileTime;
        //        Int32 secondsOld = (Int32)span.TotalSeconds;

        //        Int32 secondsRequired = Int32.Parse(ConfigurationManager.AppSettings["TimeRequiredToBeValidInSeconds"].ToString());
        //        if (!checkAgeOfFile || secondsOld > secondsRequired)
        //        {
        //            string destFileName = downloadViaSFTP(execName, server, userName, pw, srouceDir, localDownloadDir, filesDownloaded, fileEntry);

        //            MediaStoreFileHistoryDTO fileProcessed = new MediaStoreFileHistoryDTO();
        //            fileProcessed.FTPEntry = fileEntry;
        //            fileProcessed.FileName = getFileNameFromSFTPEntry(fileEntry);
        //            fileProcessed.DateTime = fileTime;

        //            if (fileProcessed.FileName.Contains("_IT.csv"))
        //            {
        //                string activeRecords = filterImageFilesForOnlyActive(fileProcessed.FileName, File.ReadAllText(destFileName));
        //                fileProcessed.RawFile = orderBySeqAndPreferredLimitToOnePerSku(activeRecords);
        //            }
        //            else
        //            {
        //                string buffer = filterImageFilesForOnlyActive(fileProcessed.FileName, File.ReadAllText(destFileName));
        //                fileProcessed.RawFile = buffer;
        //            }
        //            fileProcessed.FileProcessed = "No";
        //            fileProcessed.LoadId = loadId;
        //            mediaStoreFileHistoryDAO.insert(stagedConn, fileProcessed);
        //        }
        //        else
        //        {
        //            log.logMsg(LogController.LogClass.Warning, string.Format("Skipping Media file:({0}) because it was NOT old enough, could be in the process of being built.", fileEntry));
        //        }
        //    }

        //    return filesDownloaded;
        //}

        //public string orderBySeqAndPreferredLimitToOnePerSku(string mediaFile)
        //{
        //    List<string> rtnRecords = new List<string>();
        //    DataTable dt = new DataTable();

        //    List<string> mediaLines = mediaFile.Split('\n').ToList();
        //    List<string> fldHeaders = mediaLines[0].Replace("\r", "").Split(',').ToList();
        //    rtnRecords.Add(mediaLines[0].Replace("\r", ""));

        //    foreach (string hdr in fldHeaders)
        //    {
        //        dt.Columns.Add(new DataColumn(hdr));
        //    }

        //    Int32 i = 0;
        //    foreach (string mediaLine in mediaLines)
        //    {
        //        List<string> flds = mediaLine.Replace("\r", "").Replace("'","").Split(',').ToList();
        //        if (flds.Count > 2 && i > 0)
        //        {
        //            flds[2] = padToSize(flds[2], 6, "0");
        //            dt.Rows.Add(flds.ToArray());
        //        }
        //        i++;
        //    }

        //    DataView dv = new DataView(dt);
        //    dv.Sort = "SKU, Seq, Preferred ASC";

        //    HashSet<string> skuSet = new HashSet<string>();
        //    foreach (DataRowView drv in dv)
        //    {
        //        string rec = string.Format("{0},{1},{2},{3},{4},{5}", 
        //            drv["PK"], 
        //            drv["ProductKey"],  
        //            drv["SKU"], 
        //            drv["Seq"], 
        //            drv["Url"], 
        //            drv["Preferred"]);

        //        string currentSku = drv["SKU"].ToString();
        //        if (!skuSet.Contains(currentSku))
        //        {
        //            rtnRecords.Add(rec);
        //            skuSet.Add(currentSku);
        //        }
        //    }
        //    string rtnBuffer = string.Join("\n", rtnRecords);
        //    return rtnBuffer;
        //}

        //public string setPreferedValue(string mediaFile, Int32 prefered)
        //{
        //    ContextController contextController = ContextController.Instance;
        //    StringBuilder rtnValue = new StringBuilder();

        //    List<string> mediaLines = mediaFile.Split('\n').ToList();
        //    Int32 i = 0;

        //    foreach (string mediaLine in mediaLines)
        //    {
        //        List<string> flds = mediaLine.Replace("\r", "").Split(',').ToList();
        //        if (flds.Count > 5 && i > 0)
        //        {
        //            flds[3] = prefered.ToString();
        //            string mediaLineReplacment = string.Join(",", flds);
        //            rtnValue.Append(mediaLineReplacment + "\n");
        //        }
        //        else
        //        {
        //            rtnValue.Append(mediaLine + "\n");
        //        }
        //        i++;
        //    }
        //    return rtnValue.ToString();
        //}


        //public string filterImageFilesForOnlyActive(string fileName, string mediaFile) 
        //{
        //    ContextController contextController = ContextController.Instance;
        //    string imageType = getImageType(fileName);
        //    List<string> productRecs = new List<string>() { "PT", "PM" };
        //    StringBuilder rtnValue = new StringBuilder();

        //    List<string> mediaLines = mediaFile.Split('\n').ToList();
        //    Int32 i = 0;
        //    Int32 goodRecords = 0;
        //    Int32 badRecords = 0;

        //    foreach (string mediaLine in mediaLines)
        //    {
        //        List<string> flds = mediaLine.Replace("\r", "").Split(',').ToList();
        //        if (flds.Count > 2 && i > 0)
        //        {
        //            bool hasErrorOccured = false;

        //            if ( productRecs.Contains(imageType) )
        //            {
        //                string product = flds[1].Replace("'", "").Trim();
        //                if ( contextController.productMap != null && contextController.productMap.Keys.Count > 0)
        //                {
        //                    if (!contextController.productMap.Keys.Contains(product))
        //                    {
        //                        log.logMsg(LogController.LogClass.Debug, string.Format("ImageStoreFile: product:{0} is not yet live.", product));
        //                        hasErrorOccured = true;
        //                    }
        //                }
        //            }
        //            else 
        //            {
        //                string sku = flds[2].Replace("'", "").Trim();
        //                if (contextController.skuMap != null && contextController.skuMap.Keys.Count > 0)
        //                {
        //                    if (!contextController.skuMap.Keys.Contains(padToSize(sku,6,"0")))
        //                    {
        //                        log.logMsg(LogController.LogClass.Debug, string.Format("ImageStoreFile: sku:{0} is not yet live.", sku));
        //                        hasErrorOccured = true;
        //                    }
        //                }
        //            }

        //            if (!hasErrorOccured)
        //            {
        //                rtnValue.Append(mediaLine + "\n");
        //                goodRecords++;
        //            }
        //            else
        //            {
        //                badRecords++;
        //            }
        //        }
        //        else
        //        {
        //            rtnValue.Append(mediaLine + "\n");
        //        }
        //        i++;
        //    }
        //    log.logMsg(LogController.LogClass.Info, string.Format("ImageStoreFile file:({0}) good records:({1}) bad records:({2})", fileName, goodRecords, badRecords));
        //    return rtnValue.ToString();
        //}

        public string getImageType(string fileName)
        {
            string rtnValue = fileName.Substring(fileName.Length - 6, 2);
            return rtnValue;
        }

        public List<string> downloadFromVendorPortalViaSFTP(IDbConnection stagedConn, Int32 loadId, string fileExtension, string execName, string server, string userName, string pw, string srouceDir, string localDownloadDir, bool checkAgeOfFile)
        {
            VendorPortalFileHistoryDAO vendorPortalFileHistoryDAO = new VendorPortalFileHistoryDAO();

            List<string> filesDownloaded = new List<string>();
            List<string> fileList = getFileListViaSFTP(fileExtension, execName, server, userName, pw, srouceDir);
            foreach (string fileEntry in fileList)
            {
                DateTime fileTime = getCreatedTimeFromSFTPEntry(fileEntry);
                TimeSpan span = DateTime.Now - fileTime;
                Int32 secondsOld = (Int32)span.TotalSeconds;

                Int32 secondsRequired = Int32.Parse(ConfigurationManager.AppSettings["TimeRequiredToBeValidInSeconds"].ToString());
                if ( !checkAgeOfFile || secondsOld > secondsRequired)
                {
                    string destFileName = downloadViaSFTP(execName, server, userName, pw, srouceDir, localDownloadDir, filesDownloaded, fileEntry);

                    MediaStoreFileHistoryDTO fileProcessed = new MediaStoreFileHistoryDTO();
                    fileProcessed.FTPEntry = fileEntry;
                    fileProcessed.FileName = getFileNameFromSFTPEntry(fileEntry);
                    fileProcessed.DateTime = fileTime;
                    fileProcessed.FileProcessed = "No";
                    fileProcessed.LoadId = loadId;
                    fileProcessed.RawFile = File.ReadAllText(destFileName);
                    vendorPortalFileHistoryDAO.insert(stagedConn, fileProcessed);
                }
                else
                {
                    log.logMsg(LogController.LogClass.Warning, string.Format("Skipping Vendor Portal file:({0}) because it was NOT old enough, could be in the process of being built.", fileEntry));
                }
            }

            return filesDownloaded;
        }

        private string downloadViaSFTP(string execName, string server, string userName, string pw, string srouceDir, string localDownloadDir, List<string> filesDownloaded, string fileEntry)
        {
            string sourceFileName = srouceDir + @"/" + getFileNameFromSFTPEntry(fileEntry);
            string destFileName = localDownloadDir + @"\" + getFileNameFromSFTPEntry(fileEntry);
            string winscpOptions = "/command \"option batch abort\" \"option confirm off\"";
            string winscpOpen = string.Format("\"open sftp://{0}:{1}@{2}/\"", userName, pw, server);
            string backupFileName = destFileName + ".BAK";

            if (File.Exists(destFileName))
            {
                if (File.Exists(backupFileName))
                {
                    File.Delete(backupFileName);
                }
                File.Move(destFileName, backupFileName);
            }

            string winGetCmd = string.Format("\"get {0} {1} \"", sourceFileName, destFileName);
            string completeSection = "\"exit\"";

            string wincpCmd = string.Format("{0} {1} {2} {3}", winscpOptions, winscpOpen, winGetCmd, completeSection);

            Process p = new Process();
            p.StartInfo.UseShellExecute = false;
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.FileName = execName;
            p.StartInfo.Arguments = wincpCmd;
            p.Start();
            string output = p.StandardOutput.ReadToEnd();
            p.WaitForExit();
            filesDownloaded.Add(fileEntry);

            if (!File.Exists(destFileName) || File.ReadAllLines(destFileName).Length < 5)
            {
                if (File.Exists(backupFileName) && File.ReadAllLines(destFileName).Length > 5)
                {
                    File.Move(backupFileName, destFileName);
                    log.logMsg(LogController.LogClass.Error, 
                        string.Format("Resusing data file:({0}) because download was not successful and old file existed.", destFileName));
                }
            }
            
            log.logMsg(LogController.LogClass.Info, string.Format("SFTP Result({0})", output));
            return destFileName;
        }

        #endregion

        public void UploadFileViaSFTPWithHostKey(KeyValuePair<string, StringBuilder> currFile)
        {
            string uploadFolder = ConfigurationManager.AppSettings["SFTPDestDirectory"].ToString();
            string server       = ConfigurationManager.AppSettings["FTPUrl"].ToString();
            string userName     = ConfigurationManager.AppSettings["FTPUserName"].ToString();
            string pw           = ConfigurationManager.AppSettings["FTPPw"].ToString();
            string hostKey      = ConfigurationManager.AppSettings["HOSTkey"].ToString();
            string winscpExe    = ConfigurationManager.AppSettings["SFTPExec"].ToString();
            string tempFolder   = ConfigurationManager.AppSettings["SFTPTmpLocation"].ToString();

            Int32 lastBackSlash = currFile.Key.LastIndexOf(@"\");
            string fileName     = removeSequenceFromFile(currFile.Key.Substring(lastBackSlash + 1).Replace(".tab", ".tmp"));
            string tempFileName = tempFolder + fileName;

            File.WriteAllText(tempFileName, currFile.Value.ToString());

            string batch = "option batch abort";
            string confirm = "option confirm off";
            string open = string.Format(@"open sftp://{0}:{1}@{2}/ -hostkey=""{3}""", userName, pw, server, hostKey);
            string put = string.Format("put {0} {1}", tempFileName, uploadFolder);
            string mv = string.Format("mv {0}{1} {0}{2}", uploadFolder, fileName, fileName.Replace(".tmp", ".tab"));

            Process winscp = new Process();
            winscp.StartInfo.FileName = winscpExe;
            winscp.StartInfo.UseShellExecute = false;
            winscp.StartInfo.CreateNoWindow = true;
            winscp.StartInfo.RedirectStandardInput = true;
            winscp.StartInfo.RedirectStandardOutput = true;
            winscp.Start();

            winscp.StandardInput.WriteLine(batch);
            winscp.StandardInput.WriteLine(confirm);
            winscp.StandardInput.WriteLine(open);
            winscp.StandardInput.WriteLine(put);
            winscp.StandardInput.WriteLine(mv);
            winscp.StandardInput.Close();

            string winscpOutput = winscp.StandardOutput.ReadToEnd();

            winscp.WaitForExit();

            log.logMsg(LogController.LogClass.Info, string.Format("SFTP Result({0})", winscpOutput));

            if (winscpOutput.Contains("Warning") || winscpOutput.Contains("Terminated by user"))
            {
                log.logMsg(LogController.LogClass.Error, string.Format("Error occured on SFTP SEND, SFTP Result({0})", winscpOutput));
            }

            File.Delete(tempFileName);
        }

        //public void sendFileViaSFTPWithHostKey(KeyValuePair<string, StringBuilder> currFile)
        //{
        //    string sftpDestLocation = determineFileLocation(currFile.Key);
        //    string server = ConfigurationManager.AppSettings["FTPUrl"].ToString();
        //    string userName = ConfigurationManager.AppSettings["FTPUserName"].ToString();
        //    string pw = ConfigurationManager.AppSettings["FTPPw"].ToString();

        //    Int32 lastIndexOfSlash = currFile.Key.LastIndexOf(@"\");
        //    string fileName = currFile.Key.Substring(lastIndexOfSlash).Replace(@"\", "").Replace(".tab", ".tmp");
        //    string tmpFileName = ConfigurationManager.AppSettings["SFTPTmpLocation"].ToString() + fileName;

        //    File.WriteAllText(tmpFileName, currFile.Value.ToString());
        //    string hostKey = ConfigurationManager.AppSettings["HOSTkey"].ToString();

        //    string winscpOptions = "/command \"option batch abort\" \"option confirm off\"";
        //    hostKey = "\"" + hostKey + "\"";
        //    string winscpOpen = string.Format("\"open sftp://{0}:{1}@{2}/ -hostkey={3}", userName, pw, server, hostKey);
        //    //string winscpOpen = string.Format("\"open sftp://{0}:{1}@{2}{3}\"", userName, pw, server, string.Format(" -hostkey=""{0}"""", hostKey));
        //    //string winscpOpen = string.Format("\"open sftp://{0}:{1}@{2}/\"", userName, pw, server,
        //    //        string.Format(" -hostkey=\"{0}\"", hostKey));


        //    ftpTransmitFileWithWINSCP(sftpDestLocation, fileName, tmpFileName, winscpOptions, winscpOpen);
        //}


        //public void ftpTransmitFileWithWINSCP(string sftpDestLocation, string fileName, string tmpFileName, string winscpOptions, string winscpOpen)
        //{
        //    string winscpPut = string.Format("\"put {0} {1}\"", tmpFileName, sftpDestLocation);
        //    string winscpMv = string.Format("\"mv {0}{1} {0}{2}\"", sftpDestLocation, fileName, removeSequenceIfApplicable(fileName.Replace(".tmp", ".tab")));
        //    string completeSection = "\"exit\"";
        //    string execName = string.Format("\"{0}\"", ConfigurationManager.AppSettings["SFTPExec"].ToString());
        //    string wincpCmd = string.Format("{0} {1} {2} {3} {4} ", winscpOptions, winscpOpen, winscpPut, winscpMv, completeSection);

        //    Process p = new Process();
        //    p.StartInfo.UseShellExecute = false;
        //    p.StartInfo.RedirectStandardOutput = true;
        //    p.StartInfo.FileName = execName;
        //    p.StartInfo.Arguments = wincpCmd;
        //    p.Start();
        //    string output = p.StandardOutput.ReadToEnd();
        //    p.WaitForExit();
        //    File.Delete(tmpFileName);
        //    log.logMsg(LogController.LogClass.Info, string.Format("SFTP Result({0})", output));
        //    if (output.Contains("Warning") || output.Contains("Terminated by user"))
        //    {
        //        log.logMsg(LogController.LogClass.Error, string.Format("Error occured on SFTP SEND, SFTP Result({0})", output));
        //    }
        //}

        //public string determineFileLocation(string  currFile)
        //{
        //    string rtnValue = "";
        //    List<string> filesRaw = new List<string>();

        //    if (ConfigurationManager.AppSettings["SFTPLocationList"] != null)
        //    {
        //        filesRaw.AddRange(ConfigurationManager.AppSettings["SFTPLocationList"].ToString().Split(',').ToList());
        //    }
        //    List<string> files = new List<string>();
        //    foreach (string filename in filesRaw)
        //    {
        //        files.Add(filename.ToUpper());
        //    }

        //    for(Int32 i =0; i < files.Count && rtnValue ==""; i++)
        //    {
        //        string fileName = files[i];
        //        if (currFile.ToUpper().Contains(fileName))
        //        {
        //            rtnValue = ConfigurationManager.AppSettings[fileName].ToString();
        //        }
        //    }

        //    if (rtnValue == "")
        //    {
        //        rtnValue = ConfigurationManager.AppSettings["SFTPDestDirectory"].ToString();
        //    }
        //    return rtnValue;
        //}

        public string removeSequenceIfApplicable(string src)
        {
            string rtnValue = src;
            UtilController util = new UtilController();
            string removeSequenceFromFile = ConfigurationManager.AppSettings["FTPRemoveSequenceNumberFromFile"].ToString();
            if (removeSequenceFromFile.ToUpper().Equals("YES"))
            {
                rtnValue = util.removeSequenceFromFile(src);
            }

            return rtnValue;
        }

        public List<string> lowerCaseList(List<string> argList)
        {
            List<string> largs = new List<string>();
            foreach (string arg in argList)
            {
                largs.Add(arg.ToLower());
            }
            return largs;
        }

        //public string padToSize(string source, Int32 size, string padChar)
        //{
        //    if (source.Length >= size)
        //    {
        //        return source;
        //    }
        //    Int32 diffInSize = size - source.Length;
        //    string padding = "";
        //    for (int x = 0; x < diffInSize; x++)
        //    {
        //        padding += padChar;
        //    }
        //    return padding + source;
        //}
    }
}
