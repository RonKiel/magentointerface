﻿using MagentoVRSInterface.DTO;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagentoVRSInterface.Controller
{
    public class ProcessRecomItemsFile : FileProcessor
    {
        LogController log = LogController.Instance;

        public override List<KeyValuePair<string, StringBuilder>> processFile(System.Data.IDbConnection stagedConn, string sourceFile )
        {
            List<VRSRecomItemsDTO> rawRecsFromFile = buildDTOFromFile(sourceFile, contextController.loadId);
            base.replaceItemSkuWithExtendedVersion(rawRecsFromFile, "SKU");
            base.reassignProductIdsInPlaceOfSkusForSimpleProducts(rawRecsFromFile, "SKU");
            return processFile(stagedConn, rawRecsFromFile, contextController.loadId, contextController.priorGoodLoad);
        }

        public List<KeyValuePair<string, StringBuilder>> processFile(System.Data.IDbConnection stagedConn, List<VRSRecomItemsDTO> rawRecsList, int loadId, Int32 priorGoodId)
        {
            StringBuilder chgFileText = new StringBuilder();
            List<KeyValuePair<string, StringBuilder>> rtnList = new List<KeyValuePair<string, StringBuilder>>();

            BulkInsert((SqlConnection)stagedConn, "VRSRecomItems", rawRecsList); //HACK sql server specific.

            buildWriteBufferForChanges(stagedConn, loadId, priorGoodId, chgFileText);
            //buildWriteBufferForDeletes(stagedConn, loadId, priorGoodId, contextController.productDeleteBuffer);
            string DestDirectory = ConfigurationManager.AppSettings["FileBuildLocation"].ToString();

            string chgFile = DestDirectory + "recomitems" + loadId.ToString("D8") + ".tab";
            rtnList.Add(new KeyValuePair<string, StringBuilder>(chgFile, chgFileText));

            return rtnList;
        }

        private void buildWriteBufferForDeletes(System.Data.IDbConnection stagedConn, int loadId, Int32 priorGoodId, StringBuilder deleteFileText)
        {
            List<VRSRecomItemsDTO> deleteRecords = buildDeletedRecords(stagedConn, loadId, priorGoodId);
            deleteFileText.Append("LibraryCode\tLegacySKU\tSKU\n");
            foreach (VRSRecomItemsDTO dto in deleteRecords)
            {
                deleteFileText.Append(string.Format("-CPP\t{0}\n", dto.SKU));
            }
        }

        private void buildWriteBufferForChanges(System.Data.IDbConnection stagedConn, int loadId, Int32 priorGoodId, StringBuilder chgFileText)
        {
            List<VRSRecomItemsDTO> changedRecords = buildChangedRecords(stagedConn, loadId, priorGoodId);
            Dictionary<string, string> libraryCodeToUrlPath = buildLibraryCodeToUrlPathDictionary(stagedConn, loadId);

            foreach (VRSRecomItemsDTO dto in changedRecords)
            {
                if( libraryCodeToUrlPath.Keys.Contains(dto.LibraryCode.Trim())) 
                {
                    string urlPath = libraryCodeToUrlPath[dto.LibraryCode.Trim()];
                    string tRec = string.Format("CCP\t{0}\t{1}\t{2}\n", urlPath, dto.SKU, dto.SortKey);
                    chgFileText.Append(tRec);
                }
                else
                {
                    log.logMsg(MagentoVRSInterface.Controller.LogController.LogClass.Debug,
                        string.Format("RecomItem rec filtered out because of library code ({0}) and an invalid Cat code  SKU:({1})",
                        dto.LibraryCode.Trim(), dto.SKU));
                }
            }
        }

        private Dictionary<string, string> buildLibraryCodeToUrlPathDictionary(System.Data.IDbConnection stagedConn, int loadId)
        {
            Dictionary<string, string> libraryCodeToUrlPath = new Dictionary<string, string>();

            ProcessCategoryFile processCategoryFile = new ProcessCategoryFile();
            Dictionary<string, string> categoryRecs = processCategoryFile.getCategoryCodeToUrlPathDict(stagedConn, loadId);

            ProcessRecomLibraryFile processRecomLibraryFile = new ProcessRecomLibraryFile();
            List<VRSRecomLibraryDTO> currentLibRecs = processRecomLibraryFile.buildChangedRecords(stagedConn, loadId, -1);

            foreach (VRSRecomLibraryDTO dto in currentLibRecs)
            {
                if (dto.RootCatCode != null)
                {
                    if (!processRecomLibraryFile.codesToSkip.Contains(dto.RootCatCode.Trim()) && categoryRecs.Keys.Contains(dto.RootCatCode.Trim()))
                    {
                        string urlKey = base.urlKeyFormatting(dto.Title.ToLower().Trim());
                        string urlPath = string.Format("baskets/{0}/{1}", categoryRecs[dto.RootCatCode.Trim()], urlKey);
                        libraryCodeToUrlPath.Add(dto.LibraryCode, urlPath);
                    }
                }
            }
            return libraryCodeToUrlPath;
        }

        private List<VRSRecomItemsDTO> buildDTOFromFile(string sourceFile, Int32 loadId)
        {
            List<VRSRecomItemsDTO> rawRecords = new List<VRSRecomItemsDTO>();

            using (StreamReader sr = new StreamReader(sourceFile))
            {
                string line;
                Int32 recNo = 0;
                while ((line = sr.ReadLine()) != null)
                {
                    List<string> fields = line.Split('\t').ToList();
                    VRSRecomItemsDTO vRSRecomItemsDTO = new VRSRecomItemsDTO();
                    vRSRecomItemsDTO.LoadId = loadId;

                    vRSRecomItemsDTO.LibraryCode= fields[0];
                    vRSRecomItemsDTO.SortKey= fields[1];
                    vRSRecomItemsDTO.LegacySKU= fields[2];
                    vRSRecomItemsDTO.SKU = fields[2];

                    if (recNo != 0)
                    {
                        if (contextController.validSkuSet.Contains(base.removeAddDateAndTime(vRSRecomItemsDTO.SKU)))
                        {
                            rawRecords.Add(vRSRecomItemsDTO);
                        }
                        else
                        {
                            log.logMsg(LogController.LogClass.Debug, string.Format("RecomItemsFile filtered out because sku was not valid. sku:({0})", vRSRecomItemsDTO.SKU));
                        }
                    }
                    recNo++;
                }
                sr.Close();
            }
            return rawRecords;
        }

        private List<VRSRecomItemsDTO> buildChangedRecords(IDbConnection stagedConn, Int32 loadId, Int32 priorGoodId)
        {
            List<VRSRecomItemsDTO> rtnList = new List<VRSRecomItemsDTO>();

            string tableName = "VRSRecomItems ";
            string orderBy = "LibraryCode ";
            string headers = generateHeaderRecord(new VRSRecomItemsDTO(), ",") + " ";

            string sql = " SELECT " + headers;
            sql += " FROM " + tableName + " c ";
            sql += string.Format(" WHERE loadId = {0} ", loadId);
            sql += " EXCEPT ";
            sql += " SELECT " + headers;
            sql += " FROM " + tableName + " c2 ";
            sql += string.Format(" WHERE loadId = {0}", priorGoodId);
            sql += " Order by " + orderBy + " ";

            DataTable changedRecs = findChangedRecords(stagedConn, sql);

            foreach (DataRow dr in changedRecs.Rows)
            {
                VRSRecomItemsDTO dto = new VRSRecomItemsDTO();
                populateDTOFromDataRow(dr, dto);
                dto.LoadId = loadId;
                rtnList.Add(dto);
            }
            return rtnList;
        }

        private List<VRSRecomItemsDTO> buildDeletedRecords(IDbConnection stagedConn, Int32 loadId, Int32 priorGoodId)
        {
            List<VRSRecomItemsDTO> rtnList = new List<VRSRecomItemsDTO>();

            string tableName = "VRSRecomItems ";
            string orderBy = "LibraryCode ";
            string headers = "LibraryCode,  LegacySKU, SKU ";

            string sql = " SELECT " + headers;
            sql += " FROM " + tableName + " c ";
            sql += string.Format(" WHERE loadId = {0} ", priorGoodId );
            sql += " EXCEPT ";
            sql += " SELECT " + headers;
            sql += " FROM " + tableName + " c2 ";
            sql += string.Format(" WHERE loadId = {0}", loadId);
            sql += " Order by " + orderBy + " ";

            DataTable changedRecs = findChangedRecords(stagedConn, sql);

            foreach (DataRow dr in changedRecs.Rows)
            {
                VRSRecomItemsDTO dto = new VRSRecomItemsDTO();
                dto.LoadId = loadId;
                dto.LibraryCode = dr["LibraryCode"].ToString();
                dto.LegacySKU = dr["LegacySKU"].ToString();
                dto.SKU = dr["SKU"].ToString();
                rtnList.Add(dto);
            }
            return rtnList;
        }

    }
}
