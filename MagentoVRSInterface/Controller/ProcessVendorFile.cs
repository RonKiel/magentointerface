﻿using MagentoVRSInterface.DTO;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagentoVRSInterface.Controller
{
    public class ProcessVendorFile : FileProcessor
    {
        public override List<KeyValuePair<string, StringBuilder>> processFile(System.Data.IDbConnection stagedConn, string sourceFile)
        {
            List<VRSVendorDTO> rawRecsFromFile = buildDTOFromFile(sourceFile, contextController.loadId);
            return processFile(stagedConn, rawRecsFromFile, contextController.loadId, contextController.priorGoodLoad);
        }

        public List<KeyValuePair<string, StringBuilder>> processFile(System.Data.IDbConnection stagedConn, List<VRSVendorDTO> rawRecsList, int loadId, Int32 priorGoodId)
        {
            StringBuilder chgFileText = new StringBuilder();
            StringBuilder deleteFileText = new StringBuilder();
            List<KeyValuePair<string, StringBuilder>> rtnList = new List<KeyValuePair<string, StringBuilder>>();

            BulkInsert((SqlConnection)stagedConn, "VRSVendor", rawRecsList); //HACK sql server specific.

            buildWriteBufferForChanges(stagedConn, loadId, priorGoodId, chgFileText);
#if VRSFormat
            buildWriteBufferForDeletes(stagedConn, loadId, priorGoodId, deleteFileText);
#endif
            string DestDirectory = ConfigurationManager.AppSettings["FileBuildLocation"].ToString();

            string chgFile = DestDirectory + "tvendorfile" + loadId.ToString("D8") + ".tab";
            rtnList.Add(new KeyValuePair<string, StringBuilder>(chgFile, chgFileText));

#if VRSFormat
            string deleteFile = DestDirectory + "vendorDeletefile" + loadId.ToString("D8") + ".tab";
            rtnList.Add(new KeyValuePair<string, StringBuilder>(deleteFile, deleteFileText));
#endif
            return rtnList;
        }

        private void buildWriteBufferForDeletes(System.Data.IDbConnection stagedConn, int loadId, Int32 priorGoodId, StringBuilder deleteFileText)
        {
#if VRSFormat
            List<VRSVendorDTO> deleteRecords = buildDeletedRecords(stagedConn, loadId, priorGoodId);
            deleteFileText.Append("MfgCode\n");
            foreach (VRSVendorDTO dto in deleteRecords)
            {
                deleteFileText.Append(string.Format("{0}\n", dto.MfgCode));
            }
#else 
            List<VRSVendorDTO> deleteRecords = buildDeletedRecords(stagedConn, loadId, priorGoodId);
            foreach (VRSVendorDTO dto in deleteRecords)
            {
                string tRec = string.Format("EAO\tmanufacturer\t{0}\n", dto.CompanyName);
                deleteFileText.Append(tRec);
            }
#endif
        }

        private void buildWriteBufferForChanges(System.Data.IDbConnection stagedConn, int loadId, Int32 priorGoodId, StringBuilder chgFileText)
        {
#if VRSFormat
            string headers = generateHeaderRecord(new VRSVendorDTO(), "\t");
            chgFileText.Append(headers);

            List<VRSVendorDTO> changedRecords = buildChangedRecords(stagedConn, loadId, priorGoodId);
            foreach (VRSVendorDTO dto in changedRecords)
            {
                string tRec = base.generateTabRecord(dto);
                chgFileText.Append(tRec);
            }
#else
            List<VRSVendorDTO> changedRecords = buildChangedRecords(stagedConn, loadId, priorGoodId);
            foreach (VRSVendorDTO dto in changedRecords)
            {
                string tRec = string.Format("EAO\tmanufacturer\t{0}\n", dto.CompanyName);
                chgFileText.Append(tRec);
            }
#endif
        }

        private List<VRSVendorDTO> buildDTOFromFile(string sourceFile, Int32 loadId)
        {
            List<VRSVendorDTO> rawRecords = new List<VRSVendorDTO>();

            using (StreamReader sr = new StreamReader(sourceFile))
            {
                string line;
                Int32 recNo = 0;
                while ((line = sr.ReadLine()) != null)
                {
                    List<string> fields = line.Split('\t').ToList();
                    VRSVendorDTO vRSVendorDTO = new VRSVendorDTO();

                    vRSVendorDTO.LoadId = loadId;
                    vRSVendorDTO.MfgCode = fields[0];
                    vRSVendorDTO.CompanyName = fields[1];

                    if (recNo != 0)
                    {
                        rawRecords.Add(vRSVendorDTO);
                    }
                    recNo++;
                }
                sr.Close();
            }
            return rawRecords;
        }

        public List<VRSVendorDTO> buildChangedRecords(IDbConnection stagedConn, Int32 loadId, Int32 priorGoodId)
        {
            List<VRSVendorDTO> rtnList = new List<VRSVendorDTO>();

            string tableName = "VRSVendor ";
            string orderBy = " MfgCode ";
            string headers = generateHeaderRecord(new VRSVendorDTO(), ",") + " ";

            string sql = " SELECT " + headers;
            sql += " FROM " + tableName + " c ";
            sql += string.Format(" WHERE loadId = {0} ", loadId);
            sql += " EXCEPT ";
            sql += " SELECT " + headers;
            sql += " FROM " + tableName + " c2 ";
            sql += string.Format(" WHERE loadId = {0}", priorGoodId);
            sql += " Order by " + orderBy + " ";

            DataTable changedRecs = findChangedRecords(stagedConn, sql);

            foreach (DataRow dr in changedRecs.Rows)
            {
                VRSVendorDTO dto = new VRSVendorDTO();
                populateDTOFromDataRow(dr, dto);
                dto.LoadId = loadId;
                rtnList.Add(dto);
            }
            return rtnList;
        }

        public List<VRSVendorDTO> buildDeletedRecords( IDbConnection stagedConn, Int32 loadId, Int32 priorGoodId )
        {
            List<VRSVendorDTO> rtnList = new List<VRSVendorDTO>();

            string tableName = " VRSVendor ";
            string orderBy = " CompanyName ";
            string headers = " CompanyName ";

            string sql = " SELECT " + headers;
            sql += " FROM " + tableName + " c ";
            sql += string.Format(" WHERE loadId = {0} ", priorGoodId );
            sql += " EXCEPT ";
            sql += " SELECT " + headers;
            sql += " FROM " + tableName + " c2 ";
            sql += string.Format("WHERE loadId = {0}", loadId);
            sql += " Order by " + orderBy + " ";

            DataTable changedRecs = findChangedRecords(stagedConn, sql);

            foreach (DataRow dr in changedRecs.Rows)
            {
                VRSVendorDTO dto = new VRSVendorDTO();
                dto.LoadId = loadId;
                dto.CompanyName = dr["CompanyName"].ToString();
                rtnList.Add(dto);
            }
            return rtnList;
        }

    }
}
