﻿using MagentoVRSInterface.DTO;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagentoVRSInterface.Controller
{
    public class ProcessItemStatusFile : FileProcessor
    {
        Dictionary<string, string> vendorMap = null;
        List<string> mfgLogged = new List<string>();
        LogController log = LogController.Instance;

        public override List<KeyValuePair<string, StringBuilder>> processFile(System.Data.IDbConnection stagedConn, string sourceFile )
        {
            this.vendorMap = contextController.vendorMap;
            List<VRSItemStatusDTO> rawRecsFromFile = buildDTOFromFile(sourceFile, contextController.loadId);
            base.replaceItemSkuWithExtendedVersion(rawRecsFromFile, "SKU");
            base.reassignProductIdsInPlaceOfSkusForSimpleProducts(rawRecsFromFile, "SKU");
            rawRecsFromFile = filterOutProductsThatHadABadMfgr(rawRecsFromFile);

            return processFile(stagedConn, rawRecsFromFile, contextController.loadId, contextController.priorGoodLoad);
        }

        private List<VRSItemStatusDTO> filterOutProductsThatHadABadMfgr( List<VRSItemStatusDTO> srcList )
        {
            List<VRSItemStatusDTO> rtnList = new List<VRSItemStatusDTO>();
            foreach (VRSItemStatusDTO dto in srcList)
            {
                if ( contextController.badProductsBecauseOfMfgr.Contains(dto.SKU) )
                {
                    log.logMsg(LogController.LogClass.Debug, string.Format("ItemsStatus record filtered out for bad Product Mfgr: {0}", dto.SKU));
                }
                else
                {
                    rtnList.Add(dto);
                }
            }

            return rtnList;
        }

        public List<KeyValuePair<string, StringBuilder>> processFile(IDbConnection stagedConn, List<VRSItemStatusDTO> rawRecsList, Int32 loadId, Int32 priorGoodId)
        {
            StringBuilder chgFileText = new StringBuilder();
            List<KeyValuePair<string, StringBuilder>> rtnList = new List<KeyValuePair<string, StringBuilder>>();
            Dictionary<string, string> MfgCodeToCompanyName = buildMapFromMfgCodeToMfgName(stagedConn, loadId);

            BulkInsert((SqlConnection)stagedConn, "VRSItemStatus", rawRecsList); //HACK sql server specific.

            buildWriteBufferForChanges(stagedConn, loadId, priorGoodId, chgFileText, MfgCodeToCompanyName);
            buildWriteBufferForDeletes(stagedConn, loadId, priorGoodId, chgFileText, MfgCodeToCompanyName);
            string DestDirectory = ConfigurationManager.AppSettings["FileBuildLocation"].ToString();

            string chgFile = DestDirectory + "titemstatus" + loadId.ToString("D8") + ".tab";
            rtnList.Add(new KeyValuePair<string, StringBuilder>(chgFile, chgFileText));

            return rtnList;
        }

        private Dictionary<string, string> buildMapFromMfgCodeToMfgName(IDbConnection stagedConn, Int32 loadId)
        {
            Dictionary<string, string> MfgCodeToCompanyName = new Dictionary<string, string>();
            ProcessVendorFile processVendorFile = new ProcessVendorFile();
            List<VRSVendorDTO> vendorList = processVendorFile.buildChangedRecords(stagedConn, loadId, -1); //-1 will cause all of the records to be returned.
            foreach (VRSVendorDTO dto in vendorList)
            {
                MfgCodeToCompanyName.Add(dto.MfgCode.Trim(), dto.CompanyName.Trim());
            }
            return MfgCodeToCompanyName;
        }

        private void buildWriteBufferForDeletes(IDbConnection stagedConn, Int32 loadId, Int32 priorGoodId, StringBuilder sb, Dictionary<string, string> MfgCodeToCompanyName )
        {
            List<VRSItemStatusDTO> deleteRecords = buildDeletedRecords(stagedConn, loadId, priorGoodId);

            List<VRSItemStatusDTO> oldPriorRecords = buildChangedRecords(stagedConn, priorGoodId, -1);
            List<string> skusToDeactivate = new List<string>();
            foreach (VRSItemStatusDTO dto in deleteRecords)
            {
                skusToDeactivate.Add(dto.SKU.Trim());
            }
            List<VRSItemStatusDTO> recordsToDeactivate = oldPriorRecords.Where(x => skusToDeactivate.Contains(x.SKU.Trim())).ToList();

            foreach (VRSItemStatusDTO dto in recordsToDeactivate )
            {
                string tRec = "";

                tRec += dto.SKU + "\t";
                //tRec += companyName + "\t";  //TODO Drop this field if approved.
                tRec += dto.LegacySKU + "\t";
                tRec += "Disabled\t";
                tRec += dto.QtyOnHand + "\t";
                tRec += dto.newnoonhand + "\t";
                tRec += dto.VendorAvail + "\t";
                tRec += dto.EstArrival + "\t";
                tRec += dto.prepaidflag + "\t";
                tRec += dto.costvalue + "\t";
                tRec += dto.promoflag + "\t";
                tRec += dto.clearance + "\t";
                tRec += dto.itemstatus + "\t";
                tRec += dto.itemcartstatus + "\t";
                tRec += dto.FirstRecDate + "\t";
                tRec += dto.Launch + "\t";
                tRec += dto.ClearanceDate + "\t";
                tRec += dto.Preorder + "\t";
                tRec += dto.AddDateNa + "\t";
                tRec += dto.itemcoavail + "\t";
                tRec += dto.itemcounavail + "\t";
                tRec += dto.promoid + "\t";
                tRec += dto.Availability + "\n";

                sb.Append(tRec);
            }
        }

        private void buildWriteBufferForChanges(IDbConnection stagedConn, Int32 loadId, Int32 priorGoodId, StringBuilder chgFileText, Dictionary<string, string> MfgCodeToCompanyName )
        {
            List<VRSItemStatusDTO> changedRecords = buildChangedRecords(stagedConn, loadId, priorGoodId);
            List<VRSItemStatusDTO> itemsChangeBecauseOfattributes = buildListOfItemsChangedBecauseOfAttributeChanged(stagedConn, loadId, changedRecords, contextController.itemHaveRemovedItemAttributes, contextController.itemHaveNewItemAttributes);
            changedRecords.AddRange(itemsChangeBecauseOfattributes);

            AttributeCollection attributes = new AttributeCollection();
            attributes.assignValues(ConfigurationManager.AppSettings["ItemAttributes"].ToString());

            List<string> flds = new List<string>(new string[] { 
                "SKU",
                "LegacySKU","IsActive","QtyOnHand","newnoonhand","VendorAvail",
                "EstArrival","prepaidflag","costvalue","promoflag","clearance","itemstatus","itemcartstatus",
                "FirstRecDate","Launch","ClearanceDate","Preorder","AddDate-na","itemcoavail","itemcounavail",
                "promoid","Availability"
            });
            flds.AddRange(attributes.getMagentoValues());
            string headers = string.Join("\t", flds) + "\n";
            chgFileText.Append(headers);

            foreach (VRSItemStatusDTO dto in changedRecords)
            {
                string tRec = "";

                tRec += dto.SKU + "\t";
                tRec += dto.LegacySKU + "\t";
                tRec += ((dto.IsActive == "1") ? "Enabled" : "Disabled") + "\t";
                tRec += dto.QtyOnHand + "\t";
                tRec += dto.newnoonhand + "\t";
                tRec += dto.VendorAvail + "\t";
                tRec += dto.EstArrival + "\t";
                tRec += dto.prepaidflag + "\t";
                tRec += dto.costvalue + "\t";
                tRec += dto.promoflag + "\t";
                tRec += dto.clearance + "\t";
                tRec += dto.itemstatus + "\t";
                tRec += dto.itemcartstatus + "\t";
                tRec += dto.FirstRecDate + "\t";
                tRec += dto.Launch + "\t";
                tRec += dto.ClearanceDate + "\t";
                tRec += dto.Preorder + "\t";
                tRec += dto.AddDateNa + "\t";
                tRec += dto.itemcoavail + "\t";
                tRec += dto.itemcounavail + "\t";
                tRec += dto.promoid + "\t";
                tRec += dto.Availability + "\t";
                tRec += appendAttributeColumns(attributes.getBlueValues(), dto.SKU, contextController.itemAttributesAll) + "\n";
                chgFileText.Append(tRec);
            }
        }

        public string appendAttributeColumns(List<string> attributes, string sku, Dictionary<string, List<VRSItemAttribDTO>> itemStatusDict )
        {
            string tRec = "";
            List<VRSItemAttribDTO> vRSItemAttribDTOs = new List<VRSItemAttribDTO>();
            if (itemStatusDict.Keys.Contains(sku))
            {
                vRSItemAttribDTOs = itemStatusDict[sku];
            }
            bool firstInLoop = true;
            foreach (string attrib in attributes)
            {
                if (firstInLoop)
                {
                    firstInLoop = false;
                }
                else
                {
                    tRec += "\t";
                }
                VRSItemAttribDTO vRSItemAttribDTO = vRSItemAttribDTOs.Where(x => x.AttributeTypeCode.Trim().ToLower() == 
                                                                            attrib.ToLower().Trim()).FirstOrDefault();
                string curVal = (vRSItemAttribDTO == null) ? "" : vRSItemAttribDTO.AttributeValue;
                tRec += curVal;
            }

            return tRec;
        }
        public List<VRSItemStatusDTO> buildListOfItemsChangedBecauseOfAttributeChanged(IDbConnection stagedConn, Int32 loadId, List<VRSItemStatusDTO> changedRecords, HashSet<string> set1, HashSet<string> set2)
        {
            List<VRSItemStatusDTO> allRecords = buildChangedRecords(stagedConn, loadId, -1);
            return buildListOfItemsChangedBecauseOfAttributeChanged(allRecords, changedRecords, set1, set2);
        }
        public List<VRSItemStatusDTO> buildListOfItemsChangedBecauseOfAttributeChanged( List<VRSItemStatusDTO> allRecords, List<VRSItemStatusDTO> changedRecords, HashSet<string> set1, HashSet<string> set2)
        {
            List<VRSItemStatusDTO> rtnList = new List<VRSItemStatusDTO>();
            List<string> itemsAtribChange = set1.Union(set2).ToList();

            List<string> allItemStatuses = new List<string>();
            List<string> changedItemStatus = new List<string>();

            foreach (VRSItemStatusDTO dto in allRecords)
            {
                allItemStatuses.Add(dto.SKU.Trim());
            }
            foreach (VRSItemStatusDTO dto in changedRecords)
            {
                changedItemStatus.Add(dto.SKU.Trim());
            }

            List<string> itemStatusNeededBecauseOfAttribChange = itemsAtribChange.Except(changedItemStatus).Intersect(allItemStatuses).ToList();
            foreach (string sku in itemStatusNeededBecauseOfAttribChange)
            {
                VRSItemStatusDTO dto = allRecords.Where(x => x.SKU.Trim().Equals(sku.Trim())).First(); //Would throw if empty list, but should never be needed.
                rtnList.Add(dto);
            }

            List<string> itemStatusButNoCorrespondingItem = itemsAtribChange.Except(changedItemStatus).Except(allItemStatuses).ToList();
            foreach (string sku in itemStatusButNoCorrespondingItem) 
            {
                log.logMsg(LogController.LogClass.Debug, string.Format("Item attribute for non active item:({0})", sku));
            }

            return rtnList;
        }

        private string lookupMfgrName(Dictionary<string, string> MfgCodeToCompanyName, VRSItemStatusDTO dto)
        {
            string companyName = dto.MfgCode;
            if (MfgCodeToCompanyName.Keys.Contains(dto.MfgCode))
            {
                companyName = MfgCodeToCompanyName[dto.MfgCode];
            }
            else
            {
                if (vendorMap.Keys.Contains(dto.MfgCode))
                {
                    companyName = vendorMap[dto.MfgCode];
                    if (!mfgLogged.Contains(dto.MfgCode))
                    {
                        log.logMsg(LogController.LogClass.Debug, string.Format("MfgCode not found in tvendorfile, using blue data:({0})", dto.MfgCode));
                        mfgLogged.Add(dto.MfgCode);
                    }
                }
                else
                {
                    List<string> possibleKeys = MfgCodeToCompanyName.Keys.Where(x => x.Substring(0, 4).Equals(dto.MfgCode.Substring(0, 4))).ToList();
                    if (possibleKeys.Count > 0)
                    {
                        if (!mfgLogged.Contains(dto.MfgCode))
                        {
                            log.logMsg(LogController.LogClass.Debug, string.Format("MfgCode not found in tvendorfile, using best guess:({0})", dto.MfgCode));
                            mfgLogged.Add(dto.MfgCode);
                        }
                        possibleKeys.Sort();
                        companyName = MfgCodeToCompanyName[possibleKeys[0]]; //Use the first possible match.
                    }
                }
            }
            return companyName;
        }

        private List<VRSItemStatusDTO> buildDTOFromFile(string sourceFile, Int32 loadId)
        {
            List<VRSItemStatusDTO> rawRecords = new List<VRSItemStatusDTO>();

            using (StreamReader sr = new StreamReader(sourceFile))
            {
                string line;
                Int32 recNo = 0;
                while ((line = sr.ReadLine()) != null)
                {
                    List<string> fields = line.Split('\t').ToList();
                    VRSItemStatusDTO vRSItemStatusDTO = new VRSItemStatusDTO();
                    vRSItemStatusDTO.LoadId = loadId;

                    vRSItemStatusDTO.MfgCode = fields[0];
                    vRSItemStatusDTO.CommonPartNum = fields[1];
                    vRSItemStatusDTO.SKU = fields[2];
                    vRSItemStatusDTO.LegacySKU = fields[2];
                    vRSItemStatusDTO.IsActive = fields[3];
                    vRSItemStatusDTO.QtyOnHand = fields[4];
                    vRSItemStatusDTO.newnoonhand = fields[5];
                    vRSItemStatusDTO.VendorAvail = fields[6];
                    vRSItemStatusDTO.EstArrival = fields[7];
                    vRSItemStatusDTO.prepaidflag = fields[8];
                    vRSItemStatusDTO.costvalue = fields[9];
                    vRSItemStatusDTO.promoflag = fields[10];
                    vRSItemStatusDTO.clearance = fields[11];
                    vRSItemStatusDTO.itemstatus = fields[12];
                    vRSItemStatusDTO.itemcartstatus = fields[13];
                    vRSItemStatusDTO.FirstRecDate = fields[14];
                    vRSItemStatusDTO.Launch = fields[15];
                    vRSItemStatusDTO.ClearanceDate = fields[16];
                    vRSItemStatusDTO.Preorder = fields[17];
                    vRSItemStatusDTO.AddDateNa = fields[18];
                    vRSItemStatusDTO.itemcoavail = fields[19];
                    vRSItemStatusDTO.itemcounavail = fields[20];
                    vRSItemStatusDTO.promoid = fields[21];
                    vRSItemStatusDTO.promotitle = fields[22];
                    vRSItemStatusDTO.Availability = fields[23];

                    if (recNo != 0)
                    {
                        if (contextController.validSkuSet.Contains(base.removeAddDateAndTime(vRSItemStatusDTO.LegacySKU)))
                        {
                            rawRecords.Add(vRSItemStatusDTO);
                        }
                        else
                        {
                            log.logMsg(LogController.LogClass.Debug, string.Format("ItemStatusFile filtered out sku:({0}) because the item was filtered out because of a bad mfgr code.", vRSItemStatusDTO.LegacySKU));
                        }
                    }
                    recNo++;
                }
                sr.Close();
            }
            return rawRecords;
        }

        private List<VRSItemStatusDTO> buildChangedRecords(IDbConnection stagedConn, Int32 loadId, Int32 priorGoodId)
        {
            List<VRSItemStatusDTO> rtnList = new List<VRSItemStatusDTO>();

            string tableName = "VRSItemStatus";
            string orderBy = "MfgCode, CommonPartNum, SKU ";
            string headers = generateHeaderRecord(new VRSItemStatusDTO(), ",") + " ";

            string sql = " SELECT " + headers;
            sql += " FROM " + tableName + " c ";
            sql += string.Format(" WHERE loadId = {0} ", loadId);
            sql += " EXCEPT ";
            sql += " SELECT " + headers;
            sql += " FROM " + tableName + " c2 ";
            sql += string.Format(" WHERE loadId = {0}", priorGoodId);
            sql += " Order by " + orderBy + " ";

            DataTable changedRecs = findChangedRecords(stagedConn, sql);

            foreach (DataRow dr in changedRecs.Rows)
            {
                VRSItemStatusDTO dto = new VRSItemStatusDTO();
                populateDTOFromDataRow(dr, dto);
                dto.LoadId = loadId;
                rtnList.Add(dto);
            }
            return rtnList;
        }

        private List<VRSItemStatusDTO> buildDeletedRecords(IDbConnection stagedConn, Int32 loadId, Int32 priorGoodId)
        {
            List<VRSItemStatusDTO> rtnList = new List<VRSItemStatusDTO>();

            string tableName = " VRSItemStatus ";
            string orderBy = " SKU ";
            string headers = " SKU ";

            string sql = " SELECT " + headers;
            sql += " FROM " + tableName + " c ";
            sql += string.Format(" WHERE loadId = {0} ", priorGoodId);
            sql += " EXCEPT ";
            sql += " SELECT " + headers;
            sql += " FROM " + tableName + " c2 ";
            sql += string.Format(" WHERE loadId = {0}", loadId);
            sql += " Order by " + orderBy + " ";

            DataTable changedRecs = findChangedRecords(stagedConn, sql);

            foreach (DataRow dr in changedRecs.Rows)
            {
                VRSItemStatusDTO dto = new VRSItemStatusDTO();
                dto.LoadId = loadId;
                dto.SKU = dr["SKU"].ToString();
                rtnList.Add(dto);
            }
            return rtnList;
        }
    }

}
