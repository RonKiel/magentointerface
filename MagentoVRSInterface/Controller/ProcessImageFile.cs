﻿using MagentoVRSInterface.DTO;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagentoVRSInterface.Controller
{
    /*
     * THIS IS NOT CURRENTLY USED!!!!
     */
    public class ProcessImageFileOBSOLETE : FileProcessor
    {
        public override List<KeyValuePair<string, StringBuilder>> processFile(System.Data.IDbConnection stagedConn, string sourceFile)
        {
            List<VRSImageDTO> rawRecsFromFile = buildDTOFromFile(sourceFile, contextController.loadId);
            return processFile(stagedConn, rawRecsFromFile, contextController.loadId, contextController.priorGoodLoad);
        }

        public List<KeyValuePair<string, StringBuilder>> processFile(System.Data.IDbConnection stagedConn, List<VRSImageDTO> rawRecsList, int loadId, Int32 priorGoodId)
        {
            string DestDirectory = ConfigurationManager.AppSettings["FileBuildLocation"].ToString();

            List<KeyValuePair<string, StringBuilder>> rtnList = new List<KeyValuePair<string, StringBuilder>>();
            BulkInsert((SqlConnection)stagedConn, " VRSImage ", rawRecsList);
            
            //Changes
            List<VRSImageDTO> changedRecs = buildChangedRecords(stagedConn, " VRSImage ", contextController.loadId, contextController.priorGoodLoad);
            StringBuilder sb = buildChangeBuffer(changedRecs);
            string chgFile = DestDirectory + "imagefile" + contextController.loadId.ToString("D8") + ".tab";
            rtnList.Add(new KeyValuePair<string, StringBuilder>(chgFile, sb));


            //Deletes
            List<VRSImageDTO> recsToDelete = buildChangedRecords(stagedConn, " VRSImage ", contextController.priorGoodLoad, contextController.loadId);
            StringBuilder sbDelete = buildChangeBuffer(changedRecs);
            string chgDeleteFile = DestDirectory + "deleteimagefile" + contextController.loadId.ToString("D8") + ".tab";
            rtnList.Add(new KeyValuePair<string, StringBuilder>(chgDeleteFile, sbDelete));

            return rtnList;
        }


        private StringBuilder buildChangeBuffer( List<VRSImageDTO> dtoRecs)
        {
            StringBuilder sb = new StringBuilder();

            List<string> flds = new List<string>(new string[] { "ImageType", "ProductKey", "SKU", "LegacySKU", "Seq", "URL", "Preferred" });
            string headers = string.Join("\t", flds) + "\n";
            sb.Append(headers);

            foreach (VRSImageDTO dto in dtoRecs)
            {
                string tRec = "";
                //tRec += dto.ImageType + "\t";
                //tRec += dto.ProductKey + "\t";
                //tRec += dto.SKU + "\t";
                //tRec += dto.LegacySKU + "\t";
                //tRec += dto.LegacySKU + "\t";
                //tRec += string.Format("{0}",dto.Seq) + "\t";
                //tRec += dto.URL + "\t";
                //tRec += dto.Preferred + "\n";

                if (dto.ImageType == "PM")
                {

                }
                else if (dto.ImageType == "IT")
                {

                }

                sb.Append(tRec);
            }
            return sb;
        }



        private List<VRSImageDTO> buildChangedRecords(IDbConnection stagedConn, string tableName, Int32 loadId, Int32 priorGoodId)
        {
            List<VRSImageDTO> rtnList = new List<VRSImageDTO>();
            string headers = generateHeaderRecord(new VRSImageDTO(), ",") + " ";

            string orderBy = " sku ";

            string sql = " SELECT " + headers;
            sql += " FROM " + tableName + " c ";
            sql += string.Format(" WHERE loadId = {0} ", loadId);
            sql += " EXCEPT ";
            sql += " SELECT " + headers;
            sql += " FROM " + tableName + " c2 ";
            sql += string.Format(" WHERE loadId = {0}", priorGoodId);
            sql += " Order by " + orderBy + " ";

            DataTable changedRecs = findChangedRecords(stagedConn, sql);

            foreach (DataRow row in changedRecs.Rows)
            {
                VRSImageDTO vRSImageDTO = new VRSImageDTO();
                vRSImageDTO.LoadId = contextController.loadId;
                vRSImageDTO.ImageType = (row.Table.Columns.Contains("ImageType")) ? row["ImageType"].ToString() : "";
                vRSImageDTO.ProductKey = (row.Table.Columns.Contains("ProductKey")) ? row["ProductKey"].ToString() : "";
                vRSImageDTO.SKU = (row.Table.Columns.Contains("SKU")) ? row["SKU"].ToString() : "";
                vRSImageDTO.LegacySKU = (row.Table.Columns.Contains("LegacySKU")) ? row["LegacySKU"].ToString() : "";
                vRSImageDTO.URL = (row.Table.Columns.Contains("URL")) ? row["URL"].ToString() : "";
                vRSImageDTO.Preferred = (row.Table.Columns.Contains("Preferred")) ? row["Preferred"].ToString() : "";

                if (row.Table.Columns.Contains("Seq"))
                {
                    Int32 tmpInt = -1;
                    if( Int32.TryParse(row["Seq"].ToString(), out tmpInt) )
                    {
                        vRSImageDTO.Seq = tmpInt;
                    }
                }
                rtnList.Add(vRSImageDTO);
            }
            return rtnList;
        }

        public List<VRSImageDTO> buildDTOFromFile(string sourceFile, Int32 loadId)
        {
            List<VRSImageDTO> rawRecords = new List<VRSImageDTO>();

            using (StreamReader sr = new StreamReader(sourceFile))
            {
                string line;
                Int32 recNo = 0;
                while ((line = sr.ReadLine()) != null)
                {
                    List<string> fields = line.Split('\t').ToList();
                    VRSImageDTO vRSImageDTO = new VRSImageDTO();

                    vRSImageDTO.LoadId = loadId;

                    vRSImageDTO.ImageType = fields[0];
                    vRSImageDTO.ProductKey = fields[1];
                    vRSImageDTO.SKU = fields[2];
                    vRSImageDTO.LegacySKU = fields[3];

                    Int32 tmpInt = -1;
                    if( Int32.TryParse(fields[4].ToString(), out tmpInt) ) {
                        vRSImageDTO.Seq = tmpInt;
                    }
                    else
                    {
                        vRSImageDTO.Seq = null;
                    }
                    vRSImageDTO.URL = fields[5];
                    vRSImageDTO.Preferred = fields[6];

                    if (recNo != 0)
                    {
                        rawRecords.Add(vRSImageDTO);
                    }
                    recNo++;
                }
                sr.Close();
            }
            return rawRecords;
        }
    }
}
