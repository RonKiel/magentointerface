﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagentoVRSInterface.Controller
{
    public class AttributeCollection
    {
        List<AttributeName> attrNames = new List<AttributeName>();

        public void assignValues(string src)
        {
            List<string> attributesPairs = src.Split(',').ToList();

            foreach (string attrPair in attributesPairs)
            {
                List<string> fldParts = attrPair.Split(':').ToList();
                AttributeName attrName = new AttributeName() { attributeInBlue = fldParts[0], attributeInMagento = fldParts[1] };
                attrNames.Add(attrName);
            }
        }

        public List<string> getMagentoValues()
        {
            List<string> rtnList = new List<string>();
            foreach (AttributeName an in attrNames)
            {
                rtnList.Add(an.attributeInMagento);
            }
            return rtnList;
        }

        public List<string> getBlueValues()
        {
            List<string> rtnList = new List<string>();
            foreach (AttributeName an in attrNames)
            {
                rtnList.Add(an.attributeInBlue);
            }
            return rtnList;
        }

        public string getMagentoFromBlue(string src)
        {
            List<string> rtnList = new List<string>();
            foreach (AttributeName an in attrNames)
            {
                if (src.Trim().ToLower() == an.attributeInBlue.Trim().ToLower())
                {
                    return an.attributeInMagento;
                }
            }
            return null;
        }

        public string getBlueFromMagento(string src)
        {
            List<string> rtnList = new List<string>();
            foreach (AttributeName an in attrNames)
            {
                if (src.Trim().ToLower() == an.attributeInMagento.Trim().ToLower())
                {
                    return an.attributeInBlue;
                }
            }
            return null;
        }

    }
}
