﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagentoVRSInterface.Controller
{
    interface IDeferedWriteOfVRSData
    {
        void buildDataFileToWrite(IDbConnection stagedConn, int loadId, int priorGoodId, List<KeyValuePair<string, StringBuilder>> rtnList);
    }
}
