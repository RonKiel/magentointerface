﻿using MagentoVRSInterface.DTO;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagentoVRSInterface.Controller
{

    public class ProcessHourlyCustomerFile : ProcessCustomerFile
    {
        LogController log = LogController.Instance;

        public List<KeyValuePair<string, StringBuilder>>  processFile(IDbConnection stagedConn, string fileName, DateTime fileCreatedOn ) 
        {
            StringBuilder chgFileText = new StringBuilder();
            List<KeyValuePair<string, StringBuilder>> filesToWrite = new List<KeyValuePair<string, StringBuilder>>();

            if( !hasFileBeenProcessed(stagedConn, fileCreatedOn) ) 
            {
                Int32 fileID = insertHourlyFileInfo(stagedConn, fileCreatedOn );
                log.logMsg(LogController.LogClass.Debug, "Processing hourly customer file.");

                List<VRSCustomerDTO> rawCatList = base.buildDTOFromFile(fileName, fileID);

                string headers = base.buildHeaderRecord();

                chgFileText.Append(headers);
                foreach (VRSCustomerDTO dto in rawCatList)
                {
                    string rec = base.formatRecordForExport(dto);
                    chgFileText.Append(rec);
                }

                string DestDirectory = ConfigurationManager.AppSettings["FileBuildLocation"].ToString();
                string chgFile = DestDirectory + "customerUpdatefile" + fileID.ToString("D8") + ".tab";
                filesToWrite.Add(new KeyValuePair<string, StringBuilder>(chgFile, chgFileText));
            }
            return filesToWrite;
        }

        private Boolean hasFileBeenProcessed(IDbConnection stagedConn, DateTime fileCreatedOn)
        {
            string sql = "SELECT COUNT(*) FROM NewCUSTFileProcessed WHERE CreatedTime = @createdTime";
            IDbCommand cmd = stagedConn.CreateCommand();
            cmd.CommandText = sql;

            IDbDataParameter parm = cmd.CreateParameter();
            parm.ParameterName = "@createdTime";
            parm.Value = fileCreatedOn;
            cmd.Parameters.Add(parm);

            Int32 countOfFilesProcessed = Int32.Parse(cmd.ExecuteScalar().ToString());
            Boolean rtnValue = (countOfFilesProcessed > 0) ? true : false;

            return rtnValue;
        }

        private Int32 insertHourlyFileInfo(IDbConnection stagedConn, DateTime fileCreatedOn)
        {
            string sql = "INSERT INTO NewCUSTFileProcessed(CreatedTime) VALUES(@createdTime);SELECT @@IDENTITY";
            IDbCommand cmd = stagedConn.CreateCommand();
            cmd.CommandText = sql;

            IDbDataParameter parm = cmd.CreateParameter();
            parm.ParameterName = "@createdTime";
            parm.Value = fileCreatedOn;
            cmd.Parameters.Add(parm);

            Int32 processId = Int32.Parse(cmd.ExecuteScalar().ToString());
            return processId;
        }

    }

}
