﻿using MagentoInterface.Controller;
using MagentoVRSInterface.DAO;
using MagentoVRSInterface.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagentoVRSInterface.Controller
{
    public class UtilPopulateScratchDB : BaseController
    {
        public Int32 buildInvenScratchPad(IDbConnection conn, IDbConnection scratchPad)
        {
            Int32 theCount = 0;
            InvenDAO invenDAO = new InvenDAO();

            List<string> whereClauses = new List<string>() { "SKU like '0%'", 
                "SKU like '0%'",
                "SKU like '1%'",
                "SKU like '2%'",
                "SKU like '3%'",
                "SKU like '4%'",
                "SKU like '5%'",
                "SKU like '6%'",
                "SKU like '7%'",
                "SKU like '8%'",
                "SKU like '9%'"
            };

            foreach (string currWhereClause in whereClauses)
            {
                List<InvenDTO> dtoList = invenDAO.selectRecordsRtnList(conn, " " + currWhereClause + " ");
                foreach (InvenDTO dto in dtoList)
                {
                    invenDAO.insert(scratchPad, dto);
                }
                theCount += dtoList.Count;
            }


            return theCount;
        }

        public Int32 buildU8ScratchPad(IDbConnection conn, IDbConnection scratchPad)
        {
            Int32 theCount = 0;
            U8DAO u8DAO = new U8DAO();

            List<string> whereClauses = new List<string>() { "SKU like '0%'", 
                "SKU like '0%'",
                "SKU like '1%'",
                "SKU like '2%'",
                "SKU like '3%'",
                "SKU like '4%'",
                "SKU like '5%'",
                "SKU like '6%'",
                "SKU like '7%'",
                "SKU like '8%'",
                "SKU like '9%'"
            };

            foreach (string currWhereClause in whereClauses)
            {
                List<U8DTO> dtoList = u8DAO.selectRecordsRtnList(conn, " " + currWhereClause + " ");
                foreach (U8DTO dto in dtoList)
                {
                    u8DAO.insert(scratchPad, dto);
                }
                theCount += dtoList.Count;
            }


            return theCount;
        }

        public Int32 buildVendorScratchPad(IDbConnection conn, IDbConnection scratchPad)
        {
            Int32 theCount = 0;
            VendorDAO vendorDAO = new VendorDAO();

            List<VendorDTO> dtoList = vendorDAO.selectRecordsRtnList(conn, "");
            foreach (VendorDTO dto in dtoList)
            {
                vendorDAO.insert(scratchPad, dto);
            }
            theCount += dtoList.Count;

            return theCount;
        }

        public Int32 buildATCDDScratchPad(IDbConnection conn, IDbConnection scratchPad)
        {
            Int32 theCount = 0;
            AtcddDAO atcddDAO = new AtcddDAO();

            List<AtcddDTO> dtoList = atcddDAO.selectRecordsRtnList(conn, "");
            foreach (AtcddDTO dto in dtoList)
            {
                atcddDAO.insert(scratchPad, dto);
            }
            theCount += dtoList.Count;

            return theCount;
        }
    

    
    }

}
