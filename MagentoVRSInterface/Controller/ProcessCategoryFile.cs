﻿using MagentoInterface.DTO;
using MagentoVRSInterface.DAO;
using MagentoVRSInterface.DTO;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagentoVRSInterface.Controller
{
    public class ProcessCategoryFile : FileProcessor
    {
        LogController log = LogController.Instance;
        Dictionary<string, string> codeToAttributes = null;
        Dictionary<string, List<string>> codeToList = null;
        List<string> codesToListAdditionalCatCodes = new List<string>(new string[] { "", "1", "2", "3" }); //This is related to 

        public override List<KeyValuePair<string, StringBuilder>> processFile(IDbConnection stagedConn, string sourceFile)
        {
            buildCodeToAttributesDict();
            List<VRSCatDTO> rawCatList = buildDTOFromFile(sourceFile, contextController.loadId);
            logConflicts(rawCatList); /*We needed this because the duplicate key indicator didn't help enough to find the errors*/
            return processFile(stagedConn, rawCatList, contextController.loadId, contextController.priorGoodLoad);
        }
        
        public List<KeyValuePair<string, StringBuilder>> processFile(IDbConnection stagedConn, List<VRSCatDTO> rawCatList, Int32 loadId, Int32 priorGoodId)
        {
            StringBuilder chgFileText = new StringBuilder();
            List<KeyValuePair<string, StringBuilder>> rtnList = new List<KeyValuePair<string, StringBuilder>>();

            List<VRSCatDTO> goodVRSCatRecs = populatePositionNumber(removeAllBadReferences(rawCatList));

            BulkInsert((SqlConnection)stagedConn, "VRSCategory", goodVRSCatRecs); //HACK sql server specific.
            loadAdditionalBaseCategory(stagedConn, "New Arrivals", "100000", "1", loadId);
            loadAdditionalBaseCategory(stagedConn, "Preorder", "200000", "2", loadId);
            loadAdditionalBaseCategory(stagedConn, "Coming Soon", "300000", "3", loadId);

            buildWriteBufferForChanges(stagedConn, loadId, priorGoodId, chgFileText);
            buildWriteBufferForDeletes(stagedConn, loadId, priorGoodId, contextController.catAndBasketDeleteBuffer);

            string DestDirectory = ConfigurationManager.AppSettings["FileBuildLocation"].ToString();
            string chgFile = DestDirectory + "categoryfile" + loadId.ToString("D8") + ".tab";
            rtnList.Add(new KeyValuePair<string, StringBuilder>(chgFile, chgFileText));

            return rtnList;
        }

        private void buildCodeToAttributesDict()
        {
            AttributeCollection attributes = new AttributeCollection();
            attributes.assignValues(ConfigurationManager.AppSettings["ItemAttributes"].ToString());
            
            string catAttributeFile = ConfigurationManager.AppSettings["CategoryAttributeFile"].ToString();

            codeToAttributes = new Dictionary<string, string>();
            codeToList = new Dictionary<string, List<string>>();

            List<string> attributeList = new List<string>();
            if (File.Exists(catAttributeFile))
            {
                attributeList.AddRange(File.ReadAllLines(catAttributeFile).ToList().OrderBy(x => x));
            }

            foreach (string line in attributeList)
            {
                List<string> rec = line.Split('\t').ToList();
                string attributeCode = rec[0];
                string magentoCode = "";
                string categoryCode = rec[1];
                string visable = rec[1];

                if (!string.IsNullOrWhiteSpace(attributes.getMagentoFromBlue(attributeCode)))
                {
                    magentoCode = attributes.getMagentoFromBlue(attributeCode);
                    string delimeterCharater = ",";
                    if (!codeToAttributes.ContainsKey(categoryCode))
                    {
                        foreach (string codeId in codesToListAdditionalCatCodes)
                        {
                            codeToAttributes.Add(codeId + categoryCode, "");
                            codeToList.Add(codeId + categoryCode, new List<string>());
                        }
                        delimeterCharater = "";
                    }

                    foreach (string codeId in codesToListAdditionalCatCodes)
                    {
                        codeToAttributes[codeId + categoryCode] += delimeterCharater + magentoCode;
                        codeToList[codeId + categoryCode].Add(magentoCode);
                    }
                }
            }
        }

        //NOTE: This assumes the category import has already run!!!!  That's why that file is processed early
        // in Program.cs!!!!
        public Dictionary<string, string> getCategoryCodeToUrlPathDict(IDbConnection stagedConn, Int32 loadId) 
        {
            Dictionary<string, string> rtnDict = new Dictionary<string, string>();
            List<VRSCatDTO> goodVRSCatRecs = putInReferenceOrder( buildChangedRecords(stagedConn, loadId, -1) );
            foreach (VRSCatDTO dto in goodVRSCatRecs)
            {
                string currUrlKey = urlKeyFormatting(dto.Title.ToLower().Trim());
                string urlPath = buildURLPath(goodVRSCatRecs, dto, currUrlKey);
                // ETL-183
                rtnDict.Add(dto.CategoryID.Trim(), urlPath);
            }
            return rtnDict;
        }
        public Dictionary<string, string> getCategoryCodeToTitle(IDbConnection stagedConn, Int32 loadId)
        {
            Dictionary<string, string> rtnDict = new Dictionary<string, string>();
            List<VRSCatDTO> goodVRSCatRecs = putInReferenceOrder(buildChangedRecords(stagedConn, loadId, -1));
            foreach (VRSCatDTO dto in goodVRSCatRecs)
            {
                rtnDict.Add(dto.CategoryID.Trim(), dto.Title);
            }
            return rtnDict;
        }

        private void buildWriteBufferForDeletes(IDbConnection stagedConn, Int32 loadId, Int32 priorGoodId, StringBuilder deleteFileText)
        {
            List<string> urlFilteredOut = new List<string>(new string[] { @"new-arrivals/clearance", @"preorder/clearance", @"coming-soon/clearance" });

            List<VRSCatDTO> goodVRSCatRecs = getCurrentRecords(stagedConn, loadId);
            List<VRSCatDTO> deleteRecords = buildDeletedRecords(stagedConn, loadId, priorGoodId);
            foreach (VRSCatDTO dto in deleteRecords)
            {
                string currUrlKey = urlKeyFormatting(dto.Title.ToLower().Trim());
                string urlPath = buildURLPath(goodVRSCatRecs, dto, currUrlKey);

                bool filteredRec = isFilteredRecord(urlFilteredOut, urlPath);

                if (!filteredRec)
                {
                    deleteFileText.Append(string.Format("-CC\t{0}\n", urlPath));
                }
            }
        }

        private void buildWriteBufferForChanges(IDbConnection stagedConn, Int32 loadId, Int32 priorGoodId, StringBuilder chgFileText)
        {
            List<VRSCatDTO> goodVRSCatRecs = getCurrentRecords(stagedConn, loadId);
            List<string> urlFilteredOut = new List<string>(new string[] { @"new-arrivals/clearance", @"preorder/clearance", @"coming-soon/clearance" });

            AttributeCollection attributeCollection = new AttributeCollection();
            attributeCollection.assignValues(ConfigurationManager.AppSettings["ItemAttributes"].ToString());

            List<string> flds = new List<string>(new string[] { "Title", "url_key", "url_path", "Visible", "Description", "CategoryID"});
            flds.AddRange(attributeCollection.getMagentoValues());
            flds.Add("Position");
            string headersChanges = string.Join("\t", flds) + "\n";

            Dictionary<string, string> validKeyPairs = findBestUrlPathToCategoryId(stagedConn, loadId);
            List<VRSCatDTO> changedRecs = buildChangedRecords(stagedConn, loadId, priorGoodId);
            List<VRSCatDTO> changedRecordsInOrder = putInReferenceOrder(changedRecs);

            chgFileText.Append(headersChanges);
            foreach (VRSCatDTO dto in changedRecordsInOrder)
            {
                string oneLine = "";
                string currUrlKey = urlKeyFormatting(dto.Title.ToLower().Trim() );
                string urlPath = buildURLPath(goodVRSCatRecs, dto, currUrlKey);
                bool filteredRec = isFilteredRecord(urlFilteredOut, urlPath);

                if (!filteredRec)
                {
                    if (validKeyPairs.Keys.Contains(dto.CategoryID) && urlPath == validKeyPairs[dto.CategoryID])
                    {
                        string attributesValues = appendAttributeColumns(attributeCollection.getMagentoValues(), dto.CategoryID);
                        oneLine = string.Format("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\n", dto.Title, currUrlKey, urlPath, "Yes", dto.Description, dto.CategoryID, attributesValues, dto.Position);
                        chgFileText.Append(oneLine);
                    }
                    else
                    {
                        log.logMsg(LogController.LogClass.Warning, string.Format("Record ignored because code:{0} was not known,  urlPath of:{1}", dto.CategoryID, urlPath));
                    }
                }
            }
        }

        private static bool isFilteredRecord(List<string> filteredOutUrls, string urlPath)
        {
            bool filteredRec = false;
            foreach (string filter in filteredOutUrls)
            {
                if (urlPath.Contains(filter))
                {
                    filteredRec = true;
                }
            }
            return filteredRec;
        }

        public string appendAttributeColumns(List<string> attributes, string categoryId )
        {
            string tRec = "";
            List<VRSItemAttribDTO> vRSItemAttribDTOs = new List<VRSItemAttribDTO>();
            List<string> attibUsedByCategory = new List<string>();
            if (codeToList.Keys.Contains(categoryId))
            {
                attibUsedByCategory = codeToList[categoryId];
            }

            bool firstInLoop = true;
            foreach (string attrib in attributes)
            {
                if (firstInLoop)
                {
                    firstInLoop = false;
                }
                else
                {
                    tRec += "\t";
                }
                tRec += (attibUsedByCategory.Contains(attrib)) ? base.intToStringBool(1) : base.intToStringBool(0);
            }
            return tRec;
        }

        public Dictionary<string, string> findBestUrlPathToCategoryId(IDbConnection stagedConn, Int32 loadId)
        {
            Dictionary<string, string> tempDict = getCategoryCodeToUrlPathDict(stagedConn, loadId); //This loads the complete set, not just the changes.
            return findBestUrlPathToCategoryId(tempDict, true);
        }

        public Dictionary<string, string> findBestUrlPathToCategoryId( Dictionary<string, string> tempDict, Boolean logErrors )
        {
            Dictionary<string, string> rtnDict = new Dictionary<string, string>();
            List<string> duplicateUrlPaths = new List<string>();

            foreach (string currUrlPath in tempDict.Values)
            {
                List<string> matches = tempDict.Values.Where(x => x.Trim().Equals(currUrlPath.Trim())).ToList();
                if (matches.Count > 1 && !duplicateUrlPaths.Contains(currUrlPath))
                {
                    duplicateUrlPaths.Add(currUrlPath);
                }
            }

            //Put the non-conflicting urls back intot he dictionary.
            foreach (string key in tempDict.Keys)
            {
                if (!duplicateUrlPaths.Contains(tempDict[key]))
                {
                    rtnDict.Add(key, tempDict[key]);
                }
            }

            foreach (string dups in duplicateUrlPaths)
            {
                List<KeyValuePair<string,string>> keysPairs = tempDict.Where(x => x.Value == dups).ToList();

                //Find the lowest keyId for a given urlPath.
                KeyValuePair<string, string> myBest = keysPairs.First();
                foreach (KeyValuePair<string, string> kp in keysPairs)
                {
                    Int32 currBestInt = Int32.MaxValue;
                    Int32 myBestInt = Int32.MaxValue;
                    Int32.TryParse(myBest.Key, out myBestInt);
                    Int32.TryParse(kp.Key, out currBestInt);

                    if (currBestInt < myBestInt)
                    {
                        myBest = kp;
                    }
                }
                rtnDict.Add(myBest.Key, myBest.Value);
                if (logErrors)
                {
                    log.logMsg(LogController.LogClass.Warning, string.Format("Selected Duplicate urlpath found Key({0}), Value({1})", myBest.Key, myBest.Value));
                    foreach (KeyValuePair<string, string> kp in keysPairs)
                    {
                        log.logMsg(LogController.LogClass.Warning, string.Format("Possible values where Key({0}), Value({1})", kp.Key, kp.Value));
                    }
                }
            }

            return rtnDict;
        }

        private string buildURLPath(List<VRSCatDTO> goodVRSCatRecs, VRSCatDTO dto, string currUrlKey)
        {
            List<string> urlParts = new List<string>();
            urlParts.Add(currUrlKey);
            string parentID = dto.ParentID;
            while (!string.IsNullOrWhiteSpace(parentID))
            {
                VRSCatDTO parentRec = goodVRSCatRecs.Where(x => x.CategoryID.Equals(parentID)).FirstOrDefault();
                if (parentRec != null)
                {
                    urlParts.Add(base.urlKeyFormatting(parentRec.Title.ToLower().Trim()));
                    parentID = parentRec.ParentID;
                }
                else
                {
                    parentID = null;
                }
            }
            urlParts.Reverse();
            string urlPath = string.Join(@"/", urlParts.ToArray());
            return urlPath;
        }

        private List<VRSCatDTO> buildDTOFromFile(string sourceFile, Int32 loadId)
        {
            List<VRSCatDTO> rawCatList = new List<VRSCatDTO>();

            using (StreamReader sr = new StreamReader(sourceFile))
            {
                string line;
                Int32 recNo = 0;
                while ((line = sr.ReadLine()) != null)
                {
                    List<string> fields = line.Split('\t').ToList();
                    VRSCatDTO vRSCatDTO = new VRSCatDTO();
                    vRSCatDTO.LoadId = loadId;
                    try
                    {
                        vRSCatDTO.CategoryID = fields[0];
                        vRSCatDTO.ParentID = fields[1];
                        vRSCatDTO.Title = fields[2];
                        vRSCatDTO.Description = fields[3];

                        if (vRSCatDTO.CategoryID != "99000")
                        {
                            if (codeToAttributes.Keys.Contains(vRSCatDTO.CategoryID))
                            {
                                vRSCatDTO.Attributes = codeToAttributes[vRSCatDTO.CategoryID];
                            }

                            if (recNo != 0)
                            {
                                rawCatList.Add(vRSCatDTO);
                            }
                        }
                    }
                    catch (Exception ex1)
                    {
                        string errMsg = string.Format("Exception thrown for line:({0})", line);
                        log.logMsg(LogController.LogClass.Error, ex1.StackTrace, errMsg);
                    }
                    recNo++;
                }
                sr.Close();
            }
            return rawCatList;
        }

        private List<VRSCatDTO> buildChangedRecords(IDbConnection stagedConn, Int32 loadId, Int32 priorGoodId)
        {
            List<VRSCatDTO> rtnList = new List<VRSCatDTO>();
            string sql = " SELECT [CategoryID],[ParentID],[Title],[Description], ISNULL(Attributes,'') as Attributes, ISNULL(Position,'') as Position";
            sql += " FROM VRSCategory c ";
            sql += string.Format(" WHERE loadId = {0} ", loadId);
            sql += " EXCEPT ";
            sql += " SELECT [CategoryID],[ParentID],[Title],[Description], ISNULL(Attributes,'') as Attributes, ISNULL(Position,'') as Position ";
            sql += " FROM VRSCategory c2 ";
            sql += string.Format("WHERE loadId = {0}", priorGoodId);
            sql += " Order by CategoryID ";

            DataTable changedRecs = findChangedRecords(stagedConn, sql);

            foreach (DataRow dr in changedRecs.Rows)
            {
                VRSCatDTO dto = new VRSCatDTO();
                dto.CategoryID = dr["CategoryID"].ToString();
                dto.ParentID = dr["ParentID"].ToString();
                dto.Title = dr["Title"].ToString();
                dto.Description = dr["Description"].ToString();
                dto.Attributes = dr["Attributes"].ToString();
                dto.Position = dr["Position"].ToString();
                rtnList.Add(dto);
            }
            return rtnList;
        }

        private List<VRSCatDTO> getCurrentRecords(IDbConnection stagedConn, Int32 loadId)
        {
            List<VRSCatDTO> rtnList = new List<VRSCatDTO>();
            string sql = " SELECT [CategoryID],[ParentID],[Title],[Description], ISNULL(Attributes,'') as Attributes, ISNULL(Position,'') as Position";
            sql += " FROM VRSCategory c ";
            sql += string.Format(" WHERE loadId = {0} ", loadId);
            sql += " Order by CategoryID ";

            DataTable changedRecs = findChangedRecords(stagedConn, sql);

            foreach (DataRow dr in changedRecs.Rows)
            {
                VRSCatDTO dto = new VRSCatDTO();
                dto.CategoryID = dr["CategoryID"].ToString();
                dto.ParentID = dr["ParentID"].ToString();
                dto.Title = dr["Title"].ToString();
                dto.Description = dr["Description"].ToString();
                dto.Attributes = dr["Attributes"].ToString();
                dto.Position = dr["Position"].ToString();
                rtnList.Add(dto);
            }
            return rtnList;
        }

        private List<VRSCatDTO> buildDeletedRecords(IDbConnection stagedConn, Int32 loadId, Int32 priorGoodId)
        {
            VRSCatDAO vRSCatDAO = new VRSCatDAO();
            List<VRSCatDTO> priorRecs = vRSCatDAO.selectRecordsRtnList(stagedConn, string.Format("LoadId = {0}", priorGoodId));

            List<VRSCatDTO> rtnList = new List<VRSCatDTO>();
            string sql = "SELECT [CategoryID],[ParentID] FROM VRSCategory c ";
            sql += string.Format("WHERE loadId = {0} ", priorGoodId);
            sql += "EXCEPT SELECT [CategoryID],[ParentID] FROM VRSCategory c2 ";
            sql += string.Format("WHERE loadId = {0} Order by CategoryID", loadId);
            DataTable recordsToDelete = findChangedRecords(stagedConn, sql);


            foreach (DataRow dr in recordsToDelete.Rows)
            {
                VRSCatDTO dto = new VRSCatDTO();
                dto.CategoryID = dr["CategoryID"].ToString();
                dto.ParentID = dr["ParentID"].ToString();

                VRSCatDTO priorDto = priorRecs.Where(x => x.CategoryID.Equals(dto.CategoryID)).FirstOrDefault();
                if (priorDto != null)
                {
                    dto.Title = priorDto.Title;
                }

                rtnList.Add(dto);
            }
            return rtnList;
        }

        public List<VRSCatDTO> populatePositionNumber(List<VRSCatDTO> passedCatList)
        {
            List<VRSCatDTO> rtnList = new List<VRSCatDTO>();
            string categoryConfig = ConfigurationManager.AppSettings["CategoryOrder"].ToString();
            List<string> categorylevel1List = categoryConfig.Split(',').ToList();

            List<string> rootLevelIds = new List<string>();

            //Setup the root id's first.
            List<VRSCatDTO> rootsNotListed = new List<VRSCatDTO>();
            List<Int32> rootPositions = new List<int>();
            foreach (VRSCatDTO dto in passedCatList)
            {
                VRSCatDTO targetDTO = new VRSCatDTO() { Attributes = dto.Attributes, CategoryID = dto.CategoryID, Description = dto.Description, LoadId = dto.LoadId, ParentID = dto.ParentID, Title = dto.Title };
                string paddedCategoryId = base.padToSize(dto.CategoryID, 5, "0");
                if (categorylevel1List.Contains(paddedCategoryId))
                {
                    Int32 currPosn = (categorylevel1List.IndexOf(paddedCategoryId) * 10) + 10;
                    rootPositions.Add(currPosn);
                    targetDTO.Position = string.Format("{0}", currPosn);
                    rootLevelIds.Add(dto.CategoryID);
                }
                else
                {
                    if (string.IsNullOrWhiteSpace(dto.ParentID))
                    {
                        rootsNotListed.Add(dto);
                    }
                    else
                    {
                        dto.Position = ""; //TODO, work yet for the minor position.
                    }
                }
                rtnList.Add(targetDTO);
            }

            //If for some reason we have a new root level, we still need to assign a postion.  
            Int32 maxPosn = rootPositions.Max();
            foreach (VRSCatDTO dto in rootsNotListed)
            {
                Int32 nexPosn = maxPosn + 10;
                dto.Position = nexPosn.ToString();
                maxPosn = nexPosn;
            }

            //Setup the level sequences.
            List<string> nextLevelOfIds = rootLevelIds;
            for (int x = 0; x < 10; x++)
            {
                nextLevelOfIds = setupPostitionIdsForLevel(rtnList, nextLevelOfIds);
            }
            return rtnList;
        }

        private List<string> setupPostitionIdsForLevel(List<VRSCatDTO> rtnList, List<string> currLevelIds)
        {
            List<string> nextLevelIds = new List<string>();
            foreach (string currId in currLevelIds)
            {
                List<VRSCatDTO> l2Recs = rtnList.Where(x => x.ParentID == currId).ToList();
                Int32 postion = 10;
                foreach (VRSCatDTO l2rec in l2Recs)
                {
                    nextLevelIds.Add(l2rec.CategoryID);
                    l2rec.Position = postion.ToString();
                    postion += 10;
                }
            }
            return nextLevelIds;
        }

        public List<VRSCatDTO> removeAllBadReferences(List<VRSCatDTO> passedCatList)
        {
            List<VRSCatDTO> rtnList = new List<VRSCatDTO>();
            List<VRSCatDTO> catListLessBadFK = new List<VRSCatDTO>();
            List<VRSCatDTO> tempCatList = new List<VRSCatDTO>();

            catListLessBadFK = passedCatList.Where(x => string.IsNullOrWhiteSpace(x.ParentID) || isCategoryIdInList(passedCatList, x.ParentID)).ToList();
            for (Int32 i = 0; i < 6; i++)
            {
                tempCatList = passedCatList.Where(x => string.IsNullOrWhiteSpace(x.ParentID) || isCategoryIdInList(catListLessBadFK, x.ParentID)).ToList();
                catListLessBadFK = tempCatList;
            }

            rtnList = passedCatList.Where(x=> isCategoryIdInList(catListLessBadFK, x.CategoryID)).ToList();
            return rtnList;
        }

        public List<VRSCatDTO> putInReferenceOrder(List<VRSCatDTO> passedCatList)
        {
            List<VRSCatDTO> catInOrderList = new List<VRSCatDTO>();
            List<VRSCatDTO> remanderCatList = new List<VRSCatDTO>();
            List<VRSCatDTO> tempCatList = new List<VRSCatDTO>();

            catInOrderList.AddRange(passedCatList.Where(x => string.IsNullOrEmpty(x.ParentID)));
            remanderCatList = passedCatList.Where(x => !isCategoryIdInList(catInOrderList, x.CategoryID)).ToList();

            //Process in referencing order.
            for (Int32 i = 0; i < 6 && remanderCatList.Count > 0; i++)
            {
                tempCatList = remanderCatList.Where(x => isCategoryIdInList(catInOrderList, x.ParentID)).ToList();
                catInOrderList.AddRange(tempCatList);
                remanderCatList = passedCatList.Where(x => !isCategoryIdInList(catInOrderList, x.CategoryID)).ToList();
            }
            catInOrderList.AddRange(remanderCatList);
            return catInOrderList;
        }

        private bool isCategoryIdInList( List<VRSCatDTO> srcList, string categoryId)
        {
            foreach (VRSCatDTO dto in srcList)
            {
                if (dto.CategoryID == categoryId)
                {
                    return true;
                }
            }
            return false;
        }

        public void logConflicts(List<VRSCatDTO> rawCatList)
        {
            List<string> conflicts = findConflicts(rawCatList);
            if( conflicts.Count > 0 )
            {
                StringBuilder sb = new StringBuilder();
                foreach (string conflict in conflicts)
                {
                    string errMsg = string.Format("ProcessCategoryFile {0}", conflict);
                    log.logMsg(LogController.LogClass.Error, errMsg);
                    sb.Append(errMsg + "\n");
                }
                createMailMessage("Product Category duplicates", sb.ToString());
            }
        }

        public List<string> findConflicts( List<VRSCatDTO> rawCatList )
        {
            List<string> errors = new List<string>();
            HashSet<string> conflicts = new HashSet<string>();
            foreach ( VRSCatDTO dto in rawCatList)
            {
                List<VRSCatDTO> recConflicts = rawCatList.Where(x => !x.CategoryID.Equals(dto.CategoryID) &&
                    x.ParentID.Equals(dto.ParentID) && urlKeyFormatting(x).Equals(urlKeyFormatting(dto)) &&
                    !conflicts.Contains(x.CategoryID + " - " + dto.CategoryID) &&
                    !conflicts.Contains(dto.CategoryID + " - " + x.CategoryID) 
                    ).ToList();

                foreach (VRSCatDTO cdto in recConflicts)
                {
                    if (recConflicts.Count > 0)
                    {
                        conflicts.Add(dto.CategoryID + " - " + cdto.CategoryID);
                        errors.Add(string.Format("Category conflict between {0} and {1}", dto.CategoryID, cdto.CategoryID));
                    }
                }
            }
            return errors;
        }

        private void loadAdditionalBaseCategory(System.Data.IDbConnection conn, string title, string baseCategory, string prefix, int loadId) 
        {
            string sqlTxt = " INSERT INTO VRSCategory " +
                string.Format(" SELECT {0} as LoadID, '{1}' as CategoryID, '' as ParentID, '{2}' as Title, ", loadId, baseCategory, title) + 
                " '' as Description, null as Attributes, 0 as position " +
                " UNION ALL " +
                string.Format(" SELECT loadid, '{0}' +  rtrim(ltrim(CategoryID)) as CategoryID, '{1}' as ParentID, Title, Description, Attributes, Position ", prefix, baseCategory) +
                "  FROM VRSCategory  " +
                string.Format("  WHERE loadid = {0} ", loadId) +
                "   AND parentID = '' AND len(rtrim(ltrim(CategoryID))) = 5 " +
                " UNION ALL " +
                string.Format(" SELECT loadid, '{0}' + rtrim(ltrim(CategoryID)) as CategoryID, ",prefix) +
                string.Format("                '{0}' + rtrim(ltrim(ParentID)) as ParentID,  ", prefix)  +
                "      Title, Description, Attributes, Position " +
                " FROM VRSCategory  " +
                string.Format("  WHERE loadid = {0} ", loadId) +
                " AND parentID <> ''  AND len(rtrim(ltrim(CategoryID))) = 5  ";

            IDbCommand cmd = conn.CreateCommand();
            cmd.CommandTimeout = 1000000;
            cmd.CommandText = sqlTxt;
            Int32 recsInserted = cmd.ExecuteNonQuery();

        }


    }
}
