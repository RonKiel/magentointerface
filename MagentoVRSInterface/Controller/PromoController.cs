﻿using MagentoVRSInterface.DTO;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace MagentoVRSInterface.Controller
{
    public class PromoController : FileProcessor
    {
        LogController log = LogController.Instance;
        enum KindOfExtract { Detail, MostFrequent };

        public override List<KeyValuePair<string, StringBuilder>> processFile(System.Data.IDbConnection stagedConn, string sourceFile)
        {
            throw new NotImplementedException(); //We want to use the methods from FileProcess, bute we have a different signiture to envoke.
        }

        public List<KeyValuePair<string, StringBuilder>> processPromos(System.Data.IDbConnection stagedConn, System.Data.IDbConnection blueConn) 
        {
            string DestDirectory = ConfigurationManager.AppSettings["FileBuildLocation"].ToString();
            List<KeyValuePair<string, StringBuilder>> rtnList = new List<KeyValuePair<string, StringBuilder>>();

            //Both are views of QPRC, one with the detail the over is most frequent values.
            BulkInsert((SqlConnection)stagedConn, "VRSQPRC", retrieveQPRCRecords(blueConn, KindOfExtract.MostFrequent));
            BulkInsert((SqlConnection)stagedConn, "VRSQPRCD", retrieveQPRCRecords(blueConn, KindOfExtract.Detail)); 

            BulkInsert((SqlConnection)stagedConn, "VRSQPRE", retrieveQPRERecords(blueConn));
            BulkInsert((SqlConnection)stagedConn, "VRSmlpromo", retrievePromoRecords(blueConn));
            BulkInsert((SqlConnection)stagedConn, "VRSMLPROMOD", retrievePromoDRecords(blueConn));

            //Build change file.
            List<VRSQprcDTO> qprcChanges = buildChangedQprcRecords(stagedConn," vrsqprc ", contextController.loadId, contextController.priorGoodLoad);
            List<VRSQprcDTO> allQprcRecs = buildChangedQprcRecords(stagedConn," vrsqprc ", contextController.loadId, -1);

            List<VRSQprcDTO> qprcDChanges = buildChangedQprcRecords(stagedConn, " vrsqprcd ", contextController.loadId, contextController.priorGoodLoad);
            List<VRSQprcDTO> allQprcDRecs = buildChangedQprcRecords(stagedConn, " vrsqprcd ", contextController.loadId, -1);

            List<VRSmlpromoDTO> mlPromoChanges = buildChangedMlPromoRecords(stagedConn, contextController.loadId, contextController.priorGoodLoad);
            List<VRSmlpromoDTO> allMlPromoRecs = buildChangedMlPromoRecords(stagedConn, contextController.loadId, -1);

            List<VRSmlpromodDTO> mlPromoDChanges = buildChangedMlPromoDRecords(stagedConn, contextController.loadId, contextController.priorGoodLoad);
            List<VRSmlpromodDTO> allMlPromoDRecs = buildChangedMlPromoDRecords(stagedConn, contextController.loadId, -1);

            List<VRSQpreDTO> qpreChanges = buildChangedQpreRecords(stagedConn, contextController.loadId, contextController.priorGoodLoad);
            List<VRSQpreDTO> allQqpreRecs = buildChangedQpreRecords(stagedConn, contextController.loadId, -1);

            List<string> allChanges = buildListOfPromoThatChanged(qprcChanges, qpreChanges);

            //Promo header level.
            string promoChangeFile = DestDirectory + "promoFile" + contextController.loadId.ToString("D8") + ".tab";
            StringBuilder changeBuffer = buildChangeBufferForHeader(allQprcRecs, allQqpreRecs, allChanges);
            rtnList.Add(new KeyValuePair<string, StringBuilder>(promoChangeFile, changeBuffer));

            //Build delete file.
            if (contextController.priorGoodLoad > 0)
            {
                List<VRSQprcDTO> allPriorQprcRecs = buildChangedQprcRecords(stagedConn," vrsqprc ", contextController.priorGoodLoad, -1);
                List<VRSQpreDTO> allPriorQqpreRecs = buildChangedQpreRecords(stagedConn, contextController.priorGoodLoad, -1);

                StringBuilder sb = buildDeleteBufferForHeader(allPriorQprcRecs, allPriorQqpreRecs, allQprcRecs, allQqpreRecs);
                if (!string.IsNullOrWhiteSpace(sb.ToString()))
                {
                    string promoDeleteFile = DestDirectory + "promoDeleteFile" + contextController.loadId.ToString("D8") + ".tab";
                    rtnList.Add(new KeyValuePair<string, StringBuilder>(promoDeleteFile, sb));
                }
            }

            //Promo Detail level.
            string promoDetailChangeFile = DestDirectory + "promoDetailFile" + contextController.loadId.ToString("D8") + ".tab";
            StringBuilder promoDetailSb = buildChangeBufferForDetail(qprcDChanges);
            rtnList.Add(new KeyValuePair<string, StringBuilder>(promoDetailChangeFile, promoDetailSb));

            //Promo Detail level, delete.
            if (contextController.priorGoodLoad > 0)
            {
                List<VRSQprcDTO> qprcDRecordsRemoved = buildChangedQprcRecords(stagedConn, " vrsqprcd ", contextController.priorGoodLoad, contextController.loadId, " PROMO, MAJKY ");
                StringBuilder sb = buildPromoDetailDeleteBuffer(qprcDRecordsRemoved);

                if (!string.IsNullOrWhiteSpace(sb.ToString()))
                {
                    string promoDetailDeleteFile = DestDirectory + "promoDetailDeleteFile" + contextController.loadId.ToString("D8") + ".tab";
                    rtnList.Add(new KeyValuePair<string, StringBuilder>(promoDetailDeleteFile, sb));
                }
                else
                {
                    log.logMsg(LogController.LogClass.Info, "Skipping promoDetialDeleteFile, nothing to delete.");
                }
            }

            return rtnList;
        }

        private StringBuilder buildPromoDetailDeleteBuffer(List<VRSQprcDTO> qprcDRecordsRemoved)
        {
            StringBuilder sb = new StringBuilder();
            if (qprcDRecordsRemoved.Count > 0)
            {
                sb.Append("promoid\tsku\n");
                foreach (VRSQprcDTO dto in qprcDRecordsRemoved)
                {
                    string majky = dto.MAJKY;
                    if (!contextController.majkeyToSku.Keys.Contains(dto.MAJKY))
                    {
                        bool foundit = false;
                        foreach (string possibleKey in contextController.majkeyToSku.Keys)
                        {
                            if (!foundit)
                            {
                                if (possibleKey.Trim().Equals(dto.MAJKY.Trim()))
                                {
                                    majky = possibleKey;
                                    foundit = true;
                                    log.logMsg(LogController.LogClass.Warning, string.Format("PromoDetail (On delete): Subsitute Majkey ({0}) with ({1}) ", dto.MAJKY, possibleKey));
                                }
                            }
                        }
                    }

                    if (!contextController.majkeyToSku.Keys.Contains(majky))
                    {
                        log.logMsg(LogController.LogClass.Warning, string.Format("PromoDetail (On delete): Majkey ({0}) not found in U8.", dto.MAJKY));
                    }
                    else
                    {
                        string productSku = contextController.majkeyToSku[majky];
                        string trec = string.Format("{0}\t{1}\n", dto.PROMO, productSku);
                        sb.Append(trec);
                    }
                }
            }
            return sb;
        }

        public StringBuilder buildDeleteBufferForHeader(List<VRSQprcDTO> allPriorQprcRecs, List<VRSQpreDTO> allPriorQqpreRecs,  List<VRSQprcDTO> allQprcRecs, List<VRSQpreDTO> allQqpreRecs)
        {
            List<string> priorQprcPromos = getListOfStringFromStructureList("PROMO", allPriorQprcRecs);
            List<string> priorQprePromos = getListOfStringFromStructureList("PROMO", allPriorQqpreRecs);
            List<string> priorPromos = priorQprcPromos.Union(priorQprePromos).ToList();

            List<string> currentQprcPromos = getListOfStringFromStructureList("PROMO", allQprcRecs);
            List<string> currentQprePromos = getListOfStringFromStructureList("PROMO", allQqpreRecs);
            List<string> currentPromos = currentQprcPromos.Union(currentQprePromos).ToList();

            List<string> promosToDelete = priorPromos.Except(currentPromos).ToList();
            
            StringBuilder sb = new StringBuilder();
            if (promosToDelete.Count > 0)
            {
                sb.Append("promoid\n");
                foreach (string priorPromoToDelete in promosToDelete)
                {
                    sb.Append(priorPromoToDelete + "\n");
                }
            }

            return sb;
        }

        private StringBuilder buildChangeBufferForHeader(List<VRSQprcDTO> allQprcRecs, List<VRSQpreDTO> allQqpreRecs, List<string> allChanges)
        {
            StringBuilder sb = new StringBuilder();

            List<string> flds = new List<string>(new string[] { "promoid", "title", "startdate", "enddate", "buyqty", "maxqty" });
            string headers = string.Join("\t", flds) + "\n";
            sb.Append(headers);

            foreach (string promo in allChanges)
            {
                VRSQprcDTO vRSQprcDTO = allQprcRecs.Where(x => x.PROMO.Trim().Equals(promo.Trim())).SingleOrDefault();
                if (vRSQprcDTO != null)
                {
                    string title = vRSQprcDTO.TEXT1.Trim();
                    VRSQpreDTO vRSQpreDTO = allQqpreRecs.Where(x => x.PROMO.Trim().Equals(promo.Trim())).SingleOrDefault();
                    if (vRSQpreDTO != null && !string.IsNullOrWhiteSpace(vRSQpreDTO.TEXT1))
                    {
                        title = vRSQpreDTO.TEXT1.Trim();
                    }

                    try
                    {
                        string tRec = "";
                        tRec += promo + "\t";
                        tRec += title + "\t";
                        tRec += vRSQprcDTO.BEGDT + "\t";
                        tRec += vRSQprcDTO.ENDDT + "\t";
                        tRec += vRSQprcDTO.MINQTY + "\t";
                        tRec += vRSQprcDTO.MAXQTY + "\n";

                        sb.Append(tRec);
                    }
                    catch (Exception ex1)
                    {
                        log.logMsg(LogController.LogClass.Warning, string.Format("Promo :({0}) had exception ({1}) at ({2})", promo, ex1.Message, ex1.StackTrace));
                    }
                }
                else
                {
                    log.logMsg(LogController.LogClass.Warning, string.Format("Promo :({0}) was missing from qprc.", promo));
                }
            }
            return sb;
        }

        private StringBuilder buildChangeBufferForDetail(List<VRSQprcDTO> changesQprcDRecs )
        {
            StringBuilder sb = new StringBuilder();

            List<string> flds = new List<string>(new string[] { "promoid", "sku", "discount" });
            string headers = string.Join("\t", flds) + "\n";
            sb.Append(headers);

            foreach (VRSQprcDTO dto in changesQprcDRecs)
            {
                try
                {
                    if (!contextController.majkeyToSku.Keys.Contains(dto.MAJKY))
                    {
                        log.logMsg(LogController.LogClass.Warning, string.Format("PromoDetail : Majkey ({0}) not found in U8.", dto.MAJKY));
                    }
                    else
                    {
                        string productSku = contextController.majkeyToSku[dto.MAJKY];

                        string tRec = "";
                        tRec += dto.PROMO + "\t";
                        tRec += productSku + "\t";
                        tRec += string.Format("{0:N3}", CustomerDiscount((decimal)dto.MULT)) + "\n";

                        sb.Append(tRec);
                        if (contextController.productToSkusMapExtended[productSku].Count > 1)
                        {
                            foreach(string strSku in contextController.productToSkusMapExtended[productSku])
                            {
                                tRec = "";
                                tRec += dto.PROMO + "\t";
                                tRec += strSku + "\t";
                                tRec += string.Format("{0:N3}", CustomerDiscount((decimal)dto.MULT)) + "\n";
                                sb.Append(tRec);
                            }
                        }
                    }
                }
                catch (Exception ex1)
                {
                    log.logMsg(LogController.LogClass.Warning, string.Format("PromoDetail : Promo:({0}) Majkey:({1}) had exception ({2}) at ({3})", dto.PROMO, dto.MAJKY, ex1.Message, ex1.StackTrace));
                }                    
            }
            return sb;
        }

        public List<string> buildListOfPromoThatChanged(List<VRSQprcDTO> qprcChanges, List<VRSQpreDTO> qpreChanges)
        {
            List<string> qprcProChgs = getListOfStringFromStructureList("PROMO", qprcChanges);
            List<string> qpreProChgs = getListOfStringFromStructureList("PROMO", qpreChanges);

            List<string> allChanges = qprcProChgs.Union(qpreProChgs).ToList();
            return allChanges;
        }


        #region promod

        private List<VRSmlpromodDTO> retrievePromoDRecords(System.Data.IDbConnection blueConn)
        {
            IDbCommand cmd = blueConn.CreateCommand();
            cmd.CommandText = "select * FROM mlpromod";

            IDataReader dr = cmd.ExecuteReader();
            DataTable rawDataTable = new DataTable();

            DataSet ds = new DataSet();
            ds.Tables.Add(rawDataTable);
            ds.EnforceConstraints = false;


            rawDataTable.Load(dr);
            dr.Close();

            List<VRSmlpromodDTO> rawRecs = new List<VRSmlpromodDTO>();
            foreach (DataRow row in rawDataTable.Rows)
            {
                addPromodRow(rawRecs, row);
            }

            return rawRecs;
        }

        private List<VRSmlpromodDTO> buildChangedMlPromoDRecords(IDbConnection stagedConn, Int32 loadId, Int32 priorGoodId)
        {
            List<VRSmlpromodDTO> rtnList = new List<VRSmlpromodDTO>();

            string tableName = " VRSmlpromod ";
            string orderBy = " PROMO ";
            string headers = generateHeaderRecord(new VRSmlpromodDTO(), ",") + " ";

            string sql = " SELECT " + headers;
            sql += " FROM " + tableName + " c ";
            sql += string.Format(" WHERE loadId = {0} ", loadId);
            sql += " EXCEPT ";
            sql += " SELECT " + headers;
            sql += " FROM " + tableName + " c2 ";
            sql += string.Format(" WHERE loadId = {0}", priorGoodId);
            sql += " Order by " + orderBy + " ";

            DataTable changedRecs = findChangedRecords(stagedConn, sql);

            foreach (DataRow row in changedRecs.Rows)
            {
                addPromodRow(rtnList, row);
            }
            return rtnList;
        }

        private void addPromodRow(List<VRSmlpromodDTO> rawRecs, DataRow row)
        {
            VRSmlpromodDTO vRSmlpromodDTO = new VRSmlpromodDTO();

            vRSmlpromodDTO.LoadId = contextController.loadId;
            vRSmlpromodDTO.SKU = row["SKU"].ToString();
            vRSmlpromodDTO.PROMO = row["PROMO"].ToString();
            vRSmlpromodDTO.ITEM = row["ITEM"].ToString();
            vRSmlpromodDTO.MULT = getDecimalValue(row, "MULT");
            vRSmlpromodDTO.ADDDT = row["ADDDT"].ToString();
            vRSmlpromodDTO.CHGDT = row["CHGDT"].ToString();
            vRSmlpromodDTO.ADDBY = row["ADDBY"].ToString();
            vRSmlpromodDTO.CHGBY = row["CHGBY"].ToString();
            vRSmlpromodDTO.TOTQTY = getDecimalValue(row, "TOTQTY");
            vRSmlpromodDTO.TOTRTL = getDecimalValue(row, "TOTRTL");
            vRSmlpromodDTO.COGS = getDecimalValue(row, "COGS");
            vRSmlpromodDTO.CCOST = getDecimalValue(row, "CCOST");
            vRSmlpromodDTO.LVAR = row["LVAR"].ToString();

            rawRecs.Add(vRSmlpromodDTO);
        }

        #endregion

        #region QPRE

        private List<VRSQpreDTO> retrieveQPRERecords(System.Data.IDbConnection blueConn)
        {
            IDbCommand cmd = blueConn.CreateCommand();
            cmd.CommandText = "select * FROM qpre";

            IDataReader dr = cmd.ExecuteReader();
            DataTable rawDataTable = new DataTable();

            DataSet ds = new DataSet();
            ds.Tables.Add(rawDataTable);
            ds.EnforceConstraints = false;


            rawDataTable.Load(dr);
            dr.Close();

            List<VRSQpreDTO> rawRecs = new List<VRSQpreDTO>();
            foreach (DataRow row in rawDataTable.Rows)
            {
                addQpreRow(rawRecs, row);
            }

            List<VRSQpreDTO> bestRecs = findMostCommonQpreValuesByKey(rawRecs);
            return bestRecs;
        }

        public List<VRSQpreDTO> findMostCommonQpreValuesByKey(List<VRSQpreDTO> rawRecs)
        {
            Dictionary<string, List<object>> dataByPromoDict = new Dictionary<string, List<object>>();
            foreach (VRSQpreDTO dto in rawRecs)
            {
                if (dataByPromoDict.Keys.Contains(dto.PROMO.Trim()))
                {
                    dataByPromoDict[dto.PROMO.Trim()].Add(dto);
                }
                else
                {
                    dataByPromoDict.Add(dto.PROMO.Trim(), new List<object>() { dto });
                }
            }

            List<VRSQpreDTO> bestRecs = new List<VRSQpreDTO>();
            foreach (string key in dataByPromoDict.Keys)
            {
                VRSQpreDTO mostCommonValues = new VRSQpreDTO() { PROMO = key, LoadId = contextController.loadId };
                List<object> dtoList = dataByPromoDict[key];
                getMostCommonFieldValue(mostCommonValues, dtoList, "TEXT1");
                bestRecs.Add(mostCommonValues);
            }

            return bestRecs;
        }

        private List<VRSQpreDTO> buildChangedQpreRecords(IDbConnection stagedConn, Int32 loadId, Int32 priorGoodId)
        {
            List<VRSQpreDTO> rtnList = new List<VRSQpreDTO>();

            string tableName = " VRSQPRE ";
            string orderBy = " PROMO ";
            string headers = generateHeaderRecord(new VRSQpreDTO(), ",") + " ";

            string sql = " SELECT " + headers;
            sql += " FROM " + tableName + " c ";
            sql += string.Format(" WHERE loadId = {0} ", loadId);
            sql += " EXCEPT ";
            sql += " SELECT " + headers;
            sql += " FROM " + tableName + " c2 ";
            sql += string.Format(" WHERE loadId = {0}", priorGoodId);
            sql += " Order by " + orderBy + " ";

            DataTable changedRecs = findChangedRecords(stagedConn, sql);

            foreach (DataRow row in changedRecs.Rows)
            {
                addQpreRow(rtnList, row);
            }
            return rtnList;
        }

        private void addQpreRow(List<VRSQpreDTO> rawRecs, DataRow row)
        {
            VRSQpreDTO vRSQpreDTO = new VRSQpreDTO();

            vRSQpreDTO.LoadId = contextController.loadId;
            vRSQpreDTO.PROMO = row["PROMO"].ToString();
            vRSQpreDTO.TEXT1 = row["TEXT1"].ToString();
            vRSQpreDTO.NUMDYS = row["NUMDYS"].ToString();
            vRSQpreDTO.LVAR = row["LVAR"].ToString();

            rawRecs.Add(vRSQpreDTO);
        }


        #endregion

        #region QPRC

        private List<VRSQprcDTO> buildChangedQprcRecords(IDbConnection stagedConn, string tableName, Int32 loadId, Int32 priorGoodId)
        {
            return buildChangedQprcRecords(stagedConn, tableName, loadId, priorGoodId, generateHeaderRecord(new VRSQprcDTO(), ",") + " ");
        }
        private List<VRSQprcDTO> buildChangedQprcRecords(IDbConnection stagedConn,string tableName, Int32 loadId, Int32 priorGoodId, string headers)
        {
            List<VRSQprcDTO> rtnList = new List<VRSQprcDTO>();

            string orderBy = "Promo ";

            string sql = " SELECT " + headers;
            sql += " FROM " + tableName + " c ";
            sql += string.Format(" WHERE loadId = {0} ", loadId);
            sql += " EXCEPT ";
            sql += " SELECT " + headers;
            sql += " FROM " + tableName + " c2 ";
            sql += string.Format(" WHERE loadId = {0}", priorGoodId);
            sql += " Order by " + orderBy + " ";

            DataTable changedRecs = findChangedRecords(stagedConn, sql);

            foreach (DataRow row in changedRecs.Rows)
            {
                addQprcRow(rtnList, row);
            }
            return rtnList;
        }

        private List<VRSQprcDTO> retrieveQPRCRecords(System.Data.IDbConnection blueConn, KindOfExtract kindOfExtract)
        {
            IDbCommand cmd = blueConn.CreateCommand();
            cmd.CommandText = "select * FROM qprc";

            IDataReader dr = cmd.ExecuteReader();
            DataTable rawDataTable = new DataTable();

            DataSet ds = new DataSet();
            ds.Tables.Add(rawDataTable);
            ds.EnforceConstraints = false;

            rawDataTable.Load(dr);
            dr.Close();

            List<VRSQprcDTO> rawRecs = new List<VRSQprcDTO>();
            foreach (DataRow row in rawDataTable.Rows)
            {
                addQprcRow(rawRecs, row);
            }
            List<VRSQprcDTO> bestRecs = null;

            if (kindOfExtract == KindOfExtract.MostFrequent)
            {
                bestRecs = findMostCommonQprcValuesByKey(rawRecs);
            }
            else
            {
                bestRecs = new List<VRSQprcDTO>();
                foreach (VRSQprcDTO dto in rawRecs)
                {
                    VRSQprcDTO currRec = new VRSQprcDTO() { LoadId=contextController.loadId, PROMO = dto.PROMO, MAJKY = dto.MAJKY, MULT = dto.MULT };
                    bestRecs.Add(currRec);
                }
            }

            return bestRecs;
        }

        public List<VRSQprcDTO> findMostCommonQprcValuesByKey( List<VRSQprcDTO> rawRecs )
        {
            Dictionary<string, List<object>> dataByPromoDict = new Dictionary<string, List<object>>();
            foreach (VRSQprcDTO dto in rawRecs)
            {
                if (dataByPromoDict.Keys.Contains(dto.PROMO.Trim()))
                {
                    dataByPromoDict[dto.PROMO.Trim()].Add(dto);
                }
                else
                {
                    dataByPromoDict.Add(dto.PROMO.Trim(), new List<object>() { dto });
                }
            }

            List<VRSQprcDTO> bestRecs = new List<VRSQprcDTO>();
            foreach (string key in dataByPromoDict.Keys)
            {
                VRSQprcDTO mostCommonValues = new VRSQprcDTO() { PROMO = key, LoadId = contextController.loadId };

                List<object> dtoList = dataByPromoDict[key];

                getMostCommonFieldValue(mostCommonValues, dtoList, "BEGDT");
                getMostCommonFieldValue(mostCommonValues, dtoList, "ENDDT");
                getMostCommonFieldValue(mostCommonValues, dtoList, "TEXT1");
                getMostCommonFieldValue(mostCommonValues, dtoList, "MINQTY");
                getMostCommonFieldValue(mostCommonValues, dtoList, "MAXQTY");
/*
                getMostCommonFieldValue(mostCommonValues, dtoList, "MULT");

                List<decimal> mults = getListOfDecimalsFromStructureList("MULT", dtoList);
                mostCommonValues.MINMULT = mults.Min();
                mostCommonValues.MAXMULT = mults.Max();
*/
                bestRecs.Add(mostCommonValues);
            }

            return bestRecs;
        }

        private void addQprcRow(List<VRSQprcDTO> rawRecs, DataRow row)
        {
            VRSQprcDTO vRSQprcDTO = new VRSQprcDTO();

            vRSQprcDTO.LoadId = contextController.loadId;
            vRSQprcDTO.MAJKY = (row.Table.Columns.Contains("MAJKY")) ? row["MAJKY"].ToString() : "";
            vRSQprcDTO.BEGDT = (row.Table.Columns.Contains("BEGDT")) ? row["BEGDT"].ToString() : "";
            vRSQprcDTO.ENDDT = (row.Table.Columns.Contains("ENDDT")) ? row["ENDDT"].ToString() : "";
            vRSQprcDTO.TLXFL = (row.Table.Columns.Contains("TLXFL")) ? row["TLXFL"].ToString() : "";
            vRSQprcDTO.TEXT1 = (row.Table.Columns.Contains("TEXT1")) ? row["TEXT1"].ToString() : "";
            vRSQprcDTO.PROMO = (row.Table.Columns.Contains("PROMO")) ? row["PROMO"].ToString() : "";
            vRSQprcDTO.EVNTNO = (row.Table.Columns.Contains("EVNTNO")) ? row["EVNTNO"].ToString() : "";

            vRSQprcDTO.MULT = (row.Table.Columns.Contains("MULT")) ? getDecimalValue(row, "MULT") : null;
            vRSQprcDTO.MINQTY = (row.Table.Columns.Contains("MINQTY")) ? getDecimalValue(row, "MINQTY") : null;
            vRSQprcDTO.MAXQTY = (row.Table.Columns.Contains("MAXQTY")) ? getDecimalValue(row, "MAXQTY") : null;
            vRSQprcDTO.TOTQTY = (row.Table.Columns.Contains("TOTQTY")) ? getDecimalValue(row, "TOTQTY") : null;
            vRSQprcDTO.TOTRTL = (row.Table.Columns.Contains("TOTRTL")) ? getDecimalValue(row, "TOTRTL") : null;
            vRSQprcDTO.COGS = (row.Table.Columns.Contains("COGS")) ? getDecimalValue(row, "COGS") : null;
            vRSQprcDTO.CCOST = (row.Table.Columns.Contains("CCOST")) ? getDecimalValue(row, "CCOST") : null;

            rawRecs.Add(vRSQprcDTO);
        }

        #endregion

        #region Promo
        public List<VRSmlpromoDTO> retrievePromoRecords(System.Data.IDbConnection blueConn)
        {
            IDbCommand cmd = blueConn.CreateCommand();
            cmd.CommandText = "select * FROM mlpromo ";

            IDataReader dr = cmd.ExecuteReader();
            DataTable mlPromoTable = new DataTable();
            
            DataSet ds = new DataSet();
            ds.Tables.Add(mlPromoTable);
            ds.EnforceConstraints = false;

            mlPromoTable.Load(dr);
            dr.Close();

            List<VRSmlpromoDTO> rawRecs = new List<VRSmlpromoDTO>();
            foreach (DataRow row in mlPromoTable.Rows)
            {
                addMlPromoRow(rawRecs, row);
            }

            List<VRSmlpromoDTO> bestRecs = findMostCommonPromoValuesByKey(rawRecs);
            return bestRecs;
        }

        public List<VRSmlpromoDTO> findMostCommonPromoValuesByKey( List<VRSmlpromoDTO> rawRecs )
        {
            Dictionary<string, List<object>> mlpromoDict = new Dictionary<string, List<object>>();
            foreach (VRSmlpromoDTO dto in rawRecs)
            {
                if (mlpromoDict.Keys.Contains(dto.PROMO.Trim()))
                {
                    mlpromoDict[dto.PROMO.Trim()].Add(dto);
                }
                else
                {
                    mlpromoDict.Add(dto.PROMO.Trim(), new List<object>() { dto });
                }
            }

            List<VRSmlpromoDTO> bestRecs = new List<VRSmlpromoDTO>();
            foreach (string key in mlpromoDict.Keys)
            {
                VRSmlpromoDTO mostCommonValues = new VRSmlpromoDTO() { PROMO = key, LoadId = contextController.loadId };

                List<object> dtoList = mlpromoDict[key];

                getMostCommonFieldValue(mostCommonValues, dtoList, "BEGDT");
                getMostCommonFieldValue(mostCommonValues, dtoList, "ENDDT");
                getMostCommonFieldValue(mostCommonValues, dtoList, "MINQTY");
                getMostCommonFieldValue(mostCommonValues, dtoList, "TEXT1");
                getMostCommonFieldValue(mostCommonValues, dtoList, "ADDDT");
                getMostCommonFieldValue(mostCommonValues, dtoList, "LVAR");
                getMostCommonFieldValue(mostCommonValues, dtoList, "CHGDT");
                getMostCommonFieldValue(mostCommonValues, dtoList, "CHGBY");

                bestRecs.Add(mostCommonValues);
            }


            return bestRecs;
        }

        private List<VRSmlpromoDTO> buildChangedMlPromoRecords(IDbConnection stagedConn, Int32 loadId, Int32 priorGoodId)
        {
            List<VRSmlpromoDTO> rtnList = new List<VRSmlpromoDTO>();

            string tableName = " VRSmlpromo ";
            string orderBy = " PROMO ";
            string headers = generateHeaderRecord(new VRSmlpromoDTO(), ",") + " ";

            string sql = " SELECT " + headers;
            sql += " FROM " + tableName + " c ";
            sql += string.Format(" WHERE loadId = {0} ", loadId);
            sql += " EXCEPT ";
            sql += " SELECT " + headers;
            sql += " FROM " + tableName + " c2 ";
            sql += string.Format(" WHERE loadId = {0}", priorGoodId);
            sql += " Order by " + orderBy + " ";

            DataTable changedRecs = findChangedRecords(stagedConn, sql);

            foreach (DataRow row in changedRecs.Rows)
            {
                addMlPromoRow(rtnList, row);
            }
            return rtnList;
        }

        private void addMlPromoRow(List<VRSmlpromoDTO> rawRecs, DataRow row)
        {
            VRSmlpromoDTO vRSmlpromoDTO = new VRSmlpromoDTO();
            vRSmlpromoDTO.LoadId = contextController.loadId;
            vRSmlpromoDTO.PROMO = row["PROMO"].ToString();
            vRSmlpromoDTO.TEXT1 = row["TEXT1"].ToString();
            vRSmlpromoDTO.BEGDT = row["BEGDT"].ToString();
            vRSmlpromoDTO.ENDDT = row["ENDDT"].ToString();

            Decimal tmpDec = new decimal();
            if (Decimal.TryParse(row["MINQTY"].ToString(), out tmpDec))
            {
                vRSmlpromoDTO.MINQTY = tmpDec;
            }
            vRSmlpromoDTO.ADDDT = row["ADDDT"].ToString();
            vRSmlpromoDTO.CHGDT = row["CHGDT"].ToString();
            vRSmlpromoDTO.ADDBY = row["ADDBY"].ToString();
            vRSmlpromoDTO.CHGBY = row["CHGBY"].ToString();
            vRSmlpromoDTO.LVAR = row["LVAR"].ToString();

            rawRecs.Add(vRSmlpromoDTO);
        }
        #endregion

        private string CustomerDiscount(decimal pricingMultiplier)
        {
            string returnValue  = string.Empty;
            decimal multDecimal = ((100 - pricingMultiplier) / 100);             // Convert price multiplier into customer discount.
            string multString   = multDecimal.ToString();                        // Discount should always be a percentage, in form "0.nnnx"; we are only concerned with first 3 n's.

            switch (multString.Substring(4, 1))                                  // third decimal digit (third n).
            {
                case "0":
                case "1":
                case "2":
                    returnValue = multString.Substring(0, 4) + "0";              // truncate to two digits, third digit = 0
                    break;
                case "3":
                case "4":
                case "5":
                case "6":
                case "7":
                    returnValue = multString.Substring(0, 4) + "5";              // truncate to two digits, third digit = 5
                    break;
                case "8":
                case "9":
                    returnValue = Math.Round(multDecimal, 2).ToString() + "0";   // round up to two digits, third digit = 0
                    break;
            }
            
            return returnValue;
        }
    }
}
