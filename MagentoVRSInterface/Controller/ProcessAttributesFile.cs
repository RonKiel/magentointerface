﻿using MagentoVRSInterface.DTO;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagentoVRSInterface.Controller
{
    public class ProcessAttributesFile : FileProcessor
    {
        LogController log = LogController.Instance;

        public override List<KeyValuePair<string, StringBuilder>> processFile(IDbConnection stagedConn, string sourceFile)
        {
            List<VRSAttributeCodeDTO> rawAttribCodeList = buildDTOFromFile(sourceFile, contextController.loadId);
            return processFile(stagedConn, rawAttribCodeList, contextController.loadId, contextController.priorGoodLoad);
        }

        public List<KeyValuePair<string, StringBuilder>> processFile(IDbConnection stagedConn, List<VRSAttributeCodeDTO> rawRecsList, Int32 loadId, Int32 priorGoodId)
        {
            string sendAttribCodes = ConfigurationManager.AppSettings["SendItemAttributeCodes"].ToString();
            string DestDirectory = ConfigurationManager.AppSettings["FileBuildLocation"].ToString();
            
            List<KeyValuePair<string, StringBuilder>> rtnList = new List<KeyValuePair<string, StringBuilder>>();
            StringBuilder chgFileText = new StringBuilder();
            StringBuilder deleteFileText = new StringBuilder();

            //
            // We have the ablity to transmit the attributes themselves, but Magento may not need this.
            //
            if( sendAttribCodes.ToUpper() != "NO"  )
            {
                //Attribute Code level
#if OBSOLETECODE
                BulkInsert((SqlConnection)stagedConn, "VRSAttributeCode", rawRecsList); //HACK sql server specific.

                buildWriteBufferForChangesForHeader(stagedConn, loadId, priorGoodId, chgFileText);
                string chgFile = DestDirectory + "attributecode" + loadId.ToString("D8") + ".tab";
                rtnList.Add(new KeyValuePair<string, StringBuilder>(chgFile, chgFileText));

                buildWriteBufferForDeletes(stagedConn, loadId, priorGoodId, deleteFileText);
                string delFile = DestDirectory + "attributecodeDelete" + loadId.ToString("D8") + ".tab";
                rtnList.Add(new KeyValuePair<string, StringBuilder>(delFile, deleteFileText));
#endif 
                //Attribure Detail level. (Add/Update)
                StringBuilder detailChgsSb = new StringBuilder();
                lodAttributeDetail(stagedConn, loadId, contextController.atccdList);
                List<AtcddDTO> detailChgRecs = buildChangedRecordsDetail(stagedConn, loadId, priorGoodId);
                buildWriteBufferForChangesDetail(detailChgRecs, detailChgsSb);
                string detailFile = DestDirectory + "tattributeFile" + loadId.ToString("D8") + ".tab";
                rtnList.Add(new KeyValuePair<string, StringBuilder>(detailFile, detailChgsSb));

                //Attribure Detail level. (Delete)
                StringBuilder detailDeleteSb = new StringBuilder();
                List<AtcddDTO> detailDeleteRecs = buildChangedRecordsDetail(stagedConn, priorGoodId, loadId);
                buildWriteBufferForChangesDetail(detailDeleteRecs, detailDeleteSb);
                string detailDeleteFile = DestDirectory + "tattributeDeleteFile" + loadId.ToString("D8") + ".tab";
                rtnList.Add(new KeyValuePair<string, StringBuilder>(detailDeleteFile, detailDeleteSb));
            }

            return rtnList;
        }

        private void buildWriteBufferForDeletes(IDbConnection stagedConn, Int32 loadId, Int32 priorGoodId, StringBuilder sb)
        {
            List<VRSAttributeCodeDTO> deleteRecords = buildDeletedRecords(stagedConn, loadId, priorGoodId);
            List<VRSAttributeCodeDTO> oldPriorRecords = buildChangedRecordsHeader(stagedConn, priorGoodId, -1);

            List<string> flds = new List<string>(new string[] { "AttributeTypeCode", "AttributeDescription" });
            string headers = string.Join("\t", flds) + "\n";
            sb.Append(headers);


            List<string> codesToDeactivate = new List<string>();
            foreach (VRSAttributeCodeDTO dto in deleteRecords)
            {
                codesToDeactivate.Add(dto.AttributeTypeCode.Trim());
            }
            List<VRSAttributeCodeDTO> recordsToDeactivate = oldPriorRecords.Where(x => codesToDeactivate.Contains(x.AttributeTypeCode.Trim())).ToList();

            foreach (VRSAttributeCodeDTO dto in recordsToDeactivate)
            {
                string tRec = "";
                tRec += dto.AttributeTypeCode + "\t";
                tRec += dto.AttributeDescription + "\n";
                sb.Append(tRec);
            }
        }

        private List<VRSAttributeCodeDTO> buildDeletedRecords(IDbConnection stagedConn, Int32 loadId, Int32 priorGoodId)
        {
            List<VRSAttributeCodeDTO> rtnList = new List<VRSAttributeCodeDTO>();

            string tableName = " VRSAttributeCode ";
            string orderBy = " AttributeTypeCode ";
            string headers = " AttributeTypeCode ";

            string sql = " SELECT " + headers;
            sql += " FROM " + tableName + " c ";
            sql += string.Format(" WHERE loadId = {0} ", priorGoodId);
            sql += " EXCEPT ";
            sql += " SELECT " + headers;
            sql += " FROM " + tableName + " c2 ";
            sql += string.Format(" WHERE loadId = {0}", loadId);
            sql += " Order by " + orderBy + " ";

            DataTable changedRecs = findChangedRecords(stagedConn, sql);

            foreach (DataRow dr in changedRecs.Rows)
            {
                VRSAttributeCodeDTO dto = new VRSAttributeCodeDTO();
                dto.LoadId = loadId;
                dto.AttributeTypeCode = dr["AttributeTypeCode"].ToString();
                rtnList.Add(dto);
            }
            return rtnList;
        }

        private void buildWriteBufferForChangesDetail(List<AtcddDTO> detailChgRecs, StringBuilder chgFileText)
        {
            AttributeCollection attributes = new AttributeCollection();
            attributes.assignValues(ConfigurationManager.AppSettings["ItemAttributes"].ToString());

            foreach (AtcddDTO dto in detailChgRecs)
            {
                string magentoCode = attributes.getMagentoFromBlue(dto.ATRBCD.Trim());
                if (magentoCode != null)
                {
                    string tRec = string.Format("EAO,{0},{1}\n", magentoCode, dto.VALU.Trim()); ;
                    chgFileText.Append(tRec);
                }
            }
        }

        private void buildWriteBufferForChangesForHeader(IDbConnection stagedConn, Int32 loadId, Int32 priorGoodId, StringBuilder chgFileText)
        {
            List<VRSAttributeCodeDTO> changedRecords = buildChangedRecordsHeader(stagedConn, loadId, priorGoodId);

            List<string> flds = new List<string>(new string[] { "AttributeTypeCode", "AttributeDescription" });
            string headers = string.Join("\t", flds) + "\n";
            chgFileText.Append(headers);

            foreach (VRSAttributeCodeDTO dto in changedRecords)
            {
                string tRec = "";
                tRec += dto.AttributeTypeCode + "\t";
                tRec += dto.AttributeDescription + "\n";

                chgFileText.Append(tRec);
            }
        }

        private List<VRSAttributeCodeDTO> buildChangedRecordsHeader(IDbConnection stagedConn, Int32 loadId, Int32 priorGoodId)
        {
            List<VRSAttributeCodeDTO> rtnList = new List<VRSAttributeCodeDTO>();

            string tableName = "VRSAttributeCode";
            string orderBy = "AttributeTypeCode ";
            string headers = generateHeaderRecord(new VRSAttributeCodeDTO(), ",") + " ";

            string sql = " SELECT " + headers;
            sql += " FROM " + tableName + " c ";
            sql += string.Format(" WHERE loadId = {0} ", loadId);
            sql += " EXCEPT ";
            sql += " SELECT " + headers;
            sql += " FROM " + tableName + " c2 ";
            sql += string.Format(" WHERE loadId = {0}", priorGoodId);
            sql += " Order by " + orderBy + " ";

            DataTable changedRecs = findChangedRecords(stagedConn, sql);

            foreach (DataRow dr in changedRecs.Rows)
            {
                VRSAttributeCodeDTO dto = new VRSAttributeCodeDTO();
                populateDTOFromDataRow(dr, dto);
                dto.LoadId = loadId;
                rtnList.Add(dto);
            }
            return rtnList;
        }

        private List<AtcddDTO> buildChangedRecordsDetail(IDbConnection stagedConn, Int32 loadId, Int32 priorGoodId)
        {
            List<AtcddDTO> rtnList = new List<AtcddDTO>();

            string tableName = " VRSAttributeCodeDetail ";
            string orderBy = "AttributeTypeCode ";
            string headers = "AttributeTypeCode, AttributeValue";

            string sql = " SELECT " + headers;
            sql += " FROM " + tableName + " c ";
            sql += string.Format(" WHERE loadId = {0} ", loadId);
            sql += " EXCEPT ";
            sql += " SELECT " + headers;
            sql += " FROM " + tableName + " c2 ";
            sql += string.Format(" WHERE loadId = {0}", priorGoodId);
            sql += " Order by " + orderBy + " ";

            DataTable changedRecs = findChangedRecords(stagedConn, sql);

            foreach (DataRow dr in changedRecs.Rows)
            {
                AtcddDTO dto = new AtcddDTO();
                dto.ATRBCD = dr["AttributeTypeCode"].ToString();
                dto.VALU = dr["AttributeValue"].ToString();
                rtnList.Add(dto);
            }
            return rtnList;
        }

        protected List<VRSAttributeCodeDTO> buildDTOFromFile(string sourceFile, Int32 loadId)
        {
            List<VRSAttributeCodeDTO> rawRecords = new List<VRSAttributeCodeDTO>();

            using (StreamReader sr = new StreamReader(sourceFile))
            {
                string line;
                Int32 recNo = 0;
                while ((line = sr.ReadLine()) != null)
                {
                    List<string> fields = line.Split('\t').ToList();
                    VRSAttributeCodeDTO vRSAttributeCodeDTO = new VRSAttributeCodeDTO();
                    vRSAttributeCodeDTO.LoadId = loadId;

                    vRSAttributeCodeDTO.AttributeTypeCode = fields[0];
                    vRSAttributeCodeDTO.AttributeDescription = fields[1];
                    vRSAttributeCodeDTO.Sortkey = fields[2];

                    if (recNo != 0)
                    {
                        rawRecords.Add(vRSAttributeCodeDTO);
                    }
                    recNo++;
                }
                sr.Close();
            }
            return rawRecords;
        }

        private void lodAttributeDetail(IDbConnection stagedConn, Int32 loadId, List<AtcddDTO> currDetails)
        {
            foreach (AtcddDTO dto in currDetails)
            {

                List<AtcddDTO> atcdds = contextController.atccdList.Where(x => x.ATRBCD.Trim().Equals(dto.ATRBCD.Trim())).ToList();
                if (atcdds.Count == 0)
                {
                    log.logMsg(LogController.LogClass.Warning,
                        string.Format("atcdd filtered out because attribute does not exist Attribute:({0}), Value:({1})",dto.ATRBCD, dto.VALU));
                }
                else
                {
                    IDbCommand cmd = stagedConn.CreateCommand();

                    cmd.Parameters.Add(createParameter(cmd, dto.ATRBCD, "@AttribeCode"));
                    cmd.Parameters.Add(createParameter(cmd, dto.VALU, "@AttribeValue"));
                    cmd.CommandText = string.Format("INSERT INTO VRSAttributeCodeDetail(LoadId, AttributeTypeCode, AttributeValue) VALUES ({0}, @AttribeCode, @AttribeValue)", loadId);

                    cmd.ExecuteNonQuery();
                }
            }
        }
    }
}
