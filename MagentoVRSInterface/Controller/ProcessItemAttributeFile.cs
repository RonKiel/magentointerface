﻿using MagentoVRSInterface.DTO;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagentoVRSInterface.Controller
{
    public class ProcessItemAttributeFile : FileProcessor
    {
        LogController log = LogController.Instance;

        public override List<KeyValuePair<string, StringBuilder>> processFile(System.Data.IDbConnection stagedConn, string sourceFile)
        {
            List<VRSItemAttribDTO> rawRecs = buildDTOFromFile(sourceFile, contextController.loadId);
            base.replaceItemSkuWithExtendedVersion(rawRecs, "SKU");
            base.reassignProductIdsInPlaceOfSkusForSimpleProducts(rawRecs, "SKU");
            return processFile(stagedConn, rawRecs);
        }

        public List<KeyValuePair<string, StringBuilder>> processFile(System.Data.IDbConnection stagedConn, List<VRSItemAttribDTO> rawRecs)
        {
            string DestDirectory = ConfigurationManager.AppSettings["FileBuildLocation"].ToString();
            List<KeyValuePair<string, StringBuilder>> rtnList = new List<KeyValuePair<string, StringBuilder>>(); //Note, returns empty list, because we don't write any files from this process.

            BulkInsert((SqlConnection)stagedConn, "VRSItemAttrib", rawRecs); //HACK sql server specific.
            contextController.itemAttributesAll = populateItemAttributeContextAllValues(rawRecs);

            //Add Update file.
            StringBuilder chgFileTextSb = new StringBuilder();
            List<VRSItemAttribDTO> detailChgRecs = buildChangedRecordsDetail(stagedConn, contextController.loadId, contextController.priorGoodLoad);
            contextController.itemHaveNewItemAttributes = createUniqueHashSetOfSkusFromVRSItemAttributes(detailChgRecs);


            //Delete file.
            StringBuilder deleteFileTextSb = new StringBuilder();
            List<VRSItemAttribDTO> deleteRecs = buildChangedRecordsDetail(stagedConn, contextController.priorGoodLoad, contextController.loadId);
            contextController.itemHaveRemovedItemAttributes = createUniqueHashSetOfSkusFromVRSItemAttributes(deleteRecs);
            
            return rtnList; 
        }

        public HashSet<string> createUniqueHashSetOfSkusFromVRSItemAttributes(List<VRSItemAttribDTO> detailChgRecs)
        {
            HashSet<string> itemHasattributeChange = new HashSet<string>();

            foreach (VRSItemAttribDTO dto in detailChgRecs)
            {
                if (!itemHasattributeChange.Contains(dto.SKU.Trim()))
                {
                    itemHasattributeChange.Add(dto.SKU.Trim());
                }
            }
            return itemHasattributeChange;
        }

        public Dictionary<string, List<VRSItemAttribDTO>> populateItemAttributeContextAllValues(List<VRSItemAttribDTO> rawRecs)
        {
            Dictionary<string, List<VRSItemAttribDTO>> rtnDict = new Dictionary<string, List<VRSItemAttribDTO>>();
            foreach (VRSItemAttribDTO dto in rawRecs)
            {
                if (rtnDict.Keys.Contains(dto.SKU.Trim()))
                {
                    rtnDict[dto.SKU.Trim()].Add(dto);
                }
                else
                {
                    rtnDict.Add(dto.SKU.Trim(), new List<VRSItemAttribDTO>() { dto });
                }
            }
            return rtnDict;
        }

        private void buildWriteBufferForChanges(List<VRSItemAttribDTO> detailChgRecs, StringBuilder chgFileText)
        {
            List<string> flds = new List<string>(new string[] { "SKU", "AttributeTypeCode", "AttributeValue" });
            string headers = string.Join("\t", flds) + "\n";
            chgFileText.Append(headers);

            foreach (VRSItemAttribDTO dto in detailChgRecs)
            {
                string tRec = "";

                tRec += ((dto.SKU != null) ? dto.SKU.Trim() : "") + "\t";
                tRec += ((dto.AttributeTypeCode != null) ? dto.AttributeTypeCode.Trim() : "") + "\t";
                tRec += ((dto.AttributeValue != null) ? dto.AttributeValue.Trim() : "") + "\t";
                tRec += ((dto.Sortkey != null) ? dto.Sortkey.Trim() : "") + "\n";

                chgFileText.Append(tRec);
            }
        }

        private List<VRSItemAttribDTO> buildChangedRecordsDetail(IDbConnection stagedConn, Int32 loadId, Int32 priorGoodId)
        {
            List<VRSItemAttribDTO> rtnList = new List<VRSItemAttribDTO>();

            string tableName = " VRSItemAttrib ";
            string orderBy = " SKU ";
            string headers = generateHeaderRecord(new VRSItemAttribDTO(), ",") + " ";

            string sql = " SELECT " + headers;
            sql += " FROM " + tableName + " c ";
            sql += string.Format(" WHERE loadId = {0} ", loadId);
            sql += " EXCEPT ";
            sql += " SELECT " + headers;
            sql += " FROM " + tableName + " c2 ";
            sql += string.Format(" WHERE loadId = {0}", priorGoodId);
            sql += " Order by " + orderBy + " ";

            DataTable changedRecs = findChangedRecords(stagedConn, sql);

            foreach (DataRow dr in changedRecs.Rows)
            {
                VRSItemAttribDTO dto = new VRSItemAttribDTO();
                populateDTOFromDataRow(dr, dto);
                dto.LoadId = loadId;
                rtnList.Add(dto);
            }
            return rtnList;
        }

        protected List<VRSItemAttribDTO> buildDTOFromFile(string sourceFile, Int32 loadId)
        {
            List<VRSItemAttribDTO> rawRecords = new List<VRSItemAttribDTO>();

            using (StreamReader sr = new StreamReader(sourceFile))
            {
                string line;
                Int32 recNo = 0;
                while ((line = sr.ReadLine()) != null)
                {
                    List<string> fields = line.Split('\t').ToList();
                    VRSItemAttribDTO vRSItemAttribDTO = new VRSItemAttribDTO();
                    vRSItemAttribDTO.LoadId = loadId;

                    vRSItemAttribDTO.SKU = fields[0];
                    vRSItemAttribDTO.AttributeTypeCode = fields[1];
                    vRSItemAttribDTO.AttributeValue = fields[2];
                    vRSItemAttribDTO.Sortkey = fields[3];

                    if (recNo != 0)
                    {
                        if (!contextController.validSkuSet.Contains(base.removeAddDateAndTime(vRSItemAttribDTO.SKU)))
                        {
                            log.logMsg(LogController.LogClass.Debug, string.Format("ItemAttribute filtered out because SKU does not exist sku:({0}), Attribute:({1})", vRSItemAttribDTO.SKU, vRSItemAttribDTO.AttributeTypeCode));
                        }
                        else
                        {
                            List<AtcddDTO> atcdds = contextController.atccdList.Where(x => x.ATRBCD.Trim().Equals(vRSItemAttribDTO.AttributeTypeCode.Trim())).ToList();
                            if (atcdds.Count == 0)
                            {
                                log.logMsg(LogController.LogClass.Warning,
                                    string.Format("ItemAttribute filtered out because attribute does not exist.  sku:({0}), Attribute:({1}), Value:({2})",
                                    vRSItemAttribDTO.SKU, vRSItemAttribDTO.AttributeTypeCode, vRSItemAttribDTO.AttributeValue));
                            }
                            else
                            {
                                AtcddDTO myCode = atcdds.Where(x => x.VALU.Trim().Equals(vRSItemAttribDTO.AttributeValue.Trim())).FirstOrDefault();
                                if (myCode == null)
                                {
                                    string validOptionsAre = getValidValuesForAttribute(atcdds);

                                    log.logMsg(LogController.LogClass.Warning,
                                        string.Format("ItemAttribute filtered out because attribute VALUE does not exist.   sku:({0}), Attribute:({1}), Value:({2})  Legal values are:({3})",
                                        vRSItemAttribDTO.SKU, vRSItemAttribDTO.AttributeTypeCode, vRSItemAttribDTO.AttributeValue.Trim(), validOptionsAre));
                                }
                                else
                                {
                                    rawRecords.Add(vRSItemAttribDTO);
                                }
                            }
                        }
                    }
                    recNo++;
                }
                sr.Close();
            }
            return rawRecords;
        }

        private string getValidValuesForAttribute(List<AtcddDTO> atcdds)
        {
            List<string> validList = new List<string>();
            foreach (AtcddDTO dto in atcdds)
            {
                validList.Add(dto.VALU.Trim());
            }
            string validOptionsAre = string.Join(",", validList);
            return validOptionsAre;
        }

    }
}
