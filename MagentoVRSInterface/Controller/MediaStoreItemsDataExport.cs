﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagentoVRSInterface.Controller
{
    public class MediaStoreItemsDataExport
    {
        LogController log = LogController.Instance;
        UtilController utilController = new UtilController();

        public void sendFilesToMediaStore(Int32 sequence)
        {
            string imageFile = ConfigurationManager.AppSettings["MediaStoreImageFile"].ToString();
            string triggerFile = ConfigurationManager.AppSettings["MediaStoreImageTriggerFile"].ToString();

            sendFile(sequence, imageFile, "titemsfile.tab");
            sendFile(sequence, triggerFile, "mediastore_nmc.dms");
        }

        private void sendFile(Int32 sequence, string fileWithPath, string fileName)
        {
            StringBuilder sb = new StringBuilder();
            string rawData = File.ReadAllText(fileWithPath);
            sb.Append(rawData);
            KeyValuePair<string, StringBuilder> kp = new KeyValuePair<string, StringBuilder>(fileName, sb);
            sendImageDataToMediaStore(kp, sequence);
        }

        public void sendImageDataToMediaStore(KeyValuePair<string, StringBuilder> currFile, Int32 sequence)
        {
            string srcDir = ConfigurationManager.AppSettings["MediaStoreTmpImageUploadDirOnSrc"].ToString();
            string extention = currFile.Key.Substring(currFile.Key.IndexOf('.'));
            sendFileViaSFTPWithNOHostKey(currFile, srcDir, sequence, extention);
        }

        public void sendFileViaSFTPWithNOHostKey(KeyValuePair<string, StringBuilder> currFile, string tmpLocation, Int32 sequence, string extention)
        {
            string sftpDestLocation = ConfigurationManager.AppSettings["MediaStoreTmpImageUploadDirOnDest"].ToString();
            string server = ConfigurationManager.AppSettings["MediaStoreFTPUrl"].ToString();
            string userName = ConfigurationManager.AppSettings["MediaStoreFTPUserName"].ToString();
            string pw = ConfigurationManager.AppSettings["MediaStoreFTPPw"].ToString();

            string fileName = currFile.Key.Replace(extention, ".tmp");
            string fileNameWithSeq = currFile.Key.Replace(extention, "") + sequence.ToString("D8") + ".tmp";
            string tmpFileName = tmpLocation + @"\" + fileName;

            File.WriteAllText(tmpFileName, currFile.Value.ToString());

            string winscpOptions = "/command \"option batch abort\" \"option confirm off\"";
            string winscpOpen = string.Format("\"open sftp://{0}:{1}@{2}/\"", userName, pw, server);

            ftpTransmitFileWithWINSCP(sftpDestLocation, fileName, fileNameWithSeq, tmpFileName, winscpOptions, winscpOpen, extention);
        }

        public void ftpTransmitFileWithWINSCP(string sftpDestLocation, string fileName, string fileNameWithSeq, string tmpFileName, string winscpOptions, string winscpOpen, string extention)
        {
            string ftpTempName = sftpDestLocation + @"/" + fileNameWithSeq;
            string ftpRealName = sftpDestLocation + @"/" + fileName.Replace(".tmp", extention);
            //string winscpPut = string.Format("\"put {0} {1}\"", tmpFileName, ftpTempName);
            //string winscpMv = string.Format("\"mv {0} {1} \"", ftpTempName, ftpRealName);
            string winscpPut = string.Format("\"put {0} {1}\"", tmpFileName, ftpRealName);
            string completeSection = "\"exit\"";
            string execName = string.Format("\"{0}\"", ConfigurationManager.AppSettings["SFTPExec"].ToString());
            //string wincpCmd = string.Format("{0} {1} {2} {3} {4} ", winscpOptions, winscpOpen, winscpPut, winscpMv, completeSection);
            string wincpCmd = string.Format("{0} {1} {2} {3} ", winscpOptions, winscpOpen, winscpPut, completeSection);

            Process p = new Process();
            p.StartInfo.UseShellExecute = false;
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.FileName = execName;
            p.StartInfo.Arguments = wincpCmd;
            p.Start();
            string output = p.StandardOutput.ReadToEnd();
            p.WaitForExit();
            File.Delete(tmpFileName);
            log.logMsg(LogController.LogClass.Info, string.Format("SFTP Result({0})", output));
            if (output.Contains("Warning") || output.Contains("Terminated by user"))
            {
                log.logMsg(LogController.LogClass.Error, string.Format("Error occured on SFTP SEND, SFTP Result({0})", output));
            }
        }
    }
}
