﻿using MagentoVRSInterface.DTO;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagentoVRSInterface.Controller
{
    public class ProcessProductCatFile : FileProcessor, IDeferedWriteOfVRSData
    {
        Dictionary<string, string> categoryIdToUrlPathDict = null;
        Dictionary<string, string> productMap = null;

        public override List<KeyValuePair<string, StringBuilder>> processFile(System.Data.IDbConnection stagedConn, string sourceFile )
        {
            this.productMap = contextController.productMap;
            ProcessCategoryFile processCategoryFile = new ProcessCategoryFile();
            categoryIdToUrlPathDict = processCategoryFile.getCategoryCodeToUrlPathDict(stagedConn, contextController.loadId);

            List<VRSProductcatDTO> rawRecsFromFile = buildDTOFromFile(sourceFile, contextController.loadId);
            return processFile(stagedConn, rawRecsFromFile, contextController.loadId, contextController.priorGoodLoad);
        }

        public List<KeyValuePair<string, StringBuilder>> processFile(System.Data.IDbConnection stagedConn, List<VRSProductcatDTO> rawRecsList, int loadId, Int32 priorGoodId)
        {
            List<KeyValuePair<string, StringBuilder>> rtnList = new List<KeyValuePair<string, StringBuilder>>();

            BulkInsert((SqlConnection)stagedConn, "VRSProductCat", rawRecsList); //HACK sql server specific.
            BulkInsertMinorsToCategory(stagedConn);

            return rtnList;
        }

        public void buildDataFileToWrite(IDbConnection stagedConn, int loadId, int priorGoodId, List<KeyValuePair<string, StringBuilder>> rtnList)
        {
            StringBuilder chgFileText = new StringBuilder();

            callStoredProcedure(stagedConn, "populateNewArrivalsProductCat");
            callStoredProcedure(stagedConn, "populatePreorder");
            callStoredProcedure(stagedConn, "populateComingSoon");

            buildWriteBufferForChanges(stagedConn, loadId, priorGoodId, chgFileText);
            string DestDirectory = ConfigurationManager.AppSettings["FileBuildLocation"].ToString();

            string chgFile = DestDirectory + "tproductcatfile" + loadId.ToString("D8") + ".tab";
            rtnList.Add(new KeyValuePair<string, StringBuilder>(chgFile, chgFileText));
        }


        private void BulkInsertMinorsToCategory(System.Data.IDbConnection stagedConn)
        {
            IDbCommand cmd = stagedConn.CreateCommand();
            cmd.CommandTimeout = 1000000;

            string sqlTxt = " select c.LoadId, c.CategoryCode, c.SortKey, c.MfgCode, c.CommonPartNum, " +
                            " i.SKU as Productkey, c.IsPrimary, CHARINDEX('-', i.sku,0) as CharAt " +
                            " INTO #ItemWithCat " +
                            " from VRSTItems i JOIN VRSProductCat c ON c.ProductKey = i.ProductKey and c.LoadId = (select max(loadId) from VRSHist) " +
                            " WHERE i.LoadId = (select max(loadId) from VRSHist); ";
            cmd.CommandText = sqlTxt;
            Int32 allRecsInTempTable = cmd.ExecuteNonQuery();

            sqlTxt = " INSERT INTO VRSProductCat " +
                            " select LoadId, CategoryCode, SortKey, MfgCode, CommonPartNum, " +
                            " Productkey, IsPrimary " +
                            " FROM #ItemWithCat " +
                            " WHERE CharAt = 7; ";
            cmd.CommandText = sqlTxt;
            Int32 minorsInserted = cmd.ExecuteNonQuery();
            sqlTxt = " drop table #ItemWithCat;";
            cmd.ExecuteNonQuery();

            LogController.Instance.logMsg(LogController.LogClass.Info, string.Format("ProductCatFile MinorRecsInsertedPreDiff:({0})", minorsInserted));
        }

        private void buildWriteBufferForDeletes(System.Data.IDbConnection stagedConn, int loadId, Int32 priorGoodId, StringBuilder deleteFileText)
        {
            List<VRSProductcatDTO> deleteRecords = buildDeletedRecords(stagedConn, loadId, priorGoodId);
        }

        private void buildWriteBufferForChanges(System.Data.IDbConnection stagedConn, int loadId, Int32 priorGoodId, StringBuilder chgFileText)
        {
            List<string> urlFilteredOut = new List<string>(new string[] { @"new-arrivals/clearance", @"preorder/clearance", @"coming-soon/clearance" });
            List<VRSProductcatDTO> changedRecords = buildChangedRecords(stagedConn, loadId, priorGoodId);
            ProcessProductsFile  processProductsFile = new ProcessProductsFile();

            foreach (VRSProductcatDTO dto in changedRecords)
            {
                //If already converted into Magento ID, don't do it again.
                Int32 numberOfDashes = dto.ProductKey.Count(x => x == '-');
                string extendedProductKey = (numberOfDashes == 0) ? processProductsFile.calculateMagentoProductKey(dto.ProductKey) : dto.ProductKey;
                string urlPath = categoryIdToUrlPathDict[dto.CategoryCode.Trim()];

                bool filteredRec = isFilteredRecord(urlFilteredOut, urlPath);

                if (!filteredRec)
                {
                    string tRec = string.Format("CCP\t{0}\t{1}\t{2}\n", urlPath, extendedProductKey, dto.SortKey);
                    chgFileText.Append(tRec);
                }
            }
        }

        private static bool isFilteredRecord(List<string> filteredOutUrls, string urlPath)
        {
            bool filteredRec = false;
            foreach (string filter in filteredOutUrls)
            {
                if (urlPath.Contains(filter))
                {
                    filteredRec = true;
                }
            }
            return filteredRec;
        }

        private List<VRSProductcatDTO> buildDTOFromFile(string sourceFile, Int32 loadId)
        {
            ProcessProductsFile processProductsFile = new ProcessProductsFile();
            List<VRSProductcatDTO> rawRecords = new List<VRSProductcatDTO>();

            using (StreamReader sr = new StreamReader(sourceFile))
            {
                string line;
                Int32 recNo = 0;
                while ((line = sr.ReadLine()) != null)
                {
                    List<string> fields = line.Split('\t').ToList();
                    VRSProductcatDTO vRSProductcatDTO = new VRSProductcatDTO();

                    vRSProductcatDTO.LoadId = loadId;

                    vRSProductcatDTO.CategoryCode = fields[0];
                    vRSProductcatDTO.SortKey = fields[1];
                    vRSProductcatDTO.MfgCode = fields[2];
                    vRSProductcatDTO.CommonPartNum = fields[3];
                    vRSProductcatDTO.ProductKey = fields[4];
                    vRSProductcatDTO.IsPrimary = fields[5];

                    string expandedProductKey = processProductsFile.calculateMagentoProductKey(vRSProductcatDTO.ProductKey);

                    if (recNo != 0)
                    {
                        //if (contextController.validProductSet.Contains(expandedProductKey))
                       // {
                            if (categoryIdToUrlPathDict.Keys.Contains(vRSProductcatDTO.CategoryCode.Trim()))
                            {
                                rawRecords.Add(vRSProductcatDTO);
                            }
                            else
                            {
                                LogController.Instance.logMsg(LogController.LogClass.Warning, string.Format("ProductCatFile Product:({0}) has invalid cat code of:({1}).",expandedProductKey,vRSProductcatDTO.CategoryCode));
                            }
                        ////}
                        //else
                        //{
                        //    LogController.Instance.logMsg(LogController.LogClass.Debug, string.Format("ProductCatFile Product:({0}) not found in product file.", expandedProductKey));
                        //}
                    }
                    recNo++;
                }
                sr.Close();
            }
            return rawRecords;
        }

        private List<VRSProductcatDTO> buildChangedRecords(IDbConnection stagedConn, Int32 loadId, Int32 priorGoodId)
        {
            List<VRSProductcatDTO> rtnList = new List<VRSProductcatDTO>();

            string tableName = "VRSProductCat ";
            string orderBy = "ProductKey ";
            string headers = generateHeaderRecord(new VRSProductcatDTO(), ",") + " ";

            string sql = " SELECT " + headers;
            sql += " FROM " + tableName + " c ";
            sql += string.Format(" WHERE loadId = {0} ", loadId);
            sql += " EXCEPT ";
            sql += " SELECT " + headers;
            sql += " FROM " + tableName + " c2 ";
            sql += string.Format(" WHERE loadId = {0}", priorGoodId);
            sql += " Order by " + orderBy + " ";

            DataTable changedRecs = findChangedRecords(stagedConn, sql);

            foreach (DataRow dr in changedRecs.Rows)
            {
                VRSProductcatDTO dto = new VRSProductcatDTO();
                populateDTOFromDataRow(dr, dto);
                dto.LoadId = loadId;
                rtnList.Add(dto);
            }
            return rtnList;
        }

        private List<VRSProductcatDTO> buildDeletedRecords(IDbConnection stagedConn, Int32 loadId, Int32 priorGoodId)
        {
            List<VRSProductcatDTO> rtnList = new List<VRSProductcatDTO>();

            string tableName = "VRSProductCat ";
            string orderBy = "ProductKey ";
            string headers = "ProductKey ";

            string sql = " SELECT " + headers;
            sql += " FROM " + tableName + " c ";
            sql += string.Format(" WHERE loadId = {0} ", priorGoodId);
            sql += " EXCEPT ";
            sql += " SELECT " + headers;
            sql += " FROM " + tableName + " c2 ";
            sql += string.Format(" WHERE loadId = {0}", loadId);
            sql += " Order by " + orderBy + " ";

            DataTable changedRecs = findChangedRecords(stagedConn, sql);

            foreach (DataRow dr in changedRecs.Rows)
            {
                VRSProductcatDTO dto = new VRSProductcatDTO();
                dto.LoadId = loadId;
                dto.ProductKey = dr["ProductKey"].ToString();
                rtnList.Add(dto);
            }
            return rtnList;
        }


    }

}
