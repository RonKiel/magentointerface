﻿using System;
using System.Collections.Generic;
namespace MagentoVRSInterface.Controller
{
    public interface ILogController
    {
        void logMsg(LogController.LogClass logClass, Exception ex1);
        void logMsg(LogController.LogClass logClass, string logMessage);
        void logMsg(LogController.LogClass logClass, string stackTrace, string logMessage);
        void setupDBConnection(string connectionString, string providerName);
        void ignoreLogClass(LogController.LogClass logClass);
        void clearLogIgnoreList();
        List<MagentoVRSInterface.Controller.LogController.LogClass> returnIgnoreList();
    }
}
