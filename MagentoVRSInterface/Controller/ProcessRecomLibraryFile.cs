﻿using MagentoVRSInterface.DTO;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagentoVRSInterface.Controller
{
    public class ProcessRecomLibraryFile : FileProcessor
    {
        LogController log = LogController.Instance;
        ProcessCategoryFile processCategoryFile = new ProcessCategoryFile();

        public List<string> codesToSkip = new List<string>(new string[] { "10000", "" });
        Dictionary<string, string> categoryRecs = null;
        Dictionary<string, string> categoryTitleRecs = null;

        public override List<KeyValuePair<string, StringBuilder>> processFile(System.Data.IDbConnection stagedConn, string sourceFile)
        {
            categoryRecs = processCategoryFile.getCategoryCodeToUrlPathDict(stagedConn, contextController.loadId);
            categoryTitleRecs = processCategoryFile.getCategoryCodeToTitle(stagedConn, contextController.loadId);
            List<VRSRecomLibraryDTO> rawRecsFromFile = buildDTOFromFile(sourceFile, contextController.loadId);
            return processFile(stagedConn, rawRecsFromFile, contextController.loadId, contextController.priorGoodLoad);
        }

        public List<KeyValuePair<string, StringBuilder>> processFile(System.Data.IDbConnection stagedConn, List<VRSRecomLibraryDTO> rawRecsList, int loadId, Int32 priorGoodId)
        {
            StringBuilder chgFileText = new StringBuilder();
            List<KeyValuePair<string, StringBuilder>> rtnList = new List<KeyValuePair<string, StringBuilder>>();

            BulkInsert((SqlConnection)stagedConn, "VRSRecomLibrary", rawRecsList); //HACK sql server specific.

            buildWriteBufferForChanges(stagedConn, loadId, priorGoodId, chgFileText);
            buildWriteBufferForDeletes(stagedConn, loadId, priorGoodId, contextController.catAndBasketDeleteBuffer); 
            string DestDirectory = ConfigurationManager.AppSettings["FileBuildLocation"].ToString();

            string chgFile = DestDirectory + "recomlibrary" + loadId.ToString("D8") + ".tab";
            rtnList.Add(new KeyValuePair<string, StringBuilder>(chgFile, chgFileText));

            return rtnList;
        }

        private void buildWriteBufferForDeletes(System.Data.IDbConnection stagedConn, int loadId, Int32 priorGoodId, StringBuilder deleteFileText)
        {
            List<VRSRecomLibraryDTO> deleteRecords = buildDeletedRecords(stagedConn, loadId, priorGoodId); //Just the Library code is valid!
            List<string> libraryCodesToDeactivate = new List<string>();
            foreach(VRSRecomLibraryDTO dto in deleteRecords) 
            {
                libraryCodesToDeactivate.Add(dto.LibraryCode.Trim());
            }
            if (libraryCodesToDeactivate.Count == 0)
            {
                return;
            }
            List<VRSRecomLibraryDTO> oldPriorRecords = buildChangedRecords(stagedConn, priorGoodId, -1);
            List<VRSRecomLibraryDTO> recordsToDeactivate = oldPriorRecords.Where(x => libraryCodesToDeactivate.Contains(trimWithNull(x.LibraryCode))).ToList();

            ProcessCategoryFile processCategoryFile = new ProcessCategoryFile();
            Dictionary<string, string> categoryRecs = processCategoryFile.getCategoryCodeToUrlPathDict(stagedConn, loadId);

            foreach (VRSRecomLibraryDTO dto in recordsToDeactivate)
            {
                string trec = "";

                if (!codesToSkip.Contains(dto.RootCatCode.Trim()) && categoryRecs.Keys.Contains(dto.RootCatCode.Trim()))
                {
                    string urlKey = base.urlKeyFormatting(dto.Title.ToLower().Trim());
                    string urlPath = string.Format("baskets/{0}/{1}", categoryRecs[dto.RootCatCode.Trim()], urlKey);
                    trec += "-CC\t" + urlPath + "\n";
                    deleteFileText.Append(string.Format("-CC\t{0}\n", urlPath));
                }
            }
        }

        private string trimWithNull(string src)
        {
            if (string.IsNullOrWhiteSpace(src))
            {
                return "";
            }
            else
            {
                return src.Trim();
            }
        }

        private void buildWriteBufferForChanges(System.Data.IDbConnection stagedConn, int loadId, Int32 priorGoodId, StringBuilder chgFileText)
        {
            List<VRSRecomLibraryDTO> changedRecords = buildChangedRecords(stagedConn, loadId, priorGoodId);

            List<string> flds = new List<string>(new string[] { "Title","url_key", "url_path", "Visible", "Comments", "StartDate",	"Expire","SortKey","LibraryCode","RootCatCode", "BasketURL" });
            string headers = string.Join("\t", flds) + "\n";
            chgFileText.Append(headers);

            foreach (VRSRecomLibraryDTO dto in changedRecords)
            {
                string trec = "";

                if (dto.RootCatCode == null)
                {
                    //We are processing a header record.
                    string urlKey = "";
                    string urlPath = "";
                    string visable = (dto.Visible.Trim() == "0") ? "No" : "Yes";
                    string title = dto.Title;

                    List<string> titleParts = dto.Title.Split('/').ToList();
                    if (titleParts.Count == 1)
                    {
                        urlKey = base.urlKeyFormatting(dto.Title).ToLower();
                        urlPath = urlKey;
                    }
                    else if (titleParts.Count == 2)
                    {
                        title = dto.Comments; //This is bad.  I saved the nice friedly title here, bacause I used the title for the url path.
                        urlKey = base.urlKeyFormatting(titleParts[1].Trim()).ToLower();
                        urlPath = string.Format("{0}/{1}",titleParts[0] ,titleParts[1]);
                    }
                    trec += title + "\t";
                    trec += urlKey + "\t";
                    trec += urlPath + "\t";
                    trec += visable + "\t";
                    trec += "\t";
                    trec += "\t";  
                    trec += "\t";
                    trec += "\t";
                    trec += "\t";
                    trec += "\t";
                }
                else
                {
                    string urlKey = base.urlKeyFormatting(dto.Title.ToLower().Trim());
                    string urlPath = string.Format("baskets/{0}/{1}", categoryRecs[dto.RootCatCode.Trim()], urlKey);
                    string visable = (dto.Visible.Trim() == "0") ? "No" : "Yes";

                    trec += dto.Title + "\t";
                    trec += urlKey + "\t";
                    trec += urlPath + "\t";
                    trec += visable + "\t";
                    trec += dto.Comments + "\t";
                    trec += "\t";  //TODO Hack We don't have a Start Date
                    trec += dto.Expire + "\t";
                    trec += dto.SortKey + "\t";
                    trec += dto.LibraryCode + "\t";
                    trec += dto.RootCatCode.Trim() + "\t";
                    trec += dto.BasketImageSku;

                }

                chgFileText.Append(trec + "\n");
            }
        }

        private List<VRSRecomLibraryDTO> buildDTOFromFile(string sourceFile, Int32 loadId)
        {
            List<VRSRecomLibraryDTO> rawRecords = new List<VRSRecomLibraryDTO>();
            string basketURLTemplate = ConfigurationManager.AppSettings["BasketURL"].ToString();


            using (StreamReader sr = new StreamReader(sourceFile))
            {
                string line;
                Int32 recNo = 0;
                while ((line = sr.ReadLine()) != null)
                {
                    List<string> fields = line.Split('\t').ToList();
                    VRSRecomLibraryDTO vRSRecomLibraryDTO = new VRSRecomLibraryDTO();

                    vRSRecomLibraryDTO.LoadId = loadId;

                    vRSRecomLibraryDTO.LibraryCode = fields[0];
                    vRSRecomLibraryDTO.Title = fields[1];
                    vRSRecomLibraryDTO.Visible = fields[2];
                    vRSRecomLibraryDTO.Expire = fields[3];
                    vRSRecomLibraryDTO.SortKey = fields[4];
                    vRSRecomLibraryDTO.Comments = fields[5];
                    vRSRecomLibraryDTO.RootCatCode = fields[6];

                    if (!string.IsNullOrWhiteSpace(fields[7]))
                    {
                        vRSRecomLibraryDTO.BasketImageSku = basketURLTemplate.Replace("{SKU}",fields[7].Trim());
                    }
                    else
                    {
                        vRSRecomLibraryDTO.BasketImageSku = "";
                    }

                    if (recNo != 0)
                    {
                        if (vRSRecomLibraryDTO == null || codesToSkip == null || vRSRecomLibraryDTO.RootCatCode == null || categoryRecs == null)
                        {
                            log.logMsg(MagentoVRSInterface.Controller.LogController.LogClass.Warning, "Ignore null in recomlibraryfile." );
                        }
                        else
                        {
                            string rootCat = vRSRecomLibraryDTO.RootCatCode.Trim();
                            if ( !codesToSkip.Contains(vRSRecomLibraryDTO.RootCatCode.Trim()) && categoryRecs.Keys.Contains(rootCat))
                            {
                                rawRecords.Add(vRSRecomLibraryDTO);
                            }
                            else
                            {
                                log.logMsg(MagentoVRSInterface.Controller.LogController.LogClass.Warning,
                                    string.Format("Library code:({0}) Title:({1})  Recomlibrary rec filtered out because of root Cat code of:({2})",
                                    vRSRecomLibraryDTO.LibraryCode, vRSRecomLibraryDTO.Title,
                                    vRSRecomLibraryDTO.RootCatCode.Trim()));
                            }
                        }
                    }
                    recNo++;
                }
                sr.Close();
            }

            List<VRSRecomLibraryDTO> rtnRecords = appendHighLevelLibraryRecords(rawRecords);
            return rtnRecords;
        }

        private List<VRSRecomLibraryDTO> appendHighLevelLibraryRecords(List<VRSRecomLibraryDTO> rawRecords)
        {
            List<VRSRecomLibraryDTO> rtnRecords = new List<VRSRecomLibraryDTO>();
            rtnRecords.Add(new VRSRecomLibraryDTO() { LoadId = contextController.loadId, Visible = "1", Title = "Baskets" });

            Dictionary<string, string> rootCategoryDict = new Dictionary<string, string>();

            foreach (VRSRecomLibraryDTO dto in rawRecords)
            {
                if (dto.RootCatCode != null && !codesToSkip.Contains(dto.RootCatCode.Trim()) && categoryRecs.Keys.Contains(dto.RootCatCode.Trim()))
                {
                    if (!rootCategoryDict.Keys.Contains(dto.RootCatCode.Trim()))
                    {
                        rootCategoryDict.Add(dto.RootCatCode.Trim(), categoryRecs[dto.RootCatCode.Trim()]);
                    }
                }
            }

            foreach (string rootCode in rootCategoryDict.Keys)
            {
                string realTitle = categoryTitleRecs[rootCode];
                rtnRecords.Add(new VRSRecomLibraryDTO() { LoadId = contextController.loadId, Visible = "1", Title = @"baskets/" + rootCategoryDict[rootCode], Comments = realTitle });
            }

            foreach (VRSRecomLibraryDTO dto in rawRecords)
            {
                rtnRecords.Add(dto);
            }
            return rtnRecords;
        }

        public List<VRSRecomLibraryDTO> buildChangedRecords(IDbConnection stagedConn, Int32 loadId, Int32 priorGoodId)
        {
            List<VRSRecomLibraryDTO> rtnList = new List<VRSRecomLibraryDTO>();

            string tableName = "VRSRecomLibrary ";
            string orderBy = "LibraryCode ";
            string headers = generateHeaderRecord(new VRSRecomLibraryDTO(), ",") + " ";

            string sql = " SELECT " + headers;
            sql += " FROM " + tableName + " c ";
            sql += string.Format(" WHERE loadId = {0} ", loadId);
            sql += " EXCEPT ";
            sql += " SELECT " + headers;
            sql += " FROM " + tableName + " c2 ";
            sql += string.Format(" WHERE loadId = {0}", priorGoodId);
            sql += " Order by " + orderBy + " ";

            DataTable changedRecs = findChangedRecords(stagedConn, sql);

            foreach (DataRow dr in changedRecs.Rows)
            {
                VRSRecomLibraryDTO dto = new VRSRecomLibraryDTO();
                populateDTOFromDataRow(dr, dto);
                dto.LoadId = loadId;
                rtnList.Add(dto);
            }
            return rtnList;
        }

        private List<VRSRecomLibraryDTO> buildDeletedRecords(IDbConnection stagedConn, Int32 loadId, Int32 priorGoodId)
        {
            List<VRSRecomLibraryDTO> rtnList = new List<VRSRecomLibraryDTO>();

            string tableName = "VRSRecomLibrary ";
            string orderBy = "LibraryCode ";
            string headers = "LibraryCode ";

            string sql = " SELECT " + headers;
            sql += " FROM " + tableName + " c ";
            sql += string.Format(" WHERE loadId = {0} ", priorGoodId);
            sql += " EXCEPT ";
            sql += " SELECT " + headers;
            sql += " FROM " + tableName + " c2 ";
            sql += string.Format(" WHERE loadId = {0}", loadId);
            sql += " Order by " + orderBy + " ";

            DataTable changedRecs = findChangedRecords(stagedConn, sql);

            foreach (DataRow dr in changedRecs.Rows)
            {
                VRSRecomLibraryDTO dto = new VRSRecomLibraryDTO();
                dto.LoadId = loadId;
                dto.LibraryCode = dr["LibraryCode"].ToString();
                rtnList.Add(dto);
            }
            return rtnList;
        }
    }
}
