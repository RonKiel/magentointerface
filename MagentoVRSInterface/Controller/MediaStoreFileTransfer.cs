﻿using MagentoVRSInterface.DAO;
using MagentoVRSInterface.DTO;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MagentoVRSInterface.Controller
{
    public class MediaStoreFileTransfer
    {
        LogController log = LogController.Instance;

        public bool CopyMediaStoreFiles(IDbConnection stagedConn, Int32 loadId)
        {
            bool returnValue = false;

            string mediaStoreServer         = ConfigurationManager.AppSettings["MediaStoreServer"].ToString();
            string mediaStoreSourceFolder   = ConfigurationManager.AppSettings["MediaStoreSourceFolder"].ToString();
            string mediaStoreLocalFolder    = ConfigurationManager.AppSettings["MediaStoreLocalFolder"].ToString();

            string mediaStoreFolder           = Path.Combine(mediaStoreServer, mediaStoreSourceFolder);

            MediaStoreFileHistoryDAO mediaStoreFileHistory = new MediaStoreFileHistoryDAO();

            List<string> filePaths = Directory.EnumerateFiles(mediaStoreFolder, "*.csv").ToList<string>();
            try
            {
                foreach (string filePath in filePaths)
                {
                    string fileName = Path.GetFileName(filePath);
                    MediaStoreFileHistoryDTO mediaStoreFile = new MediaStoreFileHistoryDTO();

                    string localFileName = string.Format("{0}\\{1}", mediaStoreLocalFolder, fileName);

                    File.Copy(filePath, localFileName, true);

                    string rawFile = FilterImageFilesForOnlyActive(fileName, File.ReadAllText(localFileName));

                    if (fileName.Contains("_IT.csv"))
                    {
                        rawFile = OrderBySeqAndPreferredLimitToOnePerSku(rawFile);
                    }

                    mediaStoreFile.FTPEntry = localFileName;
                    mediaStoreFile.FileName = fileName;
                    mediaStoreFile.DateTime = File.GetCreationTime(localFileName);
                    mediaStoreFile.RawFile = rawFile;
                    mediaStoreFile.FileProcessed = "No";
                    mediaStoreFile.LoadId = loadId;

                    mediaStoreFileHistory.insert(stagedConn, mediaStoreFile);
                }

                returnValue = true;
            }
            catch (Exception exception)
            {
                log.logMsg(LogController.LogClass.Error, exception);
            }

            return returnValue;
        }
        private string FilterImageFilesForOnlyActive(string fileName, string mediaFile)
        {
            ContextController contextController = ContextController.Instance;

            string imageType            = fileName.Substring(fileName.Length - 6, 2);
            List<string> productRecs    = new List<string>() { "PT", "PM" };
            StringBuilder returnValue   = new StringBuilder();
            List<string> mediaLines     = mediaFile.Split('\n').ToList();

            Int32 i = 0;
            Int32 goodRecords = 0;
            Int32 badRecords = 0;

            foreach (string mediaLine in mediaLines)
            {
                List<string> flds = mediaLine.Replace("\r", "").Split(',').ToList();
                if (flds.Count > 2 && i > 0)
                {
                    bool hasErrorOccured = false;

                    if (productRecs.Contains(imageType))
                    {
                        string product = flds[1].Replace("'", "").Trim();
                        if (contextController.productMap != null && contextController.productMap.Keys.Count > 0)
                        {
                            if (!contextController.productMap.Keys.Contains(product))
                            {
                                log.logMsg(LogController.LogClass.Debug, string.Format("ImageStoreFile: product:{0} is not yet live.", product));
                                hasErrorOccured = true;
                            }
                        }
                    }
                    else
                    {
                        string sku = flds[2].Replace("'", "").Trim();
                        if (contextController.skuMap != null && contextController.skuMap.Keys.Count > 0)
                        {
                            if (!contextController.skuMap.Keys.Contains(PadToSize(sku, 6, "0")))
                            {
                                log.logMsg(LogController.LogClass.Debug, string.Format("ImageStoreFile: sku:{0} is not yet live.", sku));
                                hasErrorOccured = true;
                            }
                        }
                    }

                    if (!hasErrorOccured)
                    {
                        returnValue.Append(mediaLine + "\n");
                        goodRecords++;
                    }
                    else
                    {
                        badRecords++;
                    }
                }
                else
                {
                    returnValue.Append(mediaLine + "\n");
                }
                i++;
            }

            log.logMsg(LogController.LogClass.Info, string.Format("ImageStoreFile file:({0}) good records:({1}) bad records:({2})", fileName, goodRecords, badRecords));

            return returnValue.ToString();
        }
        private string OrderBySeqAndPreferredLimitToOnePerSku(string mediaFile)
        {
            List<string> rtnRecords = new List<string>();
            DataTable dt = new DataTable();

            List<string> mediaLines = mediaFile.Split('\n').ToList();
            List<string> fldHeaders = mediaLines[0].Replace("\r", "").Split(',').ToList();
            rtnRecords.Add(mediaLines[0].Replace("\r", ""));

            foreach (string hdr in fldHeaders)
            {
                dt.Columns.Add(new DataColumn(hdr));
            }

            Int32 i = 0;
            foreach (string mediaLine in mediaLines)
            {
                List<string> flds = mediaLine.Replace("\r", "").Replace("'", "").Split(',').ToList();
                if (flds.Count > 2 && i > 0)
                {
                    flds[2] = PadToSize(flds[2], 6, "0");
                    dt.Rows.Add(flds.ToArray());
                }
                i++;
            }

            DataView dv = new DataView(dt);
            dv.Sort = "SKU, Seq, Preferred ASC";

            HashSet<string> skuSet = new HashSet<string>();
            foreach (DataRowView drv in dv)
            {
                string rec = string.Format("{0},{1},{2},{3},{4},{5}",
                    drv["PK"],
                    drv["ProductKey"],
                    drv["SKU"],
                    drv["Seq"],
                    drv["Url"],
                    drv["Preferred"]);

                string currentSku = drv["SKU"].ToString();
                if (!skuSet.Contains(currentSku))
                {
                    rtnRecords.Add(rec);
                    skuSet.Add(currentSku);
                }
            }
            string rtnBuffer = string.Join("\n", rtnRecords);

            return rtnBuffer;
        }
        private string PadToSize(string source, Int32 size, string padChar)
        {
            if (source.Length >= size)
            {
                return source;
            }
            Int32 diffInSize = size - source.Length;
            string padding = "";
            for (int x = 0; x < diffInSize; x++)
            {
                padding += padChar;
            }
            return padding + source;
        }
    }
}
