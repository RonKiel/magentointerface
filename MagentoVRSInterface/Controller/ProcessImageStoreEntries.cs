﻿using MagentoVRSInterface.DAO;
using MagentoVRSInterface.DTO;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagentoVRSInterface.Controller
{
    public class ProcessImageStoreEntries : FileProcessor
    {
        MediaStoreFileHistoryDAO mediaStoreFileHistoryDAO = new MediaStoreFileHistoryDAO();
        ProcessProductsFile processProductsFile = new ProcessProductsFile();

        public override List<KeyValuePair<string, StringBuilder>> processFile(System.Data.IDbConnection stagedConn, string sourceFile)
        {
            List<KeyValuePair<string, StringBuilder>> rtnValue = new List<KeyValuePair<string, StringBuilder>>();
            string partnerCode = ConfigurationManager.AppSettings["PartnerCode"].ToString();
            string baseURL = ConfigurationManager.AppSettings["MediaStoreBaseUrl"].ToString();

            StringBuilder addChgSb = new StringBuilder();
            StringBuilder deleteSb = new StringBuilder();

            List<MediaStoreFileHistoryDTO> mediaFiles = mediaStoreFileHistoryDAO.selectRecordsRtnList(stagedConn, string.Format("LoadId = {0} AND fileName like '%_{1}_%' ", contextController.loadId, partnerCode));
            List<MediaStoreFileHistoryDTO> previousFiles = mediaStoreFileHistoryDAO.selectRecordsRtnList(stagedConn, string.Format("LoadId = {0} AND fileName like '%_{1}_%' ", contextController.priorGoodLoad, partnerCode));

            foreach (MediaStoreFileHistoryDTO dto in mediaFiles)
            {
                MediaStoreFileHistoryDTO prevFile = previousFiles.Where(x => x.FileName.Equals(dto.FileName)).SingleOrDefault();

                string mediaFile = dto.RawFile;
                string priorMediaFile = (prevFile != null) ? prevFile.RawFile : "";

                processAddOrChangedItemFiles(baseURL, addChgSb, dto, mediaFile, priorMediaFile);
                processDeleteItemFiles(deleteSb, dto, mediaFile, priorMediaFile);
            }

            string DestDirectory = ConfigurationManager.AppSettings["FileBuildLocation"].ToString();

            string chgFile = DestDirectory + "Imagefile" + contextController.loadId.ToString("D8") + ".tab";
            addChgSb.Append(deleteSb);
            rtnValue.Add(new KeyValuePair<string, StringBuilder>(chgFile, addChgSb));

            mediaStoreFileHistoryDAO.MarkFilesForRunComplete(stagedConn, null, contextController.loadId);
            return rtnValue;
        }

        private void processDeleteItemFiles(StringBuilder deleteSb, MediaStoreFileHistoryDTO dto, string mediaFile, string priorMediaFile)
        {
            Dictionary<Int32, string> mediaDictJustKeys = buildMediaDictJustPkKey(mediaFile);
            Dictionary<Int32, string> priorMediaDictJustKeys = buildMediaDictJustPkKey(priorMediaFile);
            List<Int32> keysToDelete = priorMediaDictJustKeys.Keys.Except(mediaDictJustKeys.Keys).ToList();

            foreach (Int32 recToDelete in keysToDelete)
            {
                List<string> flds = priorMediaDictJustKeys[recToDelete].Replace("\r", "").Replace("'", "").Split(',').ToList();

                string sku = lookupSku(flds[2]);
                string productSku = processProductsFile.calculateMagentoProductKey(flds[1]);

                string rec = "";
                string imageType = getImageType(dto.FileName);


                // Note that the following delete image lines are formated match the add images spec.
                // These are the columns specified by Perficient/RapidFlow to add images:
                // CPI,sku,image name,image label,position,disabled
                // Note that the image label field is empty, but it must be included for Magento to work properly.
                //
                if (imageType == "PM")
                {
                    //rec = string.Format("-CPI,{0},{1},,{2},\n", productSku, "", "").Replace(",", "\t");
                    rec = string.Format("-CPI,{0},{1},,{2},0\n", productSku, flds[4], flds[5]).Replace(",", "\t");
                    deleteSb.Append(rec);
                }
                else if (imageType == "IT")
                {
                    //rec = string.Format("-CPI,{0},{1},,{2},\n", sku, "", "").Replace(",", "\t");
                    rec = string.Format("-CPI,{0},{1},,{2},0\n", sku, flds[4], flds[5]).Replace(",", "\t");
                    deleteSb.Append(rec);
                }
                
                //deleteSb.Append(rec);
            }
        }

        private void processAddOrChangedItemFiles(string baseURL, StringBuilder addChgSb, MediaStoreFileHistoryDTO dto, string mediaFile, string priorMediaFile)
        {
            Dictionary<string, string> mediaDict = buildMediaDictAllFldsInKey(mediaFile, getImageType(dto.FileName));
            Dictionary<string, string> priorMediaDict = buildMediaDictAllFldsInKey(priorMediaFile, getImageType(dto.FileName));
            List<string> keysWithChanges = mediaDict.Keys.Except(priorMediaDict.Keys).ToList();

            foreach (string imageToAdd in keysWithChanges)
            {
                List<string> flds = mediaDict[imageToAdd].Replace("\r", "").Replace("'", "").Split(',').ToList();

                string sku = base.reassignValueProductIdsInPlaceOfSkusForSimpleProducts(lookupSku(flds[2]));
                string productSku = processProductsFile.calculateMagentoProductKey(flds[1]);

                string rec = "";
                string imageType = getImageType(dto.FileName);

                // These are the columns specified by Perficient/RapidFlow to add images:
                // CPI,sku,image name,image label,position,disabled
                // Note that the image label field is empty, but it must be included for Magento to work properly.
                //
                if (imageType == "PM")
                {
                    //rec = string.Format("CPI,{0},{1},,{2},0\n", productSku, flds[4], flds[3]).Replace(",","\t");
                    //rec = string.Format("CPI,{0},{1},{2},1,{3}\n", productSku, flds[4], flds[5], flds[3]).Replace(",", "\t");
                    rec = string.Format("CPI,{0},{1},,{2},0\n", productSku, flds[4], flds[5]).Replace(",", "\t");
                    addChgSb.Append(rec);
                }
                else if (imageType == "IT")
                {
                    //rec = string.Format("CPI,{0},{1},,{2},1\n", sku, flds[4], flds[3]).Replace(",", "\t");
                    //rec = string.Format("CPI,{0},{1},{2},1,{3}\n", sku, flds[4], flds[5], flds[3]).Replace(",", "\t");
                    rec = string.Format("CPI,{0},{1},,{2},0\n", sku, flds[4], flds[5]).Replace(",", "\t");
                    addChgSb.Append(rec);
                }
            }
        }

        public string lookupSku(string legacySku)
        {
            return base.lookupMinorSkuAppendAddDateAndTime(legacySku);
        }

        public string getImageType(string fileName)
        {
            string rtnValue = fileName.Substring(fileName.Length - 6, 2);
            return rtnValue;
        }

        private Dictionary<string, string> buildMediaDictAllFldsInKey(string mediaFile, string imageType)
        {
            Dictionary<string, string> mediaDict = new Dictionary<string, string>();
            List<string> mediaLines = mediaFile.Split('\n').ToList();
            Int32 i = 0;
            foreach (string mediaLine in mediaLines)
            {
                List<string> flds = mediaLine.Replace("\r","").Replace("'","").Split(',').ToList();
                string fixedMediaLine = fixMediaLinePadSku(flds);

                if (flds.Count > 2 && i>0)
                {
                    mediaDict.Add(imageType + fixedMediaLine, fixedMediaLine);
                }
                i++;
            }
            return mediaDict;
        }

        private Dictionary<Int32, string> buildMediaDictJustPkKey( string mediaFile )
        {
            Dictionary<Int32, string> mediaDict = new Dictionary<Int32, string>();
            List<string> mediaLines = mediaFile.Split('\n').ToList();
            Int32 i = 0;
            foreach (string mediaLine in mediaLines)
            {
                List<string> flds = mediaLine.Replace("\r", "").Replace("'", "").Split(',').ToList();
                string fixedMediaLine = fixMediaLinePadSku(flds);

                if (flds.Count > 2 && i > 0)
                {
                    mediaDict.Add(Int32.Parse(flds[0]), fixedMediaLine);
                }
                i++;
            }
            return mediaDict;
        }

        private string fixMediaLinePadSku(List<string> flds)
        {
            string fixedMediaLine = "";
            Int32 fldNdx = 0;
            foreach (string fld in flds)
            {
                if (fldNdx != 0)
                {
                    fixedMediaLine += ",";
                }
                if (fldNdx == 2)
                {
                    fixedMediaLine += base.padToSize(fld, 6, "0");
                }
                else
                {
                    fixedMediaLine += fld;
                }
                fldNdx++;
            }
            return fixedMediaLine;
        }


    }
}
