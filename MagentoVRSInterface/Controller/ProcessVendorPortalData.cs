﻿using MagentoVRSInterface.DAO;
using MagentoVRSInterface.DTO;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagentoVRSInterface.Controller
{
    public class ProcessVendorPortalData : FileProcessor
    {
        LogController log = LogController.Instance;
        VendorPortalFileHistoryDAO vendorPortalFileHistoryDAO = new VendorPortalFileHistoryDAO();
        ProcessProductsFile processProductsFile = new ProcessProductsFile();
        Dictionary<string, string> MfgCodeToCompanyNameFromVendorFile = null;
        Dictionary<string, VendorPortalToImageStoreDTO> mediaStorePdfOrYouTubeDict = null;

        public override List<KeyValuePair<string, StringBuilder>> processFile(System.Data.IDbConnection stagedConn, string sourceFile)
        {
            List<string> importFldsHeaders = new List<string>(){"iFileId", "sVendorCode", "sFileInternalName", "iFileType", "sFileLocation", "iFileUsage", 
                "iIsForAllProduct", "sFileTargetProductKey", "sFileTargetCOmmonPartNum", "iFileStatus", 
                "sSortKey", "sPreviewImageType",  "dtLaunchDate",    "dtEndDate",  "dtLastModifyTime", "sFileTitle", "sLongDescription"};

            List<KeyValuePair<string, StringBuilder>> rtnList = new List<KeyValuePair<string, StringBuilder>>();
            string DestDirectory = ConfigurationManager.AppSettings["FileBuildLocation"].ToString();
            this.MfgCodeToCompanyNameFromVendorFile = processProductsFile.buildMapFromMfgCodeToMfgName(stagedConn, contextController.loadId);

            StringBuilder addChgSb = new StringBuilder();
            StringBuilder deleteSb = new StringBuilder();
            StringBuilder allRecords = new StringBuilder();


            VendorPortalFileHistoryDTO currentFileDTO = vendorPortalFileHistoryDAO.selectRecordsRtnList(stagedConn, string.Format("LoadId = {0} ", contextController.loadId)).Where(x => x.FileName.Equals("FileInfo.dat")).SingleOrDefault();
            VendorPortalFileHistoryDTO previousFileDTO = vendorPortalFileHistoryDAO.selectRecordsRtnList(stagedConn, string.Format("LoadId = {0} ", contextController.priorGoodLoad)).Where(x => x.FileName.Equals("FileInfo.dat")).SingleOrDefault();

            string currentFile = currentFileDTO.RawFile.Replace("\"",""); //We don't want any quotes in our data!
            string priorFile = (previousFileDTO != null) ? previousFileDTO.RawFile : "";
            List<string> currentJasonRecords = generateJsonRecords(currentFile, importFldsHeaders);
            List<string> priorJasonRecords = generateJsonRecords(priorFile, importFldsHeaders);

            mediaStorePdfOrYouTubeDict = cacheMediaStoreDataAndBuildDictionary(stagedConn, currentJasonRecords);

            processChangedRecords(importFldsHeaders, rtnList, DestDirectory, addChgSb, currentJasonRecords, priorJasonRecords);
            processChangedRecords(importFldsHeaders, new List<KeyValuePair<string, StringBuilder>>(), DestDirectory, allRecords, currentJasonRecords, new List<string>());
            contextController.allVendorPortalData = allRecords;
            
            processDeletedRecords(rtnList, DestDirectory, deleteSb, currentJasonRecords, priorJasonRecords);

            vendorPortalFileHistoryDAO.MarkFilesForRunComplete(stagedConn, null, contextController.loadId);
            return rtnList;
        }

        public StringBuilder buildTmpTableForTitems(System.Data.IDbConnection stagedConn)
        {
            return contextController.allVendorPortalData;
        }

        private Dictionary<string, VendorPortalToImageStoreDTO> cacheMediaStoreDataAndBuildDictionary(System.Data.IDbConnection stagedConn, List<string> currentJasonRecords)
        {
            Dictionary<string, VendorPortalToImageStoreDTO> rtnValue = new Dictionary<string, VendorPortalToImageStoreDTO>();
            MediaStoreController mediaStoreController = new MediaStoreController();
            VendorPortalToImageStoreDAO vendorPortalToImageStoreDAO = new VendorPortalToImageStoreDAO();
            List<VendorPortalToImageStoreDTO> vendorPortalToImageStoreRecs = vendorPortalToImageStoreDAO.selectRecordsRtnList(stagedConn, "");
            foreach (string rec in currentJasonRecords)
            {
                Dictionary<string, string> currentRec = null;
                try
                {
                    currentRec = base.decodeJson(rec);

                }
                catch (Exception)
                {
                    currentRec = base.decodeJson(rec); //This was done to allow me to walk though it with the debugger if this occured.
                }

                if (!string.IsNullOrWhiteSpace(currentRec["sFileInternalName"].ToString()))
                {
                    buildCacheForPdfFile(stagedConn, rtnValue, mediaStoreController, vendorPortalToImageStoreDAO, vendorPortalToImageStoreRecs, currentRec);
                }
                else
                {
                    buildCacheForYouTube(stagedConn, rtnValue, mediaStoreController, vendorPortalToImageStoreDAO, vendorPortalToImageStoreRecs, currentRec);
                }
            }

            return rtnValue;
        }

        private void buildCacheForYouTube(System.Data.IDbConnection stagedConn, Dictionary<string, VendorPortalToImageStoreDTO> imageCacheDictionary, MediaStoreController mediaStoreController, VendorPortalToImageStoreDAO vendorPortalToImageStoreDAO, List<VendorPortalToImageStoreDTO> vendorPortalToImageStoreRecs, Dictionary<string, string> currentRec)
        {
            VendorPortalToImageStoreDTO recInDB = null;
            try
            {
                recInDB = vendorPortalToImageStoreRecs.Where(x => x.URLData.Trim() == currentRec["sFileLocation"].ToString().Trim()  && x.VendorId == currentRec["sVendorCode"].ToString()  ).SingleOrDefault();
            }
            catch (Exception ex1)
            {
                log.logMsg(LogController.LogClass.Warning, string.Format("Warning more than one element for a key values of:({0})", currentRec["sFileLocation"].ToString()));
                recInDB = vendorPortalToImageStoreRecs.Where(x => x.URLData.Trim() == currentRec["sFileLocation"].ToString().Trim()).FirstOrDefault();  
            } 

            if (recInDB == null)
            {
                Dictionary<string, string> recInMediaStore = mediaStoreController.lookupMediaObject(currentRec["sVendorCode"].ToString(), currentRec["sFileLocation"].ToString());
                if (Int32.Parse(recInMediaStore["MediaObjectId"].ToString()) != 0)
                {
                    VendorPortalToImageStoreDTO recToAdd = new VendorPortalToImageStoreDTO();
                    try
                    {
                        recToAdd.VendorId = currentRec["sVendorCode"].ToString();
                        recToAdd.MediaFileName = "";
                        recToAdd.MediaObjectId = Int32.Parse(recInMediaStore["MediaObjectId"].ToString());
                        recToAdd.URLData = currentRec["sFileLocation"].ToString(); ;
                        recToAdd.URLPreview = recInMediaStore["PreviewImageURL"].ToString();
                        recToAdd.MediaType = Int32.Parse(currentRec["iFileType"].ToString());
                        recToAdd.sFileTitle = currentRec["sFileTitle"].ToString();
                        recToAdd.sLongDescription = currentRec["sLongDescription"].ToString();
                        vendorPortalToImageStoreDAO.insert(stagedConn, recToAdd);

                        if (!imageCacheDictionary.Keys.Contains(currentRec["sFileLocation"].ToString() + "~" + currentRec["sVendorCode"].ToString()))
                        {
                            imageCacheDictionary.Add(currentRec["sFileLocation"].ToString() + "~" + currentRec["sVendorCode"].ToString(), recToAdd);
                        }
                    }
                    catch (Exception ex1)
                    {
                        log.logMsg(LogController.LogClass.Error, string.Format("Vendor Portal Data, Error inserting :({0})", currentRec["sFileLocation"].ToString()));
                    }
                }
            }
            else
            {
                if (!imageCacheDictionary.Keys.Contains(currentRec["sFileLocation"].ToString() + "~" + currentRec["sVendorCode"].ToString()))
                {
                    imageCacheDictionary.Add(currentRec["sFileLocation"].ToString() + "~" + currentRec["sVendorCode"].ToString(), recInDB);
                }
            }
        }

        private void buildCacheForPdfFile(System.Data.IDbConnection stagedConn, Dictionary<string, VendorPortalToImageStoreDTO> rtnValue, MediaStoreController mediaStoreController, VendorPortalToImageStoreDAO vendorPortalToImageStoreDAO, List<VendorPortalToImageStoreDTO> vendorPortalToImageStoreRecs, Dictionary<string, string> currentRec)
        {
            VendorPortalToImageStoreDTO recInDB = null;
            try
            {
                recInDB = vendorPortalToImageStoreRecs.Where(x => x.MediaFileName == currentRec["sFileInternalName"].ToString() && x.VendorId == currentRec["sVendorCode"].ToString()   ).SingleOrDefault();
            }
            catch (Exception)
            {
                log.logMsg(LogController.LogClass.Warning, string.Format("Warning more than one element for a key values of:({0})", currentRec["sFileInternalName"].ToString()));
                recInDB = vendorPortalToImageStoreRecs.Where(x => x.MediaFileName == currentRec["sFileInternalName"].ToString()).FirstOrDefault();
            } 

            if (recInDB == null)
            {
                Dictionary<string, string> recInMediaStore = mediaStoreController.lookupMediaObject(currentRec["sVendorCode"].ToString(), currentRec["sFileInternalName"].ToString());
                if (Int32.Parse(recInMediaStore["MediaObjectId"].ToString()) != 0)
                {
                    VendorPortalToImageStoreDTO recToAdd = new VendorPortalToImageStoreDTO();
                    recToAdd.VendorId = currentRec["sVendorCode"].ToString();
                    recToAdd.MediaFileName = currentRec["sFileInternalName"].ToString();
                    recToAdd.MediaObjectId = Int32.Parse(recInMediaStore["MediaObjectId"].ToString());
                    recToAdd.URLData = recInMediaStore["MediaObjectURL"].ToString();
                    recToAdd.URLPreview = recInMediaStore["PreviewImageURL"].ToString();
                    recToAdd.MediaType = Int32.Parse(currentRec["iFileType"].ToString());

                    recToAdd.sFileTitle = currentRec["sFileTitle"].ToString();
                    recToAdd.sLongDescription = currentRec["sLongDescription"].ToString();

                    vendorPortalToImageStoreDAO.insert(stagedConn, recToAdd);

                    if (!rtnValue.Keys.Contains(currentRec["sFileInternalName"].ToString() + "~" + currentRec["sVendorCode"].ToString()))
                    {
                        rtnValue.Add(currentRec["sFileInternalName"].ToString() + "~" + currentRec["sVendorCode"].ToString(), recToAdd);
                    }
                }
            }
            else
            {
                if (!rtnValue.Keys.Contains(currentRec["sFileInternalName"].ToString() + "~" + currentRec["sVendorCode"].ToString()))
                {
                    rtnValue.Add(currentRec["sFileInternalName"].ToString() + "~" + currentRec["sVendorCode"].ToString(), recInDB);
                }
            }
        }

        private void processDeletedRecords(List<KeyValuePair<string, StringBuilder>> rtnList, string DestDirectory, StringBuilder deleteSb, List<string> currentJasonRecords, List<string> priorJasonRecords)
        {
            List<string> currentIFielIds = base.fieldListFromJson(currentJasonRecords, "iFileId");
            List<string> priorIFielIds = base.fieldListFromJson(priorJasonRecords, "iFileId");
            List<string> recsToDelete = priorIFielIds.Except(currentIFielIds).ToList();

            if (recsToDelete.Count > 0)
            {
                deleteSb.Append("iFileId\n");
                foreach (string ifield in recsToDelete)
                {
                    deleteSb.Append(ifield + "\n");
                }
                string deleteFile = DestDirectory + "DeletePdfVideofile" + contextController.loadId.ToString("D8") + ".tab";
                rtnList.Add(new KeyValuePair<string, StringBuilder>(deleteFile, deleteSb));
            }
        }

        private void processChangedRecords(List<string> importFldsHeaders, List<KeyValuePair<string, StringBuilder>> rtnList, string DestDirectory, StringBuilder addChgSb, List<string> currentJasonRecords, List<string> priorJasonRecords)
        {
            List<string> exportHeaders = new List<string>() {"iFileId", "MfgCode","Manufacturer", "iFileUsage", "iIsForAllProduct", 
                "sFileTargetProductKey", "LegacyProduct","sSortKey", "dtLaunchDate","dtEndDate", "URL","ThumbURL",  "sFileTitle", "sLongDescription", "dtLastModifyTime" };

            List<string> changedRecs = currentJasonRecords.Except(priorJasonRecords).ToList();
            if (changedRecs.Count > 0)
            {
                bool firstRec = true;
                foreach (string rec in changedRecs)
                {
                    Dictionary<string, string> recset = base.decodeJson(rec);
                    
                    buildCalculatedFields(rec, recset);
                    if (!string.IsNullOrWhiteSpace(recset["URL"].ToString()))
                    {
                        if (firstRec)
                        {
                            string headers = string.Join("\t", exportHeaders) + "\n";
                            addChgSb.Append(headers);
                            firstRec = false;
                        }
                        string data = string.Join("\t", base.getDataValues(recset, exportHeaders)) + "\n";
                        addChgSb.Append(data);
                    }
                }
                string chgFile = DestDirectory + "PdfVideofile" + contextController.loadId.ToString("D8") + ".tab";
                rtnList.Add(new KeyValuePair<string, StringBuilder>(chgFile, addChgSb));
            }
        }

        private void buildCalculatedFields(string rec, Dictionary<string, string> recset)
        {
            string mediaStoreContent = ConfigurationManager.AppSettings["MediaStoreContent"].ToString();
            string mediaStoreThumb = ConfigurationManager.AppSettings["MediaStoreThumb"].ToString();

            recset.Add("Manufacturer", lookupMfgrName(recset["sVendorCode"], recset["sFileTargetProductKey"], rec));
            recset.Add("MfgCode", recset["sVendorCode"].ToString());
            recset.Add("LegacyProduct", recset["sFileTargetProductKey"].ToString());

            if (recset["sFileTargetProductKey"] != null && !string.IsNullOrWhiteSpace(recset["sFileTargetProductKey"].ToString()))
            {
                recset["sFileTargetProductKey"] = processProductsFile.calculateMagentoProductKey(recset["sFileTargetProductKey"].ToString());
            }

            VendorPortalToImageStoreDTO vendorPortalToImageStoreDTO = null;
            if (mediaStorePdfOrYouTubeDict.Keys.Contains(recset["sFileInternalName"] + "~" + recset["sVendorCode"].ToString()))
            {
                vendorPortalToImageStoreDTO = mediaStorePdfOrYouTubeDict[recset["sFileInternalName"].ToString() + "~" + recset["sVendorCode"].ToString()];
                recset.Add("URL", mediaStoreContent + vendorPortalToImageStoreDTO.URLData);
                recset.Add("ThumbURL", mediaStoreThumb + vendorPortalToImageStoreDTO.URLPreview);
            }
            else if (mediaStorePdfOrYouTubeDict.Keys.Contains(recset["sFileLocation"] + "~" + recset["sVendorCode"].ToString()))
            {
                vendorPortalToImageStoreDTO = mediaStorePdfOrYouTubeDict[recset["sFileLocation"].ToString() + "~" + recset["sVendorCode"].ToString()];
                recset.Add("URL", "http:" + vendorPortalToImageStoreDTO.URLData);
                recset.Add("ThumbURL", mediaStoreThumb + vendorPortalToImageStoreDTO.URLPreview);
            } 
            else
            {
                log.logMsg(LogController.LogClass.Warning, string.Format("Vendor Data record doesn't seem to be pdf or Youtube video({0}).", base.generateJson(recset)));
                recset.Add("URL", "");
                recset.Add("ThumbURL", "");
            }
            recset["iIsForAllProduct"] = base.intToStringBool(recset["iIsForAllProduct"].ToString());
            recset["iFileUsage"] = (recset["iFileUsage"].Trim() == "1") ? "Brand" : "Product";

            Dictionary<string, string> fileStatDict = new Dictionary<string, string>() { { "1", "Pending" }, { "2", "Approved" }, { "3", "Disapproved" },
                {"4", "DeleteByVendor"}, {"5", "DeleteByAdmin"}};

            recset["iFileStatus"] = fileStatDict[recset["iFileStatus"].Trim()];
            if (DateTime.Compare(DateTime.Parse(recset["dtEndDate"]), DateTime.Parse(recset["dtLaunchDate"])) < 0)
            {
                String tmpDate = recset["dtEndDate"];
                recset["dtEndDate"] = recset["dtLaunchDate"];
                recset["dtLaunchDate"] = tmpDate;
            }
        }

        private List<string> generateJsonRecords(string currentFile, List<string> importFldsHeaders)
        {
            List<string> jasonRecords = new List<string>();
            List<string> allLines = currentFile.Split('\n').ToList();
            foreach (string line in allLines)
            {
                List<string> flds = line.Replace("\r", "").Split('\t').ToList();
                if (flds.Count > 5)
                {
                    Dictionary<string, string> dataFlds = new Dictionary<string, string>();
                    for (Int32 x = 0; x < flds.Count && x < importFldsHeaders.Count; x++)
                    {
                        dataFlds.Add(importFldsHeaders[x], flds[x].Trim());
                    }
                    jasonRecords.Add(base.generateJson(dataFlds, importFldsHeaders));
                }
            }
            return jasonRecords;
        }


        private string lookupMfgrName(string mfgrCode, string productKey, string line)
        {
            string companyName = "";

            if (MfgCodeToCompanyNameFromVendorFile.Keys.Contains(mfgrCode))
            {
                companyName = MfgCodeToCompanyNameFromVendorFile[mfgrCode];
            }
            else
            {
                log.logMsg(LogController.LogClass.Warning, string.Format("Vendor Data, Product ({0}) has invalid MfgCode ({1}) Raw Line:({2})", productKey, mfgrCode, line));
                companyName = "MISSING";
            }
            return companyName;
        }



    }
}
