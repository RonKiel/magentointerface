﻿using MagentoInterface.DAO;
using MagentoInterface.DTO;
using MagentoVRSInterface.DAO;
using MagentoVRSInterface.DTO;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace MagentoVRSInterface.Controller
{
    public abstract class FileProcessor 
    {
        protected ContextController contextController = ContextController.Instance;
        private LogController log = LogController.Instance;

        public abstract List<KeyValuePair<string, StringBuilder>> processFile(IDbConnection stagedConn, string sourceFile);
        public virtual List<KeyValuePair<string, StringBuilder>> processFileBlue(IDbConnection stagedConn, IDbConnection blueConn, string sourceFile)
        { return null; }

        protected IDbDataParameter createParameter(IDbCommand cmd, string value, string parmName)
        {
            IDbDataParameter attribCode = cmd.CreateParameter();
            attribCode.DbType = DbType.AnsiString;
            attribCode.ParameterName = parmName;
            attribCode.Value = value;
            return attribCode;
        }
        public void writeFile( List<KeyValuePair<string, StringBuilder>> fileList ) 
        {
            foreach ( KeyValuePair<string,StringBuilder> kp in fileList)
            {
                string fileName = kp.Key;
                StringBuilder sb = kp.Value;
                File.WriteAllText( fileName, sb.ToString());
            }
        }
        public void reassignProductIdsInPlaceOfSkusForSimpleProducts<T>(IList<T> list, string itemSkuName)
        {
            reassignProductIdsInPlaceOfSkusForSimpleProducts(list, contextController.skusToProductsExtended,
                contextController.productToSkusMapExtended, itemSkuName);
        }
        public void reassignProductIdsInPlaceOfSkusForSimpleProducts<T>(IList<T> list, Dictionary<string, string> skusToProducts, Dictionary<string, List<string>> productToSkusMap, string itemSkuName)
        {
            var props = TypeDescriptor.GetProperties(typeof(T)).Cast<PropertyDescriptor>()
                                       .Where(propertyInfo => propertyInfo.PropertyType.Namespace.Equals("System")).ToList();

            PropertyDescriptor prop = props.Where(x => x.Name == itemSkuName).Single();
            Int32 numberWithWrongHasVariation = 0;
            Int32 numberReplacements = 0;

            foreach (var rec in list)
            {
                string currentItemSku = prop.GetValue(rec).ToString();

                if (skusToProducts.ContainsKey(currentItemSku))
                {
                    string productId = skusToProducts[currentItemSku];

                    string hasVariation = productHasVariation(productId);

                    if (productToSkusMap[productId].Count == 1)
                    {
                        if (hasVariation.Trim().Equals("1"))
                        {
                            numberWithWrongHasVariation++;
                        }
                        else
                        {
                            prop.SetValue(rec, productId);
                            numberReplacements++;
                        }
                    }
                }
            }
            log.logMsg(LogController.LogClass.Info,
                string.Format("Replacement stats of minorID to MajorId: File:({0}) Replaced:({1}) Wrong HasVariation:({2})", 
                typeof(T).ToString(), numberReplacements, numberWithWrongHasVariation));
        }

        private string productHasVariation(string productId)
        {
            string hasVariation = "0";
            if (contextController.productDict.Keys.Contains(productId))
            {
                VRSProductDTO vRSProductDTO = contextController.productDict[productId];
                hasVariation = vRSProductDTO.HasVariations;
            }
            return hasVariation;
        }

        public void replaceItemSkuWithExtendedVersion<T>(IList<T> list, string itemSkuName)
        {
            var props = TypeDescriptor.GetProperties(typeof(T)).Cast<PropertyDescriptor>()
                                       .Where(propertyInfo => propertyInfo.PropertyType.Namespace.Equals("System")).ToList();

            PropertyDescriptor prop = props.Where(x => x.Name == itemSkuName).Single();
            foreach (var rec in list)
            {
                string currentItemSku = prop.GetValue(rec).ToString();
                string extendedItemSku = "";
                if (contextController.skuMap.Keys.Contains(currentItemSku))
                {
                    extendedItemSku = contextController.skuMap[currentItemSku] + "-000000";
                }
                else
                {
                    // Debug missing ADDDT issue (MI-155).
                    //string systemName = log.systemName;
                    //log.systemName = "replaceItemSkuWithExtendedVersion";
                    //log.logMsg(LogController.LogClass.Info, string.Empty, currentItemSku);
                    //log.systemName = systemName;
                    extendedItemSku = currentItemSku + "-000000-000000";
                }
                prop.SetValue(rec, extendedItemSku);
            }
        }
        public string reassignValueProductIdsInPlaceOfSkusForSimpleProducts(string itemSku)
        {
            string rtnValue = itemSku;

            if (contextController.skusToProductsExtended.ContainsKey(itemSku))
            {
                string productId = contextController.skusToProductsExtended[itemSku];

                if (contextController.productToSkusMapExtended[productId].Count == 1)
                {
                    string hasVariation = productHasVariation(productId);
                    if (hasVariation.Trim().Equals("0"))
                    {
                        rtnValue = productId;
                    }
                }
            }
            return rtnValue;
        }
        public string lookupMinorSkuAppendAddDateAndTime(string legacySku)
        {
            string rtnValue = "";

            if (contextController.skuMap.Keys.Contains(legacySku))
            {
                rtnValue = contextController.skuMap[legacySku] + "-000000";
            }
            else
            {
                // Debug missing ADDDT issue (MI-155).
                //string systemName = log.systemName;
                //log.systemName = "reassignValueProductIdsInPlaceOfSkusForSimpleProducts";
                //log.logMsg(LogController.LogClass.Info, string.Empty, legacySku);
                //log.systemName = systemName;
                rtnValue = legacySku + "-000000-000000";
            }

            return rtnValue;
        }
        public void BulkInsert<T>(SqlConnection connection, string tableName, IList<T> list)
        {
            BulkInsert<T>(connection, null, tableName, list);
        }
        public void BulkInsert<T>(SqlConnection connection, SqlTransaction tran, string tableName, IList<T> listSrc)
        {
            IList<T> list = verifyFieldLengths<T>(connection, tableName, listSrc, log);

            using (var bulkCopy = new SqlBulkCopy(connection, SqlBulkCopyOptions.Default, tran))
            {
                bulkCopy.BatchSize = list.Count;
                bulkCopy.DestinationTableName = tableName;
                bulkCopy.BulkCopyTimeout = 60000;

                var table = new DataTable();
                var props = TypeDescriptor.GetProperties(typeof(T))
                                           .Cast<PropertyDescriptor>()
                                           .Where(propertyInfo => propertyInfo.PropertyType.Namespace.Equals("System"))
                                           .ToArray();

                foreach (var propertyInfo in props)
                {
                    bulkCopy.ColumnMappings.Add(propertyInfo.Name, propertyInfo.Name);
                    table.Columns.Add(propertyInfo.Name, Nullable.GetUnderlyingType(propertyInfo.PropertyType) ?? propertyInfo.PropertyType);
                }

                var values = new object[props.Length];
                foreach (var item in list)
                {
                    for (var i = 0; i < values.Length; i++)
                    {
                        values[i] = props[i].GetValue(item);
                    }

                    table.Rows.Add(values);
                }

                bulkCopy.WriteToServer(table);
            }
        }

        public IList<T> verifyFieldLengths<T>(SqlConnection connection, string tableName, IList<T> listSrc, ILogController log)
        {
            if (listSrc.Count == 0)
            {
                return listSrc;
            }
            IList<T> list = new List<T>();
            Dictionary<string, Decimal?> columnsValueDict = new Dictionary<string, Decimal?>();
            Dictionary<string, string> columnsDataTypeDict = new Dictionary<string, string>();

            DataTable rawDataTable = getTableColumnInfo(connection, tableName);

            foreach (DataRow row in rawDataTable.Rows)
            {
                string dataType = row["Data_type"].ToString();
                string columName = row["Column_name"].ToString();
                Decimal? charMaxLen = getDecimalValue(row, "CHARACTER_MAXIMUM_LENGTH");
                if (!dataType.Contains("varchar"))
                {
                    charMaxLen = -1;
                }
                columnsValueDict.Add(columName, charMaxLen);
                columnsDataTypeDict.Add(columName, dataType);
            }

            List<PropertyInfo> propertyList = listSrc[0].GetType().GetProperties().ToList();
            filterOutRecordsThatExceedFieldLength<T>(tableName, listSrc, log, list, columnsValueDict, columnsDataTypeDict, propertyList);
            return list;
        }

        private void filterOutRecordsThatExceedFieldLength<T>(string tableName, IList<T> listSrc, ILogController log, IList<T> list, Dictionary<string, Decimal?> columnsValueDict, Dictionary<string, string> columnsDataTypeDict, List<PropertyInfo> propertyList)
        {
            StringBuilder sb = new StringBuilder();
            foreach (var item in listSrc)
            {
                List<string> errorsEncountered = new List<string>();
                string entireRecord = getEntireRecordFromDTOAsString<T>(columnsDataTypeDict, propertyList, item);

                for (var i = 0; i < propertyList.Count; i++)
                {
                    string propName = propertyList[i].Name;
                    object dtoValue = propertyList[i].GetValue(item, null);
                    string dataType = columnsDataTypeDict[propName];

                    if (dtoValue != null)
                    {
                        if (dataType.Contains("varchar"))
                        {
                            if ((Int32)columnsValueDict[propName].Value != -1 && dtoValue.ToString().Length > (Int32)columnsValueDict[propName].Value)
                            {
                                string expVsAct = string.Format("Field:{0} max:{1} actual:{2}", propName, (Int32)columnsValueDict[propName].Value, dtoValue.ToString().Length);
                                errorsEncountered.Add(expVsAct);
                            }
                        }
                    }
                }
                if (errorsEncountered.Count == 0)
                {
                    list.Add(item);
                }
                else
                {
                    string flds = string.Join(", ", errorsEncountered);
                    string errMsg = string.Format("IMPORT table:({0}) exceeded length Errors:({1}) DATA:({2})", tableName, flds, entireRecord);
                    log.logMsg(LogController.LogClass.Error, errMsg);
                    sb.Append(errMsg + "\n");
                }
            }
            if ( sb.ToString().Length > 0)
            {
                this.createMailMessage("TABLE Length errors", sb.ToString());
            }
        }

        private static string getEntireRecordFromDTOAsString<T>(Dictionary<string, string> columnsDataTypeDict, List<PropertyInfo> propertyList, T item)
        {
            string entireRecord = "";
            for (var i = 0; i < propertyList.Count; i++)
            {
                string propName = propertyList[i].Name;
                object dtoValue = propertyList[i].GetValue(item, null);
                string dataType = columnsDataTypeDict[propName];
                if (dtoValue != null)
                {
                    entireRecord += string.Format(" {0}:{1}", propName, dtoValue.ToString());
                }
            }
            return entireRecord;
        }

        private static DataTable getTableColumnInfo(SqlConnection connection, string tableName)
        {
            IDbCommand cmd = connection.CreateCommand();

            string sql = "SELECT Column_name, Data_type, CHARACTER_MAXIMUM_LENGTH " +
                         "FROM INFORMATION_SCHEMA.COLUMNS " +
                         string.Format("WHERE TABLE_NAME = N'{0}'", tableName);

            cmd.CommandText = sql;

            IDataReader dr = cmd.ExecuteReader();
            DataTable rawDataTable = new DataTable();

            DataSet ds = new DataSet();
            ds.Tables.Add(rawDataTable);
            ds.EnforceConstraints = false;

            rawDataTable.Load(dr);
            dr.Close();
            return rawDataTable;
        }

        public string generateTabRecord(BaseDTO dto)
        {
            return generateTabRecord(dto, new List<string>());
        }
        public string generateTabRecord(BaseDTO dto, List<string> additionalFields)
        {
            List<string> fields = new List<string>();

            List<PropertyInfo> propertyList = dto.GetType().GetProperties().ToList();
            foreach (PropertyInfo pi in propertyList)
            {
                if( !pi.Name.Equals("LoadId") ) 
                {
                    object valueToSet = pi.GetValue(dto, null);
                    fields.Add(valueToSet.ToString());
                }
            }
            fields.AddRange(additionalFields);
            string rtnRec = string.Join("\t", fields) + "\n";
            return rtnRec;
        }

        public string generateHeaderRecord(BaseDTO dto, string delimeter)
        {
            List<string> fields = new List<string>();

            List<PropertyInfo> propertyList = dto.GetType().GetProperties().ToList();
            foreach (PropertyInfo pi in propertyList)
            {
                if (!pi.Name.Equals("LoadId"))
                {
                    fields.Add(pi.Name);
                }
            }
            string rtnRec = string.Join(delimeter, fields) + "\n";
            return rtnRec;
        }

        public virtual void populateDTOFromDataRow(DataRow dr, BaseDTO dto)
        {
            List<PropertyInfo> propertyList = dto.GetType().GetProperties().ToList();
            foreach (PropertyInfo pi in propertyList)
            {
                if (dr.Table.Columns.Contains(pi.Name))
                {
                    try
                    {
                        if( System.DBNull.Value.Equals(dr[pi.Name])) 
                        {
                            //Don't do anything!!
                        }
                        else if (pi.PropertyType.ToString().Contains("System.Int32"))
                        {
                            Int32? normalIntUsedByDTOs = Convert.ToInt32(dr[pi.Name]);
                            pi.SetValue(dto, normalIntUsedByDTOs, null);
                        }
                        else if (pi.PropertyType.ToString().Contains("System.Int16"))
                        {
                            Int16? normalIntUsedByDTOs = Convert.ToInt16(dr[pi.Name]);
                            pi.SetValue(dto, normalIntUsedByDTOs, null);
                        }
                        else if (pi.PropertyType.ToString().Contains("System.DateTime"))
                        {
                            DateTime? tmpDateTime = DateTime.Parse(dr[pi.Name].ToString());
                            pi.SetValue(dto, tmpDateTime, null);
                        }
                        else if (pi.PropertyType.ToString().Contains("System.Decimal"))
                        {
                            Decimal? tmpDateTime = Decimal.Parse(dr[pi.Name].ToString());
                            pi.SetValue(dto, tmpDateTime, null);
                        }
                        else
                        {
                            pi.SetValue(dto, dr[pi.Name], null);
                        }
                    }
                    catch (Exception ex1)
                    {
                        //log.logMsg(LogController.LogClass.Debug, ex1.StackTrace, string.Format("Processing field:{0}, Error:{1}", pi.Name, ex1.Message));
                    }
                }
            }
        }

        public Dictionary<string, string> buildReplaceSkuDictionary(List<InvenDTO> invenRecs)
        {
            List<InvenDTO> inveWithSkuList = invenRecs.Where(x => !string.IsNullOrWhiteSpace(x.SKU)).ToList();
            Dictionary<string, string> shortSkuToLongDict = new Dictionary<string, string>();

            foreach (InvenDTO dto in inveWithSkuList)
            {
                try
                {
                    if (!shortSkuToLongDict.Keys.Contains(dto.SKU.Trim()))
                    {
                        shortSkuToLongDict.Add(dto.SKU.Trim(), dto.SKU.Trim() + "-" + dto.ADDDT.Trim().Replace(@"/", ""));
                    }
                    else
                    {
                        string currADDT = dto.ADDDT.Trim();

                        string previousSku = shortSkuToLongDict[dto.SKU.Trim()];
                        string oldDateFromSku = previousSku.Split('-').ToList()[1];
                        string oldADDT = oldDateFromSku.Substring(0, 2) + @"/" + oldDateFromSku.Substring(2, 2) + @"/" + oldDateFromSku.Substring(4, 2);

                        Int32 currADDTi = -1;
                        Int32 oldADDTi = -1;
                        Int32.TryParse(comparableDate(currADDT), out currADDTi);
                        Int32.TryParse(comparableDate(oldADDT), out oldADDTi);

                        if (currADDTi > oldADDTi)
                        {
                            shortSkuToLongDict[dto.SKU.Trim()] = dto.SKU.Trim() + "-" + currADDT.Trim().Replace(@"/", "");
                        }
                    }

                }
                catch (Exception ex1)
                {
                    string errorMsg = string.Format("Error when determing...{0}", dto.ToString());
                    LogController log = LogController.Instance;
                    log.logMsg(LogController.LogClass.Error, errorMsg);

                }
            }
            return shortSkuToLongDict;
        }

        public string comparableDate(string srcDate)
        {
            string month = srcDate.Substring(0, 2);
            string day = srcDate.Substring(3, 2);
            string year = srcDate.Substring(6, 2);

            string rtnValue = (Int32.Parse(year) > 60) ? "19" + year : "20" + year;
            rtnValue += month;
            rtnValue += day;

            return rtnValue;
        }

        public DataTable findChangedRecords(IDbConnection stagedConn, string sql)
        {
            DataTable changedRecs = new DataTable();
            IDbCommand cmd2 = stagedConn.CreateCommand();
            cmd2.CommandText = sql;
            cmd2.CommandTimeout = 600000;
            changedRecs.Load(cmd2.ExecuteReader());
            return changedRecs;
        }

        protected void callStoredProcedure(IDbConnection stagedConn, string procedure)
        {
            IDbCommand cmd3 = stagedConn.CreateCommand();
            cmd3.CommandTimeout = 10000000;
            cmd3.CommandText = "EXEC " + procedure + ";";
            cmd3.ExecuteNonQuery();
        }


        public string urlKeyFormatting(string src, string appendedKey)
        {
            string rtnValue = urlKeyFormatting(src).Trim();

            if (rtnValue.Length == 0)
            {
                return appendedKey;
            } 
            else if (rtnValue.Length > 0)
            {
                char last = rtnValue[rtnValue.Length - 1];
                if (last == '-')
                {
                    return rtnValue + appendedKey;
                }
            }
            return rtnValue + '-'+ appendedKey;
        }

        public string urlKeyFormatting(VRSCatDTO dto)
        {
            return urlKeyFormatting(dto.Title.ToLower().Trim());
        }
        public string urlKeyFormatting(string src)
        {
            if ( string.IsNullOrWhiteSpace(src))
            {
                return "";
            }
            char[] toCompare = src.Trim().ToCharArray();
            string validCharacters = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
            StringBuilder rtnValue = new StringBuilder();
            char lastChar = ' ';
            foreach (char c in toCompare)
            {
                if (validCharacters.Contains(c))
                {
                    if (  !(lastChar == '-' && c == '-' ) )
                    {
                        rtnValue.Append(c);
                        lastChar = c;
                    }
                }
                else
                {
                    if (lastChar != '-')
                    {
                        if (rtnValue.ToString().Length > 0)
                        {
                            rtnValue.Append("-"); //We don't want leading spaces.
                        }
                        lastChar = '-';
                    }
                }

            }

            string rtnStr = rtnValue.ToString();
            if (rtnValue.Length > 1)
            {
                char last = rtnStr[rtnValue.Length - 1];
                if (last == '-')
                {
                    rtnStr = rtnStr.Substring(0, rtnStr.Length - 1);
                }
            }
            return rtnStr;
        }

        public string removeAddDateAndTime(string src)
        {
            Int32 hypenAt = src.IndexOf("-");
            if (hypenAt == -1)
            {
                return src;
            }
            else
            {
                return src.Substring(0, hypenAt);
            }
        }

        public string intToEnabledDisabled(string src)
        {
            return (src.Trim() == "0") ? "Disabled" : "Enabled";
        }
        public string intToEnabledDisabled(Int32 src)
        {
            return (src == 0) ? "Disabled" : "Enabled";
        }

        public string intToStringBool(string src)
        {
            return (src.Trim() == "0") ? "No" : "Yes";
        }
        public string intToStringBool(Int32 src)
        {
            return (src == 0) ? "No" : "Yes";
        }

        protected void getMostCommonFieldValue(object theObjectThatHoldTheResults, List<object> theListThatHasTheCandidateValues, string fieldName)
        {
            var groupBegdt = theListThatHasTheCandidateValues.GroupBy(x => getFldValue(x, fieldName)).Select(grp => grp.ToList()).ToList();
            Int32 currMaxCount = -1;
            string currMaxFieldGroup = "";
            foreach (var group in groupBegdt)
            {
                Int32 theCount = group.Count;
                if (theCount > currMaxCount)
                {
                    currMaxCount = theCount;
                    currMaxFieldGroup = getFldValue(group.FirstOrDefault(), fieldName);
                }
            }
            setFldValue(theObjectThatHoldTheResults, fieldName, currMaxFieldGroup);
        }

        public List<decimal> getListOfDecimalsFromStructureList(string fldName, IList dtoList)
        {
            List<decimal> rtnList = new List<decimal>();

            foreach (BaseDTO dto in dtoList)
            {
                decimal? fldValue = getDecimalValue(dto, fldName);
                if (fldValue != null)
                {
                    rtnList.Add((decimal)fldValue);
                }
            }

            return rtnList;
        }

        public List<string> getListOfStringFromStructureList(string fldName, IList dtoList)  
        {
            List<string> rtnList = new List<string>();

            foreach (BaseDTO dto in dtoList)
            {
                string fldValue = getFldValue(dto, fldName);
                rtnList.Add(fldValue);
            }

            return rtnList;
        }

        protected string getFldValue(object theObj, string fldName)
        {
            string rtnValue = null;
            PropertyInfo prop = theObj.GetType().GetProperty(fldName, BindingFlags.Public | BindingFlags.Instance);
            if ( prop != null && prop.GetValue(theObj, null) != null)
            {
                rtnValue = prop.GetValue(theObj, null).ToString();
            }
            return rtnValue;
        }

        protected decimal? getDecimalValue(object theObj, string fldName)
        {
            decimal rtnValue = -1;
            PropertyInfo prop = theObj.GetType().GetProperty(fldName, BindingFlags.Public | BindingFlags.Instance);
            if (prop != null && prop.GetValue(theObj, null) != null)
            {
                if (!decimal.TryParse(prop.GetValue(theObj, null).ToString(), out rtnValue))
                {
                    return null;
                }
            }
            return rtnValue;
        }

        protected void setFldValue(object theObj, string fldName, string fldValue)
        {
            PropertyInfo prop = theObj.GetType().GetProperty(fldName, BindingFlags.Public | BindingFlags.Instance);
            try
            {
                if (prop != null)
                {
                    if (prop.PropertyType.ToString().Contains("System.Int32"))
                    {
                        Int32? normalIntUsedByDTOs = Convert.ToInt32(fldValue);
                        prop.SetValue(theObj, normalIntUsedByDTOs, null);
                    }
                    else if (prop.PropertyType.ToString().Contains("System.Int16"))
                    {
                        Int16? normalIntUsedByDTOs = Convert.ToInt16(fldValue);
                        prop.SetValue(theObj, normalIntUsedByDTOs, null);
                    }
                    else if (prop.PropertyType.ToString().Contains("System.DateTime"))
                    {
                        DateTime? tmpDateTime = DateTime.Parse(fldValue);
                        prop.SetValue(theObj, tmpDateTime, null);
                    }
                    else if (prop.PropertyType.ToString().Contains("System.Decimal"))
                    {
                        Decimal? tmpDateTime = Decimal.Parse(fldValue);
                        prop.SetValue(theObj, tmpDateTime, null);
                    }
                    else
                    {
                        prop.SetValue(theObj, fldValue, null);
                    }
                }
            }
            catch (Exception)
            {
            }
        }

        protected Decimal? getDecimalValue(DataRow dr, string fldName)
        {
            Decimal rtnValue = -1;
            if (dr[fldName] != null)
            {
                if (!Decimal.TryParse(dr[fldName].ToString(), out rtnValue))
                {
                    return null;
                }
            }
            return rtnValue;
        }

        public Dictionary<string, string> decodeJson(string jsonTxt)
        {
            Dictionary<string, string> rtnValue = new Dictionary<string, string>();
            List<string> flds = jsonTxt.Split(',').ToList();
            List<string> fldsEncountered = new List<string>();
            foreach (string fld in flds)
            {
                List<string> fldParts = fld.Replace("\":\"", "\t").Replace("\"", "").Split('\t').ToList();
                if (fldParts.Count > 1)
                {
                    rtnValue.Add(fldParts[0], fldParts[1]);
                    fldsEncountered.Add(fldParts[0]);
                }
                else
                {
                    if (fldsEncountered.Contains("sLongDescription")) 
                    {
                        rtnValue["sLongDescription"] = rtnValue["sLongDescription"] + " " + fldParts[0];
                    }
                }
            }
            return rtnValue;
        }

        public List<string> fieldListFromJson(List<string> jsonRecs, string fldName)
        {
            List<string> rtnValue = new List<string>();
            foreach (string jsonRec in jsonRecs)
            {
                Dictionary<string, string> oneRecSet = decodeJson(jsonRec);
                rtnValue.Add(oneRecSet[fldName]);
            }
            return rtnValue;
        }

        public List<string> getDataValues(Dictionary<string, string> fldDataList, List<string> fldOrder)
        {
            List<string> rtnValues = new List<string>();
            foreach (string fld in fldOrder)
            {
                try
                {
                    rtnValues.Add(fldDataList[fld]);
                }
                catch (Exception ex1)
                {
                    Int32 i = 0;
                }
            }
            return rtnValues;
        }
        public string generateJson(Dictionary<string, string> fldDataList)
        {
            return generateJson(fldDataList, fldDataList.Keys.ToList());
        }
        public string generateJson(Dictionary<string, string> fldDataList, List<string> fldOrder)
        {
            return generateJson(fldDataList, fldOrder, new List<string>());
        }
        public string generateJson(Dictionary<string, string> fldDataList, List<string> fldOrder, List<string> ignoreList)
        {
            StringBuilder sb = new StringBuilder();
            bool firstfld = true;
            foreach (string fld in fldOrder)
            {
                if (fldDataList.Keys.Contains(fld))
                {
                    if (!firstfld)
                    {
                        sb.Append(",");
                    }
                    sb.Append(string.Format("~{0}~:~{1}~", fld, fldDataList[fld]).Replace("~", "\""));
                    firstfld = false;
                }
            }
            return sb.ToString();
        }

        public string generateJson(DataRow dr)
        {
            StringBuilder sb = new StringBuilder();
            bool firstfld = true;
            DataColumnCollection flds = dr.Table.Columns;

            foreach (DataColumn fld in flds)
            {
                if (!firstfld)
                {
                    sb.Append(",");
                }
                sb.Append(string.Format("~{0}~:~{1}~", fld.ColumnName, dr[fld.ToString()]).Replace("~", "\""));
                firstfld = false;
            }
            return sb.ToString();

        }

        public string padToSize(string source, Int32 size, string padChar)
        {
            if (source.Length >= size)
            {
                return source;
            }
            Int32 diffInSize = size - source.Length;
            string padding = "";
            for (int x = 0; x < diffInSize; x++)
            {
                padding += padChar;
            }
            return padding + source;
        }

        public void createMailMessage(string kindOfError, string errorMessage)
        {
            string frompwd = "sumrfl1z";

            MailAddress fromAddress = new MailAddress("ediecommnmc@gmail.com", "MagentoFeed");
            MailAddress toAddress = (ConfigurationManager.AppSettings["toAddressNotify"] != null) ?
                new MailAddress(ConfigurationManager.AppSettings["toAddressNotify"], "MagentoFeed") :
                new MailAddress("webmaster@notionsmarketing.com", "WebMaster");

            SmtpClient client = new SmtpClient();
            client.Port = 587;
            client.Host = "smtp.gmail.com";
            client.EnableSsl = true;
            client.Timeout = 10000;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;
            client.Credentials = new System.Net.NetworkCredential(fromAddress.Address, frompwd);

            MailMessage message = new MailMessage(fromAddress, toAddress);
            message.BodyEncoding = UTF8Encoding.UTF8;
            message.Subject = "Magento interface error: " + kindOfError;
            message.Body = errorMessage;
            message.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;

            try
            {
                client.Send(message);
            }
            catch (Exception ex)
            {
                log.logMsg(LogController.LogClass.Error, string.Format("Error sending email:({0})", ex.Message));
            }
        }
    }

}
