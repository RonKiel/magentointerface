﻿using MagentoVRSInterface.DAO;
using MagentoVRSInterface.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace MagentoVRSInterface.Controller
{
    public class ProcessTItemFile : FileProcessor, IDeferedWriteOfVRSData
    {
        LogController log = LogController.Instance;
        Dictionary<string, string> mfgCodeToName = new Dictionary<string, string>();
        Dictionary<string, string> products = new Dictionary<string, string>();

        public override List<KeyValuePair<string, StringBuilder>> processFile(System.Data.IDbConnection stagedConn, string sourceFile)
         {

            mfgCodeToName = lookForwardAndPopulatemfgCodeToName(stagedConn);
            products = lookForwardAndPopulateProductKeys();

            List<VRSTItemDTO> rawRecsFromFile = buildDTOFromFile(sourceFile, contextController.loadId);

            base.replaceItemSkuWithExtendedVersion(rawRecsFromFile, "SKU");
            populateContextProductToSkuMap(rawRecsFromFile);
            populateContextProductToSkuForSubsitution(rawRecsFromFile);
            contextController.productDict = populateProductDictionary(stagedConn);

            base.reassignProductIdsInPlaceOfSkusForSimpleProducts(rawRecsFromFile, "SKU");

            replaceItemDescriptionsWithDescriptionsFromProducts(stagedConn, rawRecsFromFile, contextController.productDict);
            swapDescAndName(stagedConn, rawRecsFromFile, contextController.productDict);

            return processFile(stagedConn, rawRecsFromFile, contextController.loadId, contextController.priorGoodLoad );
        }

        private Dictionary<string, string> lookForwardAndPopulatemfgCodeToName(System.Data.IDbConnection stagedConn)
        {
            Dictionary<string, string> mfgCodeToName = new Dictionary<string, string>();
            ProcessVendorFile processVendorFile = new ProcessVendorFile();
            List<VRSVendorDTO> vendorRecords = processVendorFile.buildChangedRecords(stagedConn, contextController.loadId, -1);
            foreach (VRSVendorDTO dto in vendorRecords)
            {
                mfgCodeToName.Add(dto.MfgCode.Trim(), dto.CompanyName.Trim());
            }
            return mfgCodeToName;
        }

        private Dictionary<string, string> lookForwardAndPopulateProductKeys()
        {
            Dictionary<string, string> productDict = new Dictionary<string, string>();
            string sourceDirectory = ConfigurationManager.AppSettings["SourceFileLocation"].ToString();
            string productFileName = sourceDirectory + @"\tproductsfile.tab";
            ProcessProductsFile processProductsFile = new ProcessProductsFile();
            List<VRSProductDTO> products = processProductsFile.buildRawDTOFromFileNoChangesFromFile(productFileName, ContextController.Instance.loadId);
            foreach (VRSProductDTO vRSProductDTO in products)
            {
                if (!productDict.Keys.Contains(vRSProductDTO.ProductKey.Trim()))
                {
                    if (vRSProductDTO.notsearch == "0")
                    {
                        productDict.Add(vRSProductDTO.ProductKey.Trim(), vRSProductDTO.MfgCode.Trim());
                    }
                }
            }
            return productDict;
        }

        //
        //We were asked to do this on 10/23/2015, by Andrew
        //
        private void swapDescAndName(System.Data.IDbConnection stagedConn, List<VRSTItemDTO> rawTItemRecs, Dictionary<string, VRSProductDTO> productDict)
        {
            foreach (VRSTItemDTO dto in rawTItemRecs)
            {
                if (productDict.Keys.Contains(dto.SKU))
                {
                    string tmpName = dto.Description;
                    string tmpDesc = dto.Name;
                    dto.Description = tmpDesc;
                    dto.Name = tmpName;
                }
            }
        }

        private void replaceItemDescriptionsWithDescriptionsFromProducts(System.Data.IDbConnection stagedConn, List<VRSTItemDTO> rawTItemRecs, Dictionary<string, VRSProductDTO> productDict)
        {
            //Now do the substitutions, if necessary.
            Int32 numDescChanged = 0;
            Int32 numDescNOTChanged = 0;

            foreach (VRSTItemDTO dto in rawTItemRecs)
            {
                if (productDict.Keys.Contains(dto.SKU))
                {
                    numDescChanged++;
                    VRSProductDTO productDTO = productDict[dto.SKU];
                    dto.Name = dto.Description;
                    dto.Description = productDTO.ShortDesc;//ProductName
                }
                else
                {
                    numDescNOTChanged++;
                }
            }

            log.logMsg(LogController.LogClass.Info, string.Format("titemfile desc   changed:({0})  NOT changed:({1})", numDescChanged, numDescNOTChanged));
        }

        private Dictionary<string, VRSProductDTO> populateProductDictionary(System.Data.IDbConnection stagedConn)
        {
            Dictionary<string, VRSProductDTO> productDict = new Dictionary<string, VRSProductDTO>();
            string sourceDirectory = ConfigurationManager.AppSettings["SourceFileLocation"].ToString();
            ProcessProductsFile processProductsFile = new ProcessProductsFile();
            List<VRSProductDTO> products = processProductsFile.populateVRSProdctDTORecords(stagedConn, sourceDirectory + @"\\tproductsfile.tab");
            foreach (VRSProductDTO dto in products)
            {
                if (!productDict.Keys.Contains(dto.ProductKey))
                {
                    productDict.Add(dto.ProductKey, dto);
                }
            }
            return productDict;
        }

        private void populateContextProductToSkuMap(List<VRSTItemDTO> rawRecsFromFile)
        {
            contextController.productToSkusMap = new Dictionary<string, List<string>>();
            foreach (VRSTItemDTO dto in rawRecsFromFile)
            {
                if (contextController.productToSkusMap.Keys.Contains(dto.ProductKey))
                {
                    contextController.productToSkusMap[dto.ProductKey].Add(dto.SKU);
                }
                else
                {
                    contextController.productToSkusMap.Add(dto.ProductKey, new List<string>() { dto.SKU });
                }
            }
        }

        private void populateContextProductToSkuForSubsitution( List<VRSTItemDTO> rawRecsFromFile )
        {
            ProcessProductsFile processProductsFile = new ProcessProductsFile();
            contextController.productToSkusMapExtended = new Dictionary<string, List<string>>();
            contextController.skusToProductsExtended = new Dictionary<string, string>();

            foreach (VRSTItemDTO dto in rawRecsFromFile)
            {
                string extendedProductId = processProductsFile.calculateMagentoProductKey(dto.ProductKey);

                if (contextController.productToSkusMapExtended.Keys.Contains(extendedProductId))
                {
                    contextController.productToSkusMapExtended[extendedProductId].Add(dto.SKU);
                }
                else
                {
                    contextController.productToSkusMapExtended.Add(extendedProductId, new List<string>() { dto.SKU });
                }

                contextController.skusToProductsExtended.Add(dto.SKU, extendedProductId);
            }
        }

        private List<KeyValuePair<string, StringBuilder>> processFile(System.Data.IDbConnection stagedConn, List<VRSTItemDTO> rawRecsList, int loadId, Int32 priorGoodId )
        {
            List<KeyValuePair<string, StringBuilder>> rtnList = new List<KeyValuePair<string, StringBuilder>>();
            InsertVRSTItemsRecords(stagedConn, rawRecsList);
            return rtnList;
        }

        private void InsertVRSTItemsRecords(System.Data.IDbConnection stagedConn, List<VRSTItemDTO> rawRecsList)
        {
            try
            {
                BulkInsert((SqlConnection)stagedConn, "VRSTItems", rawRecsList); //HACK sql server specific.
            }
            catch (Exception ex1)
            {
                int numberTItemsErrorsAllowed = 10;
                int numberOfErrorsEncountered = 0;
                log.logMsg(LogController.LogClass.Error, ex1.StackTrace, string.Format("TITEMS Bulk INSERT failed, attempting single record inserts... Error:{0}", ex1.Message));

                VRSTItemDAO dao = new VRSTItemDAO();
                for (int i = 0; i < rawRecsList.Count && numberOfErrorsEncountered <= numberTItemsErrorsAllowed; i++)
                {
                    VRSTItemDTO dto = rawRecsList[i];
                    try
                    {
                        dao.insert(stagedConn, dto);
                    }
                    catch (Exception ex2)
                    {
                        log.logMsg(LogController.LogClass.Error, ex1.StackTrace, string.Format("TITEMS single INSERT FAILED, :{0}  {1}", dto.ToString(), ex2.Message));
                        numberOfErrorsEncountered++;
                    }
                }
                if (numberOfErrorsEncountered >= numberTItemsErrorsAllowed)
                {
                    throw new ApplicationException("TItems file export failed!");
                }
            }
        }

        private void buildWriteBufferForDeletes(System.Data.IDbConnection stagedConn, int loadId, Int32 priorGoodId, StringBuilder deleteFileText)
        {
            List<VRSTItemDTO> deleteRecords = buildDeletedRecords(stagedConn, loadId, priorGoodId);
            foreach (VRSTItemDTO dto in deleteRecords)
            {
                deleteFileText.Append(string.Format("-CP\t{0}\n", dto.SKU));
            }
        }

        private void buildWriteBufferForChanges(System.Data.IDbConnection stagedConn, int loadId, Int32 priorGoodId, StringBuilder chgFileText)
        {
            List<string> flds = new List<string>(new string[] { 
                "SKU","MfgCode","Manufacturer", "LegacySKU", "PartNum", "MfgPartNum", "IsActive",	"MfgSugRetailPrice",
                "Weight", "Height",	"Width", "Depth", "MinQty", "CurrentRetailPrice", "Description", "UPC", "NewDate",
                "StartDate", "fobdesc", "discreason", "subsku", "specorder", "ean",	"VendorAvail", "EstArrival",
                "Launch", "whse", "disc", "itemcoavail", "itemcounavail","url_key", "Name", "WhatsNewMulti"
            });

            string headers = string.Join("\t", flds) + "\n";
            chgFileText.Append(headers);

            List<VRSTItemDTO> changedRecords = buildChangedRecords(stagedConn, loadId, priorGoodId);
            try
            {
                foreach (VRSTItemDTO dto in changedRecords)
                {
                    string tRec = dto.SKU + "\t";
                    tRec += dto.MfgCode.Trim() + "\t";
                    tRec += (mfgCodeToName.ContainsKey(dto.MfgCode.Trim()) ? mfgCodeToName[dto.MfgCode.Trim()] : "") + "\t";
                    tRec += dto.LegacySKU + "\t";
                    tRec += dto.PartNum + "\t";
                    tRec += dto.MfgPartNum + "\t";
                    tRec += ((dto.IsActive == "1") ? "Enabled" : "Disabled") + "\t";
                    tRec += dto.MfgSugRetailPrice + "\t";
                    tRec += dto.Weight + "\t";
                    tRec += dto.Height + "\t";
                    tRec += dto.Width + "\t";
                    tRec += dto.Depth + "\t";
                    tRec += dto.MinQty + "\t";
                    tRec += dto.CurrentRetailPrice + "\t";
                    tRec += dto.Description + "\t";
                    tRec += dto.UPC + "\t";
                    tRec += dto.NewDate + "\t";
                    tRec += dto.StartDate + "\t";
                    tRec += dto.fobdesc + "\t";
                    tRec += dto.discreason + "\t";
                    tRec += dto.subsku + "\t";
                    tRec += dto.specorder + "\t";
                    tRec += dto.ean + "\t";
                    tRec += dto.VendorAvail + "\t";
                    tRec += dto.EstArrival + "\t";
                    tRec += dto.Launch + "\t";
                    tRec += dto.whse + "\t";
                    tRec += base.intToStringBool(dto.disc) + "\t";
                    tRec += dto.itemcoavail + "\t";
                    tRec += dto.itemcounavail + "\t";

                    tRec += (dto.Description == "&nbsp;") ? dto.SKU.Trim() : base.urlKeyFormatting(dto.Description, dto.SKU.Trim()).Trim().ToLower(); //url_key
                    tRec += "\t";
                    tRec += dto.Name + "\t";
                    tRec += dto.WhatsNewMulti;
                    tRec += "\n";

                    chgFileText.Append(tRec);
                }
            }
            catch (Exception ex)
            {
                log.logMsg(LogController.LogClass.Error,
                    String.Format(
                    "Exception, ProcessTitemFile.buildWriteBufferForChanges: {0}.",
                    ex.Message));
            }
        }

        private List<VRSTItemDTO> buildDTOFromFile(string sourceFile, Int32 loadId)
        {
            List<VRSTItemDTO> rawRecords = new List<VRSTItemDTO>();
            contextController.validSkuSet = new HashSet<string>();

            using (StreamReader sr = new StreamReader(sourceFile))
            {
                string line;
                Int32 recNo = 0;
                while ((line = sr.ReadLine()) != null)
                {
                    List<string> fields = line.Split('\t').ToList();
                    VRSTItemDTO vRSTItemDTO = new VRSTItemDTO();
                    vRSTItemDTO.LoadId = loadId;
                    vRSTItemDTO.MfgCode = fields[0];
                    vRSTItemDTO.CommonPartNum = fields[1];
                    vRSTItemDTO.ProductKey = fields[2];
                    vRSTItemDTO.SKU = fields[3];
                    vRSTItemDTO.LegacySKU = fields[3];
                    vRSTItemDTO.PartNum = fields[4];
                    vRSTItemDTO.MfgPartNum = fields[5];
                    vRSTItemDTO.IsActive = fields[6];
                    vRSTItemDTO.MfgSugRetailPrice = fields[7];
                    vRSTItemDTO.Weight = fields[8];
                    vRSTItemDTO.Height = fields[9];
                    vRSTItemDTO.Width = fields[10];
                    vRSTItemDTO.Depth = fields[11];
                    vRSTItemDTO.MinQty = fields[12];
                    vRSTItemDTO.OrderQty = fields[13];
                    vRSTItemDTO.CurrentRetailPrice = fields[14];
                    vRSTItemDTO.ReceiptText = fields[15];
                    vRSTItemDTO.Description = fields[16];
                    vRSTItemDTO.UPC = fields[17];
                    vRSTItemDTO.NewDate = fields[18];
                    vRSTItemDTO.StartDate = fields[19];
                    vRSTItemDTO.InventoryFlag = fields[20];
                    vRSTItemDTO.fobdesc = fields[21];
                    vRSTItemDTO.discflag = fields[22];
                    vRSTItemDTO.discreason = fields[23];
                    vRSTItemDTO.subsku = fields[24];
                    vRSTItemDTO.specorder = fields[25];
                    vRSTItemDTO.ean = fields[26];
                    vRSTItemDTO.VendorAvail = fields[27];
                    vRSTItemDTO.EstArrival = fields[28];
                    vRSTItemDTO.Launch = fields[29];
                    vRSTItemDTO.whse = fields[30];
                    vRSTItemDTO.disc = fields[31];
                    vRSTItemDTO.itemcoavail = fields[32];
                    vRSTItemDTO.itemcounavail = fields[33];
                    vRSTItemDTO.Name = "";  //We will put a value in this field later if it's a simple product.

                    if (recNo != 0)
                    {
                        if ( string.IsNullOrWhiteSpace(vRSTItemDTO.Description) )
                        {
                            vRSTItemDTO.Description = "&nbsp;";
                        }
                        if ((String.IsNullOrEmpty(vRSTItemDTO.MfgCode) || String.IsNullOrWhiteSpace(vRSTItemDTO.MfgCode)))
                        {
                            rawRecords.Add(vRSTItemDTO);
                            contextController.validSkuSet.Add(vRSTItemDTO.SKU.Trim());
                        }
                        else if ( mfgCodeToName.Keys.Contains(vRSTItemDTO.MfgCode.Trim()))
                        {
                            if ( products.Keys.Contains(vRSTItemDTO.ProductKey.Trim()))
                            {
                                if (mfgCodeToName.Keys.Contains(products[vRSTItemDTO.ProductKey.Trim()].Trim()))
                                {
                                    rawRecords.Add(vRSTItemDTO);
                                    contextController.validSkuSet.Add(vRSTItemDTO.SKU.Trim());
                                }
                                else
                                {
                                    log.logMsg(LogController.LogClass.Error,
                                        string.Format("titemfile item({0}) filtered out because the product that contains this simple item has and invalid mfgcode. Product key:({1})",
                                        vRSTItemDTO.SKU, vRSTItemDTO.ProductKey.Trim()));
                                }
                            }
                            else
                            {
                                log.logMsg(LogController.LogClass.Error,
                                    string.Format("titemfile item({0}) filtered out because either the product was missing or inactive.   Product key:({1})",
                                    vRSTItemDTO.SKU, vRSTItemDTO.ProductKey.Trim()));
                            }
                        }
                        else
                        {
                            log.logMsg(LogController.LogClass.Debug,
                                string.Format("titemfile item({0}) filtered out because bad mfg code:({1})", 
                                vRSTItemDTO.SKU, vRSTItemDTO.MfgCode));
                        }
                    }
                    recNo++;
                }
                sr.Close();
            }
            return rawRecords;
        }

        private List<VRSTItemDTO> buildChangedRecords(IDbConnection stagedConn, Int32 loadId, Int32 priorGoodId)
        {
            string sql = "";
            List<VRSTItemDTO> rtnList = new List<VRSTItemDTO>();

            //This complicated query is to avoid marking a record as changed if the start date changed.
            //
            sql += "SELECT COM.SKU,COM.MfgCode,COM.CommonPartNum,COM.ProductKey,COM.LegacySKU,COM.PartNum,COM.MfgPartNum,COM.IsActive,COM.MfgSugRetailPrice, ";
            sql += "COM.Weight,COM.Height,COM.Width,COM.Depth,COM.MinQty,COM.OrderQty,COM.CurrentRetailPrice,COM.ReceiptText,COM.Description, ";
            sql += "COM.UPC,COM.NewDate,COM.InventoryFlag,COM.fobdesc,COM.discflag,COM.discreason,COM.subsku,COM.specorder,COM.ean, ";
            sql += "COM.VendorAvail,COM.EstArrival,COM.Launch,COM.whse,COM.disc,COM.itemcoavail,COM.itemcounavail, COM.Name, ";
            sql += "Curr.StartDate AS StartDate, COM.WhatsNewMulti ";
            sql += "FROM (	";
            sql += "    SELECT SKU,MfgCode,CommonPartNum,ProductKey,LegacySKU,PartNum,MfgPartNum,IsActive,MfgSugRetailPrice, ";
            sql += "    Weight,Height,Width,Depth,MinQty,OrderQty,CurrentRetailPrice,ReceiptText,Description, ";
            sql += "    UPC,NewDate,InventoryFlag,fobdesc,discflag,discreason,subsku,specorder,ean, ";
            sql += "    VendorAvail,EstArrival,Launch,whse,disc,itemcoavail,itemcounavail, Name, WhatsNewMulti ";
            sql += "    FROM VRSTItems  c  ";
            sql += string.Format("WHERE loadId = {0} ", loadId);
            sql += "    EXCEPT  ";
            sql += "    SELECT SKU,MfgCode,CommonPartNum,ProductKey,LegacySKU,PartNum,MfgPartNum,IsActive,MfgSugRetailPrice, ";
            sql += "    Weight,Height,Width,Depth,MinQty,OrderQty,CurrentRetailPrice,ReceiptText,Description, ";
            sql += "    UPC,NewDate,InventoryFlag,fobdesc,discflag,discreason,subsku,specorder,ean,VendorAvail, ";
            sql += "    EstArrival,Launch,whse,disc,itemcoavail,itemcounavail, Name, WhatsNewMulti  ";
            sql += "    FROM VRSTItems  c2  ";
            sql += string.Format("WHERE loadId = {0} ", priorGoodId);
            sql += "    ) COM ";
            sql += string.Format("JOIN VRSTItems Curr ON Curr.SKU = COM.SKU and Curr.LoadId = {0} ",loadId);
            sql += " ORDER BY COM.SKU ";
            
            DataTable changedRecs = findChangedRecords(stagedConn, sql);

            foreach (DataRow dr in changedRecs.Rows)
            {
                VRSTItemDTO dto = new VRSTItemDTO();
                populateDTOFromDataRow(dr, dto);
                dto.LoadId = loadId;
                rtnList.Add(dto);
            }
            return rtnList;
        }

        private List<VRSTItemDTO> buildDeletedRecords(IDbConnection stagedConn, Int32 loadId, Int32 priorGoodId)
        {
            List<VRSTItemDTO> rtnList = new List<VRSTItemDTO>();

            string tableName = "VRSTItems ";
            string orderBy = " SKU ";
            string headers = " SKU ";

            string sql = " SELECT " + headers;
            sql += " FROM " + tableName + " c ";
            sql += string.Format(" WHERE loadId = {0} ", priorGoodId);
            sql += " EXCEPT ";
            sql += " SELECT " + headers;
            sql += " FROM " + tableName + " c2 ";
            sql += string.Format(" WHERE loadId = {0}", loadId);
            sql += " Order by " + orderBy + " ";

            DataTable changedRecs = findChangedRecords(stagedConn, sql);

            foreach (DataRow dr in changedRecs.Rows)
            {
                VRSTItemDTO dto = new VRSTItemDTO();
                dto.LoadId = loadId;
                dto.SKU = dr["SKU"].ToString();
                rtnList.Add(dto);
            }

            return rtnList;
        }

        public List<string> findProductsWithMedia(String rawData)
        {
            List<string> lines = rawData.Split('\n').ToList();
            string headerLine = lines[0];
            List<string> flds = headerLine.Split('\t').ToList();

            DataTable dt = new DataTable();
            foreach (string fld in flds)
            {
                dt.Columns.Add(fld, typeof(string));
            }

            for (int i = 1; i < lines.Count; i++)
            {
                List<string> dataLine = lines[i].Split('\t').ToList();
                dt.Rows.Add(dataLine.ToArray());
            }

            var query = from vendorLine in dt.AsEnumerable()
                        select new { LegacyProduct = vendorLine.Field<string>("LegacyProduct") };


            List<string> products = new List<string>();
            foreach (var product in query)
            {
                if (!string.IsNullOrWhiteSpace(product.LegacyProduct))
                {
                    products.Add(product.LegacyProduct);
                }
            }

            return products;
        }

        public void buildDataFileToWrite(IDbConnection stagedConn, int loadId, int priorGoodId, List<KeyValuePair<string, StringBuilder>> rtnList)
        {
            StringBuilder chgFileText = new StringBuilder();

            ProcessVendorPortalData processVendorPortalData = new ProcessVendorPortalData();
            
            StringBuilder vendorData = processVendorPortalData.buildTmpTableForTitems(stagedConn);
            List<string> productsWithMedia = findProductsWithMedia(vendorData.ToString());
            foreach (string product in productsWithMedia)
            {
                IDbCommand cmd2 = stagedConn.CreateCommand();
                cmd2.CommandText = 
                    string.Format("UPDATE VRSTItems SET  WhatsNewMulti = 'Products With Media' where loadId = {0} AND ProductKey = '{1}'",
                    loadId, product.Trim());
                cmd2.ExecuteNonQuery();
            }

            callStoredProcedure(stagedConn, "SetAllNewOnTitems");
            callStoredProcedure(stagedConn, "SetComingSoonOnTitems");
            callStoredProcedure(stagedConn, "SetNewArrivalsOnTitems");
            callStoredProcedure(stagedConn, "SetPreorderOnTitems");

            buildWriteBufferForChanges(stagedConn, loadId, priorGoodId, chgFileText);
            buildWriteBufferForDeletes(stagedConn, loadId, priorGoodId, contextController.productDeleteBuffer);

            string DestDirectory = ConfigurationManager.AppSettings["FileBuildLocation"].ToString();

            string chgFile = DestDirectory + "titemsfile" + loadId.ToString("D8") + ".tab";
            rtnList.Add(new KeyValuePair<string, StringBuilder>(chgFile, chgFileText));
        }
    }

}
