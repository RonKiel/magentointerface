﻿using MagentoVRSInterface.DAO;
using MagentoVRSInterface.DTO;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagentoVRSInterface.Controller
{
    public class LogController : MagentoVRSInterface.Controller.ILogController
    {
        public enum LogClass { Debug, Info, Warning, Error, Fatal }
        private List<LogClass> ignoreList = new List<LogClass>();
        public string systemName = "MagentoVRS";
        LogDAO logDAO = new LogDAO();


        private LogController() { }
        private static volatile LogController instance;
        private static object syncRoot = new Object();
        IDbConnection myConnection = null;
        string myConnectionString = null;
        string myProviderName = null;
        public static LogController Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                        {
                            try
                            {
#if WEBAPP
                                if (System.Web.HttpRuntime.Cache["LOGCONTROLLER"] != null)
                                {
                                    instance = (LogController)System.Web.HttpRuntime.Cache["LOGCONTROLLER"];
                                }
#endif
                                if (instance == null)
                                {
                                    instance = new LogController();
                                    GC.KeepAlive(instance);  //Don't let garbage collection happen on this instance.
#if WEBAPP
                                    System.Web.HttpRuntime.Cache.Insert("LOGCONTROLLER", instance, null, System.Web.Caching.Cache.NoAbsoluteExpiration, System.Web.Caching.Cache.NoSlidingExpiration,
                                        System.Web.Caching.CacheItemPriority.NotRemovable, null);
#endif
                                }
                            }
                            catch (Exception)
                            {
                                //Just CYA
                                instance = new LogController();
                                GC.KeepAlive(instance);  //Don't let garbage collection happen on this instance.
                            }
                        }
                    }
                }
                return instance;
            }
        }

        public void ignoreLogClass(LogController.LogClass logClass) 
        {
            ignoreList.Add(logClass);
        }

        public void clearLogIgnoreList()
        {
            ignoreList = new List<LogClass>();
        }

        public List<LogClass> returnIgnoreList()
        {
            return ignoreList;
        }

        public void setupDBConnection(string connectionString, string providerName)
        {
            lock (syncRoot)
            {
                if (myConnection == null || myConnection.State != ConnectionState.Open)
                {
                    myConnectionString = connectionString;
                    myProviderName = providerName;

                    setupInternalConnection();
                }
            }
        }

        private void setupInternalConnection()
        {
            System.Data.Common.DbProviderFactory factory = System.Data.Common.DbProviderFactories.GetFactory(myProviderName);
            IDbConnection conn = factory.CreateConnection();
            conn.ConnectionString = myConnectionString;
            conn.Open();
            myConnection = conn;
        }

        private void resetConnectionIfNecessary()
        {
            if (myConnection.State == ConnectionState.Open)
            {
                return;
            }
            else if (myConnection.State == ConnectionState.Closed)
            {
                myConnection.Open();
                return;
            }
            else
            {
                myConnection.Close();
                setupInternalConnection();
            }
        }

        public void logMsg(LogClass logClass, Exception ex1)
        {
            logMsg(logClass, ex1.StackTrace, ex1.Message);
        }

        public void logMsg(LogClass logClass, string logMessage)
        {
            logMsg(logClass, null, logMessage);
        }

        public void logMsg(LogClass logClass, string stackTrace, string logMessage)
        {
            string surpressDebugMessages = "No";
            if (ConfigurationManager.AppSettings["SurpressDebugMessages"] != null)
            {
                surpressDebugMessages = ConfigurationManager.AppSettings["SurpressDebugMessages"].ToString();
            }

            lock (syncRoot)
            {
                if (!ignoreList.Contains(logClass))
                {
                    if (logClass.Equals(LogClass.Debug) && surpressDebugMessages.ToUpper().Equals("YES") )
                    {
                        //DON'T LOG!
                    } 
                    else
                    {
                        try
                        {
                            resetConnectionIfNecessary();
                            LogDTO dto = new LogDTO() { sLogClass = logClass.ToString(), sSystemName = systemName, sStackTrace = stackTrace, sLogMessage = logMessage };
                            logDAO.insert(myConnection, dto);
                        }
                        catch (Exception)
                        {
                        }
                    }
                }
            }
        }

    }

}
