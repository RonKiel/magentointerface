﻿using MagentoInterface.Controller;
using MagentoVRSInterface.MediaStore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagentoVRSInterface.Controller
{
    public class MediaStoreController : FileProcessor 
    {
        VendorMediaSoapClient imageStoreService = new VendorMediaSoapClient();
        string mediaKey = "830sor927ks";

        public override List<KeyValuePair<string, StringBuilder>> processFile(IDbConnection stagedConn, string sourceFile)
        {
            throw new NotImplementedException();  //This is a hack.  I just wanted methods from the base class.
        }

        public List<string> getVendorList() 
        {
            List<string> rtnList = new List<string>();
            DataTable dt = imageStoreService.GetVendorList(mediaKey);

            foreach (DataRow dr in dt.Rows)
            {
                rtnList.Add(dr["MfgCode"].ToString());
            }
            return rtnList;
        }

        public Dictionary<string, List<string>> getMediaListByVendor( bool ignoreErrors )
        {
            Dictionary<string, List<string>> rtnValue = new Dictionary<string,List<string>>();
            List<string> vendors = getVendorList();

            foreach (string mfgr in vendors)
            {
                if (ignoreErrors)
                {
                    try
                    {
                        DataTable dt = imageStoreService.GetMediaListByVendor(mediaKey, mfgr);
                        rtnValue.Add(mfgr, new List<string>());
                        foreach (DataRow dr in dt.Rows)
                        {
                            rtnValue[mfgr].Add(base.generateJson(dr));
                        }
                    }
                    catch (Exception)
                    {
                    }
                }
                else
                {
                    DataTable dt = imageStoreService.GetMediaListByVendor(mediaKey, mfgr);
                    rtnValue.Add(mfgr, new List<string>());
                    foreach (DataRow dr in dt.Rows)
                    {
                        rtnValue[mfgr].Add(base.generateJson(dr));
                    }
                }
            }
            return rtnValue;
        }


        public Dictionary<string, string> lookupMediaObject(string vendorId, string MediaFileName)
        {
            Dictionary<string, string> rtnRec = new Dictionary<string, string>();
            int MediaObjectId = -1;
            string MediaObjectURL = "";
            string PreviewImageURL = "";

            imageStoreService.LookupMediaObject(mediaKey, vendorId, MediaFileName, ref MediaObjectId, ref MediaObjectURL, ref PreviewImageURL);
            rtnRec.Add("MediaObjectId", MediaObjectId.ToString());
            rtnRec.Add("MediaObjectURL", MediaObjectURL);
            rtnRec.Add("PreviewImageURL", PreviewImageURL);

            return rtnRec;
        }



    }
}
