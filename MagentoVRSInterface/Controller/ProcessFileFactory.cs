﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagentoVRSInterface.Controller
{
    public class ProcessFileFactory
    {
        List<KeyValuePair<string, FileProcessor>> processorList = new List<KeyValuePair<string, FileProcessor>>();

        public ProcessFileFactory()
        {
            // debug
            //processorList.Add(new KeyValuePair<string, FileProcessor>("customerfile.tab", new ProcessCustomerFile()));
            // break.

            processorList.Add(new KeyValuePair<string, FileProcessor>("attributecode.tab", new ProcessAttributesFile()));
            processorList.Add(new KeyValuePair<string, FileProcessor>("itemattrib.tab", new ProcessItemAttributeFile()));
            processorList.Add(new KeyValuePair<string, FileProcessor>("categoryfile.tab", new ProcessCategoryFile()));
            processorList.Add(new KeyValuePair<string, FileProcessor>("customerfile.tab", new ProcessCustomerFile()));
            processorList.Add(new KeyValuePair<string, FileProcessor>("itemstatus.tab", new ProcessItemStatusFile()));
            processorList.Add(new KeyValuePair<string, FileProcessor>("titemsfile.tab", new ProcessTItemFile()));
            processorList.Add(new KeyValuePair<string, FileProcessor>("tvendorfile.tab", new ProcessVendorFile()));
            processorList.Add(new KeyValuePair<string, FileProcessor>("tproductsfile.tab", new ProcessProductsFile()));
            processorList.Add(new KeyValuePair<string, FileProcessor>("tproductcatfile.tab", new ProcessProductCatFile()));
            processorList.Add(new KeyValuePair<string, FileProcessor>("recomlibrary.tab", new ProcessRecomLibraryFile()));
            processorList.Add(new KeyValuePair<string, FileProcessor>("recomitems.tab", new ProcessRecomItemsFile()));
        }

        public FileProcessor getProcessor(string fileName)
        {
            foreach ( KeyValuePair<string, FileProcessor> currProcessor in processorList)
            {
                if ( fileName.Trim().ToLower().Contains(currProcessor.Key.Trim().ToLower()))
                {
                    return currProcessor.Value;
                }
            }
            return null;
        }
    }
}
