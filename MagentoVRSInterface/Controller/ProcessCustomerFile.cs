﻿using MagentoInterface.DTO;
using MagentoVRSInterface.DTO;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace MagentoVRSInterface.Controller
{
    public class ProcessCustomerFile : FileProcessor
    {
        private List<CustomerShowCostDto> _customerShowCost;

        public override List<KeyValuePair<string, StringBuilder>> processFile(System.Data.IDbConnection stagedConn, string sourceFile)
        {
            //List<VRSCustomerDTO> rawCatList = buildDTOFromFile(sourceFile, contextController.loadId);
            //return processFile(stagedConn, rawCatList, contextController.loadId, contextController.priorGoodLoad);
            return null;
        }
        public override List<KeyValuePair<string, StringBuilder>> processFileBlue(System.Data.IDbConnection stagedConn, IDbConnection blueConn, string sourceFile)
        {
            List<VRSCustomerDTO> rawCatList = buildDTOFromFile(sourceFile, contextController.loadId );
            return processFile(stagedConn, blueConn, rawCatList, contextController.loadId, contextController.priorGoodLoad);
        }
        public List<KeyValuePair<string, StringBuilder>> processFile(IDbConnection stagedConn, IDbConnection blueConn, List<VRSCustomerDTO> rawRecsList, Int32 loadId, Int32 priorGoodId)
        {
            StringBuilder chgFileText = new StringBuilder();
            StringBuilder deleteFileText = new StringBuilder();

            _customerShowCost = CreateCustomerShowCostLookup(blueConn);

            List<KeyValuePair<string, StringBuilder>> rtnList = new List<KeyValuePair<string, StringBuilder>>();

            BulkInsert((SqlConnection)stagedConn, "VRSCustomer", rawRecsList); //HACK sql server specific.

            buildWriteBufferForChanges(stagedConn, loadId, priorGoodId, chgFileText);
            //buildWriteBufferForDeletes(stagedConn, loadId, priorGoodId, deleteFileText);  //TODO remove unneeded code.

            string DestDirectory = ConfigurationManager.AppSettings["FileBuildLocation"].ToString();
            
            string chgFile = DestDirectory + "customerfile" + loadId.ToString("D8") + ".tab";
            rtnList.Add(new KeyValuePair<string, StringBuilder>(chgFile, chgFileText));

            //string deleteFile = DestDirectory + "customerDeletefile" + loadId.ToString("D8") + ".tab";
            //rtnList.Add(new KeyValuePair<string, StringBuilder>(deleteFile, deleteFileText));

            return rtnList;
        }

        private void        buildWriteBufferForDeletes(IDbConnection stagedConn, Int32 loadId, Int32 priorGoodId, StringBuilder deleteFileText)
        {
            List<VRSCustomerDTO> deleteRecords = buildDeletedRecords(stagedConn, loadId, priorGoodId);
            deleteFileText.Append("CustomerAcctNo\n");
            foreach (VRSCustomerDTO dto in deleteRecords)
            {
                deleteFileText.Append(string.Format("{0}\n", dto.CustomerAcctNo));
            }
        }
        private void        buildWriteBufferForChanges(IDbConnection stagedConn, Int32 loadId, Int32 priorGoodId, StringBuilder chgFileText)
        {
            string headers = buildHeaderRecord();

            List<VRSCustomerDTO> changedRecords = buildChangedRecords(stagedConn, loadId, priorGoodId);
            chgFileText.Append(headers);
            foreach (VRSCustomerDTO dto in changedRecords)
            {
                string rec = formatRecordForExport(dto);
                chgFileText.Append(rec);
            }
        }
        protected string    buildHeaderRecord()
        {
            string headers = "CustomerAcctNo\tCompany\tStatus\tEmail\tPassword\tAddrCompany\tAddr1\tAddr2\t";
            headers += "City\tStateProv\tPostalCode\tCountry\tPhone\tTerms\tShipVia\t";
            headers += "rep\tbankcards\tCountryCode\tWebsite\tisShowCost\n";
            headers += "rep\tbankcards\tCountryCode\tWebsite\n";
            return headers;
        }
        protected string    formatRecordForExport(VRSCustomerDTO dto)
        {
            string status = (dto.Terms.Equals("STOP")) ? "no" : "yes";
            string rec = string.Format("{0}\t", dto.CustomerAcctNo);
            rec += string.Format("{0}\t", dto.Company);
            rec += string.Format("{0}\t", status);
            rec += string.Format("{0}\t", dto.Email);
            rec += string.Format("{0}\t", dto.Password);
            rec += string.Format("{0}\t", dto.AddrCompany);
            rec += string.Format("{0}\t", dto.Addr1);
            rec += string.Format("{0}\t", dto.Addr2);
            rec += string.Format("{0}\t", dto.City);
            rec += string.Format("{0}\t", dto.StateProv);
            rec += string.Format("{0}\t", dto.PostalCode);
            rec += string.Format("{0}\t", dto.Country);
            rec += string.Format("{0}\t", dto.Phone);
            rec += string.Format("{0}\t", dto.Terms);
            rec += string.Format("{0}\t", dto.ShipVia);
            rec += string.Format("{0}\t", dto.rep);
            rec += string.Format("{0}\t", dto.bankcards);
            rec += string.Format("{0}\t", dto.CountryCode);
            rec += string.Format("{0}\t", "base");
            rec += string.Format("{0}", FormatCustomerShowCost(dto.CustomerAcctNo));
            rec += "\n";
            return rec;
        }
        public string       FormatCustomerShowCost(string customerAcctNo)
        {
            string returnValue = string.Empty;

            try
            {
                bool showCost = false;

                var exists = _customerShowCost.FirstOrDefault(x => x.CustomerAcctNo == customerAcctNo);

                if (exists != null)
                {
                    showCost = exists.ShowCost;
                }

                if (showCost)
                {
                    returnValue = "Yes";
                }
                else
                {
                    returnValue = "No";
                }
            }
            catch (Exception exception)
            {

            }

            return returnValue;
        }
        public bool         isRegAcct(string loginId)
        {
            //if (IsMasterAcct || IsSubAcct )
            //   IsRegAcct = false;
            if (string.IsNullOrWhiteSpace(loginId) || loginId.Length < 4)
            {
                return false;
            }
            if (isMasterAcct(loginId) || isSubAcct(loginId))
            {
                return false;
            }
            return true;
        }
        public bool         isAllowedSubAcct(string loginId)
        {
            //TODO this list should be put into the database!  We don't want to have to deploy each time to have a change.
            List<string> allowedPrefixes = new List<string>(new string[] { "1AN", "1AH", "1TV", "1LS127", "1LS184", "1LS197" });
            bool matchFound = false;

            //if (IsSubAcct)
            //    IsAllowedSubAcct = (Pattern.matches("1AN((?!000).)*$", LoginID) ||     //matches 1ANxxx sub accouts
            //                         Pattern.matches("1AH((?!000).)*$", LoginID) ||     //matches 1AHxxx sub accouts
            //                         Pattern.matches("1TV((?!000).)*$", LoginID) ||     //matches 1TVxxx sub accouts
            //                         Pattern.matches("1LS127$", LoginID) ||
            //                         Pattern.matches("1LS184$", LoginID) ||
            //                         Pattern.matches("1LS197$", LoginID));
            if (isSubAcct(loginId))
            {
                foreach (string prefix in allowedPrefixes)
                {
                    if (loginId.Length >= prefix.Length && prefix == loginId.Substring(0, prefix.Length))
                    {
                        matchFound = true;
                    }
                }
            }
            return matchFound;
        }
        public bool         isEcomAcct(string loginId)
        {
            //if Login ID starts with 5,6,7 or 8  (Acct could be both MastetAcct and EcomAcct, ex: 8OV000 - overstock )
            List<string> econAcctPrefixes = new List<string>(new string[] { "5", "6", "7", "8" });

            if (string.IsNullOrWhiteSpace(loginId) || loginId.Length < 4)
            {
                return false;
            }
            return econAcctPrefixes.Contains(loginId.Substring(0, 1));
        }
        public bool         isMasterAcct(string loginId)
        {
            //if Login ID starts with a digit and ends with three 0s, then it is a master acct, add a column to the table.
            if (string.IsNullOrWhiteSpace(loginId) || loginId.Length < 4)
            {
                return false;
            }
            if (isNumeric(loginId.Substring(0, 1)) && loginId.Substring(loginId.Length - 3).Equals("000"))
            {
                return true;
            }
            return false;
        }
        public bool         isSubAcct(string loginId)
        {
            //if Login ID starts with a digit and ends with NOT three 0s, then it is a sub acct, block the view of account payable.
            if (string.IsNullOrWhiteSpace(loginId) || loginId.Length < 4)
            {
                return false;
            }
            if (isNumeric(loginId.Substring(0, 1)) && !loginId.Substring(loginId.Length - 3).Equals("000"))
            {
                return true;
            }
            return false;
        }
        private bool        isNumeric(string srcStr)
        {
            string pattern = @"^[-+]?[0-9]*\.?[0-9]*$";
            return Regex.IsMatch(srcStr, pattern);
        }

        protected List<VRSCustomerDTO>      buildDTOFromFile(string sourceFile, Int32 loadId)
        {
            List<VRSCustomerDTO> rawRecords = new List<VRSCustomerDTO>();

            using (StreamReader sr = new StreamReader(sourceFile))
            {
                string line;
                Int32 recNo = 0;
                while ((line = sr.ReadLine()) != null)
                {
                    List<string> fields = line.Split('\t').ToList();
                    VRSCustomerDTO vRSCustomerDTO = new VRSCustomerDTO();
                    vRSCustomerDTO.LoadId = loadId;

                    vRSCustomerDTO.CustomerAcctNo = fields[0];
                    vRSCustomerDTO.Company = fields[1];
                    vRSCustomerDTO.Status = fields[2];
                    vRSCustomerDTO.Email = fields[3];
                    vRSCustomerDTO.Password = fields[4];
                    vRSCustomerDTO.AddrCompany = fields[5];
                    vRSCustomerDTO.Addr1 = fields[6];
                    vRSCustomerDTO.Addr2 = fields[7];
                    vRSCustomerDTO.City = fields[8];
                    vRSCustomerDTO.StateProv = fields[9];
                    vRSCustomerDTO.PostalCode = fields[10];
                    vRSCustomerDTO.Country = fields[11];
                    vRSCustomerDTO.Phone = fields[12];
                    vRSCustomerDTO.Terms = fields[13];
                    vRSCustomerDTO.ShipVia = fields[14];
                    vRSCustomerDTO.NowNumber = fields[15];
                    vRSCustomerDTO.StmtFlag = fields[16];
                    vRSCustomerDTO.OkToFax = fields[17];
                    vRSCustomerDTO.OkToEmail = fields[18];
                    vRSCustomerDTO.rep = fields[19];
                    vRSCustomerDTO.bankcards = fields[20];
                    vRSCustomerDTO.CountryCode = fields[21];

                    if (string.IsNullOrWhiteSpace(vRSCustomerDTO.Email))
                    {
                        vRSCustomerDTO.Email = vRSCustomerDTO.CustomerAcctNo + "@notionsmarketing.com";
                    }

                    if (recNo != 0)
                    {
                        rawRecords.Add(vRSCustomerDTO);
                    }
                    recNo++;
                }
                sr.Close();
            }
            return rawRecords;
        }
        private List<VRSCustomerDTO>        buildChangedRecords(IDbConnection stagedConn, Int32 loadId, Int32 priorGoodId)
        {
            string tableName = "VRSCustomer";
            string orderBy = "CustomerAcctNo";
            string headers = "CustomerAcctNo,Company,Status,Email,Password,AddrCompany,Addr1,Addr2,";
            headers += "City,StateProv,PostalCode,Country,Phone,Terms,ShipVia,NowNumber,StmtFlag,OkToFax,";
            headers += "OkToEmail,rep,bankcards,CountryCode ";

            List<VRSCustomerDTO> rtnList = new List<VRSCustomerDTO>();
            string sql = " SELECT " + headers;
            sql += " FROM " + tableName + " c ";
            sql += string.Format(" WHERE loadId = {0} ", loadId);
            sql += " EXCEPT ";
            sql += " SELECT " + headers;
            sql += " FROM " + tableName + " c2 ";
            sql += string.Format("WHERE loadId = {0}", priorGoodId);
            sql += " Order by " + orderBy + " ";

            DataTable changedRecs = findChangedRecords(stagedConn, sql);

            foreach (DataRow dr in changedRecs.Rows)
            {
                VRSCustomerDTO dto = new VRSCustomerDTO();
                populateDTOFromDataRow(dr, dto);
                dto.LoadId = loadId;
                rtnList.Add(dto);
            }
            return rtnList;
        }
        private List<VRSCustomerDTO>        buildDeletedRecords(IDbConnection stagedConn, Int32 loadId, Int32 priorGoodId)
        {
            string tableName = "VRSCustomer ";
            string orderBy = "CustomerAcctNo ";
            string headers = "CustomerAcctNo ";

            List<VRSCustomerDTO> rtnList = new List<VRSCustomerDTO>();
            string sql = " SELECT " + headers;
            sql += " FROM " + tableName + " c ";
            sql += string.Format(" WHERE loadId = {0} ", priorGoodId);
            sql += " EXCEPT ";
            sql += " SELECT " + headers;
            sql += " FROM " + tableName + " c2 ";
            sql += string.Format(" WHERE loadId = {0}", loadId);
            sql += " Order by " + orderBy + " ";

            DataTable changedRecs = findChangedRecords(stagedConn, sql);

            foreach (DataRow dr in changedRecs.Rows)
            {
                VRSCustomerDTO dto = new VRSCustomerDTO();
                dto.CustomerAcctNo = dr["CustomerAcctNo"].ToString();
                dto.LoadId = loadId;
                rtnList.Add(dto);
            }
            return rtnList;
        }
        private List<CustomerShowCostDto>   CreateCustomerShowCostLookup(IDbConnection blueConn)
        {
            List<CustomerShowCostDto> showCustomerCost = new List<CustomerShowCostDto>();

            string sql = "select * from portal.CustomerShowCost";
            DataTable dataTable = findChangedRecords(blueConn, sql);

            foreach (DataRow dataRow in dataTable.Rows)
            {
                CustomerShowCostDto showCustomerCostDto = new CustomerShowCostDto();
                showCustomerCostDto.CustomerAcctNo = dataRow["CustomerAcctNo"].ToString();
                showCustomerCostDto.ShowCost = Convert.ToBoolean(dataRow["ShowCost"].ToString());
                showCustomerCost.Add(showCustomerCostDto);
            }

            return showCustomerCost;
        }

    }
}
