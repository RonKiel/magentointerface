﻿using MagentoVRSInterface.DAO;
using MagentoVRSInterface.DTO;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagentoVRSInterface.Controller
{
    public class ProcessProductsFile : FileProcessor
    {
        Dictionary<string, string> vendorMapForDBOnlyUsedWhenNotFoundInFile = null;
        Dictionary<string, string> MfgCodeToCompanyNameFromVendorFile = null; 
        List<string> mfgLogged = new List<string>();
        LogController log = LogController.Instance;
        Int32 goodRecords = 0;
        Int32 badRecords = 0;
        Boolean logSimpleRecordsIgnored = false;

        public override List<KeyValuePair<string, StringBuilder>> processFile(System.Data.IDbConnection stagedConn, string sourceFile )
        {
            List<KeyValuePair<string, StringBuilder>> rtnFileList = new List<KeyValuePair<string, StringBuilder>>();

            List<VRSProductDTO> rawRecsFromFile = populateVRSProdctDTORecords(stagedConn, sourceFile);

            contextController.validProductSet = new HashSet<string>();
            foreach (VRSProductDTO dto in rawRecsFromFile)
            {
                contextController.validProductSet.Add(dto.ProductKey);
            }

            rtnFileList = processFile(stagedConn, rawRecsFromFile, contextController.loadId, contextController.priorGoodLoad, contextController.productMap);
            procesProductItemFile(stagedConn, rtnFileList, rawRecsFromFile);

            return rtnFileList;
        }

        public void procesProductItemFile(System.Data.IDbConnection stagedConn, List<KeyValuePair<string, StringBuilder>> rtnFileList, List<VRSProductDTO> rawRecsFromFile)
        {

            List<MagentoProductItemDTO> productItemList = new List<MagentoProductItemDTO>();
            HashSet<string> productsSeen = new HashSet<string>();
            foreach (VRSProductDTO dto in rawRecsFromFile)
            {
                if (!productsSeen.Contains(dto.ProductKey))
                {
                    productsSeen.Add(dto.ProductKey);
                    List<string> skuList = lookupSkuFromProductCode(dto.ProductKey);

                    if (base.intToStringBool(dto.HasVariations).Equals("Yes") || skuList.Count > 1 )
                    {
                        foreach (string currSku in skuList)
                        {
                            if (contextController.validSkuSet.Contains(base.removeAddDateAndTime(currSku)))
                            {
                                productItemList.Add(new MagentoProductItemDTO() { LoadId = contextController.loadId, ProductKey = dto.ProductKey, SKU = currSku });
                            }
                        }
                    }
                }
            }
            BulkInsert((SqlConnection)stagedConn, "MagentoProductItem", productItemList); //HACK sql server specific.

            String sqlTxt = string.Format(" SELECT sku, productkey FROM MagentoProductItem WHERE loadId={0} ", contextController.loadId) +
                            " EXCEPT " +
                            string.Format(" SELECT sku, productkey FROM MagentoProductItem WHERE loadId={0}; ", contextController.priorGoodLoad);

            IDbCommand cmd = stagedConn.CreateCommand();
            cmd.CommandText = sqlTxt;
            cmd.CommandTimeout = 60000;
            IDataReader dr = cmd.ExecuteReader();
            StringBuilder chgProdItemFileText = new StringBuilder();
            while (dr.Read())
            {
                chgProdItemFileText.Append(string.Format("CPGI\t{0}\t{1}\n", dr["productKey"].ToString(), dr["sku"].ToString()));
            }
            dr.Close();

            //
            //  This file has the mapping from Product to sku.
            //
            string DestDirectory = ConfigurationManager.AppSettings["FileBuildLocation"].ToString();
            string prodItemFile = DestDirectory + "productItemfile" + contextController.loadId.ToString("D8") + ".tab";
            rtnFileList.Add(new KeyValuePair<string, StringBuilder>(prodItemFile, chgProdItemFileText));
        }

        public List<VRSProductDTO> populateVRSProdctDTORecords(System.Data.IDbConnection stagedConn, string sourceFile)
        {
            this.vendorMapForDBOnlyUsedWhenNotFoundInFile = contextController.vendorMap;
            goodRecords = 0;
            badRecords = 0;
            this.MfgCodeToCompanyNameFromVendorFile = buildMapFromMfgCodeToMfgName(stagedConn, contextController.loadId);
            List<VRSProductDTO> rawRecsFromFile = buildDTOFromFile(sourceFile, contextController.loadId);

            return rawRecsFromFile;
        }

        public List<KeyValuePair<string, StringBuilder>> processFile(System.Data.IDbConnection stagedConn, List<VRSProductDTO> rawRecsList, int loadId, Int32 priorGoodId, Dictionary<string, string> productMap)
        {
            StringBuilder chgFileText = new StringBuilder();

            List<KeyValuePair<string, StringBuilder>> rtnList = new List<KeyValuePair<string, StringBuilder>>();

            BulkInsert((SqlConnection)stagedConn, "VRSProducts", rawRecsList); //HACK sql server specific.
            //singleInserts(stagedConn, rawRecsList);

            buildWriteBufferForChanges(stagedConn, loadId, priorGoodId, chgFileText);
            buildWriteBufferForDeletes(stagedConn, loadId, priorGoodId, contextController.productDeleteBuffer);

            string DestDirectory = ConfigurationManager.AppSettings["FileBuildLocation"].ToString();

            string chgFile = DestDirectory + "tproductsfile" + loadId.ToString("D8") + ".tab";
            rtnList.Add(new KeyValuePair<string, StringBuilder>(chgFile, chgFileText));

            return rtnList;
        }

        //private void bulkInsertSmallerChuncks(System.Data.IDbConnection stagedConn, List<VRSProductDTO> rawRecsList)
        //{
        //    List<List<VRSProductDTO>> brokenList = new List<List<VRSProductDTO>>();
        //    Int32 i = 0;
        //    foreach (VRSProductDTO dto in rawRecsList)
        //    {
        //        if (brokenList.Count <= i)
        //        {
        //            brokenList.Add(new List<VRSProductDTO>());
        //        }
        //        brokenList[i].Add(dto);
        //        if (brokenList[i].Count % 100 == 0)
        //        {
        //            i++;
        //        }
        //    }
        //    foreach (List<VRSProductDTO> listPtr in brokenList)
        //    {
        //        BulkInsert((SqlConnection)stagedConn, "VRSProducts", listPtr); //HACK sql server specific.
        //    }
        //}

        private List<string> lookupSkuFromProductCode(string productKey )
        {
            List<string> skuList = new List<string>();
            string productKeyLessAddDate = productKey.Substring(0, productKey.IndexOf('-'));

            if (contextController.productToSkusMap.Keys.Contains(productKeyLessAddDate))
            {
                skuList.AddRange(contextController.productToSkusMap[productKeyLessAddDate]);
            }

            return skuList;
        }

        public string calculateMagentoProductKey(string productKey)
        {
            string rtnvalue = "";

            if (contextController.productMap.Keys.Contains(productKey))
            {
                rtnvalue = contextController.productMap[productKey] + "-000000";
            }
            else
            {
                if (productKey.Length >= 3)
                {
                    string prefix = productKey.Substring(0, 2);
                    string sku = productKey.Substring(2);

                    if (contextController.skuMap.Keys.Contains(sku))
                    {
                        rtnvalue = prefix + contextController.skuMap[sku] + "-000000";
                    }
                    else
                    {
                        // Debug missing ADDDT issue (MI-155).
                        //string systemName = log.systemName;
                        //log.systemName = "calculateMagentoProductKey";
                        //log.logMsg(LogController.LogClass.Info, productKey, sku);
                        //log.systemName = systemName;
                        rtnvalue = productKey + "-000000-000000";
                    }
                }
                else
                {
                    // Debug missing ADDDT issue (MI-155).
                    //string systemName = log.systemName;
                    //log.systemName = "calculateMagentoProductKey";
                    //log.logMsg(LogController.LogClass.Info, productKey, string.Empty);
                    //log.systemName = systemName;
                    rtnvalue = productKey + "-000000-000000";
                }
            }

            return rtnvalue;
        }

        private void buildWriteBufferForDeletes(System.Data.IDbConnection stagedConn, int loadId, Int32 priorGoodId, StringBuilder deleteFileText)
        {
            List<VRSProductDTO> deleteRecords = buildDeletedRecords(stagedConn, loadId, priorGoodId);
            foreach (VRSProductDTO dto in deleteRecords)
            {
                deleteFileText.Append(string.Format("-CP\t{0}\n", dto.ProductKey));
            }
        }

        private void buildWriteBufferForChanges(System.Data.IDbConnection stagedConn, int loadId, Int32 priorGoodId, StringBuilder chgFileText )
        {
            contextController.changedProductSet = new HashSet<string>();
            List<VRSProductSortKeyDTO> changedRecords = buildChangedRecords(stagedConn, loadId, priorGoodId);
            List<string> flds = new List<string>(new string[] { 
               "SKU", "MfgCode", "Manufacturer", "CommonPartNum", "ProductKey", "HasVariations",
               "IsActive","Keywords","CatalogPage","ProductName","RetailerDesc","NewDate","frtcollect",
               "unitmeas","ormd","countryavail","countryunavail","dsminorder","dsvendorreq","consumerdesc",
               "notsearch","prodclearance","Countryorigin","Tariffcode","url_key", "product_type",
               "imperialDepth", "imperialHeight", "imperialWidth", "metricDepth", "metricHeight", "metricWidth", "SortKey"
            });
            string headers = string.Join("\t", flds) + "\n";
            chgFileText.Append(headers);

            foreach (VRSProductSortKeyDTO dto in changedRecords)
            {
                contextController.changedProductSet.Add(dto.ProductKey);

                string tRec = "";
                string mfgName = "";
                lookupMfgrName(dto.MfgCode.Trim(), dto.ProductKey, out mfgName, dto.ToString());

                tRec += dto.ProductKey + "\t";
                tRec += dto.MfgCode.Trim() + "\t";
                tRec += mfgName + "\t";
                tRec += dto.CommonPartNum + "\t";
                tRec += base.removeAddDateAndTime(dto.ProductKey) + "\t";
                tRec += base.intToStringBool(dto.HasVariations) + "\t";
                tRec += base.intToEnabledDisabled(dto.IsActive) + "\t";
                tRec += dto.Keywords + "\t";
                tRec += dto.CatalogPage + "\t";
                tRec += dto.ShortDesc + "\t"; //ProductName
                tRec += dto.LongDesc + "\t";  //RetailerDesc
                tRec += dto.NewDate + "\t";
                tRec += dto.frtcollect + "\t";
                tRec += dto.unitmeas + "\t";
                tRec += dto.ormd + "\t";
                tRec += dto.countryavail + "\t";
                tRec += dto.countryunavail + "\t";
                tRec += dto.dsminorder + "\t";
                tRec += dto.dsvendorreq + "\t";
                tRec += dto.consumerdesc + "\t";
                tRec += ((dto.notsearch == "0") ? "Catalog, Search" : "Not visible individually") + "\t";
                tRec += dto.prodclearance + "\t";
                tRec += dto.countryorigin + "\t"; //Countryorigin
                tRec += dto.tariffcode + "\t";    //Tariffcode
                tRec += base.urlKeyFormatting(dto.ShortDesc, dto.ProductKey).Trim().ToLower()+"\t";    //url_key
                tRec += ((dto.HasVariations.Trim() == "1") ? "grouped" : "simple") + "\t";
                tRec += dto.imperialDepth + "\t";
                tRec += dto.imperialHeight + "\t";
                tRec += dto.imperialWidth + "\t";
                tRec += dto.metricDepth + "\t";
                tRec += dto.metricHeight + "\t";
                tRec += dto.metricWidth + "\t";
                tRec += dto.SortKey;
                tRec += "\n";

                chgFileText.Append(tRec);
            }

            log.logMsg(LogController.LogClass.Info, string.Format("Product file import status. Good Records:({0}) Bad Records:({1}) Changed Records:({2})", goodRecords, badRecords, changedRecords.Count));
        }

        private List<VRSProductDTO> buildDTOFromFile(string sourceFile, Int32 loadId)
        {
            List<VRSProductDTO> rawRecords = new List<VRSProductDTO>();
            contextController.badProductsBecauseOfMfgr = new List<string>();

            using (StreamReader sr = new StreamReader(sourceFile))
            {
                string line;
                Int32 recNo = 0;
                while ((line = sr.ReadLine()) != null)
                {
                    List<string> fields = line.Split('\t').ToList();
                    VRSProductDTO vRSProductDTO = new VRSProductDTO();

                    vRSProductDTO.LoadId = loadId;
                    vRSProductDTO.MfgCode = fields[0];
                    vRSProductDTO.CommonPartNum = fields[1];
                    vRSProductDTO.ProductKey = fields[2];
                    vRSProductDTO.HasVariations = fields[3];
                    vRSProductDTO.IsActive = fields[4];
                    vRSProductDTO.Keywords = fields[5];
                    vRSProductDTO.CatalogPage = fields[6];
                    vRSProductDTO.ShortDesc = fields[7];
                    vRSProductDTO.LongDesc = fields[8];
                    vRSProductDTO.NewDate = fields[9];
                    vRSProductDTO.frtcollect = fields[10];
                    vRSProductDTO.unitmeas = fields[11];
                    vRSProductDTO.OldCommonPartNum = fields[12];
                    vRSProductDTO.ormd = fields[13];
                    vRSProductDTO.noexport = fields[14];
                    vRSProductDTO.countryavail = fields[15];
                    vRSProductDTO.countryunavail = fields[16];
                    vRSProductDTO.dsminorder = fields[17];
                    vRSProductDTO.dsvendorreq = fields[18];
                    vRSProductDTO.consumerdesc = fields[19];
                    vRSProductDTO.notsearch = fields[20];
                    vRSProductDTO.prodclearance = fields[21];
                    vRSProductDTO.countryorigin = fields[22];
                    vRSProductDTO.tariffcode = fields[23];
                    vRSProductDTO.map = fields[24];
                    vRSProductDTO.imperialDepth = fields[25];
                    vRSProductDTO.imperialHeight = fields[26];
                    vRSProductDTO.imperialWidth = fields[27];
                    vRSProductDTO.metricDepth = fields[28];
                    vRSProductDTO.metricHeight = fields[29];
                    vRSProductDTO.metricWidth = fields[30];

                    if (recNo != 0)
                    {
                        vRSProductDTO.ProductKey = calculateMagentoProductKey(vRSProductDTO.ProductKey);
                        List<string> skuList = lookupSkuFromProductCode(vRSProductDTO.ProductKey);
                        string firstSku = skuList.FirstOrDefault();
                        string mfgName = "";
                        Boolean goodMfg = lookupMfgrName(vRSProductDTO.MfgCode.Trim(), firstSku, out mfgName, line);
                        if (goodMfg)
                        {
                            rawRecords.Add(vRSProductDTO);
                            goodRecords++;
                        }
                        else
                        {
                            badRecords++;
                            contextController.badProductsBecauseOfMfgr.Add(vRSProductDTO.ProductKey);
                        }
                    }
                    recNo++;
                }
                sr.Close();
            }
            return rawRecords;
        }

        // This is called from other file processors.  
        // We needed this because we had a chicken and egg case.  Where We need to process the
        // Simple items first because that contaned the informaton as to what minors are contained
        // in what product.  However The items needed to verify that the products will be processed
        // correctly.  If not, they need to be filtered out, and log an error.
        public List<VRSProductDTO> buildRawDTOFromFileNoChangesFromFile(string sourceFile, Int32 loadId)
        {
            List<VRSProductDTO> rawRecords = new List<VRSProductDTO>();

            using (StreamReader sr = new StreamReader(sourceFile))
            {
                string line;
                Int32 recNo = 0;
                while ((line = sr.ReadLine()) != null)
                {
                    List<string> fields = line.Split('\t').ToList();
                    VRSProductDTO vRSProductDTO = new VRSProductDTO();

                    vRSProductDTO.LoadId = loadId;
                    vRSProductDTO.MfgCode = fields[0];
                    vRSProductDTO.CommonPartNum = fields[1];
                    vRSProductDTO.ProductKey = fields[2];
                    vRSProductDTO.HasVariations = fields[3];
                    vRSProductDTO.IsActive = fields[4];
                    vRSProductDTO.Keywords = fields[5];
                    vRSProductDTO.CatalogPage = fields[6];
                    vRSProductDTO.ShortDesc = fields[7];
                    vRSProductDTO.LongDesc = fields[8];
                    vRSProductDTO.NewDate = fields[9];
                    vRSProductDTO.frtcollect = fields[10];
                    vRSProductDTO.unitmeas = fields[11];
                    vRSProductDTO.OldCommonPartNum = fields[12];
                    vRSProductDTO.ormd = fields[13];
                    vRSProductDTO.noexport = fields[14];
                    vRSProductDTO.countryavail = fields[15];
                    vRSProductDTO.countryunavail = fields[16];
                    vRSProductDTO.dsminorder = fields[17];
                    vRSProductDTO.dsvendorreq = fields[18];
                    vRSProductDTO.consumerdesc = fields[19];
                    vRSProductDTO.notsearch = fields[20];
                    vRSProductDTO.prodclearance = fields[21];
                    vRSProductDTO.countryorigin = fields[22];
                    vRSProductDTO.tariffcode = fields[23];
                    vRSProductDTO.map = fields[24];
                    vRSProductDTO.imperialDepth = fields[25];
                    vRSProductDTO.imperialHeight = fields[26];
                    vRSProductDTO.imperialWidth = fields[27];
                    vRSProductDTO.metricDepth = fields[28];
                    vRSProductDTO.metricHeight = fields[29];
                    vRSProductDTO.metricWidth = fields[30];

                    if (recNo != 0)
                    {
                        rawRecords.Add(vRSProductDTO);
                        goodRecords++;
                    }
                    recNo++;
                }
                sr.Close();
            }
            return rawRecords;
        }

        private List<VRSProductSortKeyDTO> buildChangedRecords(IDbConnection stagedConn, Int32 loadId, Int32 priorGoodId)
        {
            List<VRSProductSortKeyDTO> rtnList  = new List<VRSProductSortKeyDTO>();
            string sql                          = string.Format("select ProductKey,SortKey from VRSProductCat where LoadId={0} order by ProductKey", loadId);
            DataTable dataTableProductCat       = findChangedRecords(stagedConn, sql);
            DataView dataViewProductCat         = new DataView(dataTableProductCat);
            dataViewProductCat.Sort             = "ProductKey";

            sql = string.Format("select MfgCode,CommonPartNum,ProductKey,HasVariations,IsActive,Keywords," +
                "CatalogPage,ShortDesc,LongDesc,NewDate,frtcollect,unitmeas,OldCommonPartNum,ormd,noexport," +
                "countryavail,countryunavail,dsminorder,dsvendorreq,consumerdesc,notsearch,prodclearance," +
                "countryorigin,tariffcode,map,imperialDepth,imperialHeight,imperialWidth,metricDepth,metricHeight,metricWidth " +
                "from VRSProducts c " +
                "where LoadId = {0} " +
                "except " +
                "select MfgCode,CommonPartNum,ProductKey,HasVariations,IsActive,Keywords," +
                "CatalogPage,ShortDesc,LongDesc,NewDate,frtcollect,unitmeas,OldCommonPartNum,ormd,noexport," +
                "countryavail,countryunavail,dsminorder,dsvendorreq,consumerdesc,notsearch,prodclearance," +
                "countryorigin,tariffcode,map,imperialDepth,imperialHeight,imperialWidth,metricDepth,metricHeight,metricWidth " +
                "from VRSProducts c2 " +
                "where LoadId = {1} " +
                "order by c.CommonPartNum", loadId, priorGoodId);
            DataTable changedRecs = findChangedRecords(stagedConn, sql);

            foreach (DataRow dr in changedRecs.Rows)
            {
                VRSProductSortKeyDTO dto = new VRSProductSortKeyDTO();
                populateDTOFromDataRow(dr, dto);
                dto.LoadId = loadId;
                dto.SortKey = GetProductCategorySortKey(dataViewProductCat, dto.ProductKey);
                rtnList.Add(dto);
            }
            return rtnList;
        }

        private string GetProductCategorySortKey(DataView dataViewProductCat, string productKey)
        {
            string returnValue = string.Empty;
            int rowIndex = dataViewProductCat.Find(new object[] { productKey.Substring(0,8) });
            if (rowIndex != -1)
            {
                returnValue = dataViewProductCat[rowIndex].Row["SortKey"].ToString();
            }

            return returnValue;
        }

        private List<VRSProductDTO> buildDeletedRecords(IDbConnection stagedConn, Int32 loadId, Int32 priorGoodId)
        {
            List<VRSProductDTO> rtnList = new List<VRSProductDTO>();

            string tableName = "VRSProducts ";
            string orderBy = "ProductKey ";
            string headers = "ProductKey ";

            string sql = " SELECT " + headers;
            sql += " FROM " + tableName + " c ";
            sql += string.Format(" WHERE loadId = {0} ", priorGoodId);
            sql += " EXCEPT ";
            sql += " SELECT " + headers;
            sql += " FROM " + tableName + " c2 ";
            sql += string.Format(" WHERE loadId = {0}", loadId);
            sql += " Order by " + orderBy + " ";

            DataTable changedRecs = findChangedRecords(stagedConn, sql);

            foreach (DataRow dr in changedRecs.Rows)
            {
                VRSProductDTO dto = new VRSProductDTO();
                dto.LoadId = loadId;
                dto.ProductKey = dr["ProductKey"].ToString();
                rtnList.Add(dto);
            }
            return rtnList;
        }

        private Boolean lookupMfgrName(string mfgrCode, string productKey, out string companyName, string line )
        {
            Boolean success = true;

            if (MfgCodeToCompanyNameFromVendorFile.Keys.Contains(mfgrCode))
            {
                companyName = MfgCodeToCompanyNameFromVendorFile[mfgrCode];
            }
            else
            {
                if (vendorMapForDBOnlyUsedWhenNotFoundInFile.Keys.Contains(mfgrCode))
                {
                    log.logMsg(LogController.LogClass.Warning, string.Format("Item:({0}) Using mfg from DB rather than file for MfgCode ({1})", productKey, mfgrCode));
                    companyName = vendorMapForDBOnlyUsedWhenNotFoundInFile[mfgrCode];
                    success = false; //Let's not use this anymore, and filter out the item.
                }
                else
                {
                    log.logMsg(LogController.LogClass.Warning, string.Format("Product ({0}) has invalid MfgCode ({1}) Raw Line:({2})", productKey, mfgrCode, line));
                    companyName = "MISSING";
                    success = false;
                }
            }
            
            return success;
        }

        public Dictionary<string, string> buildMapFromMfgCodeToMfgName(IDbConnection stagedConn, Int32 loadId)
        {
            Dictionary<string, string> MfgCodeToCompanyName = new Dictionary<string, string>();
            ProcessVendorFile processVendorFile = new ProcessVendorFile();
            List<VRSVendorDTO> vendorList = processVendorFile.buildChangedRecords(stagedConn, loadId, -1); //-1 will cause all of the records to be returned.
            foreach (VRSVendorDTO dto in vendorList)
            {
                MfgCodeToCompanyName.Add(dto.MfgCode.Trim(), dto.CompanyName.Trim());
            }
            return MfgCodeToCompanyName;
        }

    }

}
