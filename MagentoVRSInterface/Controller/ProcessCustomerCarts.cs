﻿using MagentoVRSInterface.VRSCartService;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MagentoVRSInterface.Controller
{
    public class ProcessCustomerCarts :FileProcessor
    {
        public ICartServiceController cartServiceController = new CartServiceController();
        public ILogController log = LogController.Instance;

        public Dictionary<string, ResponseCustomerCartsDTO> customerWithCartsDict = new Dictionary<string, ResponseCustomerCartsDTO>();
        public Dictionary<string, string> customerWithPasswordDict = new Dictionary<string, string>();

        public object sharedObject = new object();
        public List<string> allCustomers = new List<string>();
        public List<string> reservedCustomers = new List<string>();
        public List<string> redCustomers = new List<string>();
        public List<string> reserveFortransmitCustomers = new List<string>();
        public List<string> transmittedCustomers = new List<string>();

        public override List<KeyValuePair<string, StringBuilder>> processFile(System.Data.IDbConnection stagedConn, string sourceFile )
        {
            if (!File.Exists(sourceFile))
            {
                log.logMsg(LogController.LogClass.Warning, "No customer data file was available to call API with.");
            }
            else
            {
                initializeStartStructures(sourceFile);

                for (Int32 x = 0; x < 7; x++)
                {
                    Thread reader = new Thread(new ThreadStart(readDataFromSource));
                    reader.Start();
                }
                for (Int32 x = 0; x < 5; x++)
                {
                    Thread writer = new Thread(new ThreadStart(transmitCartData));
                    writer.Start();
                }

                Thread completionThread = new Thread(new ThreadStart(checkForCompletion));
                completionThread.Start();
                completionThread.Join();

            }

            return new List<KeyValuePair<string,StringBuilder>>(); //No files are written.
        }

        public void initializeStartStructures(string sourceFile)
        {
            List<string> customerFile = File.ReadAllLines(sourceFile).ToList();
            customerWithPasswordDict = populateCustomerPwdDict(customerFile);
            allCustomers = customerWithPasswordDict.Keys.ToList();
        }

        public void readDataFromSource()
        {
            string nextCustomer = "FIRSTTime";
            string password = "";

            while (nextCustomer != null)
            {
                lock (sharedObject)
                {
                    nextCustomer = allCustomers.Except(reservedCustomers).ToList().FirstOrDefault();
                    if (nextCustomer != null)
                    {
                        reservedCustomers.Add(nextCustomer);
                        password = customerWithPasswordDict[nextCustomer];
                    }
                }

                if (nextCustomer != null)
                {
                    ResponseCustomerCartsDTO customerCarts = cartServiceController.GetCustomerCarts(nextCustomer);
                    customerCarts = appendCartsWithNoItems(nextCustomer, password, customerCarts); //If possible will order by the weblink API
                    lock (sharedObject)
                    {
                        customerWithCartsDict.Add(nextCustomer, customerCarts);
                        redCustomers.Add(nextCustomer);
                    }
                }
            }
        }

        private ResponseCustomerCartsDTO appendCartsWithNoItems(string nextCustomer, string password, ResponseCustomerCartsDTO customerCarts)
        {
            ResponseCustomerCartsDTO rtnResponseCustomerCartsDTO = customerCarts;
            List<KeyValuePair<Int32?, string>> allCarts = getCartListFromWeblink(nextCustomer, password);
            List<string> cartsWithData = new List<string>();
            foreach (CartDataDTO cart in customerCarts.cartList)
            {
                cartsWithData.Add(cart.cartName);
            }
            List<string> allCartNames = new List<string>();
            foreach (KeyValuePair<Int32?, string> kp in allCarts)
            {
                allCartNames.Add(kp.Value);
            }

            List<string> missingCarts = allCartNames.Except(cartsWithData).ToList();
            foreach (string missingCart in missingCarts)
            {
                customerCarts.cartList.Add(new CartDataDTO() { cartName = missingCart });
            }
            rtnResponseCustomerCartsDTO = ifAllCartNamesAreUniquePutInOrderByWeblinkAPI(customerCarts, rtnResponseCustomerCartsDTO, allCartNames);
            return rtnResponseCustomerCartsDTO;
        }

        private ResponseCustomerCartsDTO ifAllCartNamesAreUniquePutInOrderByWeblinkAPI(ResponseCustomerCartsDTO customerCarts, ResponseCustomerCartsDTO rtnResponseCustomerCartsDTO, List<string> allCartNames)
        {
            List<string> distinctNames = allCartNames.Distinct().ToList();
            if (allCartNames.Count == distinctNames.Count)
            {
                rtnResponseCustomerCartsDTO = new ResponseCustomerCartsDTO();

                rtnResponseCustomerCartsDTO.statusCode = customerCarts.statusCode;
                rtnResponseCustomerCartsDTO.statusMsg = customerCarts.statusMsg;
                rtnResponseCustomerCartsDTO.customerAcctNum = customerCarts.customerAcctNum;
                rtnResponseCustomerCartsDTO.cartList = new List<CartDataDTO>();

                foreach (string cartName in allCartNames)
                {
                    CartDataDTO cart = customerCarts.cartList.Where(x => x.cartName == cartName).SingleOrDefault();
                    if (cart != null)
                    {
                        rtnResponseCustomerCartsDTO.cartList.Add(cart);
                    }
                }
            }
            return rtnResponseCustomerCartsDTO;
        }

        public List<KeyValuePair<Int32?, string>> getCartListFromWeblink(string customer, string password)
        {
            ICartServiceController cartServiceControllerForCartList = new CartServiceController();
            cartServiceControllerForCartList.setUrlForWeblinkInterface("https://store.notionsmarketing.com/WeblinkApi");
            List<KeyValuePair<Int32?, string>> carts = new List<KeyValuePair<Int32?, string>>();

            BasicWeblinkRequestDTO requestForCartList = new BasicWeblinkRequestDTO();
            requestForCartList.appName = "Magento Migration Tool";
            requestForCartList.appVersion = "1";
            requestForCartList.partnerCode = "NMC"; //For production NMC, for test NDEV
            requestForCartList.loginId = customer;
            requestForCartList.accountNumber = requestForCartList.loginId;
            requestForCartList.password = password;
            requestForCartList.actionCode = "ExportCarts";
            ResponseExportCartListDTO cartExportDTO = cartServiceControllerForCartList.sendExportCartListToWeblink(requestForCartList);

            foreach(CartDataDTO cartData in cartExportDTO.cartList) 
            {
                carts.Add( new KeyValuePair<Int32?,string>(cartData.cartId,cartData.cartName));
            }

            return carts;
        }

        public void checkForCompletion()
        {
            bool areWeDone = false;
            while (!areWeDone)
            {
                lock (sharedObject)
                {
                    if (transmittedCustomers.Count == allCustomers.Count)
                    {
                        areWeDone = true;
                    }
                }
                Thread.Sleep(5000);
            }
        }

        public void transmitCartData()
        {
            bool areWeDone = false;
            string nextCustomer = null;

            while (!areWeDone)
            {
                lock (sharedObject)
                {
                    nextCustomer = redCustomers.Except(reserveFortransmitCustomers).ToList().FirstOrDefault();
                    if (nextCustomer == null)
                    {
                        if (transmittedCustomers.Count == allCustomers.Count)
                        {
                            areWeDone = true;
                        }
                    }
                    else
                    {
                        reserveFortransmitCustomers.Add(nextCustomer);
                    }
                }

                //Let the reader build up a queue.
                if (nextCustomer == null && !areWeDone)
                {
                    log.logMsg(LogController.LogClass.Debug, "Pausing for reader...");
                    Thread.Sleep(5000);
                }

                if (nextCustomer != null)
                {
                    Int32 importOverrideNumber = 0;
                    string password = null;

                    lock (sharedObject)
                    {
                        password = customerWithPasswordDict[nextCustomer];
                    }
                    
                    foreach (CartDataDTO cartdata in customerWithCartsDict[nextCustomer].cartList)
                    {
                        RequestQueueCartDTO request = formatACartRequest(nextCustomer, password, ref importOverrideNumber, cartdata);
                        ResponseQueueCartDTO response = cartServiceController.sendSingleCart(request);
                        if ( !"OK".Equals(response.statusMsg))
                        {
                            string msg = string.Format("Transmit Cart Error:({0}) Customer:({1}) Cart:({2})", 
                                response.statusMsg, nextCustomer, cartdata.cartName.Replace("\t", ""));
                            log.logMsg(LogController.LogClass.Error, msg);
                        }
                    }

                    lock (sharedObject)
                    {
                        transmittedCustomers.Add(nextCustomer);
                    }
                }
            }

        }

        private RequestQueueCartDTO formatACartRequest(string nextCustomer, string pasword, ref Int32 importOverrideNumber, CartDataDTO cartdata)
        {
            RequestQueueCartDTO request = new RequestQueueCartDTO();
            string partnerCode = ConfigurationManager.AppSettings["PartnerCode"].ToString();
            request.appName = "Magento Migration Tool";
            request.appVersion = "1";
            request.partnerCode = partnerCode;
            request.loginId = nextCustomer;
            request.accountNumber = request.loginId;
            request.password = pasword;
            request.actionCode = "QueueCart";
            request.cartId = 0;

            string cartName = cartdata.cartName;
            List<string> barcodeWithQtyList = new List<string>();
            if (string.IsNullOrWhiteSpace(cartName))
            {
                cartName = string.Format("Imported Cart:{0}", ++importOverrideNumber);
            }
            request.cartName = cartName;

            foreach (CartItemDataDTO item in cartdata.cartItemList)
            {
                string itemCode = "";
                if (!string.IsNullOrWhiteSpace(item.SKU))
                {
                    itemCode = item.SKU;
                }
                else
                {
                    if (!string.IsNullOrWhiteSpace(item.AltPartNum))
                    {
                        itemCode = item.AltPartNum;
                    }
                }
                if (!string.IsNullOrWhiteSpace(itemCode))
                {
                    barcodeWithQtyList.Add(itemCode);
                    if (item.Qty > 1)
                    {
                        barcodeWithQtyList.Add(string.Format("Q{0}", (item.Qty - 1)));
                    }
                }
            }
            request.barcodeList = barcodeWithQtyList;
            return request;
        }

        public Dictionary<string, string> populateCustomerPwdDict( List<string> customerFile)
        {
            Dictionary<string, string> rtnData = new Dictionary<string, string>();

            Int32 recNo = 0;
            Int32 CustomerAcctNoPosn = -1;
            Int32 PasswordPosn = -1;

            foreach (string rec in customerFile)
            {
                List<string> flds = rec.Split('\t').ToList();

                if (flds.Count > 4)
                {
                    if (recNo == 0)
                    {
                        for (Int32 x = 0; x < flds.Count; x++)
                        {
                            if (flds[x].Trim().ToLower().Equals("customeracctno"))
                            {
                                CustomerAcctNoPosn = x;
                            }
                            else if (flds[x].Trim().ToLower().Equals("password"))
                            {
                                PasswordPosn = x;
                            }
                        }
                    }
                    else
                    {
                        string customerNo = flds[CustomerAcctNoPosn].Trim();
                        string password = flds[PasswordPosn].Trim();
                        rtnData.Add(customerNo, password);
                    }
                    recNo++;
                }
            }

            return rtnData;
        }

        private StringBuilder addCustomerCartInformationForCustomer(Int32 recCount, string rec)
        {
            StringBuilder sb = new StringBuilder();
            Int32 importOverrideNumber = 0;

            List<string> flds = rec.Split('\t').ToList();
            string customerNo = flds[0];

            if (recCount != 0)
            {
                ResponseCustomerCartsDTO customerCarts = cartServiceController.GetCustomerCarts(customerNo);

                foreach (CartDataDTO cartdata in customerCarts.cartList)
                {
                    string cartName = cartdata.cartName.Replace("\t", "");
                    if (string.IsNullOrWhiteSpace(cartName))
                    {
                        cartName = string.Format("Imported Cart:{0}", ++importOverrideNumber);
                    }
                    foreach (CartItemDataDTO item in cartdata.cartItemList)
                    {
                        sb.Append(string.Format("{0}\t{1}\t{2}\t{3}\n", customerNo, cartName.Replace("\t", ""), item.SKU, item.Qty));
                    }
                }
            }
            else
            {
                sb.Append(string.Format("{0}\t{1}\t{2}\t{3}\n", "CustomerAcctNo", "CartName", "LegacySKU", "Qty"));
            }
            return (sb);
        }

    }
}
