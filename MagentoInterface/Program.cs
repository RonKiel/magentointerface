﻿using MagentoInterface.Controller;
using MagentoInterface.DAO;
using MagentoInterface.DTO;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MagentoInterface
{
    class Program
    {
        static void Main(string[] args)
        {
            BlueScreenUtil bsu = new BlueScreenUtil();

            IDbConnection blueScreenConnection = bsu.getConnection(ConfigurationManager.ConnectionStrings["BlueScreen"].ProviderName,
                                                   ConfigurationManager.ConnectionStrings["BlueScreen"].ConnectionString);

            // IDbConnection blueScreenConnection = bsu.getConnection(ConfigurationManager.ConnectionStrings["BJRTest"].ProviderName,
            //                                       ConfigurationManager.ConnectionStrings["BJRTest"].ConnectionString);


            IDbConnection stagedConn = bsu.getConnection(ConfigurationManager.ConnectionStrings["StagedData"].ProviderName,
                                                   ConfigurationManager.ConnectionStrings["StagedData"].ConnectionString);

            CategoryController categoryController = new CategoryController();
            CustomerController customerController = new CustomerController();

            try
            {
                System.Console.WriteLine("Run at: " + DateTime.Now.ToString());
                System.Console.WriteLine("Output Directory: " + ConfigurationManager.AppSettings["FileBuildLocation"].ToString());

                System.Console.WriteLine("Building Category Files");
                List<string> categoryFiles = categoryController.buildFiles(blueScreenConnection, stagedConn);

                System.Console.WriteLine("Building Customer Files");
                List<string> customerFiles = customerController.buildFiles(blueScreenConnection, stagedConn);
                
                System.Console.WriteLine("Done");
            }
            catch (Exception ex1)
            {
                System.Console.WriteLine("Exception: " + ex1.Message);
            } 
            finally
            {
                blueScreenConnection.Close();
                stagedConn.Close();
                Thread.Sleep(2000);
            }
            

        }




    }
}


