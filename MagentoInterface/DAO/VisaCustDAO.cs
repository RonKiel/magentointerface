﻿using MagentoInterface.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagentoInterface.DAO
{
    public class VisaCustDAO : BaseDAO
    {
        public override string getTableName()
        {
            return "visacust";
        }
        public override string getPrimaryKey()
        {
            return null;
        }
        public override object getEmptyObject()
        {
            return new VisaCustDTO();
        }
        public override List<string> getInsertIgnoreList()
        {
            return new List<string>();
        }
        public override string getIdentityColumnName()
        {
            return null;
        }

        public List<VisaCustDTO> convertToMyList(List<object> rawList)
        {
            List<VisaCustDTO> rtnList = new List<VisaCustDTO>();
            foreach (object rawobj in rawList)
            {
                rtnList.Add((VisaCustDTO)rawobj);
            }
            return rtnList;
        }

        public List<VisaCustDTO> selectRecordsRtnList(IDbConnection currConnection, string whereClause)
        {
            return selectRecordsRtnList(currConnection, whereClause, null, null);
        }
        public List<VisaCustDTO> selectRecordsRtnList(IDbConnection currConnection, string whereClause, string orderBy)
        {
            return selectRecordsRtnList(currConnection, whereClause, null, orderBy);
        }
        public List<VisaCustDTO> selectRecordsRtnList(IDbConnection currConnection, string whereClause, List<IDbDataParameter> parameters)
        {
            return selectRecordsRtnList(currConnection, null, whereClause, parameters, null);
        }
        public List<VisaCustDTO> selectRecordsRtnList(IDbConnection currConnection, string whereClause, List<IDbDataParameter> parameters, string orderBy)
        {
            return selectRecordsRtnList(currConnection, null, whereClause, parameters, orderBy);
        }
        public List<VisaCustDTO> selectRecordsRtnList(IDbConnection currConnection, IDbTransaction currTran, string whereClause)
        {
            return selectRecordsRtnList(currConnection, currTran, whereClause, null, null);
        }
        public List<VisaCustDTO> selectRecordsRtnList(IDbConnection currConnection, IDbTransaction currTran, string whereClause, string orderBy)
        {
            return selectRecordsRtnList(currConnection, currTran, whereClause, null, orderBy);
        }
        public List<VisaCustDTO> selectRecordsRtnList(IDbConnection currConnection, IDbTransaction currTran, string whereClause, List<IDbDataParameter> parameters, string orderBy)
        {
            DataTable dt = null;
            if (string.IsNullOrWhiteSpace(orderBy))
            {
                dt = base.selectRecordsRtnDataTable(currConnection, currTran, whereClause, parameters);
            }
            else
            {
                dt = base.sortDataTable(base.selectRecordsRtnDataTable(currConnection, currTran, whereClause, parameters), orderBy);
            }
            List<object> rawObjects = base.moveFromDataRowsToRawObjectList(dt);
            return convertToMyList(rawObjects);
        }
    }
}
