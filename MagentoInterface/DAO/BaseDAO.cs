﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace MagentoInterface.DAO
{
    public abstract class BaseDAO
    {
        string replacementQuery = "";
        public abstract string getTableName();
        public abstract string getPrimaryKey();
        public abstract object getEmptyObject();
        public virtual List<string> getInsertIgnoreList()
        {
            return new List<string>(new string[] { getPrimaryKey() });
        }
        public virtual string getIdentityColumnName()
        {
            return getPrimaryKey();
        }
        public virtual void beforeInsert(object anyDTO) { }
        public virtual void beforeDelete(object anyDTO) { }
        public virtual void beforeUpdate(object anyDTO) { }
        public virtual void beforeSelect(IDbConnection currConnection) { }

        public void setBuldSelectStatementQuery(string rawquery)
        {
            replacementQuery = rawquery;
        }

        public IDbDataParameter addParameterWithValue(IDbConnection currConnection, string name, object value)
        {
            IDbCommand cmd = currConnection.CreateCommand();
            IDbDataParameter parm = cmd.CreateParameter();
            parm.ParameterName = name;
            parm.Value = value;
            return parm;
        }

        public DataTable sortDataTable(DataTable src, string orderByClause)
        {
            DataView dv = src.DefaultView;
            dv.Sort = orderByClause;
            DataTable sortedDT = dv.ToTable();
            return sortedDT;
        }

        public IDataReader selectRecordsRtnReader(IDbConnection currConnection, string whereClause)
        {
            return selectRecordsRtnReader(currConnection, whereClause, null);
        }
        public IDataReader selectRecordsRtnReader(IDbConnection currConnection, string whereClause, List<IDbDataParameter> parameters)
        {
            return selectRecordsRtnReader(currConnection, null, whereClause, parameters);
        }
        public IDataReader selectRecordsRtnReader(IDbConnection currConnection, IDbTransaction currTran, string whereClause)
        {
            return selectRecordsRtnReader(currConnection, currTran, whereClause, null);
        }
        public IDataReader selectRecordsRtnReader(IDbConnection currConnection, IDbTransaction currTran, string whereClause, List<IDbDataParameter> parameters)
        {
            beforeSelect(currConnection);
            IDbCommand cmd = currConnection.CreateCommand();
            cmd.Connection = currConnection;
            cmd.Transaction = currTran;
            string whereIfRelevant = (string.IsNullOrWhiteSpace(whereClause)) ? "" : " WHERE " + whereClause;
            cmd.CommandText = getBaseQuery() + " " + whereIfRelevant;

            if (parameters != null)
            {
                foreach (IDataParameter p in parameters)
                {
                    cmd.Parameters.Add(p);
                }
            }
            IDataReader dr = cmd.ExecuteReader();
            return dr;
        }

        private string getBaseQuery()
        {
            if (string.IsNullOrWhiteSpace(replacementQuery))
            {
                return "SELECT * FROM " + getTableName();
            }
            return replacementQuery;
        }

        public DataTable selectRecordsRtnDataTable(IDbConnection currConnection, string whereClause)
        {
            return selectRecordsRtnDataTable(currConnection, whereClause, null);
        }
        public DataTable selectRecordsRtnDataTable(IDbConnection currConnection, IDbTransaction currTran, string whereClause)
        {
            return selectRecordsRtnDataTable(currConnection, currTran, whereClause, null);
        }
        public DataTable selectRecordsRtnDataTable(IDbConnection currConnection, string whereClause, List<IDbDataParameter> parameters)
        {
            return selectRecordsRtnDataTable(currConnection, null, whereClause, parameters);
        }
        public DataTable selectRecordsRtnDataTable(IDbConnection currConnection, IDbTransaction currTran, string whereClause, List<IDbDataParameter> parameters)
        {
            IDataReader dr = selectRecordsRtnReader(currConnection, currTran, whereClause, parameters);
            DataTable rtnTable = new DataTable();

            rtnTable.Columns.Clear();
            for (int i = 0; i < dr.FieldCount; i++)
            {
                rtnTable.Columns.Add(dr.GetName(i));
            }
            rtnTable.Load(dr);

            dr.Close();
            return rtnTable;
        }

        protected List<string> getColumnNamesFromDataTable(DataTable dt)
        {
            List<string> columnNames = new List<string>();
            foreach (DataColumn col in dt.Columns)
            {
                columnNames.Add(col.ColumnName);
            }
            return columnNames;
        }

        protected List<object> moveFromDataRowsToRawObjectList(DataTable dt)
        {
            List<object> rawObjects = new List<object>();
            List<string> columnNames = getColumnNamesFromDataTable(dt);
            List<string> dtoColumnNames = normalizeFieldList(columnNames, getEmptyObject());

            Dictionary<string, string> dtoToRowMap = new Dictionary<string, string>();
            foreach (string dtoColName in dtoColumnNames)
            {
                string dbColName = columnNames.Where(x => x.ToUpper().Trim().Equals(dtoColName.ToUpper().Trim())).FirstOrDefault();
                dtoToRowMap.Add(dtoColName, dbColName);
            }

            foreach (DataRow row in dt.Rows)
            {
                object anyDTO = getEmptyObject();
                foreach (string columnName in dtoColumnNames)
                {
                    string dbColumnName = dtoToRowMap[columnName];
                    PropertyInfo prop = anyDTO.GetType().GetProperty(columnName, BindingFlags.Public | BindingFlags.Instance);
                    if (null != prop && prop.CanWrite && !(row[dbColumnName] is System.DBNull))
                    {
                        try
                        {
                            if (row[dbColumnName].GetType().Equals(typeof(System.Int32)))
                            {
                                Int32? normalIntUsedByDTOs = Convert.ToInt32(row[dbColumnName]);
                                prop.SetValue(anyDTO, normalIntUsedByDTOs, null);
                            }
                            else if (row[dbColumnName].GetType().Equals(typeof(System.Int16)))
                            {
                                Int32? normalIntUsedByDTOs = Convert.ToInt32(row[dbColumnName]);
                                prop.SetValue(anyDTO, normalIntUsedByDTOs, null);
                            }
                            else if (row[dbColumnName].GetType().Equals(typeof(System.DateTime)))
                            {
                                DateTime tmpDateTime = Convert.ToDateTime(row[dbColumnName]);
                                prop.SetValue(anyDTO, tmpDateTime, null);
                            }
                            else
                            {
                                if (prop.PropertyType.ToString().Contains("System.Int32"))
                                {
                                    Int32? normalIntUsedByDTOs = Convert.ToInt32(row[dbColumnName]);
                                    prop.SetValue(anyDTO, normalIntUsedByDTOs, null);
                                }
                                else if (prop.PropertyType.ToString().Contains("System.Int16"))
                                {
                                    Int16? normalIntUsedByDTOs = Convert.ToInt16(row[dbColumnName]);
                                    prop.SetValue(anyDTO, normalIntUsedByDTOs, null);
                                }
                                else if (prop.PropertyType.ToString().Contains("System.DateTime"))
                                {
                                    DateTime? tmpDateTime = DateTime.Parse(row[dbColumnName].ToString());
                                    prop.SetValue(anyDTO, tmpDateTime, null);
                                }
                                else if (prop.PropertyType.ToString().Contains("System.Decimal"))
                                {
                                    Decimal? tmpDateTime = Decimal.Parse(row[dbColumnName].ToString());
                                    prop.SetValue(anyDTO, tmpDateTime, null);
                                }
                                else
                                {
                                    prop.SetValue(anyDTO, row[dbColumnName], null);
                                }
                            }

                        }
                        catch (Exception)
                        {
                            Int16 i = 1;
                            i++;
                        }
                    }
                }
                rawObjects.Add(anyDTO);
            }
            return rawObjects;
        }

        public bool updateViaKey(IDbConnection currConnection, object anyDTO)
        {
            return updateViaKey(currConnection, anyDTO, null);
        }
        public bool updateViaKey(IDbConnection currConnection, IDbTransaction currTran, object anyDTO)
        {
            return updateViaKey(currConnection, currTran, anyDTO, null);
        }
        public bool updateViaKey(IDbConnection currConnection, object anyDTO, string fieldList)
        {
            return updateViaKey(currConnection, null, anyDTO, fieldList);
        }
        public bool updateViaKey(IDbConnection currConnection, IDbTransaction currTran, object anyDTO, string fieldList)
        {
            beforeUpdate(anyDTO);
            List<IDbDataParameter> parms = new List<IDbDataParameter>();
            IDbCommand cmd = currConnection.CreateCommand();

            List<string> columnList = buildColumnList(anyDTO, fieldList, new List<string>());
            string setPortion = buildParametersForSQLUpdateCmd(anyDTO, parms, cmd, columnList);

            string sql = "UPDATE " + getTableName() + " SET " + setPortion + " WHERE " + getPrimaryKey() + " = " + buildCorrespondingBindName(currConnection, getPrimaryKey());

            cmd.CommandText = sql;
            cmd.Connection = currConnection;
            cmd.Transaction = currTran;

            bindParametersToCmd(parms, cmd);

            int rowEffected = cmd.ExecuteNonQuery();

            return (rowEffected > 0) ? true : false;
        }

        public bool deleteViaKey(IDbConnection currConnection, object anyDTO)
        {
            return deleteViaKey(currConnection, null, anyDTO);
        }
        public bool deleteViaKey(IDbConnection currConnection, IDbTransaction currTran, object anyDTO)
        {
            beforeDelete(anyDTO);
            List<IDbDataParameter> parms = new List<IDbDataParameter>();
            IDbCommand cmd = currConnection.CreateCommand();

            string sql = "DELETE FROM " + getTableName() + " WHERE " + getPrimaryKey() + " = " + buildCorrespondingBindName(currConnection, getPrimaryKey()); ;

            cmd.CommandText = sql;
            cmd.Connection = currConnection;
            cmd.Transaction = currTran;

            parms.Add(buildSingleParameterValue(anyDTO, cmd, getPrimaryKey()));

            bindParametersToCmd(parms, cmd);

            int rowEffected = cmd.ExecuteNonQuery();

            return (rowEffected > 0) ? true : false;
        }

        public Int32? insert(IDbConnection currConnection, object anyDTO)
        {
            return insert(currConnection, anyDTO, null);
        }
        public Int32? insert(IDbConnection currConnection, IDbTransaction currTran, object anyDTO)
        {
            return insert(currConnection, currTran, anyDTO, null);
        }
        public Int32? insert(IDbConnection currConnection, object anyDTO, string fieldList)
        {
            return insert(currConnection, null, anyDTO, fieldList);
        }
        public Int32? insert(IDbConnection currConnection, IDbTransaction currTran, object anyDTO, string fieldList)
        {
            beforeInsert(anyDTO);
            List<IDbDataParameter> parms = new List<IDbDataParameter>();
            IDbCommand cmd = currConnection.CreateCommand();

            List<string> columnList = buildColumnList(anyDTO, fieldList, getInsertIgnoreList());
            KeyValuePair<string, string> insertParts = buildParametersForSQLInsertCmd(currConnection, anyDTO, parms, cmd, columnList);
            string firstPartOfInsertFieldList = insertParts.Key;
            string boundList = insertParts.Value;

            string sql = "INSERT INTO  " + getTableName() + "(" + firstPartOfInsertFieldList + " ) values ( " + boundList + " ) ";

            cmd.CommandText = sql;
            cmd.Connection = currConnection;
            cmd.Transaction = currTran;

            bindParametersToCmd(parms, cmd);

            cmd.ExecuteNonQuery();
            Int32? ident = null;
            if (!string.IsNullOrWhiteSpace(getIdentityColumnName()))
            {
                if (currConnection is SqlConnection)
                {
                    IDbCommand cmd2 = currConnection.CreateCommand();
                    cmd2.CommandText = "select @@IDENTITY ";
                    cmd2.Connection = currConnection;
                    cmd2.Transaction = currTran;
                    object result = cmd2.ExecuteScalar();
                    ident = Int32.Parse(result.ToString());

                    PropertyInfo prop = anyDTO.GetType().GetProperty(getIdentityColumnName(), BindingFlags.Public | BindingFlags.Instance);
                    prop.SetValue(anyDTO, ident, null);
                }
                else
                {
                    throw new NotImplementedException(); //HACK Additional work needs to be done here to support other databases.
                }
            }

            return ident;
        }


        private void bindParametersToCmd(List<IDbDataParameter> parms, IDbCommand cmd)
        {
            foreach (IDbDataParameter myParm in parms)
            {
                cmd.Parameters.Add(myParm);
            }
        }

        private KeyValuePair<string, string> buildParametersForSQLInsertCmd(IDbConnection targetConn, object anyDTO, List<IDbDataParameter> parms, IDbCommand cmd, List<string> columnList)
        {
            string firstFldList = "";
            string valueList = "";
            for (int x = 0; x < columnList.Count; x++)
            {
                if (firstFldList.Length != 0)
                {
                    firstFldList += ", ";
                    valueList += ", ";
                }
                if (targetConn is SqlConnection)
                {
                    firstFldList += "[" + columnList[x] + "]";
                }
                else
                {
                    firstFldList += columnList[x];
                }
                valueList += buildCorrespondingBindText(cmd, columnList[x]);

                IDbDataParameter parm = cmd.CreateParameter();
                PropertyInfo prop = anyDTO.GetType().GetProperty(columnList[x], BindingFlags.Public | BindingFlags.Instance);
                object valueToSet = prop.GetValue(anyDTO, null);
                if (valueToSet == null)
                {
                    if (prop.ToString().Contains("Byte[]"))
                    {
                        parm.DbType = DbType.Binary;
                    }
                    parm.Value = System.DBNull.Value;
                }
                else
                {
                    parm.Value = prop.GetValue(anyDTO, null);
                    if (valueToSet.GetType().Equals(typeof(System.Byte[])))
                    {
                        parm.DbType = DbType.Binary;
                    }
                }
                parm.ParameterName = buildCorrespondingBindName(cmd, columnList[x]);
                parms.Add(parm);
            }

            return new KeyValuePair<string, string>(firstFldList, valueList);
        }

        private string buildParametersForSQLUpdateCmd(object anyDTO, List<IDbDataParameter> parms, IDbCommand cmd, List<string> columnList)
        {
            string setPortion = "";
            for (int x = 0; x < columnList.Count; x++)
            {
                if (!columnList[x].Equals(getPrimaryKey()))
                {
                    if (setPortion.Length != 0)
                    {
                        setPortion += ", ";
                    }
                    setPortion += columnList[x] + " = " + buildCorrespondingBindText(cmd, columnList[x]);
                    IDbDataParameter parm = buildSingleParameterValue(anyDTO, cmd, columnList, x);
                    parms.Add(parm);
                }
            }

            IDbDataParameter parm2 = cmd.CreateParameter();
            PropertyInfo prop2 = anyDTO.GetType().GetProperty(getPrimaryKey(), BindingFlags.Public | BindingFlags.Instance);
            parm2.Value = prop2.GetValue(anyDTO, null);
            parm2.ParameterName = buildCorrespondingBindName(cmd, getPrimaryKey());
            parms.Add(parm2);
            return setPortion;
        }

        private IDbDataParameter buildSingleParameterValue(object anyDTO, IDbCommand cmd, string fieldName )
        {
            return buildSingleParameterValue(anyDTO, cmd, new List<string>( new string[]{fieldName} ),0);
        }
        private IDbDataParameter buildSingleParameterValue(object anyDTO, IDbCommand cmd, List<string> columnList, int x)
        {
            IDbDataParameter parm = cmd.CreateParameter();
            PropertyInfo prop = anyDTO.GetType().GetProperty(columnList[x], BindingFlags.Public | BindingFlags.Instance);
            object valueToSet = prop.GetValue(anyDTO, null);
            if (valueToSet == null)
            {
                if (prop.ToString().Contains("Byte[]"))
                {
                    parm.DbType = DbType.Binary;
                }
                parm.Value = System.DBNull.Value;
            }
            else
            {
                parm.Value = prop.GetValue(anyDTO, null);
                if (valueToSet.GetType().Equals(typeof(System.Byte[])))
                {
                    parm.DbType = DbType.Binary;
                }
            }
            parm.ParameterName = buildCorrespondingBindName(cmd, columnList[x]);
            return parm;
        }

        //
        //These routines handle if it's Sqlserver or someother DB like Oracle or PostgreSQL
        //
        private string buildCorrespondingBindName(IDbCommand cmd, string fldName)
        {
            return (cmd is SqlCommand) ? "@" + fldName : ":";
        }
        private string buildCorrespondingBindName(IDbConnection conn, string fldName)
        {
            return (conn is SqlConnection) ? "@" + fldName : ":";
        }

        private string buildCorrespondingBindText(IDbCommand cmd, string fldName)
        {
            return (cmd is SqlCommand) ? "@" + fldName : "?";
        }
        private string buildCorrespondingBindText(IDbConnection conn, string fldName)
        {
            return (conn is SqlConnection) ? "@" + fldName : "?";
        }

        private List<string> buildColumnList(object anyDTO, string fieldList, List<string> ignoreList)
        {
            List<string> columnList = new List<string>();

            if (!string.IsNullOrWhiteSpace(fieldList))
            {
                List<string> tmpList = fieldList.Split(',').ToList();
                foreach (string fld in tmpList)
                {
                    if (!ignoreList.Contains(fld))
                    {
                        columnList.Add(fld.Trim());
                    }
                }
                columnList = normalizeFieldList(columnList, anyDTO);
            }
            else
            {
                List<PropertyInfo> propertyList = anyDTO.GetType().GetProperties().ToList();
                foreach (PropertyInfo pi in propertyList)
                {
                    if (!ignoreList.Contains(pi.Name))
                    {
                        columnList.Add(pi.Name);
                    }
                }
            }
            return columnList;
        }

        private List<string> normalizeFieldList(List<string> srcList, object anyDTO)
        {
            List<string> rtnList = new List<string>();
            List<string> columnList = new List<string>();
            List<PropertyInfo> propertyList = anyDTO.GetType().GetProperties().ToList();
            foreach (PropertyInfo pi in propertyList)
            {
                columnList.Add(pi.Name);
            }

            foreach (string srcFld in srcList)
            {
                string exactName = columnList.Where(x => x.ToUpper().Trim().Equals(srcFld.ToUpper().Trim())).FirstOrDefault();
                if (!string.IsNullOrWhiteSpace(exactName))
                {
                    rtnList.Add(exactName);
                }
            }

            return rtnList;
        }

    }

}
