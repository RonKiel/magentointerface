﻿using MagentoInterface.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagentoInterface.DAO
{
    public class GenericBlueItemInfoDAO : BaseDAO
    {
        public override void beforeSelect(IDbConnection currConnection)
        {
            if ( !( currConnection is SqlConnection ) )
            {
                string query = "";
                query = "select INVEN.ITEM as ItemNo,     " +
                       "SUBSTRING(INVEN.ITEM,0,2) as Section,  " +
                       "SUBSTRING(INVEN.ITEM,0,10) as MajorKey,  " +
                       "SUBSTRING(INVEN.ITEM,11,5) as MinorKey,  " +
                       "INVEN.SKU,  " +
                       "INVEN.UPC,  " +
                       "INVEN.EAN,  " +
                       "U8.DESC + ' ' + INVEN.DESC as Description,  " +
                       "U8.DESC as  MajorDescription,  " +
                       "MAJAD.DESC as MajorLongDescription,  " +
                       "MINAD.DESC as MinorLongDescription,  " +
                       "INVEN.STAT as ItemStatusCode,  " +
                       "U8.VEND as VendorId,  " +
                       "U8.BRAND as BrandCode,  " +
                       "U8.COO as CountryOfOrigin,  " +
                       "U8.TARCD as TariffCode,  " +
                       "INVEN.CLS as Class,  " +
                       "INVEN.CSC1 as CategoryCode1,  " +
                       "INVEN.CSC2 as CategoryCode2,  " +
                       "INVEN.CSC3 as CategoryCode3,  " +
                       "U8.VARPRC as UseMinorRetailPricing,  " +
                       "U8.RTL as RetailPrice_1,  " +
                       "INVEN.RTL as RetailPrice_2,  " +
                       "U8.MS as MinSell_1,  " +
                       "INVEN.MS as MinSell_2,  " +
                       "U8.VARDIM as UseMajorDimensions,  " +
                       "U8.HT as Height1,  " +
                       "U8.WD as Width1,  " +
                       "U8.DP as Depth1,  " +
                       "INVEN.HT as Height2,  " +
                       "INVEN.WD as Width2,  " +
                       "INVEN.DP as Depth2  " +
                       "from INVEN INNER JOIN U8 ON SUBSTRING(INVEN.ITEM,0,10) = U8.MAJKY  " +
                           "LEFT JOIN MAJAD ON (MAJAD.MAJKY = U8.MAJKY)  " +
                           "LEFT JOIN MINAD ON (INVEN.ITEM = MINAD.ITEM)  "; 
                base.setBuldSelectStatementQuery(query);
            }

        }
        public override string getTableName()
        {
            return "GenericBlueItemInfo";
        }
        public override string getPrimaryKey()
        {
            return "ItemNo";
        }
        public override object getEmptyObject()
        {
            return new GenericBlueItemInfoDTO();
        }
        public override List<string> getInsertIgnoreList()
        {
            return new List<string>();
        }
        public override string getIdentityColumnName()
        {
            return null;
        }

        public List<GenericBlueItemInfoDTO> convertToMyList(List<object> rawList)
        {
            List<GenericBlueItemInfoDTO> rtnList = new List<GenericBlueItemInfoDTO>();
            foreach (object rawobj in rawList)
            {
                rtnList.Add((GenericBlueItemInfoDTO)rawobj);
            }
            return rtnList;
        }

        public List<GenericBlueItemInfoDTO> selectRecordsRtnList(IDbConnection currConnection, string whereClause)
        {
            return selectRecordsRtnList(currConnection, whereClause, null, null);
        }
        public List<GenericBlueItemInfoDTO> selectRecordsRtnList(IDbConnection currConnection, string whereClause, string orderBy)
        {
            return selectRecordsRtnList(currConnection, whereClause, null, orderBy);
        }
        public List<GenericBlueItemInfoDTO> selectRecordsRtnList(IDbConnection currConnection, string whereClause, List<IDbDataParameter> parameters)
        {
            return selectRecordsRtnList(currConnection, null, whereClause, parameters, null);
        }
        public List<GenericBlueItemInfoDTO> selectRecordsRtnList(IDbConnection currConnection, string whereClause, List<IDbDataParameter> parameters, string orderBy)
        {
            return selectRecordsRtnList(currConnection, null, whereClause, parameters, orderBy);
        }
        public List<GenericBlueItemInfoDTO> selectRecordsRtnList(IDbConnection currConnection, IDbTransaction currTran, string whereClause)
        {
            return selectRecordsRtnList(currConnection, currTran, whereClause, null, null);
        }
        public List<GenericBlueItemInfoDTO> selectRecordsRtnList(IDbConnection currConnection, IDbTransaction currTran, string whereClause, string orderBy)
        {
            return selectRecordsRtnList(currConnection, currTran, whereClause, null, orderBy);
        }
        public List<GenericBlueItemInfoDTO> selectRecordsRtnList(IDbConnection currConnection, IDbTransaction currTran, string whereClause, List<IDbDataParameter> parameters, string orderBy)
        {
            DataTable dt = null;
            if (string.IsNullOrWhiteSpace(orderBy))
            {
                dt = base.selectRecordsRtnDataTable(currConnection, currTran, whereClause, parameters);
            }
            else
            {
                dt = base.sortDataTable(base.selectRecordsRtnDataTable(currConnection, currTran, whereClause, parameters), orderBy);
            }
            List<object> rawObjects = base.moveFromDataRowsToRawObjectList(dt);
            return convertToMyList(rawObjects);
        }

    }

}
