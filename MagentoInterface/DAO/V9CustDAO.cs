﻿using MagentoInterface.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagentoInterface.DAO
{
    public class V9CustDAO : BaseDAO
    {
        public override string getTableName()
        {
            return "V9";
        }
        public override string getPrimaryKey()
        {
            return "CUST";
        }
        public override object getEmptyObject()
        {
            return new V9CustDTO();
        }
        public override List<string> getInsertIgnoreList()
        {
            return new List<string>();
        }
        public override string getIdentityColumnName()
        {
            return null;
        }

        public List<V9CustDTO> convertToMyList(List<object> rawList)
        {
            List<V9CustDTO> rtnList = new List<V9CustDTO>();
            foreach (object rawobj in rawList)
            {
                rtnList.Add((V9CustDTO)rawobj);
            }
            return rtnList;
        }

        public List<V9CustDTO> selectRecordsRtnList(IDbConnection currConnection, string whereClause)
        {
            return selectRecordsRtnList(currConnection, whereClause, null, null);
        }
        public List<V9CustDTO> selectRecordsRtnList(IDbConnection currConnection, string whereClause, string orderBy)
        {
            return selectRecordsRtnList(currConnection, whereClause, null, orderBy);
        }
        public List<V9CustDTO> selectRecordsRtnList(IDbConnection currConnection, string whereClause, List<IDbDataParameter> parameters)
        {
            return selectRecordsRtnList(currConnection, null, whereClause, parameters, null);
        }
        public List<V9CustDTO> selectRecordsRtnList(IDbConnection currConnection, string whereClause, List<IDbDataParameter> parameters, string orderBy)
        {
            return selectRecordsRtnList(currConnection, null, whereClause, parameters, orderBy);
        }
        public List<V9CustDTO> selectRecordsRtnList(IDbConnection currConnection, IDbTransaction currTran, string whereClause)
        {
            return selectRecordsRtnList(currConnection, currTran, whereClause, null, null);
        }
        public List<V9CustDTO> selectRecordsRtnList(IDbConnection currConnection, IDbTransaction currTran, string whereClause, string orderBy)
        {
            return selectRecordsRtnList(currConnection, currTran, whereClause, null, orderBy);
        }
        public List<V9CustDTO> selectRecordsRtnList(IDbConnection currConnection, IDbTransaction currTran, string whereClause, List<IDbDataParameter> parameters, string orderBy)
        {
            DataTable dt = null;
            if (string.IsNullOrWhiteSpace(orderBy))
            {
                dt = base.selectRecordsRtnDataTable(currConnection, currTran, whereClause, parameters);
            }
            else
            {
                dt = base.sortDataTable(base.selectRecordsRtnDataTable(currConnection, currTran, whereClause, parameters), orderBy);
            }
            List<object> rawObjects = base.moveFromDataRowsToRawObjectList(dt);
            return convertToMyList(rawObjects);
        }




    }
}
