﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagentoInterface.DTO
{
    public class GenericBlueItemInfoDTO : BaseDTO
    {
        public string ItemNo { get; set; }
        public string Section { get; set; }
        public string MajorKey { get; set; }
        public string MinorKey { get; set; }
        public string SKU { get; set; }
        public string UPC { get; set; }
        public string EAN { get; set; }
        public string Description { get; set; }
        public string MajorDescription { get; set; }
        public string MajorLongDescription { get; set; }
        public string MinorLongDescription { get; set; }
        public string ItemStatusCode { get; set; }
        public string VendorId { get; set; }
        public string BrandCode { get; set; }
        public string CountryOfOrigin { get; set; }
        public string TariffCode { get; set; }
        public string Class { get; set; }
        public string CategoryCode1 { get; set; }
        public string CategoryCode2 { get; set; }
        public string CategoryCode3 { get; set; }
        public string UseMinorRetailPricing { get; set; }
        public Decimal? RetailPrice_1 { get; set; }
        public Decimal? RetailPrice_2 { get; set; }
        public Decimal? MinSell_1 { get; set; }
        public Decimal? MinSell_2 { get; set; }
        public string UseMajorDimensions { get; set; }
        public Decimal? Height1 { get; set; }
        public Decimal? Width1 { get; set; }
        public Decimal? Depth1 { get; set; }
        public Decimal? Height2 { get; set; }
        public Decimal? Width2 { get; set; }
        public Decimal? Depth2 { get; set; }
    }

}
