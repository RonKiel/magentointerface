﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace MagentoInterface.DTO
{
    public class BaseDTO
    {
        public override string ToString()
        {
            string buffer = string.Format("Class: {0} ", this.GetType().Name);
            try
            {
                List<PropertyInfo> propertyList = this.GetType().GetProperties().ToList();
                foreach (PropertyInfo pi in propertyList)
                {
                    object valueToSet = pi.GetValue(this, null);
                    if (valueToSet != null && !valueToSet.GetType().Equals(typeof(System.Byte[])))
                    {
                        string nextFld = string.Format(" {0}:{1} ", pi.Name, valueToSet.ToString());
                        buffer += nextFld;
                    }
                }
            }
            catch (Exception ex1)
            {
                buffer += ex1.Message;
            }
            return buffer;
        }

    }
}
