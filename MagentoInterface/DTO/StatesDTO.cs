﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagentoInterface.DTO
{
    public class StatesDTO : BaseDTO
    {
        public string ST { get; set; }
        public string NAME { get; set; }
        public string CNTRY { get; set; }
        public string CCD { get; set; }
        public string LOW48 { get; set; }
    }
}
