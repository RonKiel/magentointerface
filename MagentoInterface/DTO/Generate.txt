﻿
       --Used from SQL server ONLY!!!!
	   --
	   SELECT CASE ty.name WHEN 'nvarchar' THEN  'public string ' +  cols.name + '{ get; set; }'
                            WHEN 'uniqueidentifier' THEN  'public string ' +  cols.name + '{ get; set; }'
                            WHEN 'varchar' THEN  'public string ' +  cols.name + '{ get; set; }'
                            WHEN 'char' THEN  'public string ' +  cols.name + '{ get; set; }'
                            WHEN 'smallint' THEN  'public Int32? ' +  cols.name + '{ get; set; }'
                            WHEN 'tinyint' THEN  'public Int32? ' +  cols.name + '{ get; set; }'
                            WHEN 'bigint' THEN  'public Int32? ' +  cols.name + '{ get; set; }'
                            WHEN 'int' THEN  'public Int32? ' +  cols.name + '{ get; set; }'
                            WHEN 'numeric' THEN 'public Decimal? ' +  cols.name + '{ get; set; }'
                            WHEN 'decimal' THEN 'public Decimal? ' +  cols.name + '{ get; set; }'
                            WHEN 'float' THEN 'public float? ' +  cols.name + '{ get; set; }'
                            WHEN 'smalldatetime' THEN 'public DateTime? ' +  cols.name + '{ get; set; }'
                            WHEN 'datetime' THEN 'public DateTime? ' +  cols.name + '{ get; set; }'
                            WHEN 'date' THEN 'public DateTime? ' +  cols.name + '{ get; set; }'
                            WHEN 'bit' THEN 'public Boolean? ' +  cols.name + '{ get; set; }'
                            WHEN 'image' THEN 'public Byte[] ' +  cols.name + '{ get; set; }'
                            WHEN 'varbinary' THEN 'public Byte[] ' +  cols.name + '{ get; set; }'
                            WHEN 'text' THEN 'public string ' +  cols.name + '{ get; set; }'
        ELSE ty.name + ' NOT DEFINED ' + 'for ' + cols.name END
        from sys.columns cols JOIN sys.types ty ON cols.system_type_id = ty.system_type_id
        where object_id = object_id('V9')
        and  ty.name NOT IN ('sysname')
        order by cols.column_id;

	   SELECT CASE ty.name 
                    WHEN 'numeric' THEN 'VRSmlpromodDTO.' + cols.name + ' = getDecimalValue(row, "'+cols.name +'");' 
                    WHEN 'decimal' THEN 'VRSmlpromodDTO.' + cols.name + ' = getDecimalValue(row, "'+cols.name +'");' 
					ELSE 'VRSmlpromodDTO.' + cols.name + ' = row["' + cols.name + '"].ToString();' END
        from sys.columns cols JOIN sys.types ty ON cols.system_type_id = ty.system_type_id
        where object_id = object_id('VRSMLPROMOD')
        and  ty.name NOT IN ('sysname')
        order by cols.column_id;

