﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagentoInterface.DTO
{
    public class MagentoCustomerDTO : BaseDTO
    {
        public Int32? LoadId { get; set; }
        public string CustomerAcctNo { get; set; }
        public string Company { get; set; }
        public string Status { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string AddrCompany { get; set; }
        public string Addr1 { get; set; }
        public string Addr2 { get; set; }
        public string City { get; set; }
        public string StateProv { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }
        public string Phone { get; set; }
        public string Terms { get; set; }
        public string ShipVia { get; set; }
        public string rep { get; set; }
        public string bankcards { get; set; }
        public string CountryCode { get; set; }
        public Int32? isShowCost { get; set; }
    }

}
