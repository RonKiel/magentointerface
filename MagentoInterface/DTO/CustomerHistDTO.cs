﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagentoInterface.DTO
{
    public class CustomerHistDTO : BaseDTO
    {
        public Int32? LoadId { get; set; }
        public DateTime? LoadDate { get; set; }
    }

}
