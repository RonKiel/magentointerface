﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagentoInterface.DTO
{
    public class MagentoCategoryDTO :BaseDTO
    {
        public Int32? LoadId { get; set; }
        public string Title { get; set; }
        public string url_key { get; set; }
        public string url_path { get; set; }
        public string Visible { get; set; }
        public string Description { get; set; }
        public Int32 CategoryId { get; set; }
        public Int32 Level { get; set; }
        public Int32? parentId { get; set; }
    }

}
