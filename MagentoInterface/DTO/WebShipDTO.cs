﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagentoInterface.DTO
{
    public class WebShipDTO : BaseDTO
    {
        public string CUST { get; set; }
        public string NAME { get; set; }
        public string AD1 { get; set; }
        public string AD2 { get; set; }
        public string CSZ { get; set; }
        public string STNAME { get; set; }
        public string STAD1 { get; set; }
        public string STAD2 { get; set; }
        public string STCSZ { get; set; }
        public string TERMS { get; set; }
        public string SVIA { get; set; }
        public string UPDDT { get; set; }
        public string UPDTM { get; set; }
        public string EMAIL { get; set; }
        public string CNTRY { get; set; }
        public string PH { get; set; }
    }

}
