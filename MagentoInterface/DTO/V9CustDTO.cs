﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagentoInterface.DTO
{
    public class V9CustDTO : BaseDTO
    {
        public string CUST { get; set; }
        public string NAME { get; set; }
        public string AD1 { get; set; }
        public string AD2 { get; set; }
        public string CSZ { get; set; }
        public string PRIREP { get; set; }
        public string REP { get; set; }
        public string LYTDSLS { get; set; }
        public string CURINV { get; set; }
        public string YTDSLS { get; set; }
        public string ONACC { get; set; }
        public string CRLIM { get; set; }
        public string YTDCR { get; set; }
        public string DELCD { get; set; }
        public string BLANK1 { get; set; }
        public string MAILOK { get; set; }
        public string FINFL { get; set; }
        public string ANLYSFL { get; set; }
        public string SIGNFL { get; set; }
        public string TAXEXMT { get; set; }
        public string SERVFL { get; set; }
        public string VIDFL { get; set; }
        public string BOOK { get; set; }
        public string SERVACT { get; set; }
        public string STMTFL { get; set; }
        public string FAXOK { get; set; }
        public string BTFL { get; set; }
        public string CHAMBR { get; set; }
        public string GUARFL { get; set; }
        public string EMLOK { get; set; }
        public string FUTFL { get; set; }
        public string ABOFL { get; set; }
        public string CATADR { get; set; }
        public string HOLDFL { get; set; }
        public string OCONF { get; set; }
        public string WAIVFL { get; set; }
        public string SCONF { get; set; }
        public string CATORD { get; set; }
        public string CCFL { get; set; }
        public string PCHGFL { get; set; }
        public string UPCPL { get; set; }
        public string UPCINV { get; set; }
        public string UPCCON { get; set; }
        public string DESCFL { get; set; }
        public string PLCOST { get; set; }
        public string INVDTL { get; set; }
        public string STKRFL { get; set; }
        public string POSFL { get; set; }
        public string INVPDF { get; set; }
        public string VISSTR { get; set; }
        public string PAYDT { get; set; }
        public string CURCR { get; set; }
        public string CURDB { get; set; }
        public string PH { get; set; }
        public string TEXT1 { get; set; }
        public string COUNTY { get; set; }
        public string DEPT { get; set; }
        public string STORE { get; set; }
        public string SERVW { get; set; }
        public string CLS { get; set; }
        public string TERMS { get; set; }
        public string ADDDT { get; set; }
        public string CATS { get; set; }
        public string ORDCNT { get; set; }
        public string PYTDSLS { get; set; }
        public string ORDDT { get; set; }
        public string VISDT { get; set; }
        public string CALLDT { get; set; }
        public string APCONT { get; set; }
        public string DL { get; set; }
        public string NOI { get; set; }
        public string NETSLS { get; set; }
        public string NETCOGS { get; set; }
        public string PROJSLS { get; set; }
        public string CHGDATE { get; set; }
        public string CALLSTR { get; set; }
        public string FAX { get; set; }
        public string ACIAMT { get; set; }
        public string DISTCTR { get; set; }
        public string PPM { get; set; }
        public string PPM2 { get; set; }
        public string PPMMARK { get; set; }
        public string NETDSLS { get; set; }
        public string NETDCOGS { get; set; }
        public string EMAIL { get; set; }
        public string BLANK2 { get; set; }
        public string LYTDDSS { get; set; }
        public string LYTDDSC { get; set; }
        public string CCRD { get; set; }
        public string CCEXP { get; set; }
        public string CRREP { get; set; }
        public string GRPS { get; set; }
        public string SHPNUMC { get; set; }
        public string COLLDATE { get; set; }
    }

}
