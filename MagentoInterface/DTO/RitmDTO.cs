﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagentoInterface.DTO
{
    public class RitmDTO : BaseDTO
    {
        public string BASKNO { get; set; }
        public string DESC { get; set; }
        public string DISPFL { get; set; }
        public string EXPDT { get; set; }
        public string SORTKY { get; set; }
        public string CSC { get; set; }
        public string TEXT { get; set; }
        public string LVAR { get; set; }
    }

}
