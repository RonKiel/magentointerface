﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagentoInterface.DTO
{
    public class VisaCustDTO : BaseDTO
    {
        public string CUSTNO { get; set; }
        public string CARDS { get; set; }
        public string HOLDERNAME { get; set; }
        public string ADDRESS { get; set; }
        public string CITY { get; set; }
        public string STATE { get; set; }
        public string ZIP { get; set; }
    }

}
