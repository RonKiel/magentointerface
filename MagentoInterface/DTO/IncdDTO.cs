﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagentoInterface.DTO
{
    public class IncdDTO : BaseDTO
    {
        public string CSC { get; set; }
        public string DESC { get; set; }
        public string MAJFL { get; set; }
        public string WDESC { get; set; }
        public string WLVL { get; set; }
        public string WTAGS { get; set; }
        public string LVAR { get; set; }
    }

}
