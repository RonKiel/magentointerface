﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagentoInterface.DTO
{
    public class V9AddDTO : BaseDTO
    {
        public string CUSTNO { get; set; }
        public string SLSPHONE { get; set; }
        public string HOMEPHONE { get; set; }
        public string WEBSITE { get; set; }
        public string CONTACT1 { get; set; }
        public string CONTACT2 { get; set; }
        public string CONTACT3 { get; set; }
        public string CONTACT4 { get; set; }
        public string CATSTO { get; set; }
        public string ORDERBY { get; set; }
        public string OTHERSTORES { get; set; }
        public string BUSFORM { get; set; }
        public string STORESIZE { get; set; }
        public string ANNUALSLS { get; set; }
        public string EMPLOYEES { get; set; }
        public string STORELOC { get; set; }
        public string EXTRA1 { get; set; }
        public string EXTRA2 { get; set; }
        public string EXTRA3 { get; set; }
        public string EXTRA4 { get; set; }
        public string EXTRA5 { get; set; }
        public string STOREPASS { get; set; }
        public string SPCHANGEBY { get; set; }
        public string SPCHANGEDATE { get; set; }
        public string PORTALPASS { get; set; }
        public string PPCHANGEBY { get; set; }
        public string PPCHANGEDATE { get; set; }
    }

}
