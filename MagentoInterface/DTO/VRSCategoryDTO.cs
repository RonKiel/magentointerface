﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagentoInterface.DTO
{
    public class VRSCategoryDTO : BaseDTO
    {
        public int? categoryId { get; set; }
        public int? parentId { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public int? level { get; set; }
        public string WTAGS { get; set; }
    }
}
