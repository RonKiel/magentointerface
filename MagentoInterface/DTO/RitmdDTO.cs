﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagentoInterface.DTO
{
    public class RitmdDTO : BaseDTO
    {
        public string BASKNO { get; set; }
        public string SKU { get; set; }
        public string SORTKY { get; set; }
        public string LVAR { get; set; }
    }

}
