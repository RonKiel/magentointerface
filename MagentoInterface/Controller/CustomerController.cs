﻿using MagentoInterface.DAO;
using MagentoInterface.DTO;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace MagentoInterface.Controller
{
    public class CustomerController : BaseController
    {
        /*
        CREATE TABLE CustomerHist (
           LoadId Int Identity(1,1) Primary Key,
           LoadDate DATETIME default getdate()
        )          
         
        CREATE TABLE MagentoCustomer
        (
	        LoadId Int  NULL Foreign Key References CustomerHist ON DELETE CASCADE, 
	        CustomerAcctNo	VARCHAR(256),
	        Company	VARCHAR(256),
            Status	VARCHAR(256),
            Email	VARCHAR(256),
	        Password	VARCHAR(256),
            AddrCompany		VARCHAR(256),
	        Addr1		VARCHAR(256),
	        Addr2		VARCHAR(256),
	        City		VARCHAR(256),
	        StateProv		VARCHAR(256),
	        PostalCode		VARCHAR(256),
	        Country		VARCHAR(256),
	        Phone		VARCHAR(256),
	        Terms		VARCHAR(256),
	        ShipVia		VARCHAR(256),
	        rep		VARCHAR(256),
	        bankcards		VARCHAR(256),
	        CountryCode		VARCHAR(256),
	        isShowCost Int 
        )

         */

        public Int32 buildScratchPadV9(IDbConnection conn, IDbConnection scratchPad)
        {
            V9CustDAO v9CustDAO = new V9CustDAO();
            List<V9CustDTO> blueRecList = v9CustDAO.selectRecordsRtnList(conn, ""); 

            foreach (V9CustDTO dto in blueRecList)
            {
                v9CustDAO.insert(scratchPad, dto);
            }
            return blueRecList.Count;
        }

        public Int32 buildScratchPadStates(IDbConnection conn, IDbConnection scratchPad)
        {
            StatesDAO statesDAO = new StatesDAO();
            List<StatesDTO> blueRecList = statesDAO.selectRecordsRtnList(conn, "");

            foreach (StatesDTO dto in blueRecList)
            {
                statesDAO.insert(scratchPad, dto);
            }
            return blueRecList.Count;
        }

        public Int32 buildScratchPadV9ADD(IDbConnection conn, IDbConnection scratchPad)
        {
            V9AddDAO v9AddDAO = new V9AddDAO();
            List<V9AddDTO> blueRecList = v9AddDAO.selectRecordsRtnList(conn, "");

            foreach (V9AddDTO dto in blueRecList)
            {
                v9AddDAO.insert(scratchPad, dto);
            }
            return blueRecList.Count;
        }

        public Int32 buildScratchPadVisaCust(IDbConnection conn, IDbConnection scratchPad)
        {
            VisaCustDAO visaCustDAO = new VisaCustDAO();
            List<VisaCustDTO> vcustRecs = visaCustDAO.selectRecordsRtnList(conn, "");

            foreach (VisaCustDTO dto in vcustRecs)
            {
                visaCustDAO.insert(scratchPad, dto);
            }

            return vcustRecs.Count;
        }

        public Int32 buildScratchPadWebShip(IDbConnection conn, IDbConnection scratchPad)
        {
            WebShipDAO webShipDAO = new WebShipDAO();
            List<WebShipDTO> vcustRecs = webShipDAO.selectRecordsRtnList(conn, "");

            foreach (WebShipDTO dto in vcustRecs)
            {
                webShipDAO.insert(scratchPad, dto);
            }
            return vcustRecs.Count;
        }

        public List<string> buildFiles(IDbConnection conn, IDbConnection stagedConn)
        {
            StringBuilder fileChanges = new StringBuilder();

            IDbTransaction tran = stagedConn.BeginTransaction();
            Int32 customerLoadId = -1;
            try
            {
                customerLoadId = process(conn, stagedConn, tran, fileChanges);
                tran.Commit();
            }
            catch (Exception ex1)
            {
                tran.Rollback();
                System.Console.WriteLine("Error :" + ex1.Message);
            }
            string catChangesTxt = ConfigurationManager.AppSettings["FileBuildLocation"].ToString() + string.Format("customerfile{0}.tab", customerLoadId.ToString("D8"));
            File.WriteAllText(catChangesTxt, fileChanges.ToString());

            return new List<string>(new string[] { catChangesTxt });
        }

        public Int32 process(IDbConnection conn, IDbConnection stagedConn, IDbTransaction stagedTran, StringBuilder changes)
        {
            V9CustDAO v9CustDAO = new V9CustDAO();
            V9AddDAO v9AddDAO = new V9AddDAO();
            StatesDAO statesDAO = new StatesDAO();
            VisaCustDAO visaCustDAO = new VisaCustDAO();
            WebShipDAO webShipDAO = new WebShipDAO();

            List<V9CustDTO> v9Recs = v9CustDAO.selectRecordsRtnList(conn, "");
            List<V9AddDTO> v9AddRecs = v9AddDAO.selectRecordsRtnList(conn, "");
            List<StatesDTO> stateRecs = statesDAO.selectRecordsRtnList(conn, "");
            List<VisaCustDTO> visaCustRecs = visaCustDAO.selectRecordsRtnList(conn, "");
            List<WebShipDTO> webShipRecs = webShipDAO.selectRecordsRtnList(conn, "");

            List<MagentoCustomerDTO> magentoCustomerRecs = new List<MagentoCustomerDTO>();
            Int32 loadId = populateMagentoCustRecords(stagedConn, stagedTran, v9Recs, v9AddRecs, stateRecs, visaCustRecs, webShipRecs, magentoCustomerRecs);

            BulkInsert((SqlConnection)stagedConn, (SqlTransaction)stagedTran, "MagentoCustomer", magentoCustomerRecs); //HACK sql server specific.

            string headersChanges = string.Format("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}\t{10}\t{11}\t{12}\t{13}\t{14}\t{15}\t{16}\t{17}\t{18}\n", 
                "CustomerAcctNo", "Company", "Status", "Email", "Password", "AddrCompany", "Addr1", "Addr2", "City", 
                "StateProv", "PostalCode", "Country", "Phone", "Terms", "ShipVia", "rep", "bankcards",	
                "CountryCode", "isShowCost"); 
            changes.Append(headersChanges);
            buildChangedRecords(stagedConn, stagedTran, changes, loadId);
            deleteOutdatedHistory(stagedConn, stagedTran, loadId);

            return loadId;
        }

        public void deleteOutdatedHistory(IDbConnection stagedConn, IDbTransaction stagedTran, Int32 loadId)
        {
            CustomerHistDAO customerHistDAO = new CustomerHistDAO();
            string whereClause = string.Format("LoadId < {0}", loadId - 10);
            List<CustomerHistDTO> custHistRecs = customerHistDAO.selectRecordsRtnList(stagedConn, stagedTran, whereClause);

            foreach (CustomerHistDTO dto in custHistRecs)
            {
                customerHistDAO.deleteViaKey(stagedConn, stagedTran, dto);
            }
        }

        public void buildChangedRecords(IDbConnection stagedConn, IDbTransaction stagedTran, StringBuilder changes, Int32 loadId)
        {
            string sql = "SELECT [CustomerAcctNo],[Company],[Status],[Email],[Password],[AddrCompany],[Addr1],[Addr2] " +
                         ",[City],[StateProv],[PostalCode],[Country],[Phone],[Terms],[ShipVia],[rep],[bankcards],[CountryCode],[isShowCost] ";
            sql += "FROM MagentoCustomer m ";
            sql += string.Format("WHERE loadId = {0} ", loadId);
            sql += "EXCEPT ";
            sql += "SELECT [CustomerAcctNo],[Company],[Status],[Email],[Password],[AddrCompany],[Addr1],[Addr2] " +
                         ",[City],[StateProv],[PostalCode],[Country],[Phone],[Terms],[ShipVia],[rep],[bankcards],[CountryCode],[isShowCost] ";
            sql += "FROM MagentoCustomer m2 ";
            sql += string.Format("WHERE loadId = {0}", loadId - 1);
            sql += "Order by CustomerAcctNo ";

            DataTable changedRecs = findChangedRecords(stagedConn, stagedTran, sql);

            foreach (DataRow dr in changedRecs.Rows)
            {
                string oneLine = "";
                string activeFlg = (dr["isShowCost"].ToString() == "1") ? "yes" : "no";
                oneLine = string.Format("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}\t{10}\t{11}\t{12}\t{13}\t{14}\t{15}\t{16}\t{17}\t{18}\n",
                     dr["CustomerAcctNo"],dr["Company"],dr["Status"],dr["Email"],dr["Password"],dr["AddrCompany"],dr["Addr1"],dr["Addr2"],
                     dr["City"], dr["StateProv"], dr["PostalCode"], dr["Country"], dr["Phone"], dr["Terms"], dr["ShipVia"], dr["rep"], dr["bankcards"], dr["CountryCode"], dr["isShowCost"] 
                    );
                changes.Append(oneLine);
            }
        }

        public DataTable findChangedRecords(IDbConnection stagedConn, IDbTransaction stagedTran, string sql)
        {
            DataTable changedRecs = new DataTable();
            IDbCommand cmd = stagedConn.CreateCommand();
            cmd.CommandText = sql;
            cmd.Transaction = stagedTran;
            changedRecs.Load(cmd.ExecuteReader());
            return changedRecs;
        }

        public Int32 populateMagentoCustRecords(IDbConnection stagedConn, IDbTransaction stagedTran, List<V9CustDTO> currentRecs, 
            List<V9AddDTO> v9AddRecs, List<StatesDTO> stateRecs, List<VisaCustDTO> visaCustRecs, List<WebShipDTO> webShipRecs, 
            List<MagentoCustomerDTO> magentoCustRecs)
        {
            Dictionary<string, string> passwordDictionary = new Dictionary<string, string>();
            Dictionary<string, StatesDTO> stateDictionary = new Dictionary<string, StatesDTO>();
            Dictionary<string, VisaCustDTO> visaCustDictionary = new Dictionary<string, VisaCustDTO>();
            Dictionary<string, WebShipDTO> webShipDictionary = new Dictionary<string, WebShipDTO>();

            foreach (V9AddDTO dto in v9AddRecs)
            {
                passwordDictionary.Add(dto.CUSTNO.Trim(), dto.PORTALPASS.Trim());
            }

            foreach (StatesDTO dto in stateRecs)
            {
                stateDictionary.Add(dto.ST.Trim(), dto);
            }

            foreach (VisaCustDTO dto in visaCustRecs)
            {
                visaCustDictionary.Add(dto.CUSTNO.Trim(), dto);
            }

            foreach (WebShipDTO dto in webShipRecs)
            {
                webShipDictionary.Add(dto.CUST.Trim(), dto);
            }

            foreach (V9CustDTO v9dto in currentRecs)
            {
                string password = null;
                string custNo = v9dto.CUST.Trim();
                if (passwordDictionary.Keys.Contains(custNo))  
                {
                    password = passwordDictionary[custNo];
                }

                MagentoCustomerDTO magentoCustomerDTO = new MagentoCustomerDTO();
                magentoCustomerDTO.CustomerAcctNo = custNo;
                magentoCustomerDTO.Email = v9dto.EMAIL.Trim();
                magentoCustomerDTO.Password = password;
                magentoCustomerDTO.Addr1 = v9dto.AD1.Trim();

                //The CSZ containing multiple fields, Very bad!!!
                magentoCustomerDTO.City = v9dto.CSZ.Substring(0,15).Trim(); 
                magentoCustomerDTO.StateProv = v9dto.CSZ.Substring(16, 2).Trim();
                magentoCustomerDTO.PostalCode = v9dto.CSZ.Substring(19).Trim();

                magentoCustomerDTO.Phone = v9dto.PH.Trim();
                magentoCustomerDTO.rep = v9dto.PRIREP.Trim();
                magentoCustomerDTO.bankcards = buildCCardString(visaCustDictionary, v9dto, magentoCustomerDTO.CustomerAcctNo);


                magentoCustomerDTO.AddrCompany = v9dto.NAME.Trim(); //HACK should this be the same as company?
                magentoCustomerDTO.Company = v9dto.NAME.Trim();


                StatesDTO stateDTO = null;
                if (stateDictionary.Keys.Contains(magentoCustomerDTO.StateProv))
                {
                    stateDTO = stateDictionary[magentoCustomerDTO.StateProv];
                    magentoCustomerDTO.Country = stateDTO.CNTRY.Trim();
                    magentoCustomerDTO.CountryCode = stateDTO.CCD.Trim();
                }

                if (webShipDictionary.Keys.Contains(custNo))
                {
                    magentoCustomerDTO.ShipVia = webShipDictionary[custNo].SVIA;
                }

                magentoCustomerDTO.Terms = v9dto.TERMS.Trim(); //TODO the blue screen and magento might not match.
                if (magentoCustomerDTO.Terms.Equals("STOP"))
                {
                    magentoCustomerDTO.Status = "no"; 
                } else 
                {
                    magentoCustomerDTO.Status = "yes"; 
                }

                magentoCustomerDTO.isShowCost = showCostForCustomer(custNo);
                magentoCustRecs.Add(magentoCustomerDTO);
            }

            IDbCommand cmd = stagedConn.CreateCommand();
            cmd.Transaction = stagedTran;
            cmd.CommandText = "INSERT INTO CustomerHist(LoadDate) values(getdate());SELECT @@IDENTITY"; //HACK sql server specific. 
            Int32 loadId = Int32.Parse(cmd.ExecuteScalar().ToString());

            foreach (MagentoCustomerDTO dto in magentoCustRecs)
            {
                dto.LoadId = loadId;
            }
            return loadId;
        }

        public Int32 showCostForCustomer(string loginId)
        {
            // <c:if test="${RegAcct == true || MasterAcct == true || AllowedSubAcct == true }">
            if ( isRegAcct(loginId) || isMasterAcct(loginId) || isAllowedSubAcct(loginId) ) 
            {
                return 1;
            }
            return 0;
        }
        public bool isRegAcct(string loginId)
        {
            //if (IsMasterAcct || IsSubAcct )
            //   IsRegAcct = false;
            if (string.IsNullOrWhiteSpace(loginId) || loginId.Length < 4)
            {
                return false;
            }
            if ( isMasterAcct(loginId) || isSubAcct(loginId))
            {
                return false;
            }
            return true;
        }

        public bool isAllowedSubAcct(string loginId)
        {
            //TODO this list should be put into the database!  We don't want to have to deploy each time to have a change.
            List<string> allowedPrefixes = new List<string>(new string[] { "1AN", "1AH", "1TV", "1LS127", "1LS184", "1LS197" });
            bool matchFound = false;

            //if (IsSubAcct)
            //    IsAllowedSubAcct = (Pattern.matches("1AN((?!000).)*$", LoginID) ||     //matches 1ANxxx sub accouts
            //                         Pattern.matches("1AH((?!000).)*$", LoginID) ||     //matches 1AHxxx sub accouts
            //                         Pattern.matches("1TV((?!000).)*$", LoginID) ||     //matches 1TVxxx sub accouts
            //                         Pattern.matches("1LS127$", LoginID) ||
            //                         Pattern.matches("1LS184$", LoginID) ||
            //                         Pattern.matches("1LS197$", LoginID));
            if ( isSubAcct(loginId) )
            {
                foreach ( string prefix in allowedPrefixes)
                {
                    if (loginId.Length >= prefix.Length && prefix == loginId.Substring(0, prefix.Length))
                    {
                        matchFound = true;
                    }
                }
            }
            return matchFound;
        }

        public bool isEcomAcct(string loginId)
        {
            //if Login ID starts with 5,6,7 or 8  (Acct could be both MastetAcct and EcomAcct, ex: 8OV000 - overstock )
            List<string> econAcctPrefixes = new List<string>( new string[]{ "5", "6", "7", "8" });

            if (string.IsNullOrWhiteSpace(loginId) || loginId.Length < 4)
            {
                return false;
            }
            return econAcctPrefixes.Contains(loginId.Substring(0, 1));
        }

        public bool isMasterAcct(string loginId)
        {
            //if Login ID starts with a digit and ends with three 0s, then it is a master acct, add a column to the table.
            if (string.IsNullOrWhiteSpace(loginId) || loginId.Length < 4)
            {
                return false;
            }
            if (isNumeric(loginId.Substring(0, 1)) && loginId.Substring(loginId.Length - 3).Equals("000"))
            {
                return true;
            }
            return false;
        }

        public bool isSubAcct(string loginId)
        {
            //if Login ID starts with a digit and ends with NOT three 0s, then it is a sub acct, block the view of account payable.
            if (string.IsNullOrWhiteSpace(loginId) || loginId.Length < 4)
            {
                return false;
            }
            if ( isNumeric(loginId.Substring(0,1)) && !loginId.Substring(loginId.Length-3).Equals("000") ) {
                return true;
            }
            return false;
        }

        private bool isNumeric(string srcStr) {
            string pattern = @"^[-+]?[0-9]*\.?[0-9]*$";
            return Regex.IsMatch(srcStr, pattern);
        }

        public string buildCCardString(Dictionary<string, VisaCustDTO> visaCustDictionary, V9CustDTO v9dto, string custno)
        {
            VisaCustDTO vistaCustRec = null;
            if (visaCustDictionary.Keys.Contains(custno))
            {
                vistaCustRec = visaCustDictionary[custno];
            }

            List<string> bankCards = new List<string>();
            Int32 ccrdLength = v9dto.CCRD.Trim().Length;
            if (ccrdLength > 4)
            {
                bankCards.Add(v9dto.CCRD.Trim() + " EXP " + v9dto.CCEXP);
            }
            if (vistaCustRec != null && vistaCustRec.CARDS.Replace(" ", "").Length > 0)
            {
                string ccBuffer = vistaCustRec.CARDS.Replace(" ", "");
                List<string> ccCards = new List<string>();
                for (Int32 x = 0; x < (ccBuffer.Length / 20); x++)
                {
                    string currCard = ccBuffer.Substring((x * 20), 20);
                    bankCards.Add(currCard.Substring(0, 16) + " EXP " + currCard.Substring(currCard.Length - 4)); //TODO I didn't think we wanted the CC on the website.
                }
            }
            string rtnValue = String.Join("|", bankCards);
            return rtnValue;
        }

    }
}
