﻿using MagentoInterface.DAO;
using MagentoInterface.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagentoInterface.Controller
{
    public class CategoryController : BaseController
    {
        /*
        CREATE TABLE CategoryHist (
           LoadId Int Identity(1,1) Primary Key,
           LoadDate DATETIME default getdate()
        );
          
          
        CREATE Table Category
         (
	        LoadId Int NULL Foreign Key References CategoryHist ON DELETE CASCADE,
            Title VARCHAR(255) NULL,
	        url_key VARCHAR(Max) NULL,
	        url_path VARCHAR(Max) NULL,
	        Visible VARCHAR(5) NULL,
	        Description VARCHAR(Max) NULL,
	        CategoryId Int NULL,
	        Level Int NULL,
	        parentId Int NULL
        )
         
         */

        public void buildScratchPad(IDbConnection conn, IDbConnection scratchPad)
        {
            IncdDAO incdDAO = new IncdDAO();
            List<IncdDTO> catList = incdDAO.selectRecordsRtnList(conn, "");

            foreach (IncdDTO dto in catList)
            {
                incdDAO.insert(scratchPad, dto);
            }
        }

        public List<string> buildFiles(IDbConnection conn, IDbConnection stagedConn)
        {
            StringBuilder catChanges = new StringBuilder();
            StringBuilder catDeletes = new StringBuilder();

            Int32 catLoadId = process(conn, stagedConn, catChanges, catDeletes);

            string catChangesTxt = ConfigurationManager.AppSettings["FileBuildLocation"].ToString() + string.Format("categoryfile{0}.tab", catLoadId.ToString("D8"));
            string catDeleteTxt = ConfigurationManager.AppSettings["FileBuildLocation"].ToString() + string.Format("categorydeletion{0}.tab", catLoadId.ToString("D8"));

            File.WriteAllText(catChangesTxt, catChanges.ToString());
            File.WriteAllText(catDeleteTxt, catDeletes.ToString());

            return new List<string>(new string[] { catChangesTxt, catDeleteTxt });
        }

        public Int32 process(IDbConnection conn, IDbConnection stagedConn, StringBuilder changes, StringBuilder deletes) 
        {

            List<MagentoCategoryDTO> magentoCatRecs = new List<MagentoCategoryDTO>();
            Int32 loadId = buildMagentoCategoryRecords(conn, stagedConn, magentoCatRecs);

            BulkInsert((SqlConnection)stagedConn, "Category", magentoCatRecs); //HACK sql server specific.

            string headersChanges = string.Format("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\n","Title", "url_key", "url_path", "Visible","Description", "CategoryID" );
            string headerDeletes = string.Format("{0}\t{1}\n", "Title", "url_path");
            changes.Append(headersChanges);
            deletes.Append(headerDeletes);

            buildChangedRecords(stagedConn, changes, loadId);
            buildDeletedRecords(stagedConn, deletes, loadId);

            //Delete oldest versions.
            IDbCommand cmd3 = stagedConn.CreateCommand();
            cmd3.CommandText = string.Format("DELETE FROM CategoryHist where LoadId < {0}",  loadId-10 );
            cmd3.ExecuteNonQuery();

            return loadId;
        }

        private void buildDeletedRecords(IDbConnection stagedConn, StringBuilder deletes, Int32 loadId)
        {
            string sql = "SELECT [Title],[url_key],[url_path],[Visible],[Description],[CategoryId],[Level],[parentId] ";
            sql += "FROM Category c ";
            sql += string.Format("WHERE loadId = {0} ", loadId - 1);
            sql += "EXCEPT ";
            sql += "SELECT [Title],[url_key],[url_path],[Visible],[Description],[CategoryId],[Level],[parentId] ";
            sql += "FROM Category c2 ";
            sql += string.Format("WHERE loadId = {0}", loadId);
            sql += "Order by CategoryId ";
            DataTable recordsToDelete = findChangedRecords(stagedConn, sql);

            foreach (DataRow dr in recordsToDelete.Rows)
            {
                string oneLine = "";
                oneLine = string.Format("{0}\t{1}\n",dr["Title"], dr["url_path"]);
                deletes.Append(oneLine);
            }
        }

        private void buildChangedRecords(IDbConnection stagedConn, StringBuilder changes, Int32 loadId)
        {
            string sql = "SELECT [Title],[url_key],[url_path],[Visible],[Description],[CategoryId],[Level],[parentId] ";
            sql += "FROM Category c ";
            sql += string.Format("WHERE loadId = {0} ", loadId);
            sql += "EXCEPT ";
            sql += "SELECT [Title],[url_key],[url_path],[Visible],[Description],[CategoryId],[Level],[parentId] ";
            sql += "FROM Category c2 ";
            sql += string.Format("WHERE loadId = {0}", loadId - 1);
            sql += "Order by CategoryId ";

            DataTable changedRecs = findChangedRecords(stagedConn, sql);

            foreach (DataRow dr in changedRecs.Rows)
            {
                string oneLine = "";
                oneLine = string.Format("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\n",
                    dr["Title"], dr["url_key"], dr["url_path"], dr["Visible"],
                    dr["Description"], dr["CategoryId"]);

                changes.Append(oneLine);
            }
        }

        private int buildMagentoCategoryRecords(IDbConnection conn, IDbConnection stagedConn, List<MagentoCategoryDTO> magentoCatRecs)
        {
            IncdDAO incdDAO = new IncdDAO();
            List<IncdDTO> catList = incdDAO.selectRecordsRtnList(conn, "");
            List<VRSCategoryDTO> vrsCatList = populateVrsRecordList(catList);

            List<VRSCategoryDTO> level1Cats = vrsCatList.Where(x => x.level == 1).OrderBy(x => x.categoryId).ToList();
            List<VRSCategoryDTO> level2Cats = vrsCatList.Where(x => x.level == 2).OrderBy(x => x.categoryId).ToList();
            List<VRSCategoryDTO> level3Cats = vrsCatList.Where(x => x.level == 3).OrderBy(x => x.categoryId).ToList();


            foreach (VRSCategoryDTO vRSCategoryDTO in level1Cats)
            {
                assignCategoryParent(level1Cats, level2Cats, vRSCategoryDTO);
            }

            foreach (VRSCategoryDTO vRSCategoryDTO in level2Cats)
            {
                assignCategoryParent(level2Cats, level3Cats, vRSCategoryDTO);
            }


            Dictionary<Int32, VRSCategoryDTO> vrsDictionary = new Dictionary<int, VRSCategoryDTO>();

            foreach (VRSCategoryDTO vrsDTO in vrsCatList)
            {
                vrsDictionary.Add((Int32)vrsDTO.categoryId, vrsDTO);
            }

            foreach (VRSCategoryDTO vrsDTO in vrsCatList)
            {
                List<Int32?> allowedCatLevels = new List<int?>(new Int32?[] { 1, 2, 3 });
                if (allowedCatLevels.Contains(vrsDTO.level))
                {
                    MagentoCategoryDTO magentoCategoryDTO = new MagentoCategoryDTO();
                    if (vrsDTO.level == 1)
                    {
                        magentoCategoryDTO.Description = vrsDTO.WTAGS;
                    }
                    magentoCategoryDTO.Title = vrsDTO.title;
                    magentoCategoryDTO.url_key = urlKeyFormatting(vrsDTO.title.ToLower());
                    magentoCategoryDTO.Visible = "Yes";
                    magentoCategoryDTO.CategoryId = (Int32)vrsDTO.categoryId;
                    magentoCategoryDTO.Level = (Int32)vrsDTO.level;
                    magentoCategoryDTO.url_path = buildUrlPath("", magentoCategoryDTO.CategoryId, vrsDictionary);
                    magentoCategoryDTO.parentId = vrsDTO.parentId;
                    magentoCatRecs.Add(magentoCategoryDTO);
                }
            }

            //Load the current State of the Categories
            IDbCommand cmd = stagedConn.CreateCommand();
            cmd.CommandText = "INSERT INTO CategoryHist(LoadDate) values(getdate());SELECT @@IDENTITY"; //HACK sql server specific. 
            Int32 loadId = Int32.Parse(cmd.ExecuteScalar().ToString());

            foreach (MagentoCategoryDTO mcatRec in magentoCatRecs)
            {
                mcatRec.LoadId = loadId;
            }
            return loadId;
        }

        private DataTable findChangedRecords(IDbConnection stagedConn, string sql)
        {
            DataTable changedRecs = new DataTable();
            IDbCommand cmd2 = stagedConn.CreateCommand();
            cmd2.CommandText = sql;
            changedRecs.Load(cmd2.ExecuteReader());
            return changedRecs;
        }

        public string buildUrlPath(string buildPath, Int32 catId, Dictionary<Int32, VRSCategoryDTO> vrsDictionary)
        {
            VRSCategoryDTO currRec = vrsDictionary[catId];
            string currUrlKey = urlKeyFormatting(currRec.title.ToLower());

            if (currRec.level == 1 || currRec.parentId == null )
            {
                return currUrlKey + @"/" + buildPath;
            }
            string tmpBuildPath = "";
            if (string.IsNullOrWhiteSpace(buildPath))
            {
                tmpBuildPath = buildUrlPath(currUrlKey, (Int32)currRec.parentId, vrsDictionary);
            }
            else
            {
                tmpBuildPath = buildUrlPath(currUrlKey + @"/" + buildPath, (Int32)currRec.parentId, vrsDictionary);
            }
            return tmpBuildPath;
        }

        private List<VRSCategoryDTO> populateVrsRecordList(List<IncdDTO> catList)
        {
            List<VRSCategoryDTO> vrsCatList = new List<VRSCategoryDTO>();

            foreach (IncdDTO incdDTO in catList)
            {
                Int32 catId;
                Int32 myLevel = 0;
                if (!string.IsNullOrWhiteSpace(incdDTO.WLVL))
                {
                    Int32.TryParse(incdDTO.WLVL.Replace("0", "").Replace(".", ""), out myLevel);
                }

                VRSCategoryDTO vRSCategoryDTO = new VRSCategoryDTO() { title = incdDTO.WDESC, level = myLevel, WTAGS=incdDTO.WTAGS };
                if (int.TryParse(incdDTO.CSC, out catId))
                {
                    vRSCategoryDTO.categoryId = catId;
                    vrsCatList.Add(vRSCategoryDTO);
                }
            }
            return vrsCatList;
        }

        public void assignCategoryParent(List<VRSCategoryDTO> currentLevelCats, List<VRSCategoryDTO> childLevelCats, VRSCategoryDTO vRSCategoryDTO)
        {
            VRSCategoryDTO nexgCatAtCurrentLevelRec = null;
            List<VRSCategoryDTO> nextTmpList = currentLevelCats.Where(x => x.categoryId > vRSCategoryDTO.categoryId).ToList().OrderBy(x => x.categoryId).ToList();
            if (nextTmpList.Count > 0)
            {
                nexgCatAtCurrentLevelRec = nextTmpList[0];
            }
            List<VRSCategoryDTO> children = childLevelCats.Where(x => x.parentId == null && (nexgCatAtCurrentLevelRec == null || x.categoryId < nexgCatAtCurrentLevelRec.categoryId)).ToList();
            foreach (VRSCategoryDTO currCatDTO in children)
            {
                currCatDTO.parentId = vRSCategoryDTO.categoryId;
            }
        }

        public static string urlKeyFormatting(string src)
        {
            char[] toCompare = src.Trim().ToCharArray();
            string validCharacters = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
            StringBuilder rtnValue = new StringBuilder();
            foreach (char c in toCompare )
            {
                if (validCharacters.Contains(c))
                {
                    rtnValue.Append(c);
                }
                else
                {
                    rtnValue.Append("-");
                }
            }

            return rtnValue.ToString();
        }
    }

}
