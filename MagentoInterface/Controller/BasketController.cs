﻿using MagentoInterface.DAO;
using MagentoInterface.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagentoInterface.Controller
{
    public class BasketController : BaseController
    {
        public Int32 buildScratchPadRitm(IDbConnection conn, IDbConnection scratchPad)
        {
            RitmDAO ritmDAO = new RitmDAO();
            List<RitmDTO> blueRecList = ritmDAO.selectRecordsRtnList(conn, "");

            foreach (RitmDTO dto in blueRecList)
            {
                ritmDAO.insert(scratchPad, dto);
            }
            return blueRecList.Count;
        }

        public Int32 buildScratchPadRitmd(IDbConnection conn, IDbConnection scratchPad)
        {
            RitmdDAO ritmdDAO = new RitmdDAO();
            List<RitmdDTO> blueRecList = ritmdDAO.selectRecordsRtnList(conn, "");

            foreach (RitmdDTO dto in blueRecList)
            {
                ritmdDAO.insert(scratchPad, dto);
            }
            return blueRecList.Count;
        }

        public Int32 buildScratchPadGenericItemInfo(IDbConnection conn, IDbConnection scratchPad)
        {
            GenericBlueItemInfoDAO genericBlueItemInfoDAO = new GenericBlueItemInfoDAO();
            List<GenericBlueItemInfoDTO> blueRecList = genericBlueItemInfoDAO.selectRecordsRtnList(conn, "");

            foreach (GenericBlueItemInfoDTO dto in blueRecList)
            {
                genericBlueItemInfoDAO.insert(scratchPad, dto);
            }
            return blueRecList.Count;
        }

  
    }

}
