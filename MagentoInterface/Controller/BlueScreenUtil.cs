﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagentoInterface.Controller
{
    public class BlueScreenUtil
    {
        public IDbConnection getConnection(string providerName, string connectionString)
        {
            System.Data.Common.DbProviderFactory factory = System.Data.Common.DbProviderFactories.GetFactory(providerName);
            IDbConnection conn = factory.CreateConnection();
            conn.ConnectionString = connectionString;
            conn.Open();

            return conn;
        }
    }

}
